/**
 *
 */
package br.com.svconsultoria.newmobile.remotedebug;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jorge.lucas
 *
 */
public class DebugMessages {

	private static final String TAG = "DebugMessages";
	private static DebugMessages instance;
	private static List<StringBuilder> messages;

	private DebugMessages() {
		messages = new ArrayList<>();
	}

	// static initializer to automatically start debug messages instance
	static {
		getInstance();
	}

	/**
	 * retrieve debug messages instance
	 * @return
	 */
	public static DebugMessages getInstance() {
		if (instance == null) {
			instance = new DebugMessages();
		}
		return instance;
	}

	/**
	 * add message to debug message list
	 * @param msg
	 */
	public synchronized static void addMessages(StringBuilder msg) {
		messages.add(msg);
	}

	/**
	 * Return list from 0 to offset, removing messages from debug messages
	 * @param offset
	 * @return
	 */
	public synchronized static List<StringBuilder> retrieveList(int offset) {
		ArrayList<StringBuilder> auxList = new ArrayList<>();
		for (int i = 0; i < offset; i ++)
			auxList.add(new StringBuilder(messages.get(i))); // add message to new list

		for (int i = 0; i < offset; i ++)
			messages.remove(0); // alter the old list

		return auxList;
	}
	/**
	 * Return entire list, clearing debug messages list
	 * @return
	 */
	public synchronized static List<StringBuilder> retrieveList() {
		return retrieveList(messages.size());
	}

	public static int messagesListSize() {
		return messages.size();
	}
}
