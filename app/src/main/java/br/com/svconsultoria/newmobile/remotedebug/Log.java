/**
 *
 */
package br.com.svconsultoria.newmobile.remotedebug;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Custom class to replace regular android Log (android.util.Log) for
 * remote debug purposes. <br />
 * Also, this class is responsible for remote debug filtering. This class
 * decides what to send to remote debug server evaluating if message matches
 * current filters list and defaultFilterAction variable.
 * @author jorge.lucas
 *
 */
public class Log {

	private static final String TAG = "Log";
	private static String hexDeviceId;
	private static ArrayList<String> filterList; // array list to hold filters list

	/**
	 * default filter behavior<br/>
	 * - true: send all logcat messages skipping messages matched by some filter <br />
	 * - false: send all messages which match some filter<br />
	 */
	private static boolean defaultFilterAction = false; // discard remote debug messages

	// CUSTOM jorge.lucas 24022015
	/**
	 * stores android logged tags. doesnt accepts dupplicates (since it is a java set)
	 */
	private static Set<String> tagsSet = new TreeSet<>();
	// FIM CUSTOM

	static {
		filterList = new ArrayList<>();

		if (hexDeviceId == null) {
			// CUSTOM jorge.lucas 09022015 sending deviceId (hexa) to RDServer...
			hexDeviceId = Long.toHexString(ApplicationControl.getDeviceId()).toUpperCase();

			if (hexDeviceId != null)
				while (hexDeviceId.length() < 4) hexDeviceId = "0" + hexDeviceId;
			else
				hexDeviceId = "0000";
			// FIM CUSTOM
		}
	}

	/**
	 * Return TAG from Log message. <br />
	 * ex: 000A [CardManipulatorTools] Retrieve data ... (continued) <br />
	 * For above input, this function returns [CardManipulatorTools]
	 * @param logMessage
	 * @return
	 */
	private static String getTagFromLogMessage(String logMessage) {
		Pattern pattern = Pattern.compile("\\b \\[.*\\] \\b");
		Matcher matcher = pattern.matcher(logMessage);
		if (matcher.find())
			return matcher.group().trim();

		return null;
	}

	protected static void queueMessage(String msg) {


		// CUSTOM jorge.lucas 20022015
		// adds in tags set the logged TAG
		tagsSet.add(Log.getTagFromLogMessage(msg));
		// FIM CUSTOM

		if (!RemoteDebugMessageSender.getRemoteDebugMessageSenderInstance().isRemoteServerEnabled())
			return;

		for (String regex : filterList) {

			if (Pattern.matches(regex, msg)) {
				android.util.Log.e(TAG, regex + " filter match regex " + msg);

				if (!defaultFilterAction)
					DebugMessages.addMessages(
							new StringBuilder(
										"<span class='logcat'>" +
												msg +
										"</span>"
									)

							);
				return;
			}

		}
		// FIM CUSTOM

		if (defaultFilterAction)
			DebugMessages.addMessages(
					new StringBuilder(
								"<span class='logcat'>" +
										msg +
								"</span>"
							)

					);
	}

	// filter related methods
	public static void insertFilter(String regex) { filterList.add(regex); }
	public static List<String> getFilterList() { return filterList; }
	public static void removeFilter(int filterIndex) { filterList.remove(filterIndex); }
	public static void removeAllFilters() { filterList.clear(); }

	// logged tags related methods
	public static Set<String> getLoggedTagsSet() { return Log.tagsSet; }
	public static int getLoggedTagsSetSize() { return Log.tagsSet.size(); }

	// default filter action related methods
	public static boolean getDefaultFilterAction() { return Log.defaultFilterAction; }
	public static void setDefaultFilterAction(boolean src) { Log.defaultFilterAction = src; }

	/**
	 * overloaded method to display in remote debug server messages with logcat alike colors
	 * @param msg
	 * @param logcatFilterCategory
	 */
	protected static void queueMessage(String msg, String logcatFilterCategory) {
		if (!logcatFilterCategory.startsWith("-"))
			logcatFilterCategory = "-" + logcatFilterCategory; // css classes in remote debug server are defined by logcat-<logcatFilterCategory>

		queueMessage(
				"<span class='logcat"+logcatFilterCategory+"'>" +
					hexDeviceId + " " +
					msg +
				"</span>"
				);
	}

	public static int d(String tag, String msg) {

		queueMessage("["+tag+"] " + msg, "debug");

		return android.util.Log.d(tag, msg);
	}

	public static int e(String tag, String msg) {

		queueMessage("["+tag+"] " + msg, "error");

		return android.util.Log.e(tag, msg);
	}

	public static int w(String tag, String msg) {

		queueMessage("["+tag+"] " + msg, "warning");

		return android.util.Log.w(tag, msg);
	}

	public static int i(String tag, String msg) {

		queueMessage("["+tag+"] " + msg, "info");

		return android.util.Log.i(tag, msg);
	}



}
