/**
 *
 */
package br.com.svconsultoria.newmobile.asynctask;

import android.os.AsyncTask;
import android.os.SystemClock;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;

import br.com.svconsultoria.newmobile.remotedebug.Log;

//import br.com.svconsultoria.newmobile.remotedebug.Log;

/**
 * @author jorge.lucas
 *
 */
public class UpdateDateTimeAsyncTask extends AsyncTask<Long, Void, Void> {

	public static final String TAG = "UpdateDateTimeAsyncTask";

	@Override
	protected Void doInBackground(Long... params) {

		Log.i(TAG, "System.currentTimeMillis() " + System.currentTimeMillis());
		long newCurrentTimeMillis = params[0];

		Process sh;
		try {
			//
			Process p = Runtime.getRuntime().exec(new String[]{"su", "-c", "system/bin/sh"});
			DataOutputStream stdin = new DataOutputStream(p.getOutputStream());
			//from here all commands are executed with su permissions
			stdin.writeBytes("ls -l /dev/alarm\n"); // \n executes the command
			InputStream stdout = p.getInputStream();
			byte[] buffer = new byte[65535];
			int read;
			String out = "";

			//read method will wait forever if there is nothing in the stream
			//so we need to read it in another way than while((read=stdout.read(buffer))>0)
			while(true){
			    read = stdout.read(buffer);
			    out += new String(buffer, 0, read);
			    if(read<65535){
			        //we have read everything
			    	Log.d(TAG, "timeMillis out " + out);
			        break;
			    }
			}

			if (out.charAt(8) != 'w') {
				sh = Runtime.getRuntime().exec(new String[]{"su", "-c", "/system/bin/sh -c \"chmod 666 /dev/alarm\""});
				sh.waitFor();
			}
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, e.getClass() + " " + e.getLocalizedMessage());
		}

		SystemClock.setCurrentTimeMillis(newCurrentTimeMillis); // 1422884224000l

		return null;
	}


	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);

//		// CUSTOM jorge.lucas 06022015 event to date time update
//		// event log params
//		Device device = SVDeviceDao.getObject();
//		long deviceId = device.getDeviceId();
//		long xId = device.getEventLogXid();
//
//		long eventTypeId = 4; // 4 - operacao
//		long eventCodeId = 197; // alteracao de data e hora
//		java.util.Date eventDt = new Date();
//
//		EventLog updateDatetimeEventLog = new EventLog();
//		updateDatetimeEventLog.setDeviceId(deviceId);
//		updateDatetimeEventLog.setXId(xId);
//		updateDatetimeEventLog.setEventTypeId(eventTypeId);
//		updateDatetimeEventLog.setEventCodeId(eventCodeId);
//		updateDatetimeEventLog.setEventDt(eventDt);
//
//		updateDatetimeEventLog.setAuditLogId(0l);
//		updateDatetimeEventLog.setCardEventSeq(0);
//
//		updateDatetimeEventLog.setSiteId(device.getSiteId());
//		updateDatetimeEventLog.setSectorId(device.getSectorId());
//
//		SVEventDao.insertNewEvent(updateDatetimeEventLog);
//		// FIM CUSTOM

		Log.i(TAG, "Update date time event inserted successfully!");
	}
}
