/**
 *
 */
package br.com.svconsultoria.newmobile.remotedebug;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.BuildConfig;
import com.br.svconsultoria.svnfcmanager.MainActivity;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoMaster;
import com.br.svconsultoria.svnfcmanager.model.orm.MobileParam;
import com.br.svconsultoria.svnfcmanager.model.orm.MobileParamDao;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import br.com.svconsultoria.newmobile.asynctask.UpdateDateTimeAsyncTask;

/**
 * @author jorge.lucas
 *
 */
public class RemoteDebugMessageSender extends TimerTask {

	private static String TAG = "RemoteDebugMsgSender";
	private static RemoteDebugMessageSender instance; // instance
	private static int TIMER_DELAY = 0; // timer delay to start
	private static int TIMER_PERIOD = 2000; // timer period
	private static Timer rdTimer; // timer to schedule message sending
	private static long counter; // package counter

	private static int REFRESH_RD_PARAMS_PERIOD = 2; // every N refreshs, the task update RD Server params
	private static int refreshRDParamsCounter = -1; // refresh counter (-1 when app start)

	// piggy back controllers
	private static boolean hasPiggyBackCommandAnswer = false;
    private static String piggyBackCommandAnswer = "";

    // socket params
	private String SVRD_SERVER_HOST = null;
    private int SVRD_PORT = 0;
    private boolean SVRD_ENABLED = false;
    private static final int SVRD_MAX_PACKET_SIZE = 4096;

	private RemoteDebugMessageSender() {
	}

	public boolean isRemoteServerEnabled() { return SVRD_ENABLED; }

	/**
	 * get remote debug message sender instance and schedule task in a internal timer
	 * @return
	 */
	public synchronized static RemoteDebugMessageSender getRemoteDebugMessageSenderInstance() {
		if (instance == null) {
			instance = new RemoteDebugMessageSender();
			scheduleInstance(instance);
		}
		return instance;
	}

	/**
	 *  schedule remote debug instance sender and schedule in a timertask
	 * @param instance
	 */
	private static void scheduleInstance(RemoteDebugMessageSender instance) {
		rdTimer = new Timer();
		rdTimer.scheduleAtFixedRate(instance, TIMER_DELAY, TIMER_PERIOD);
	}

	@Override
	public void run() {

		// RD Params initializing
		if (refreshRDParamsCounter == -1) { // -1 when app starts
			this.prepareRDServerParams(); // load params
			refreshRDParamsCounter = 0; // initialize refresh counter
			android.util.Log.i(TAG, "Initializing params and counter...");
		}

		// RD Params refreshing
		if (++refreshRDParamsCounter == REFRESH_RD_PARAMS_PERIOD) {
			this.prepareRDServerParams(); // refresh params
			refreshRDParamsCounter = 0; // restart counter
			android.util.Log.i(TAG, "refreshing RD params...");
		}

		if (!SVRD_ENABLED) return; // if remote debug not enabled, abort execution

		// RemoteDebug Message Sending Core
		try {

			if (DebugMessages.messagesListSize() > 0) {
				// send messages
				sendMessages(DebugMessages.retrieveList());
				android.util.Log.i(TAG, "finish send!");
			} else {
				// heartbeat
				sendMessages(new ArrayList<StringBuilder>(Collections.singletonList(new StringBuilder("<span class='ignore'>heart beat message</span>"))));
			}

		} catch(Exception e) {
			android.util.Log.d(TAG, "Exception " + e.getClass() + " " + e.getLocalizedMessage());
		} // catch
	}

	/**
	 * iterate over list sending messages to RD Server
	 * @param queueMsg
	 * @throws IOException
	 */
	private void sendMessages(List<StringBuilder> queueMsg) throws IOException {

    	List<StringBuilder> list = queueMsg;

    	synchronized(this) {
	    	if (hasPiggyBackCommandAnswer) {
	    		hasPiggyBackCommandAnswer = false;

	    		while (piggyBackCommandAnswer.length() > 0) {
	    			if (piggyBackCommandAnswer.length() >= 2048) { // piggy back answer fragmentation to 2048
	    				android.util.Log.d(TAG, "PBCA1 " + piggyBackCommandAnswer.substring(0, 16) + " ... " + piggyBackCommandAnswer.substring(2048 - 16, 2048));
	    				list.add(new StringBuilder("piggyBackCommandAnswer " + piggyBackCommandAnswer.substring(0, 2048)));
	    				piggyBackCommandAnswer = piggyBackCommandAnswer.substring(2048, piggyBackCommandAnswer.length());
	    			} else {
	    				android.util.Log.d(TAG, "PBCA2 " + piggyBackCommandAnswer.substring(0, 16) + " ... " + piggyBackCommandAnswer.substring(piggyBackCommandAnswer.length() - 16, piggyBackCommandAnswer.length()));
	    	    		list.add(new StringBuilder("piggyBackCommandEnd " + piggyBackCommandAnswer));
	    				piggyBackCommandAnswer = "";
	    			} // else
	    		} // while
	    	} // if
    	} // syncronized

    	for (StringBuilder sMsgToSend : queueMsg) {
    		android.util.Log.d(TAG, "hasPiggyBackCommandAnswer " + hasPiggyBackCommandAnswer + " " + counter + " " + sMsgToSend.substring(0, (sMsgToSend.length() > 16 ? 16 : sMsgToSend.length())));

    		// <pkg number>//<actual message>
    		if (counter == Long.MAX_VALUE) counter = -1;
        	String sformattedMessage = counter++ + "//" + sMsgToSend;

        	DatagramSocket clientSocket = new DatagramSocket();
        	InetAddress IPAddress = InetAddress.getByName(SVRD_SERVER_HOST);

        	byte[] sendData = new byte[SVRD_MAX_PACKET_SIZE];
        	byte[] receiveData = new byte[SVRD_MAX_PACKET_SIZE];

        	sendData = sformattedMessage.getBytes();
        	DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, SVRD_PORT);
        	clientSocket.setSoTimeout(100);
        	clientSocket.send(sendPacket);

        	DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
        	clientSocket.receive(receivePacket);
        	String modifiedSentence = new String(receivePacket.getData()).trim();

        	if (modifiedSentence.equals("OK")) {
        		// Nothing to do: "OK" means server got the message!
//        		android.util.Log.d("LogAsyncTask", "COMMAND FROM SERVER: OK");
        	}
        	// ----------------------------- PIGGYBACK COMMAND ---------------------------- //
        	else if (modifiedSentence.startsWith("piggyBackCommand")) {
        		android.util.Log.d("LogAsyncTask", "COMMAND FROM SERVER:" + modifiedSentence);

        		// PROCESS COMMAND
        		final String piggyBackFullCommand = modifiedSentence.substring("piggyBackCommand".length()).trim();
        		android.util.Log.d(TAG, "my piggy command: " + piggyBackFullCommand + " " + piggyBackFullCommand.length());
        		if (piggyBackFullCommand.length() > 0) {

	        		final String[] piggyBackCommandParts = piggyBackFullCommand.split("\\W+");
	        		String piggyBackCommandParam = piggyBackFullCommand.substring(piggyBackCommandParts[0].length()).trim();

	        		try {
		        		switch (piggyBackCommandParts[0]) {
		        			case "showmessage": // open new activity to show message
		        				MainActivity.showServerMessage(piggyBackCommandParam); // String message
		        				piggyBackCommandAnswer = "<span class='info'>started show message activity with message: " + piggyBackCommandParam + "</span>";
		        				break;
		        			case "setmillis": // set datetime (in milliseconds)
		        				UpdateDateTimeAsyncTask asyncTask = new UpdateDateTimeAsyncTask();
		        				asyncTask.execute(Long.valueOf(piggyBackCommandParam.trim())); // update datetime (millis as param)
		        				piggyBackCommandAnswer = "<p>updated device date time: " + piggyBackCommandParam + "</p>";
		        				break;
		        			case "doquery": // run raw sql query

		        				SQLiteOpenHelper helper = new DaoMaster.DevOpenHelper(MainActivity.applicationContext, "svronda", null);
		        				SQLiteDatabase db = helper.getWritableDatabase();

		        				String limitedResultsMessage = ""; // custom message to indicate when query was limited

		        				// CUSTOM jorge.lucas 17022015 if select query without limit clause, insert it
		        				if (piggyBackCommandParam.toLowerCase().startsWith("select") && !piggyBackCommandParam.toLowerCase().matches("^.*limit.*$")) {
		        					piggyBackCommandParam += " limit 0, 40";
		        					limitedResultsMessage = "(limit 0,40 inserted)";
		        				}
		        				// FIM CUSTOM

		        				Cursor cursor = db.rawQuery(piggyBackCommandParam, null);

		        				piggyBackCommandAnswer = "<b>sql answer - count: " + cursor.getCount() + " " +limitedResultsMessage+ " </b><br/>\n";

		        				piggyBackCommandAnswer += "<table class='sqlresults' >"; // start table

		        				// query column names into table
		        				piggyBackCommandAnswer += "<tr>"; // start column name row
		        				String columnNames[] = cursor.getColumnNames();
		        				for (String columnName : columnNames)
		        					piggyBackCommandAnswer += "<td>"+columnName+"</td>";
		        				piggyBackCommandAnswer += "</tr>"; // end column name row

		        				// data into table
			        			while (cursor.moveToNext()) {
			        				piggyBackCommandAnswer += "<tr>"; // start data row
			        				for (int i = 0; i < cursor.getColumnCount(); i ++)
			        					piggyBackCommandAnswer += "<td>" + cursor.getString(i) + "</td>";
			        				piggyBackCommandAnswer += "</tr>"; // end data row
			        			}

			        			piggyBackCommandAnswer += "</table>"; // end table
		        				break;
	        				case "disableremotedebug": // disable remote debug
		        					MobileParam newRemoteDebugParms = new MobileParam();
		        					ArrayList<MobileParam> auxList = new ArrayList<>();
		        		        	auxList.add(newRemoteDebugParms);
		        					newRemoteDebugParms.setParamName("remote_debug_enabled");
		        	        		newRemoteDebugParms.setParamValue("false");
		        	        		SVMobileParamsDao.insertMobileParams(auxList); // update objects
		        	        		piggyBackCommandAnswer = "remote debug disabled";
		        	        		break;
	        				case "currentmillis": // DCO current time millis
	        					piggyBackCommandAnswer = "<span class='info'>current millis: " + System.currentTimeMillis() + "</span>";
	        					break;
	        				case "help": // help command
	        					piggyBackCommandAnswer  = "<span class='info'>Available commands:<br />";

	        					piggyBackCommandAnswer += "<b>showmessage [msg]</b> - open an activity to show a message to the user<br />";
	        					piggyBackCommandAnswer += "<b>setmillis [date time in millis]</b> - set device current time millis<br />";
	        					piggyBackCommandAnswer += "<b>currentmillis</b> - retrieve device current time millis<br />";
	        					piggyBackCommandAnswer += "<b>doquery [sqlquery]</b> - execute raw query<br />";
	        					piggyBackCommandAnswer += "<b>disableremotedebug</b> - disables remote debug<br />";
	        					piggyBackCommandAnswer += "<b>help</b> - show help<br />";
	        					piggyBackCommandAnswer += "<b>addfilter [filter]</b> - add filter (tip: .*([word to filter]).*)<br />";
	        					piggyBackCommandAnswer += "<b>listfilter</b> - list current filters<br />";
	        					piggyBackCommandAnswer += "<b>removefilter [filter index]</b> - remove filter by index<br />";
	        					piggyBackCommandAnswer += "<b>removeallfilters</b> - remove all filters<br />";
	        					piggyBackCommandAnswer += "<b>getloggedtags</b> - list already logged tags<br />";
	        					piggyBackCommandAnswer += "<b>setdefaultfilteraction [true|false]</b> - true: send all logcat messages skipping messages matched by some filter, false: send all messages which match some filter<br />";
	        					piggyBackCommandAnswer += "<b>getdefaultfilteraction</b> - returns true or false about current default filter action<br />";
		        				piggyBackCommandAnswer += "<b>getappversion</b> - returns current version running on device<br />";
	        					piggyBackCommandAnswer += "<b>apprunningsince</b> - returns since when app is running<br />";
		        				piggyBackCommandAnswer += "<b>lastsync</b> - returns apps last sync<br />";
		        				piggyBackCommandAnswer += "<b>lasteventsend</b> - returns apps last event send<br />";

	        					piggyBackCommandAnswer += "</span>";
	        					break;
	        				// CUSTOM jorge.lucas 23022015 FILTERS RELATED COMMANDS
	        				case "addfilter": // addfilter <regex>
	        					Log.insertFilter(piggyBackCommandParam);
	        					piggyBackCommandAnswer = "<span class='info'>Filter inserted: " + piggyBackCommandParam + "</span>";
	        					break;
	        				case "listfilter":
	        				case "listfilters": // listfilters (#n: <regex>)
	        					String defaultFilterActionDescription =
	        						(Log.getDefaultFilterAction() ?
	        								"all messages EXCEPT filtered messages" :
	        									"only filtered messages"
	        								);
	        					piggyBackCommandAnswer = "<span class='info'>Current filters (default filter action:<b> " + Log.getDefaultFilterAction() +"</b> (" + defaultFilterActionDescription +")):</span><br/> ";
	        					int i = 0;
	        					if (Log.getFilterList().size() > 0) {
	        						for (String filter : Log.getFilterList())
	        							piggyBackCommandAnswer += "<span class='info'><b>#" + i++ + " "  + filter + "</b></span><br />";
	        					} else {
	        						piggyBackCommandAnswer += "<span class='info'><b>no filters</b></span>";
	        					}
	        					break;
	        				case "removefilter": // removefilter <filter index>
	        					int auxFilterIndex = Integer.valueOf(piggyBackCommandParam);
	        					if (auxFilterIndex < 0 || auxFilterIndex > Log.getFilterList().size())
	        						if (Log.getFilterList().size() > 0)
	        							piggyBackCommandAnswer = "<span class='info'><b>error: filter index must be between 0 and " + Log.getFilterList().size() + "</b></span>";
	        						else // empty list
	        							piggyBackCommandAnswer = "<span class='info'><b>filter list is empty!</b></span>";
	        					else { // valid index
	        						piggyBackCommandAnswer = "<span class='info'><b>Filter #" + auxFilterIndex + " " + Log.getFilterList().get(auxFilterIndex) + " removed</b></span>";
	        						Log.getFilterList().remove(auxFilterIndex);
	        					}
	        					break;
	        				case "removeallfilter":
	        				case "removeallfilters":
	        					Log.removeAllFilters();
	        					piggyBackCommandAnswer = "<span class='info'><b>All filters removed</b></span>";

	        					break;
	        				// FIM CUSTOM
	        				case "getloggedtags":
	        					piggyBackCommandAnswer = "<span class='info'>Previous logged tags:</span> <br />";
	        					if (Log.getLoggedTagsSetSize() > 0) {
	        						Iterator<String> iterator = Log.getLoggedTagsSet().iterator();
	        						while (iterator.hasNext())
		        						piggyBackCommandAnswer += "<span class='info'><b>"+iterator.next()+"</b></span><br/>";
	        					} else {
	        						piggyBackCommandAnswer += "<span class='info'>no current logged tags</span>";
	        					} // else
	        					break;
	        				case "setdefaultfilteraction":
	        					Log.setDefaultFilterAction(Boolean.valueOf(piggyBackCommandParam));
	        					piggyBackCommandAnswer = "<span class='info'>defaultFilterAction set to " + piggyBackCommandParam + " </span>";
	        					break;
	        				case "getdefaultfilteraction":
	        					piggyBackCommandAnswer = "<span class='info'>defaultFilterAction is " + Log.getDefaultFilterAction() + " </span>";
	    	        			break;
	        				case "getappversion":
	        					piggyBackCommandAnswer = "<span class='info'>current app version is " + BuildConfig.VERSION_NAME+ " </span>";
	    	        			break;
	        				case "apprunningsince":
	        					piggyBackCommandAnswer = "<span class='info'>app running since " + ApplicationControl.stAppStartDate + " </span>";
	    	        			break;
	        				case "lastsync":
	        					piggyBackCommandAnswer = "<span class='info'>app last sync " + ApplicationControl.stAppLastSyncHour + " </span>";
	    	        			break;
	        				case "lasteventsend":
	        					piggyBackCommandAnswer = "<span class='info'>last data (events or occurrence) send " + ApplicationControl.stAppLastDataSendHour + " </span>";
	    	        			break;
//

		        			default:
		        				piggyBackCommandAnswer = "<span class='error'>unknown command</span>";
		        				break;
		        		} // switch
	        		} catch (Exception e) {
	        			piggyBackCommandAnswer = "<span class='error'>piggy back exception: " + e.getClass() + " "+ e.getLocalizedMessage() + "</span>";
    				} // catch

	        		hasPiggyBackCommandAnswer = true;
	    		} // if
    		} // else if
        	// ----------------------------- END PIGGYBACK COMMAND ---------------------------- //
        	clientSocket.close();

        } // for

	}

	private void prepareRDServerParams() {

		MobileParam addressParam = SVMobileParamsDao.getMobileParamByName("remote_debug_address");
		MobileParam portParam = SVMobileParamsDao.getMobileParamByName("remote_debug_port");
		MobileParam enabledParam = SVMobileParamsDao.getMobileParamByName("remote_debug_enabled");

     	if (addressParam != null) SVRD_SERVER_HOST = addressParam.getParamValue();
     	else SVRD_SERVER_HOST = "";

     	if (portParam != null && !portParam.getParamValue().equals("")) SVRD_PORT = Integer.valueOf(portParam.getParamValue().trim());
     	else SVRD_PORT = 0;

     	if (enabledParam != null)
 			SVRD_ENABLED = "true".equals(enabledParam.getParamValue());

	}

}
