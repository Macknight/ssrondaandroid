package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.Checkpoint;
import com.br.svconsultoria.svnfcmanager.model.orm.CheckpointDao;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;

import java.util.List;

public class SVCheckpointDao {


	private static DaoSession session = ApplicationControl.daoSession;
	public static void insertOrUpdateCheckpoints(final List<Checkpoint> list) {
		Log.d("SVCheckpointDao", "insertOrUpdateCheckpoints() Starting...");
		session.runInTx(new Runnable() {

			@Override
			public void run() {
				CheckpointDao checkpointdao = session.getCheckpointDao();
				checkpointdao.insertOrReplaceInTx(list);
			}
		});

		long xid = SVUpdateSequenceDao.getXid(UpdateSequenceMap.SRT_CHECKPOINTS).getxId();
		for (Checkpoint object : list) {
			if (xid < object.getXId()) {
				xid = object.getXId();
			}
		}
		SVUpdateSequenceDao.updateXId(new UpdateSequence(null, UpdateSequenceMap.SRT_CHECKPOINTS, xid));
		Log.d("SVCheckpointDao", "insertOrUpdateCheckpoints() Finished!");
	}

	public static Checkpoint getCheckpointById(long checkpointId) {
		try {
			return session.getCheckpointDao().queryBuilder()
					.where(CheckpointDao.Properties.CheckPointId.eq(checkpointId))
					.where(CheckpointDao.Properties.StatusId.eq(1))
					.uniqueOrThrow();
		} catch (Exception e) {
			Log.d("SVCheckpointDao", "getCheckpointById() - Checkpoint not found.");
		}
		return null;
	}

	public static void insertOrReplaceCheckpoint(Checkpoint checkpoint) {
		Log.d("SVCheckpointDao", "insertOrReplaceCheckpoint()");
		session.getCheckpointDao().insertOrReplace(checkpoint);
		Log.d("SVCheckpointDao", "insertOrReplaceCheckpoint() Finished!");
	}
}
