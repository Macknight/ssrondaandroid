package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Entity mapped to table SRTM_CHECKPOINTS.
 */
@Entity
public class Checkpoint {

    @Id
    private Long checkPointId;
    private Long customerId;
    private Long xId;
    private long statusId;
    private String description;
    private Float longitude;
    private Float latitude;
    private long tagId;
    private byte[] photo;
    private String checkpointChecks;
    @Generated(hash = 1852837973)
    public Checkpoint(Long checkPointId, Long customerId, Long xId, long statusId,
            String description, Float longitude, Float latitude, long tagId,
            byte[] photo, String checkpointChecks) {
        this.checkPointId = checkPointId;
        this.customerId = customerId;
        this.xId = xId;
        this.statusId = statusId;
        this.description = description;
        this.longitude = longitude;
        this.latitude = latitude;
        this.tagId = tagId;
        this.photo = photo;
        this.checkpointChecks = checkpointChecks;
    }
    @Generated(hash = 447644603)
    public Checkpoint() {
    }
    public Long getCheckPointId() {
        return this.checkPointId;
    }
    public void setCheckPointId(Long checkPointId) {
        this.checkPointId = checkPointId;
    }
    public Long getCustomerId() {
        return this.customerId;
    }
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }
    public Long getXId() {
        return this.xId;
    }
    public void setXId(Long xId) {
        this.xId = xId;
    }
    public long getStatusId() {
        return this.statusId;
    }
    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Float getLongitude() {
        return this.longitude;
    }
    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }
    public Float getLatitude() {
        return this.latitude;
    }
    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }
    public long getTagId() {
        return this.tagId;
    }
    public void setTagId(long tagId) {
        this.tagId = tagId;
    }
    public byte[] getPhoto() {
        return this.photo;
    }
    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }
    public String getCheckpointChecks() {
        return this.checkpointChecks;
    }
    public void setCheckpointChecks(String checkpointChecks) {
        this.checkpointChecks = checkpointChecks;
    }

}
