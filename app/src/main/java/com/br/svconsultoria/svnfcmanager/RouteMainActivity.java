package com.br.svconsultoria.svnfcmanager;

import android.animation.LayoutTransition;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.br.svconsultoria.svnfcmanager.dao.SVCheckpointDao;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRfidTagDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteCompositionDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteScheduleDao;
import com.br.svconsultoria.svnfcmanager.dao.SVSysParamsDao;
import com.br.svconsultoria.svnfcmanager.map.EventTypeMap;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.model.SystemCheckpoint;
import com.br.svconsultoria.svnfcmanager.model.orm.Checkpoint;
import com.br.svconsultoria.svnfcmanager.model.orm.RfidTag;
import com.br.svconsultoria.svnfcmanager.model.orm.Route;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteComposition;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteSchedule;
import com.br.svconsultoria.svnfcmanager.model.orm.SysParam;
import com.br.svconsultoria.svnfcmanager.services.RouteMonitorService;
import com.br.svconsultoria.svnfcmanager.services.SilentAlertService;
import com.br.svconsultoria.svnfcmanager.tools.CheckpointTools;
import com.br.svconsultoria.svnfcmanager.tools.EventBuilder;
import com.br.svconsultoria.svnfcmanager.util.GPSDiff;
import com.br.svconsultoria.svnfcmanager.util.SVUtil;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import org.greenrobot.greendao.DaoException;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.svconsultoria.newmobile.remotedebug.Log;

import static java.lang.Integer.parseInt;

public class RouteMainActivity extends BaseNFCActivity implements OnClickListener {

    private static final String TAG = "RouteMainActivity";

    private static class ClockHandler extends Handler {

        private WeakReference<RouteMainActivity> mActivity;

        public ClockHandler(RouteMainActivity activity) {
            mActivity = new WeakReference<RouteMainActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {

            synchronized (mActivity.get()) {
                if (!IS_THIS_ACTIVITY_ALIVE) {
                    removeMessages(1);
                    return;
                }

                mActivity.get().setClock();
                sendMessageDelayed(obtainMessage(1), 1000);
            }
        }
    }

    private ClockHandler clockHandler = new ClockHandler(this);

    public static boolean IS_THIS_ACTIVITY_ALIVE = false;
    private boolean IS_VIBRATE_ALLOWED = false;
    public static boolean IS_READ_ALLOWED = false;

    public static boolean DOES_OCCURRENCE_EXIST = false;

    private ArrayList<Integer> selectedItens;
    private static List<String> checked;

    private long routeId = 0;

    private AlertDialog taskDialog;

    private TextView checkpointDescription = null;
    private TextView tvSchedule = null;
    public static TextView clock = null;
    private static TextView tvRoute = null;
    private ImageView photo = null;

    private SystemCheckpoint expectedCheckpoint = null;
    private RfidTag expectedCheckpointTag = null;

    private long orderCount = 0;

    private List<SystemCheckpoint> checkpoints = null;

    private ImageView qrCodeRead = null;

    private static final int QRCODE_REQUEST = 0;

    private boolean resetTime = false;
    private long resetTimeSec = 0;
    private boolean canShowCountDownDialog = false;


    private boolean idlenessEventEnable = false;
    private long idlenessEventTimeSeconds = 60*60;
    private boolean idlenessEventCreated = false;

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        SVUtil.touch(this); //can touch this
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_route_main);
        selectedItens = new ArrayList<Integer>();
        // Custom font
        Typeface typeface = Typeface.createFromAsset(getAssets(), "tt0246m_.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
        ((TextView) findViewById(R.id.title)).setTypeface(typeface2);
        ((TextView) findViewById(R.id.tvRoute)).setTypeface(typeface);
        ((TextView) findViewById(R.id.tvSchedule)).setTypeface(typeface);
        ((TextClock) findViewById(R.id.digitalClockRouteList)).setTypeface(typeface);


        LayoutTransition l = new LayoutTransition();
        l.enableTransitionType(LayoutTransition.CHANGING);

        ImageView itemMap = (ImageView) findViewById(R.id.route_main_menu_item_map);
        itemMap.setOnClickListener(this);

        ImageView itemOccurrence = (ImageView) findViewById(R.id.route_main_menu_item_occurence);
        itemOccurrence.setOnClickListener(this);

        ImageView itemSkip = (ImageView) findViewById(R.id.route_main_menu_item_skip);
        itemSkip.setOnClickListener(this);

        ImageView itemExit = (ImageView) findViewById(R.id.route_main_menu_item_exit);
        itemExit.setOnClickListener(this);


        qrCodeRead = (ImageView) findViewById(R.id.route_main_qrcode);
        qrCodeRead.setVisibility(View.GONE);

        if (SVMobileParamsDao.isQrCodeAllowed()) {
            qrCodeRead.setVisibility(View.VISIBLE);
            qrCodeRead.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    callQrCodeReader();
                }
            });
        }

        checkpointDescription = (TextView) findViewById(R.id.routeTextView);
        clock = (TextView) findViewById(R.id.clock);
        tvRoute = (TextView) findViewById(R.id.tvRoute);
        tvSchedule = (TextView) findViewById(R.id.tvSchedule);

        //////// panic button
        ImageButton panicActivationButton = (ImageButton) findViewById(R.id.panicButton);
        panicActivationButton.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PanicConfirmationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return false;
            }
        });
        /////// end panic button

        photo = (ImageView) findViewById(R.id.icon_no_photo);
        long routeScheduleId = ApplicationControl.userSession.getRouteScheduleId();

        RouteSchedule routeSchedule = SVRouteScheduleDao.getRouteScheduleById(routeScheduleId);

        List<RouteComposition> composition = SVRouteCompositionDao.getRouteComposition(routeSchedule.getRouteId());
        Route route = SVRouteDao.getRouteById(routeSchedule.getRouteId());
        routeId = route.getRouteId();

        if (savedInstanceState == null) {
            resetTime = true;
        }
        // Load route progression data from user session
        if (ApplicationControl.userSession.getOrderCount() == 0) {
            orderCount = composition.get(0).getOrderId();
        } else {
            orderCount = ApplicationControl.userSession.getOrderCount();
        }

        if (ApplicationControl.userSession.getCheckpoints() == null) {
            checkpoints = CheckpointTools.getSystemCheckpoints(composition, routeSchedule);
            ApplicationControl.userSession.setCheckpoints(checkpoints);
        } else {
            checkpoints = ApplicationControl.userSession.getCheckpoints();
        }

        SysParam param = SVSysParamsDao.getSysParamByName(SystemParamsMap.IDLENESS_EVENT_ENABLED);
        idlenessEventEnable = param != null && Boolean.parseBoolean(param.getValue());
        param = SVSysParamsDao.getSysParamByName(SystemParamsMap.IDLENESS_EVENT_TIME_MINUTES);
        idlenessEventTimeSeconds = param==null?60*60:parseInt(param.getValue())*60;

        Log.d(TAG, "onCreate() - OrderCount: " + orderCount);
        nextCheckpoint();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putLong("resetTimeSec", resetTimeSec);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("resetTimeSec")) {
            resetTimeSec = savedInstanceState.getLong("resetTimeSec");
        }
    }

    @SuppressWarnings("deprecation")
    private void setClock() {
        Date now = new Date();

        long nowSecondsOfDay = (((now.getHours() * (60 * 60)) + ((now.getMinutes()) * 60))) + now.getSeconds();

        if (resetTime) {
            if (expectedCheckpoint.isWait()) {
                resetTimeSec = nowSecondsOfDay + (expectedCheckpoint.getStayTimeMinutes() * 60);
            } else {
                resetTimeSec = nowSecondsOfDay + (expectedCheckpoint.getTravelMinutes() * 60);
            }
            resetTime = false;
        }

        long diffSec = Math.abs(nowSecondsOfDay - resetTimeSec);

        if (resetTimeSec >= nowSecondsOfDay) {
            clock.setText(String.format(Locale.getDefault(), "%02d:%02d", diffSec / 60, diffSec % 60));
        } else {
            clock.setText(String.format(Locale.getDefault(), "-%02d:%02d", diffSec / 60, diffSec % 60));
        }

        if ((resetTimeSec == nowSecondsOfDay) && IS_VIBRATE_ALLOWED) {
            IS_VIBRATE_ALLOWED = false;
            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(2000);
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
            // r.stop();
        }

        if(idlenessEventEnable){
            if(!idlenessEventCreated && (nowSecondsOfDay - resetTimeSec)>idlenessEventTimeSeconds){
//                EventBuilder.createIdlenessEvent(routeId);
                idlenessEventCreated=true;
                Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(2000);
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                r.play();

                //Show alert
                showIdlenessDialog();
            }
        }

        IS_READ_ALLOWED = true;

    }

    private void showIdlenessDialog(){
        //Dialog creation
        final Dialog dialog = new Dialog(RouteMainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_idleness);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        final TextView text = (TextView) dialog.findViewById(R.id.dialog_idleness_message);
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
        text.setTypeface(typeface2);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialog_idleness_ok);
        dialogButton.setTypeface(typeface2);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!text.getText().toString().isEmpty()){
                    EventBuilder.createIdlenessEvent(routeId, text.getText().toString());
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private void prepareAndExecuteSyncTask() {
        Log.d(TAG, "prepareAndExecuteSyncTask()");
        ApplicationControl.startSend();
    }


    @Override
    public void onBackPressed() {
       skipRouteConfirm();
    }

    @Override
    protected void onResume() {
        super.onResume();

        RouteMonitorService.setServiceInactive();

        Log.d(TAG, "onResume()");
        if (!IS_THIS_ACTIVITY_ALIVE) {
            IS_THIS_ACTIVITY_ALIVE = true;
            clockHandler.sendMessage(clockHandler.obtainMessage(1));
        }

        if (ApplicationControl.userSession == null
                || ApplicationControl.userSession.getLoggedVigilant() == null
                || ApplicationControl.userSession.getLoggedVigilant().getVigilantId() == null){
            Toast.makeText(this, "Sessão expirada!", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        RouteMonitorService.setServiceActive();
        Log.d(TAG, "onPause()");
        IS_THIS_ACTIVITY_ALIVE = false;

    }

    @Override
    protected void handleTag(String sTag) {
        validateCheckpoint();
    }

    @Override
    protected void onStop() {
        super.onStop();
        IS_THIS_ACTIVITY_ALIVE = false;
    }


    private void createAndShowTaskDialog() {

        List<String> activities = new ArrayList<>();
        CharSequence[] cs;
        if (expectedCheckpoint.getCheckpointChecks() != null && !expectedCheckpoint.getCheckpointChecks().isEmpty()) {
            //TODO: set the tasks to the activity.
            activities = expectedCheckpoint.getTaskList();
            cs = activities.toArray(new CharSequence[activities.size()]);
        } else {
            cs = null;
        }
        final List<String> fTasks = new ArrayList<String>();
        fTasks.addAll(activities);
        selectedItens.clear();

        //Dialog creation
        AlertDialog.Builder builder = new AlertDialog.Builder(RouteMainActivity.this);
        builder.setTitle("ATIVIDADES A SEREM EXECUTADAS")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (selectedItens.size() != 0) {
                            checked = new ArrayList<String>();
                            for (int i = 0; i < selectedItens.size(); i++) {
                                checked.add(fTasks.get(selectedItens.get(i)));
                            }
                        }
                        expectedCheckpoint.setCheckedTasks(expectedCheckpoint.concatTasks(checked));
                        generateEventAndProceed2NextCheckpoint(true);

                    }
                })
                .setMultiChoiceItems(cs, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface taskDialog, int which, boolean isChecked) {
                                if (isChecked) {
                                    selectedItens.add(which);
                                } else if (selectedItens.contains(which)) {
                                    selectedItens.remove(Integer.valueOf(which));
                                }

                            }
                        })
                .setCancelable(false);

        taskDialog = builder.create();
        taskDialog.getWindow().setBackgroundDrawableResource(android.R.color.black);
        taskDialog.show();

    }


    private void callQrCodeReader() {
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
        startActivityForResult(intent, QRCODE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == QRCODE_REQUEST) {
            if (resultCode == RESULT_OK) {
                String content = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                Log.d(TAG, "QR CODE - Content: " + content + " Format: " + format);

                sTagSerial = content;

                Vibrator vibrator;
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(150);

                validateCheckpoint();

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Leitura falhou!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void setPhoto(byte[] picture) {
        Bitmap bmp = BitmapFactory.decodeByteArray(picture, 0, picture.length);
        photo.setImageBitmap(bmp);
    }

    private void validateCheckpoint() {

        float minutesDiff = 0f;
        int remainingTime = 0;
        float measured = 0f; // distance measured between ckpoint read and ckpoint coordinates

        // Toast.makeText(this, "hasAccuracy: " + hasAccuracy, Toast.LENGTH_SHORT).show();

        Log.d(TAG, "check point id: " + expectedCheckpoint.getCheckpointId());

        if (sTagSerial.equals(expectedCheckpointTag.getSerialNumber())) { // expected expectedCheckpoint found

            // hold time millis when this expectedCheckpoint is read
            expectedCheckpoint.setReadTimeMillis(System.currentTimeMillis());
            Log.i(TAG, "current time millis for expectedCheckpointTag: " + expectedCheckpoint.getReadTimeMillis() + " orderCount " + orderCount);

            // if expectedCheckpoint have waiting time flag, indicates that this
            // expectedCheckpoint were added to expectedCheckpoint list in result of stay time
            // of previous expectedCheckpoint.
            if ((orderCount - 1) != 0) {
                if (expectedCheckpoint.isWait()) {
                    Log.i(TAG, "expectedCheckpoint.isWait() " + expectedCheckpoint.isWait() + " (checkpoints.get((int) orderCount - 2) != null " + (checkpoints.get((int) orderCount - 2) != null));

                    // minutes difference between current read expectedCheckpoint and previous expectedCheckpoint
                    long millisDiff = expectedCheckpoint.getReadTimeMillis() - checkpoints.get((int) orderCount - 2).getReadTimeMillis();
                    minutesDiff = millisDiff / 60000;

                    Log.i(TAG, "stayed " + minutesDiff + " (supposed to stay " + checkpoints.get((int) orderCount - 2).getStayTimeMinutes() + ")");
                    Log.i(TAG, "minutesDiff > checkpoints.get((int) orderCount - 2).getStayTimeMinutes() " + (minutesDiff > checkpoints.get((int) orderCount - 2).getStayTimeMinutes()));

                    // if time elapsed waiting dont surpasses current stay time, informs user
                    if (minutesDiff < checkpoints.get((int) orderCount - 2).getStayTimeMinutes()) {
                        remainingTime = Math.round(checkpoints.get((int) orderCount - 2).getStayTimeMinutes() - minutesDiff);

                        Log.e(TAG, "Stay more " + remainingTime + " minutes");

                        // alert dialog informing user that route have finished
                        new AlertDialog.Builder(this)
                                .setMessage("Por favor, aguarde neste ponto mais " + (remainingTime > 0 ? remainingTime : "1") + " minuto(s) ou pule o ponto para seguir")
                                .setCancelable(false)
                                .setNegativeButton("Ok", new DialogInterface.OnClickListener() { // read expectedCheckpoint again
                                    public void onClick(DialogInterface dialog, int id) {
                                        // no action
                                    }
                                })
                                .show();

                        return;
                    }

                }
            }


            ////////////////////////////// checks gps distance
            // if gps present & distance check enabled
            if (SilentAlertService.gps.getLatitude() != 0 && SilentAlertService.gps.getLongitude() != 0 // if gps available
                    && SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE_ENABLED) != null // if distance check parameter present
                    && "true".equals(SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE_ENABLED).getValue()) // if distance check enabled
                    && (expectedCheckpoint.getLatitude() != 0.0f && expectedCheckpoint.getLongitude() != 0.0f) // if point have coordinates currently
                    ) {
                measured = 0.0f; // km

                // calculate distance between current GPS position and expectedCheckpoint position
                measured = (float) GPSDiff.GPSDistance(SilentAlertService.gps.getLatitude(),
                        SilentAlertService.gps.getLongitude(),
                        expectedCheckpoint.getLatitude(),
                        expectedCheckpoint.getLongitude());

                // retrieve current distance tolerance (if any)
                float tolerance = (SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE) != null ?  // if distance tolerance present in database, retrieve
                        Float.valueOf(SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE).getValue()) : .8f); // else, use .8 (km) as default
                // km // tolerance (local and sync)

                if (measured > tolerance) { // if too far from original expectedCheckpoint position, shows a dialog informing user about it

                    // to proceed, vigilant must create an occurrence
                    if (DOES_OCCURRENCE_EXIST) {
                        DOES_OCCURRENCE_EXIST = false; // value reset
                        RouteMainActivity.this.generateEventAndProceed2NextCheckpoint(false); // generate event and proceed to next expectedCheckpoint
                    } else
                        new AlertDialog.Builder(this)
                                .setMessage("Você está longe da posição original do expectedCheckpoint! \n\nCrie uma ocorrência para justificar a distância e leia o expectedCheckpoint novamente.")
                                .setCancelable(false)
                                .setPositiveButton("Criar ocorrência", new DialogInterface.OnClickListener() { // proceed with normal method execution
                                    public void onClick(DialogInterface dialog, int id) {
                                        // occurrence button click
                                        View v = new View(RouteMainActivity.this);
                                        v.setId(R.id.route_main_menu_item_occurence);
                                        RouteMainActivity.this.onClick(v);
                                    }
                                })
                                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() { // read expectedCheckpoint again
                                    public void onClick(DialogInterface dialog, int id) {
                                        return; // no action - waits again for current expectedCheckpoint
                                    }
                                })
                                .show();
                } else {// calculated distance between current GPS coords and expectedCheckpoint coords are within current tolerance. Proceed...
                    this.generateEventAndProceed2NextCheckpoint(false); // generate event and proceed 2 next expectedCheckpoint
                }
            } ///////////////// end gps checking distance
            else { // no GPS position read or GPS verification disabled; proceed
                this.generateEventAndProceed2NextCheckpoint(false); // generate event and proceed 2 next expectedCheckpoint
            }

        } else { // invalid expectedCheckpoint. Try Again
            EventBuilder.createOutOfOrderEvent(routeId, "", expectedCheckpointTag.getTagId());
            Toast.makeText(this, "Ponto inválido. Vá até o ponto correto e tente novamente.", Toast.LENGTH_LONG).show();
            prepareAndExecuteSyncTask();
        }
    }

    private void generateEventAndProceed2NextCheckpoint(boolean tasksChecked) {

        //Show Task check dialog if necessary
        if (!tasksChecked
                && (expectedCheckpoint.getCheckpointChecks() != null)
                && (!expectedCheckpoint.getCheckpointChecks().isEmpty())
                && (!expectedCheckpoint.getCheckpointChecks().equals("[]"))) {
            createAndShowTaskDialog();
            return;
        }

        // builds an event for expectedCheckpoint read
        EventBuilder.createCheckpointTagEvent(SVRfidTagDao.getTagBySerialNumber(sTagSerial.trim()).getTagId(), routeId, expectedCheckpoint.getCheckpointId(), EventTypeMap.CHECKPOINT_POSITIVE_TAG_READ_EVENT, "", expectedCheckpoint.getCheckedTasks());


        // updates expectedCheckpoint GPS position if latitude not present
        if (expectedCheckpoint.getLatitude() == 0) {
            if (SilentAlertService.gps.hasAccuracy()) {
                Log.d(TAG, "validateCheckpoint() - Updating expectedCheckpoint (id = " + expectedCheckpoint.getCheckpointId() + ")");
                expectedCheckpoint.setLatitude(SilentAlertService.gps.getLatitude());
                expectedCheckpoint.setLongitude(SilentAlertService.gps.getLongitude());
                CheckpointTools.updateCheckpointCoordinates(expectedCheckpoint);
                Log.d(TAG, "validateCheckpoint() - Update finished!");
            }
        }

        Toast.makeText(this, "Ponto registrado com sucesso!", Toast.LENGTH_LONG).show();
        prepareAndExecuteSyncTask();
        orderCount++;
        resetTime = true;
        canShowCountDownDialog = true;

        nextCheckpoint();

    }

    private void nextCheckpoint() {

        Log.d(TAG, "nextCheckpoint() - OrderCount: " + orderCount + " CheckpointsSize: " + checkpoints.size());

        int nextIndex = (int) (orderCount - 1);

        if (orderCount > (checkpoints.size())) {
            routeFinished();
        } else {

            ApplicationControl.userSession.setOrderCount(orderCount);
            Log.d(TAG, "nextCheckpoint() - nextIndex: " + nextIndex);

            expectedCheckpoint = checkpoints.get(nextIndex);
            IS_VIBRATE_ALLOWED = true;

            try {
                expectedCheckpointTag = SVRfidTagDao.getTagById(expectedCheckpoint.getTagId());
            } catch (DaoException e) {
                Toast.makeText(this, "Tag não cadastrada no sistema.", Toast.LENGTH_LONG).show();
            }

            showCheckpoint();
        }
    }


    private void showCheckpoint() {

        RouteSchedule routeSchedule = SVRouteScheduleDao.getRouteScheduleById(ApplicationControl.userSession.getRouteScheduleId());

        Route route = SVRouteDao.getRouteById(routeSchedule.getRouteId());
        tvRoute.setText(route.getDescription());

        checkpointDescription.setText("Próximo ponto: " + expectedCheckpoint.getDescription());
        checkpointDescription.setAllCaps(true);
        checkpointDescription.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "tt0246m_.ttf");
        checkpointDescription.setTypeface(typeface);
        Checkpoint cp = SVCheckpointDao.getCheckpointById(expectedCheckpoint.getCheckpointId());

        tvSchedule.setText(String.format(Locale.getDefault(), "Agendado para: %02d:%02d", expectedCheckpoint.getTime() / 60, expectedCheckpoint.getTime() % 60));

        if ((cp.getPhoto() != null)) {
            setPhoto(cp.getPhoto());
        } else {
            int imageResource = R.drawable.icon_no_photo;
            Drawable image = getResources().getDrawable(imageResource);
            photo.setImageDrawable(image);
        }

        // countdown timer dialog
        if (canShowCountDownDialog && expectedCheckpoint.isWait()) {
            canShowCountDownDialog = false;

            final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Aguarde neste ponto");
            alertDialog.setMessage("00:00");
            alertDialog.show();

            new CountDownTimer(checkpoints.get((int) (orderCount - 2)).getStayTimeMinutes() * 60 * 1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                    // refreshs countdown timer
                    alertDialog.setMessage(
                            "Aguarde " + (millisUntilFinished / 1000) / 60 + ":"
                                    +
                                    (String.valueOf((millisUntilFinished / 1000) % 60).length() == 2 ?
                                            String.valueOf((millisUntilFinished / 1000) % 60) :
                                            "0" + String.valueOf((millisUntilFinished / 1000) % 60))
                    );
                }

                @Override
                public void onFinish() {
                    // closes dialog when countdown timer finishes and changes screen message
                    alertDialog.cancel();
                    checkpointDescription.setText("PRÓXIMO PONTO: " + expectedCheckpoint.getDescription().split("AGUARDE")[0]);
                }
            }.start();
        }
        // end countdown timer dialog

    }

    private void routeFinished() {
        checkpointDescription.setText("Rota encerrada...");
        ApplicationControl.userSession.setRouteScheduleId(0);
        ApplicationControl.userSession.setVehicleCheck(false);
        ApplicationControl.userSession.setOrderCount(0);
        ApplicationControl.userSession.setCheckpoints(null);


        final Intent i = new Intent(this, UserValidationActivity.class);
        String sSerial = "";

        if (ApplicationControl.userSession != null)
            if (ApplicationControl.userSession.getLoggedVigilant() != null)
                if (ApplicationControl.userSession.getLoggedVigilant().getLogonTagId() != null)
                    sSerial = SVRfidTagDao.getTagById(ApplicationControl.userSession.getLoggedVigilant().getLogonTagId()).getSerialNumber();

        i.putExtra("serial", sSerial);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        // alert dialog informing user that route have finished
        new AlertDialog.Builder(this)
                .setMessage("Rota encerrada!")
                .setCancelable(false)
                .setNegativeButton("Prosseguir", new DialogInterface.OnClickListener() { // read expectedCheckpoint again
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(i);
                    }
                })
                .show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.route_main, menu);
        return true;
    }

    private void skipCheckpointConfirm() {

        // IF gerado para não permitir que o vigilante pule um ponto sem criar ocorrência
        if ((!expectedCheckpoint.isMandatory()) || DOES_OCCURRENCE_EXIST) {

            // custom dialog
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.alertdialog_close_session);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            // set the custom dialog components - text, image and button
            TextView text = (TextView) dialog.findViewById(R.id.message);
            text.setText("Você confirma que deseja abandonar o ponto atual?");
            Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
            text.setTypeface(typeface2);

            Button dialogButton2 = (Button) dialog.findViewById(R.id.noButton);
            Button dialogButton = (Button) dialog.findViewById(R.id.yesButton);
            dialogButton.setTypeface(typeface2);
            dialogButton2.setTypeface(typeface2);

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBuilder.createSkipCheckpointEvent(routeId, expectedCheckpoint, "");
                    orderCount++;
                    nextCheckpoint();
                    dialog.dismiss();
                }

            });

            dialog.show();

            dialogButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        } else {
            // custom dialog
            final Dialog dialog = new Dialog(this);
            // requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.alertdialog_small);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            // set the custom dialog components - text, image and button
            TextView text = (TextView) dialog.findViewById(R.id.message);

            text.setText("Por favor, crie uma ocorrência justificando o motivo de pular o ponto!");
            Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
            text.setTypeface(typeface2);

            Button dialogButton = (Button) dialog.findViewById(R.id.okButton);
            dialogButton.setTypeface(typeface2);

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }

            });
            dialog.show();
            //			Toast.makeText(this, "Por favor, crie uma ocorrência justificando o motivo do abandono da rota", Toast.LENGTH_LONG).show();
        }
        DOES_OCCURRENCE_EXIST = false;
    }

    private void skipRouteConfirm() {

        boolean hasPendingMandatoryCheckpoints = false;

        for (SystemCheckpoint ck : checkpoints) {
            if (ck.isMandatory() && (ck.getReadTimeMillis() == 0)) {
                hasPendingMandatoryCheckpoints = true;
                break;
            }
        }

        // Não permitir que o vigilante saia da rota com ponto obrigatório pendente sem criar ocorrência
        if ((!hasPendingMandatoryCheckpoints) || DOES_OCCURRENCE_EXIST) {

            // custom dialog
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.alertdialog_close_session);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            // set the custom dialog components - text, image and button
            TextView text = (TextView) dialog.findViewById(R.id.message);
            text.setText("Você confirma que deseja abandonar a rota atual?");
            Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
            text.setTypeface(typeface2);

            Button dialogButton2 = (Button) dialog.findViewById(R.id.noButton);
            Button dialogButton = (Button) dialog.findViewById(R.id.yesButton);
            dialogButton.setTypeface(typeface2);
            dialogButton2.setTypeface(typeface2);

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBuilder.createSkipRouteEvent(routeId, "");
                    prepareAndExecuteSyncTask();
                    routeFinished();
                }

            });
            dialogButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }else{
            // custom dialog
            final Dialog dialog = new Dialog(this);
            // requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.alertdialog_small);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            // set the custom dialog components - text, image and button
            TextView text = (TextView) dialog.findViewById(R.id.message);

            text.setText("Ainda existem pontos obrigatórios na rota. Por favor, crie uma ocorrência justificando o motivo de abandonar a rota atual!");
            Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
            text.setTypeface(typeface2);

            Button dialogButton = (Button) dialog.findViewById(R.id.okButton);
            dialogButton.setTypeface(typeface2);

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }

            });
            dialog.show();
            //			Toast.makeText(this, "Por favor, crie uma ocorrência justificando o motivo do abandono da rota", Toast.LENGTH_LONG).show();
        }
        DOES_OCCURRENCE_EXIST = false;
    }

    @Override
    public void onClick(View v) {
        int which = v.getId();
        Log.i(TAG, "onClick which " + which);
        if (which == R.id.route_main_menu_item_map) {
            Intent i = new Intent(this, MapsActivity.class);
            List<MapsActivity.Marker> markers = new ArrayList<>();
            for (SystemCheckpoint ck : checkpoints){
                if(ck==expectedCheckpoint){
                    markers.add(0,new MapsActivity.Marker(ck.getLongitude(),ck.getLatitude(),ck.getDescription(), BitmapDescriptorFactory.HUE_BLUE));
                }else if(ck.getReadTimeMillis()>0){
                    markers.add(new MapsActivity.Marker(ck.getLongitude(),ck.getLatitude(),ck.getDescription(), BitmapDescriptorFactory.HUE_GREEN));
                }else{
                    markers.add(new MapsActivity.Marker(ck.getLongitude(),ck.getLatitude(),ck.getDescription(), BitmapDescriptorFactory.HUE_RED));
                }
            }
            i.putExtra(MapsActivity.EXTRA_MARKERS, (Serializable) markers);
            startActivity(i);

        } else if (which == R.id.route_main_menu_item_occurence) {
            if(!FreeRouteActivity.csnReport) {
                Intent i = new Intent(this, OccurrenceActivity.class);
                Log.d("CustomerFilter", "Opening OccurrenceActivity w/ extra: " + this.routeId);
                i.putExtra("routeId", this.routeId);
                startActivity(i);
            }else{
                Intent i = new Intent(this, ComplexOccurrenceActivity.class);
                Log.d("CustomerFilter", "Opening OccurrenceActivity w/ extra: " + this.routeId);
                i.putExtra("routeId", this.routeId);
                startActivity(i);
            }

        } else if (which == R.id.route_main_menu_item_skip) {
            skipCheckpointConfirm();
        } else if (which == R.id.route_main_menu_item_exit) {
            skipRouteConfirm();
        }

    }

}