package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.RfidTag;
import com.br.svconsultoria.svnfcmanager.model.orm.RfidTagDao;

public class SVSystemTagDao {


	private static DaoSession session = ApplicationControl.daoSession;

	public static boolean isValidBySerial(String serialCode) {

		RfidTagDao nfcTagDao = session.getRfidTagDao();

		long count = 0;

		try {
			count = nfcTagDao.queryBuilder().where(RfidTagDao.Properties.SerialNumber.eq(serialCode)).count();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		if (count > 0) {
			Log.d("SVRouteDao", "isValidBySerial() = true");
			return true;
		} else {
			Log.d("SVRouteDao", "isValidBySerial() = false");
			return false;
		}
	}

	public static RfidTag getUserTagBySerial(String sSerial) {
		Log.d("SVRouteDao", "getUserTagBySerial()");
		RfidTagDao nfcTagDao = session.getRfidTagDao();
		RfidTag tag = nfcTagDao.queryBuilder().where(RfidTagDao.Properties.SerialNumber.eq(sSerial)).unique();
		Log.d("SVRouteDao", "getUserTagBySerial() Finished!");
		return tag;
	}

}
