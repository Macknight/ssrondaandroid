package com.br.svconsultoria.svnfcmanager.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bruno.hernandes on 18/01/2017.
 */
public class DrawableImageView extends ImageView implements View.OnTouchListener{

    List<Pair<Float,Float>> points = new ArrayList<>();
    Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
    boolean needSave=true;

    public DrawableImageView(Context context) {
        super(context);
        p.setColor(Color.RED);
        p.setTextSize(20f);
        setOnTouchListener(this);
        setDrawingCacheEnabled(true);
    }

    public DrawableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        p.setColor(Color.RED);
        p.setTextSize(20f);
        setOnTouchListener(this);
        setDrawingCacheEnabled(true);
    }

    public DrawableImageView(Context context, AttributeSet attrs,int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        p.setColor(Color.RED);
        p.setTextSize(20f);
        setOnTouchListener(this);
        setDrawingCacheEnabled(true);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        for(Pair<Float,Float> point :points){
            canvas.drawText("X",point.first,point.second,p);
        }
//        if(needSave){
//            needSave=false;
//            getDrawingCache();
//        }
    }

    public Bitmap getBitmap() {
        return getDrawingCache();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        int action = event.getAction();

        switch (action)
        {
            case MotionEvent.ACTION_DOWN:
                points.add(new Pair<Float, Float>(getPointerCoords(event)[0],getPointerCoords(event)[1]));
                needSave=true;
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_CANCEL:
                break;
            default:
                break;
        }
        return true;
    }

    final float[] getPointerCoords(MotionEvent e) {
        final int index = e.getActionIndex();
        final float[] coords = new float[] { e.getX(index), e.getY(index) };
        return coords;
    }

    public void clearPoints(){
        points.clear();
        invalidate();
    }

    public void removeLastPoint(){
        if(!points.isEmpty()){
            points.remove(points.size()-1);
            invalidate();
        }
    }
}
