package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import java.util.Date;

/**
 * Entity mapped to table SRTM_EVENT_LOG.
 */
@Entity
public class EventLog {

    @Id
    private Long eventLogId;
    private long mobileId;
    private Long tagId;
    private Long eventTypeId;
    private java.util.Date eventDt;
    private Float latitude;
    private Float longitude;
    private Long vigilantId;
    private Long routeId;
    private Long checkpointId;
    private String comments;
    private String checkpointChecks;
    private String vehicleCheckId;
    @Generated(hash = 207860880)
    public EventLog(Long eventLogId, long mobileId, Long tagId, Long eventTypeId,
            java.util.Date eventDt, Float latitude, Float longitude,
            Long vigilantId, Long routeId, Long checkpointId, String comments,
            String checkpointChecks, String vehicleCheckId) {
        this.eventLogId = eventLogId;
        this.mobileId = mobileId;
        this.tagId = tagId;
        this.eventTypeId = eventTypeId;
        this.eventDt = eventDt;
        this.latitude = latitude;
        this.longitude = longitude;
        this.vigilantId = vigilantId;
        this.routeId = routeId;
        this.checkpointId = checkpointId;
        this.comments = comments;
        this.checkpointChecks = checkpointChecks;
        this.vehicleCheckId = vehicleCheckId;
    }
    @Generated(hash = 915828638)
    public EventLog() {
    }
    public Long getEventLogId() {
        return this.eventLogId;
    }
    public void setEventLogId(Long eventLogId) {
        this.eventLogId = eventLogId;
    }
    public long getMobileId() {
        return this.mobileId;
    }
    public void setMobileId(long mobileId) {
        this.mobileId = mobileId;
    }
    public Long getTagId() {
        return this.tagId;
    }
    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }
    public Long getEventTypeId() {
        return this.eventTypeId;
    }
    public void setEventTypeId(Long eventTypeId) {
        this.eventTypeId = eventTypeId;
    }
    public java.util.Date getEventDt() {
        return this.eventDt;
    }
    public void setEventDt(java.util.Date eventDt) {
        this.eventDt = eventDt;
    }
    public Float getLatitude() {
        return this.latitude;
    }
    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }
    public Float getLongitude() {
        return this.longitude;
    }
    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }
    public Long getVigilantId() {
        return this.vigilantId;
    }
    public void setVigilantId(Long vigilantId) {
        this.vigilantId = vigilantId;
    }
    public Long getRouteId() {
        return this.routeId;
    }
    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }
    public Long getCheckpointId() {
        return this.checkpointId;
    }
    public void setCheckpointId(Long checkpointId) {
        this.checkpointId = checkpointId;
    }
    public String getComments() {
        return this.comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }
    public String getCheckpointChecks() {
        return this.checkpointChecks;
    }
    public void setCheckpointChecks(String checkpointChecks) {
        this.checkpointChecks = checkpointChecks;
    }
    public String getVehicleCheckId() {
        return this.vehicleCheckId;
    }
    public void setVehicleCheckId(String vehicleCheckId) {
        this.vehicleCheckId = vehicleCheckId;
    }

}
