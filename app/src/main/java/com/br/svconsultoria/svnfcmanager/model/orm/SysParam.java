package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Unique;

/**
 * Entity mapped to table SRTM_SYS_PARAMS.
 */
@Entity
public class SysParam {

    @Id
    private Long sysParamId;
    private long xId;
    private long customerId;
    @Unique
    private String param;
    private String paramDesc;
    private String value;

    @Generated(hash = 1411941648)
    public SysParam(Long sysParamId, long xId, long customerId, String param,
            String paramDesc, String value) {
        this.sysParamId = sysParamId;
        this.xId = xId;
        this.customerId = customerId;
        this.param = param;
        this.paramDesc = paramDesc;
        this.value = value;
    }
    @Generated(hash = 1530132318)
    public SysParam() {
    }
    public Long getSysParamId() {
        return this.sysParamId;
    }
    public void setSysParamId(Long sysParamId) {
        this.sysParamId = sysParamId;
    }
    public long getXId() {
        return this.xId;
    }
    public void setXId(long xId) {
        this.xId = xId;
    }
    public long getCustomerId() {
        return this.customerId;
    }
    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }
    public String getParamDesc() {
        return this.paramDesc;
    }
    public void setParamDesc(String paramDesc) {
        this.paramDesc = paramDesc;
    }
    public String getValue() {
        return this.value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public String getParam() {
        return this.param;
    }
    public void setParam(String param) {
        this.param = param;
    }

}
