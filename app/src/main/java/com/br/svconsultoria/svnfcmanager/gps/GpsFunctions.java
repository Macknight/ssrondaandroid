package com.br.svconsultoria.svnfcmanager.gps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

public class GpsFunctions {


	private static final int  expectedAccuracy =350;
	private static final String TAG = "GpsFunctions";

	public GpsFunctions(Context context) {
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
	}
	Context ctx;



	private LocationManager locationManager;
	private LocationListener locationListener;
	private Criteria criteria = null;
	private float latitude = (float) 0.0;
	private float longitude = (float) 0.0;
	private float accuracy = (float) 0.0;
	private boolean hasAccuracy = false;

	public void initializeGpsServices(Context ctx) {
		this.ctx = ctx;

		criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);

		locationListener = new LocationListener() {

			@Override
				public void onLocationChanged(Location newLocation) {
					if (newLocation != null) {
						latitude = (float) newLocation.getLatitude();
						longitude = (float) newLocation.getLongitude();
						accuracy = newLocation.getAccuracy();
						hasAccuracy = accuracy <= expectedAccuracy ;
					}
				}

			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
				Log.d(TAG, "GPS provider "+(provider)+"status: " + status);
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onProviderDisabled(String provider) {
			}
		};

		refreshGps();
	}



	public void refreshGps(){
		if (ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
				&& ActivityCompat.checkSelfPermission(ctx, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			Toast.makeText(ctx, "Aplicativo não possi permissão para ativar localização!", Toast.LENGTH_SHORT).show();
		}else{
			locationManager.requestLocationUpdates(getBestProvider(criteria), 0, 0, locationListener);
			Location here = locationManager.getLastKnownLocation(getBestProvider(criteria));
            if(here!=null) {
                longitude = (float) here.getLongitude();
                latitude = (float) here.getLatitude();
                accuracy = here.getAccuracy();
                hasAccuracy = here.getAccuracy() <= expectedAccuracy;
            }

		}
	}

	private String getBestProvider(Criteria criteria) {
		return locationManager.getBestProvider(criteria, true);
	}



	public float getLongitude() {
		Log.d(TAG, "GPS Longitude: " + longitude);
		refreshGps();
		return longitude;
	}

	public float getLatitude() {
		Log.d(TAG, "GPS Latitude: " + latitude);
		refreshGps();
		return latitude;
	}

	public float getAccuracy() {
		Log.d(TAG, "GPS Accuracy: " + accuracy);
		return accuracy;
	}
	public boolean hasAccuracy(){
		refreshGps();
		return hasAccuracy;
	}
}
