package com.br.svconsultoria.svnfcmanager.map;

public class EventTypeMap {

	public static final long USER_READ_TAG_EVENT = 1;
	public static final long CHECKPOINT_POSITIVE_TAG_READ_EVENT = 2;
	public static final long CHECKPOINT_NEGATIVE_TAG_READ_EVENT = 3;
	public static final long SKIP_CHECKPOINT_EVENT = 4;
	public static final long SKIP_ROUTE_EVENT = 5;
	public static final long PHONE_FALL_EVENT = 6;
	public static final long PANIC_SHAKE_EVENT = 7;
	public static final long PANIC_CANCEL_EVENT = 8;
	public static final long PANIC_SMS_SENT_EVENT = 9;
	public static final long PANIC_SMS_DELIVERED_EVENT = 10;
	public static final long PANIC_EMAIL_SENT_EVENT = 11;
	public static final long NEW_OCCURRENCE_EVENT = 12;
	public static final long CHECKPOINT_OUTOF_ORDER = 13;
	public static final long CHECKPOINT_READ_OUT_OF_POSITION = 14;
	public static final long VEHICLE_CHECK = 15;
	public static final long IDLENESS = 16;

}
