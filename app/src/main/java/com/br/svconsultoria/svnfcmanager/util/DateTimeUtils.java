package com.br.svconsultoria.svnfcmanager.util;

import android.annotation.SuppressLint;
import android.util.Log;

import com.br.svconsultoria.svnfcmanager.model.orm.RouteComposition;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteSchedule;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SuppressLint("SimpleDateFormat")
@SuppressWarnings("deprecation")
public class DateTimeUtils {

	private static final String TAG = "DateTimeUtils";

	public static Date formatDate(Date date) {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		try {
			date = df.parse(df.format(date));
		} catch (ParseException e) {
			Log.d(TAG, "formatDate() - Error formatting date. (ParseException)");
			e.printStackTrace();
		}
		return date;
	}

	public static Date formatFullDate(Date date) {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		try {
			date = df.parse(df.format(date));
		} catch (ParseException e) {
			Log.d(TAG, "formatFullDate() - Error formatting date. (ParseException)");
			e.printStackTrace();
		}
		return date;
	}

	public static String getTimeTable(long lMinutes) {
		int iHour = (int) (lMinutes / 60);
		int iMinutes = (int) (lMinutes % 60);
		Date date = new Date(0, 0, 0, iHour, iMinutes);
		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		return df.format(date);
	}

}
