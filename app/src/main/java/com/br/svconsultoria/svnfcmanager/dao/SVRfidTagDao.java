package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.RfidTag;
import com.br.svconsultoria.svnfcmanager.model.orm.RfidTagDao;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;

import java.util.List;

public class SVRfidTagDao {


	private static DaoSession session = ApplicationControl.daoSession;


	public static void insertOrUpdateRfidTags(final List<RfidTag> list) {
		Log.d("SVRfidTagDao", "insertOrUpdateRfidTags() Starting...");
		session.runInTx(new Runnable() {
			@Override
			public void run() {
				RfidTagDao rfidtagdao = session.getRfidTagDao();
				rfidtagdao.insertOrReplaceInTx(list);
			}
		});

		long xid = SVUpdateSequenceDao.getXid(UpdateSequenceMap.SRT_RFID_TAGS).getxId();
		for (RfidTag object : list) {
			if (xid < object.getXId()) {
				xid = object.getXId();
			}
		}
		SVUpdateSequenceDao.updateXId(new UpdateSequence(null, UpdateSequenceMap.SRT_RFID_TAGS, xid));

		Log.d("SVRfidTagDao", "insertOrUpdateRfidTags() Finished!");
	}

	public static void insertOrReplaceRfidTag(RfidTag tag) {
		Log.d("SVRfidTagDao", "insertOrReplaceRfidTag() Starting...");
		RfidTagDao rfidtagdao = session.getRfidTagDao();
		rfidtagdao.insertOrReplace(tag);
		Log.d("SVRfidTagDao", "insertOrReplaceRfidTag() Finished!");
	}

	public static RfidTag getTagById(long tagId) {
		Log.d("SVRfidTagDao", "getTagById() tagId = " + tagId);
		RfidTag tag = session.getRfidTagDao().queryBuilder().where(RfidTagDao.Properties.TagId.eq(tagId)).where(RfidTagDao.Properties.StatusId.eq(1)).uniqueOrThrow();
		Log.d("SVRfidTagDao", "getTagById() Finished!");
		return tag;
	}

	public static RfidTag getTagBySerialNumber(String serial) {
		Log.d("SVRfidTagDao", "getTagBySerialNumber() serialNumber = " + serial);
		RfidTag tag = session.getRfidTagDao().queryBuilder().where(RfidTagDao.Properties.SerialNumber.eq(serial)).where(RfidTagDao.Properties.StatusId.eq(1)).uniqueOrThrow();
		Log.d("SVRfidTagDao", "getTagBySerialNumber() Finished!");
		return tag;
	}
}