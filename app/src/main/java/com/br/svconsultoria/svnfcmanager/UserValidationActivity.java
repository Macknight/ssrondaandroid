package com.br.svconsultoria.svnfcmanager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.br.svconsultoria.svnfcmanager.dao.SVEventDao;
import com.br.svconsultoria.svnfcmanager.dao.SVMobilePhoneDao;
import com.br.svconsultoria.svnfcmanager.dao.SVOccurrenceDao;
import com.br.svconsultoria.svnfcmanager.dao.SVSystemTagDao;
import com.br.svconsultoria.svnfcmanager.dao.SVUserDao;
import com.br.svconsultoria.svnfcmanager.dao.SVVehicleCheckDao;
import com.br.svconsultoria.svnfcmanager.dao.SVVigilantDao;
import com.br.svconsultoria.svnfcmanager.dao.SVVigilantPhotoDao;
import com.br.svconsultoria.svnfcmanager.map.StatusMap;
import com.br.svconsultoria.svnfcmanager.model.orm.RfidTag;
import com.br.svconsultoria.svnfcmanager.model.orm.User;
import com.br.svconsultoria.svnfcmanager.model.orm.Vigilant;
import com.br.svconsultoria.svnfcmanager.model.orm.VigilantPhotos;
import com.br.svconsultoria.svnfcmanager.services.SilentAlertService;
import com.br.svconsultoria.svnfcmanager.util.SVUtil;

import java.io.ByteArrayOutputStream;

@SuppressLint("ShowToast")
public class UserValidationActivity extends Activity {

    private static final String TAG = "UserValidationActivity";

    private ImageView photo = null;
    private static final int CAMERA_REQUEST = 1888;
    private Bitmap bitmap;

    private String sSerial = "";
    private Vigilant vigilant = null;
    private User user = null;
    private TextView errorView = null;
    private TextClock digitalClock1 = null;
    private ImageView routesButton = null;
    private ImageView occurrenceButton = null;

    private RelativeLayout userLayout = null;
    private LinearLayout errorLayout = null;
    private LinearLayout menuLayout = null;
    private RelativeLayout userValidationLayout = null;
    private RelativeLayout monitorLayout = null;

    private TextView userName = null;
    private TextView identDoc = null;
    private TextView contactInfo = null;

    final Context context = this;
    public static TextView numberEvent = null;
    public static TextView numberOccurrence = null;
    public static TextView numberVehicleCkeck = null;
    public static TextView tvServer = null;
    public static TextView tvNextRoute = null;
    public static TextView tvNextRouteText = null;
    public static ImageView ivMessage = null;
    public static String message = null;

    private Button button;

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        SVUtil.touch(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_user_validation);

        userLayout = (RelativeLayout) findViewById(R.id.userLayout);
        errorLayout = (LinearLayout) findViewById(R.id.errorLayout);
        menuLayout = (LinearLayout) findViewById(R.id.menuLayout);
        monitorLayout = (RelativeLayout) findViewById(R.id.monitorLayout);
        userValidationLayout = (RelativeLayout) findViewById(R.id.userValidationActivity);
        errorView = (TextView) errorLayout.findViewById(R.id.errorView);

        userLayout.setVisibility(View.INVISIBLE);
        errorLayout.setVisibility(View.INVISIBLE);
        menuLayout.setVisibility(View.INVISIBLE);
        monitorLayout.setVisibility(View.INVISIBLE);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "tt0246m_.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");

        userName = (TextView) userLayout.findViewById(R.id.userName);
        ((TextView) findViewById(R.id.userName)).setTypeface(typeface);
        ((TextClock) findViewById(R.id.digitalClock1)).setTypeface(typeface);
        identDoc = (TextView) userLayout.findViewById(R.id.userDoc);
        ((TextView) findViewById(R.id.userDoc)).setTypeface(typeface);
        contactInfo = (TextView) userLayout.findViewById(R.id.userContactInfo);
        ((TextView) findViewById(R.id.userContactInfo)).setTypeface(typeface);

        numberEvent = (TextView) userValidationLayout.findViewById(R.id.tvEvent);
        ((TextView) findViewById(R.id.tvEvent)).setTypeface(typeface);
        numberOccurrence = (TextView) userValidationLayout.findViewById(R.id.tvOccurrence);
        ((TextView) findViewById(R.id.tvOccurrence)).setTypeface(typeface);
        numberVehicleCkeck= (TextView) userValidationLayout.findViewById(R.id.tvVehicleCheck);
        ((TextView) findViewById(R.id.tvVehicleCheck)).setTypeface(typeface);

        tvServer = (TextView) userValidationLayout.findViewById(R.id.tvServer);
        ((TextView) findViewById(R.id.tvServer)).setTypeface(typeface);

        tvNextRoute = (TextView) userValidationLayout.findViewById(R.id.tvNextRoute);
        ((TextView) findViewById(R.id.tvNextRoute)).setTypeface(typeface2);
        tvNextRouteText = (TextView) userValidationLayout.findViewById(R.id.tvNextRouteText);
        ((TextView) findViewById(R.id.tvNextRouteText)).setTypeface(typeface2);
        ivMessage = (ImageView) userValidationLayout.findViewById(R.id.ivMessage);

        ((TextView) findViewById(R.id.title)).setTypeface(typeface2);

        tvNextRoute.setVisibility(View.INVISIBLE);
        tvNextRouteText.setVisibility(View.INVISIBLE);
        ivMessage.setVisibility(View.INVISIBLE);

        routesButton = (ImageView) findViewById(R.id.tvRoutes);
        routesButton.setEnabled(false);
        routesButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                startUserSession();
            }
        });

        occurrenceButton = (ImageView) findViewById(R.id.tvOccurrences);
        if(!FreeRouteActivity.csnReport) {
            occurrenceButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), OccurrenceActivity.class));
                }
            });
        }else{
            //TODO: [MACK] CALL csn complexoccurrence activity
            occurrenceButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), ComplexOccurrenceActivity.class));
                }
            });
        }
        photo = (ImageView) findViewById(R.id.routePhoto);

        photo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        try {
            Bundle extras = getIntent().getExtras();
            sSerial = extras.getString("serial");
            Log.d(TAG, "Tag Serial: " + sSerial);
            if (SVSystemTagDao.isValidBySerial(sSerial)) {
                Log.d(TAG, "Usuário encontrado (isValidTag(tagSerial))");

                RfidTag nfcTag = SVSystemTagDao.getUserTagBySerial(sSerial);
                Log.d(TAG, "TagID: " + nfcTag.getTagId());

                if (SVVigilantDao.isValidByTag(nfcTag.getTagId())) {
                    vigilant = SVVigilantDao.getVigilantByTagId(nfcTag.getTagId());
                    Log.d(TAG, "VigilantID: " + vigilant.getVigilantId());

                    if (vigilant.getStatusId() == StatusMap.STATUS_ACTIVE) {
                        try {
                            user = SVUserDao.getUserById(vigilant.getUserId());
                            Log.d(TAG, "UserID: " + user.getUserId());
                        } catch (Exception e) {
                            // Toast.makeText(this, "Nenhum usuário relacionado com esse vigilante.", Toast.LENGTH_LONG).show();
                            setTextResultView("Usuário não encontrado.\nConfigure um usuário relacionado\n a esse cartão.");
                        }

                        if (user != null) {
                            ApplicationControl.lastLoggedViggilantId = vigilant.getVigilantId();
                            ApplicationControl.userSession.setLoggedUser(user);
                            ApplicationControl.userSession.setLoggedVigilant(vigilant);
                            // Usuário encontrado. Gerando tela de dados do mesmo
                            routesButton.setEnabled(true);
                            setUserResultView();
                        }
                    } else {
                        Log.d(TAG, "Vigilant statusID = 0. This vigilant is deactivated.");
                        setTextResultView("O seu cadastro está desativado.\nEntre em contato com a administração.\n\nSerial: " + sSerial);
                    }
                } else {
                    Log.d(TAG, "Tag is available but not linked with a vigilant.");
                    setTextResultView("Este cartão está cadastrado no sistema mas não está vinculado a um vigilante ou o vigilante foi inativado.\n\nSerial: " + sSerial);
                }

            } else {
                Log.d(TAG, "Tag is available.");
                routesButton.setEnabled(false);
                setTextResultView("Tag não cadastrada.\n\nSerial: " + sSerial + "\n\nLatitude: " + SilentAlertService.gps.getLatitude() + "\n\nLongitude: " + SilentAlertService.gps.getLongitude());
            }
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }

        // ALAN - Criação de chamada de emergência

        button = (Button) findViewById(R.id.emergencyButton);
        final String telNumber = SVMobilePhoneDao.getSosNumber();

        // add PhoneStateListener
        PhoneCallListener phoneListener = new PhoneCallListener();
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);

        // add button listener
        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (telNumber == null) {
                    Toast.makeText(context, "Número não configurado", Toast.LENGTH_LONG);
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse(telNumber));
                    if (ActivityCompat.checkSelfPermission(UserValidationActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(UserValidationActivity.this, "Aplicativo não possui permissão para fazer ligações!", Toast.LENGTH_LONG);
                    } else {
                        startActivity(callIntent);
                    }

                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        if (user != null) {

            // custom dialog
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.alertdialog_close_session);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            // set the custom dialog components - text, image and button
            TextView text = (TextView) dialog.findViewById(R.id.message);
            text.setText("Deseja fechar a sua sessão?");
            Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
            text.setTypeface(typeface2);

            Button dialogButton2 = (Button) dialog.findViewById(R.id.noButton);
            Button dialogButton = (Button) dialog.findViewById(R.id.yesButton);
            dialogButton.setTypeface(typeface2);
            dialogButton2.setTypeface(typeface2);

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }
            });

            dialog.show();

            dialogButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    dialog.dismiss();
                    // Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    // intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    // startActivity(intent);
                }
            });

            dialog.show();

            // AlertDialog.Builder builder = new AlertDialog.Builder(this);
            // builder.setMessage("Deseja fechar a sua sessão?").setCancelable(false).setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            // @Override
            // public void onClick(DialogInterface dialog, int id) {
            // finish();
            // Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            // intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            // startActivity(intent);
            // }
            // }).setNegativeButton("Não", new DialogInterface.OnClickListener() {
            // @Override
            // public void onClick(DialogInterface dialog, int id) {
            // dialog.cancel();
            // }
            // });
            // AlertDialog alert = builder.create();
            // alert.show();
            // return;
            // } else {
        } else {
            // Quando user <> null, esse código sai da tela
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }

    private void setTextResultView(String text) {

        userLayout.setVisibility(View.INVISIBLE);
        errorLayout.getLayoutParams().height = LayoutParams.FILL_PARENT;
        errorLayout.getLayoutParams().width = LayoutParams.FILL_PARENT;
        errorLayout.setVisibility(View.VISIBLE);
        errorView.setText(text);
    }

    private void setUserResultView() {
        userLayout.setVisibility(View.VISIBLE);
        monitorLayout.setVisibility(View.VISIBLE);
        menuLayout.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.INVISIBLE);
        tvNextRoute.setVisibility(View.VISIBLE);
        tvNextRouteText.setVisibility(View.VISIBLE);
        ivMessage.setVisibility(View.VISIBLE);

        userName.setText(user.getName());
        identDoc.setText(user.getIdentDoc());
        contactInfo.setText(user.getContactInfo());

        tvNextRoute.setText(new RouteListActivity().getNextRoute());

        if (ApplicationControl.IS_SERVER_ON) {
            tvServer.setText("ON");
        } else {
            tvServer.setText("OFF");
        }

        numberEvent.setText(String.valueOf(SVEventDao.getEvents().size()));
        numberOccurrence.setText(String.valueOf(SVOccurrenceDao.getOccurrences().size()));
        numberVehicleCkeck.setText(String.valueOf(SVVehicleCheckDao.getVehicleChecksCount()));

        // Log.d(TAG, "user.getPhoto().length: " + user.getPhoto().length);

        if (SVVigilantPhotoDao.vigilantHasPhotos(vigilant.getVigilantId())) {
            setPhoto(SVVigilantPhotoDao.getVigilantPhoto(vigilant.getVigilantId()));
        } else if ((user.getPhoto() != null) && (user.getPhoto().length > 10000)) {
            setPhoto(user.getPhoto());
        }
    }

    private void setPhoto(byte[] picture) {
        Bitmap bmp = BitmapFactory.decodeByteArray(picture, 0, picture.length);
        photo.setImageBitmap(bmp);
    }

    private void saveUserData() {
        ApplicationControl.userSession.setLoggedUser(user);
        ApplicationControl.userSession.setLoggedVigilant(vigilant);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == CAMERA_REQUEST) && (resultCode == RESULT_OK)) {

            try {
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                // Toast.makeText(this, "Tamanho: " + byteArray.length, Toast.LENGTH_SHORT).show();

                user.setPhoto(byteArray);

                VigilantPhotos vPhoto = new VigilantPhotos();
                vPhoto.setPhoto(byteArray);
                vPhoto.setVigilantId(vigilant.getVigilantId());
                SVVigilantPhotoDao.insertNewVigilantPhoto(vPhoto);

                setPhoto(byteArray);
                ApplicationControl.sendVigilantPhotos();
                Toast.makeText(getApplicationContext(), "Sincronizando foto...", Toast.LENGTH_LONG).show();

            } catch (Exception e) {
                Toast.makeText(this, "Falha ao salvar a foto...", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.user_validation, menu);
        return true;
    }

    private void startUserSession() {
        // Toast.makeText(this, "Starting user session...", Toast.LENGTH_SHORT).show();
        saveUserData();
        // Intent i = new Intent(this, RoutesActivity.class);
        Intent i = new Intent(this, RouteListActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        // i.putExtra("serial", sTagSerial);
        startActivity(i);
    }

    public void showMessage(View view) {
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertdialog_ok);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.message);
        text.setText("Não há mensagens");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
        text.setTypeface(typeface2);

        Button dialogButton = (Button) dialog.findViewById(R.id.okButton);
        dialogButton.setTypeface(typeface2);

        // AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if ((message == null) || message.equals("true")) {
            message = "Não há mensagens";
        }
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // builder.setMessage(message).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                // @Override
                // public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        dialog.show();
        return;
    }

    public void prepareAndExecuteSyncTask(View view) {

        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alertdialog_close_session);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.message);
        text.setText("Deseja sincronizar os dados com o servidor?");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
        text.setTypeface(typeface2);

        Button dialogButton2 = (Button) dialog.findViewById(R.id.noButton);
        Button dialogButton = (Button) dialog.findViewById(R.id.yesButton);
        dialogButton.setTypeface(typeface2);
        dialogButton2.setTypeface(typeface2);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "prepareAndExecuteSyncTask()");

                if (SVEventDao.hasEvents())
                    ApplicationControl.startSend();
                else
                    ApplicationControl.syncWithServer();

                Toast.makeText(getApplicationContext(), "Sincronizando dados...", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });

        dialog.show();

        dialogButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });

        dialog.show();

        // AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // builder.setMessage("Deseja sincronizar os dados com o servidor?").setCancelable(false).setPositiveButton("Sim", new DialogInterface.OnClickListener() {
        // @Override
        // public void onClick(DialogInterface dialog, int id) {
        // Log.d(TAG, "prepareAndExecuteSyncTask()");
        // ApplicationControl.startSend();
        // Toast.makeText(getApplicationContext(), "Enviando para o servidor...", Toast.LENGTH_LONG).show();
        // // if (ApplicationControl.SEND_FINISHED) {
        // // ApplicationControl.startSend();
        // // Toast.makeText(getApplicationContext(), "Enviando para o servidor...", Toast.LENGTH_LONG).show();
        // // }
    }

    // }).setNegativeButton("Não", new DialogInterface.OnClickListener() {
    // @Override
    // public void onClick(DialogInterface dialog, int id) {
    // dialog.cancel();
    // }
    // });
    // AlertDialog alert = builder.create();
    // alert.show();
    // return;
    // }

    // monitor phone call activities
    private class PhoneCallListener extends PhoneStateListener {

        private boolean isPhoneCalling = false;

        String LOG_TAG = "LOGGING 123";

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (TelephonyManager.CALL_STATE_RINGING == state) {
                // phone ringing
                Log.i(LOG_TAG, "RINGING, number: " + incomingNumber);
            }

            if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                // active
                Log.i(LOG_TAG, "OFFHOOK");

                isPhoneCalling = true;
            }

            if (TelephonyManager.CALL_STATE_IDLE == state) {
                // run when class initial and phone call ended,
                // need detect flag from CALL_STATE_OFFHOOK
                Log.i(LOG_TAG, "IDLE");

                if (isPhoneCalling) {

                    Log.i(LOG_TAG, "restart app");

                    // restart app
                    Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);

                    isPhoneCalling = false;
                }

            }
        }
    }
}
