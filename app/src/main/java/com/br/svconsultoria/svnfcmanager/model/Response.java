package com.br.svconsultoria.svnfcmanager.model;

import java.util.ArrayList;

public class Response {
	private Boolean success;
	private String message;
	private String type;
	private String tableName;
	private ArrayList<?> list;
	private String responseContent;
	
	Response(Boolean success, String message, ArrayList<?> list) {
		this.success = success;
		this.message = message;
		this.list = list;
	}
	
	public Response() {}
	
	public Boolean getSuccess() {
		return success;
	}
	
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getTableName() {
		return tableName;
	}
	
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public ArrayList<?> getList() {
		return list;
	}
	
	public void setList(ArrayList<?> list) {
		this.list = list;
	}
	
	public String getResponseContent() {
		return responseContent;
	}
	
	public void setResponseContent(String responseContent) {
		this.responseContent = responseContent;
	}
	
}
