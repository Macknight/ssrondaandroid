package com.br.svconsultoria.svnfcmanager.tools;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.View;

import com.br.svconsultoria.svnfcmanager.MainActivity;

public class AnimateLoading {

	private static final String TAG = "AnimateLoading";
	private static Context context;
	private static View loadingView;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private static void showProgress(final boolean show) {

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = context.getResources().getInteger(android.R.integer.config_shortAnimTime);

			loadingView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					loadingView.setVisibility(show ? View.VISIBLE : View.GONE);
				}
			});

		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			loadingView.setVisibility(show ? View.VISIBLE : View.GONE);
		}
	}

	public static void animate(Context c, View lv) {
		Log.d(TAG, "animate()");
		context = c;
		loadingView = lv;
		showProgress(true);
		startAnimationSync();
	}

	private static void startAnimationSync() {
		Log.d(TAG, "startAnimationSync()");
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				Log.d(TAG, "startAnimationSync() - doInBackground()");
//				while (!ApplicationControl.SEND_FINISHED) {
//					try {
//						Thread.sleep(1000);
//					} catch (InterruptedException e) {}
//				}
				stopAnimation();
				return null;
			}
		};

		task.execute();
	}

	public static void animateSplashScreen(Context c, View lv) {
		Log.d(TAG, "animateSplashScreen()");
		context = c;
		loadingView = lv;
		showProgress(true);
		startSplashScreenAnimation();
	}

	private static void stopAnimation() {
		Log.d(TAG, "stopAnimation()");
		showProgress(false);
	}

	private static void startSplashScreenAnimation() {
		Log.d(TAG, "startSplashScreenAnimation()");
		AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				Log.d(TAG, "startSplashScreenAnimation() - doInBackground()");
				try {
					Thread.sleep(3000);
					Log.d(TAG, "startSplashScreenAnimation() - doInBackground() - Sleeping...");
				} catch (InterruptedException e) {}
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				Log.d(TAG, "startSplashScreenAnimation() - onPostExecute()");
				stopAnimation();
				Intent i = new Intent(context, MainActivity.class);
				context.startActivity(i);
				super.onPostExecute(result);
			}
		};

		task.execute();
	}

}
