package com.br.svconsultoria.svnfcmanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.View;
import android.widget.Chronometer;
import android.widget.Chronometer.OnChronometerTickListener;
import android.widget.TextView;

import com.br.svconsultoria.svnfcmanager.dao.SVCustomerDao;
import com.br.svconsultoria.svnfcmanager.dao.SVOccurrenceTypesDao;
import com.br.svconsultoria.svnfcmanager.dao.SVPriorityDao;

import java.util.Timer;
import java.util.TimerTask;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class FirstSyncActivity extends Activity {

	public static final String TAG = "FirstSyncActivity";

	// starts after 'startsAfterMs' miliseg, repeat after 'repeatAfterMs' miliseg
	final int startsAfterMs = 5000;
	final int repeatAfterMs = 500;

	// total milisseconds elapsed
	 int totalElapsed = 0;

	 // max loading minutes
	 int maxLoadingMinutes = 10 * 60 * 1000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first_sync);
		setTitle("SSRonda");

		ApplicationControl.syncWithServer();

		// retrieve chronometer object
		final Chronometer c = (Chronometer) findViewById(R.id.chronometer);

		// start chronometer
		c.start();

		// change chronometer time format
		c.setOnChronometerTickListener(new OnChronometerTickListener() {
		    public void onChronometerTick(Chronometer cArg) {
		        long t = SystemClock.elapsedRealtime() - cArg.getBase();
		        cArg.setText(DateFormat.format("mm:ss", t));
		    }
		});

		// timer to check every X seconds if synchronizing is still running
		final Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {

			// times sync flag was detected as false (when SYNC_COUNTER is evaluated as 0)
			int synchronizingIsFalse = 0;

			@Override
			public void run() {

				totalElapsed += repeatAfterMs;
				Log.d(TAG, "elapsed " + totalElapsed + " maxLoading " + maxLoadingMinutes);

				if (ApplicationControl.SYNC_COUNTER == 0) {

					synchronizingIsFalse++; // times SYNC_COUNTER were detected as 0 tables to update.

					if (synchronizingIsFalse == 10) {
						Log.i(TAG, "First sync finished.");

						runOnUiThread(new Thread("finishedSyncUIMessagesThread") {

							@Override
							public void run() {
							  // change text, back button visible, stop chronometer, hide animation
							  TextView tv =  (TextView) findViewById(R.id.first_run_loading_message_id);
							  if (SVOccurrenceTypesDao.getOccurrenceTypes().size() == 0 ||
									  SVPriorityDao.getPriorityLevelDescs().size() == 0 ||
									  SVCustomerDao.getCustomerList().size() == 0
									  )
								  tv.setText("A sincronização parece não ter sido concluída corretamente. Verifique ou tente novamente!" +
								  		"\nTipos de ocorrência recebidos: " + SVOccurrenceTypesDao.getOccurrenceTypes().size() +
								  		"\nNíveis de prioridade recebidas: " + SVPriorityDao.getPriorityLevelDescs().size() +
								  		"\nClientes recebidos: " + SVCustomerDao.getCustomerList().size()
										  ); // change original text
							  else
									  tv.setText("           Sincronização concluída!"); // change original text
							  tv.setGravity(Gravity.CENTER); // center text

							  findViewById(R.id.cancel_button).setVisibility(View.VISIBLE);
							  findViewById(R.id.progressBar_splash).setVisibility(View.INVISIBLE);
							  c.stop();
							  timer.cancel();
							  // FIM CUSTOM
							}
						});

					}
				} else { synchronizingIsFalse = 0; }

				if ((totalElapsed) > maxLoadingMinutes) { // time elapsed exceeded max loading minutes

					// clean database
//					ApplicationControl.daoSession.getCheckpointDao().deleteAll();
//					ApplicationControl.daoSession.getCustomerDao().deleteAll();
//					ApplicationControl.daoSession.getEventLogDao().deleteAll();
//					ApplicationControl.daoSession.getEventTypeDao().deleteAll();
//					ApplicationControl.daoSession.getInsertSequenceDao().deleteAll();
//					ApplicationControl.daoSession.getMobilePhoneDao().deleteAll();
//					ApplicationControl.daoSession.getOccurrenceDao().deleteAll();
//					ApplicationControl.daoSession.getOccurrenceTypeDao().deleteAll();
//					ApplicationControl.daoSession.getPriorityDao().deleteAll();
//					ApplicationControl.daoSession.getRfidTagDao().deleteAll();
//					ApplicationControl.daoSession.getRouteCompositionDao().deleteAll();
//					ApplicationControl.daoSession.getRouteDao().deleteAll();
//					ApplicationControl.daoSession.getRouteScheduleDao().deleteAll();
////					ApplicationControl.daoSession.getSysParamDao().deleteAll();
//					ApplicationControl.daoSession.getSystemParamDao().deleteAll();
//					ApplicationControl.daoSession.getUpdateSequenceDao().deleteAll();
//					ApplicationControl.daoSession.getUserDao().deleteAll();
//					ApplicationControl.daoSession.getVigilantDao().deleteAll();
//					ApplicationControl.daoSession.getVigilantPhotosDao().deleteAll();

					runOnUiThread(new Thread("firstSyncFailure") {

						@Override
						public void run() {
							// change text, back button visible, stop chronometer, hide animation
							TextView tv =  (TextView) findViewById(R.id.first_run_loading_message_id);
							tv.setText("Ocorreu um erro na sincronização. Tente novamente"); // change original text
							tv.setGravity(Gravity.CENTER); // center text

							findViewById(R.id.cancel_button).setVisibility(View.VISIBLE);
							findViewById(R.id.progressBar_splash).setVisibility(View.INVISIBLE);
							c.stop();
							timer.cancel();
							// FIM CUSTOM
						}
					});
				}
			}
		}, startsAfterMs, repeatAfterMs);


	}

	public void invokeMainActivity(View v) {
		// invoke MainActivity
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
	}
}
