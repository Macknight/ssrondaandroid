package com.br.svconsultoria.svnfcmanager.http.events;

import android.os.Handler;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.UserValidationActivity;
import com.br.svconsultoria.svnfcmanager.dao.SVEventDao;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.http.HttpAuthenticate;
import com.br.svconsultoria.svnfcmanager.map.HttpConnAction;
import com.br.svconsultoria.svnfcmanager.model.Response;
import com.br.svconsultoria.svnfcmanager.model.orm.EventLog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class EventSender {

    private String TAG = "EventSender";

    private static final int AUTHENTICATION_CONNECTION_TIMEOUT = 30000;
    private static final int AUTHENTICATION_SOCKET_TIMEOUT = 30000;

    private Handler handler = new Handler();
    private Runnable changeUIRunnable = new Runnable() {
        @Override
        public void run() {
            if (UserValidationActivity.numberEvent != null) {
                UserValidationActivity.numberEvent.setText(String.valueOf(SVEventDao.getEvents().size()));
            }
            if (UserValidationActivity.tvServer != null) {
                if (ApplicationControl.IS_SERVER_ON) {
                    UserValidationActivity.tvServer.setText("ON");

                } else {
                    UserValidationActivity.tvServer.setText("OFF");
                }
            }
        }
    };

    private Timer timer = new Timer();
    private TimerTask sendEventsTask = new TimerTask() {
        @Override
        public void run() {

            if (SVEventDao.hasEvents() && HttpAuthenticate.authenticate()) {

                HttpPost post = new HttpPost();
                Response resp;

                Log.d(TAG, "Starting connection with server");

                String response;
                ArrayList<EventLog> eventList;
                String uri = HttpConnAction.HTTP + SVMobileParamsDao.getHostParam() + HttpConnAction.EVENTS_URI;
                android.util.Log.d(TAG, "sendEvents() - URI: " + uri);


                List<NameValuePair> args = new ArrayList<NameValuePair>();
                args.add(new BasicNameValuePair(HttpConnAction.ACTION_FIELD, HttpConnAction.INSERT_VALUE));
                eventList = SVEventDao.getEventsLimit();

                Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyyHH-mm-ss").create();

                String json = gson.toJson(eventList);
                android.util.Log.d(TAG, "sendEvents() - JSON Event List: " + json);
                args.add(new BasicNameValuePair(HttpConnAction.LIST_FIELD, json.toString()));

                try {

                    post.setURI(new URI(uri));
                    post.setEntity(new UrlEncodedFormEntity(args));


                    HttpClient httpclient = ApplicationControl.httpclient;
                    HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), AUTHENTICATION_CONNECTION_TIMEOUT);
                    HttpConnectionParams.setSoTimeout(httpclient.getParams(), AUTHENTICATION_SOCKET_TIMEOUT);

                    android.util.Log.d(TAG, "EventSenderThread - URI: " + post.getURI());

                    ResponseHandler<String> responseHandler = new BasicResponseHandler();
                    String responseBody = "";

                    httpclient.getConnectionManager().closeExpiredConnections();

                    responseBody = httpclient.execute(post, responseHandler);
                    android.util.Log.d(TAG, "doInBackground() - Response: " + responseBody);

                    resp = gson.fromJson(responseBody, Response.class);
                    resp.setType(HttpConnAction.EVENTS_URI);
                    resp.setList(eventList);

                    SVEventDao.deleteCommittedEvents((ArrayList<EventLog>) resp.getList());
                    ApplicationControl.stAppLastDataSendHour = ApplicationControl.getStrCurrentDateHour("HH:mm:ss");

                    response = "true";

                } catch (Exception e) {
                    android.util.Log.e(TAG, "Error sync events: ", e);
                    response = "Verifique se o servidor está ativo e ou o endereço IP está correto: " + e.getMessage();
                    e.printStackTrace();
                }
                android.util.Log.d(TAG, "authenticate() - Finishing...");

                ApplicationControl.IS_SERVER_ON = response.equals("true");
                UserValidationActivity.message = response;

                handler.post(changeUIRunnable);
            }

        }

    };

    protected EventSender() {

    }

    private static final EventSender instance = new EventSender();


    public static void startEventSenderTask(){
        instance.timer.schedule(instance.sendEventsTask, 1000, 1000);
    }

}
