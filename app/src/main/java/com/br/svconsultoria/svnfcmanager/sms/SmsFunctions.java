package com.br.svconsultoria.svnfcmanager.sms;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import com.br.svconsultoria.svnfcmanager.dao.SVSysParamsDao;
import com.br.svconsultoria.svnfcmanager.map.EventTypeMap;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.tools.EventBuilder;

public class SmsFunctions {

	private static final String TAG = "SmsFunctions";
	private Context context = null;
	private Activity activity = null;
	private String phoneNumber = "";
	private String message = "";
	private BroadcastReceiver receiver1 = null;
	private BroadcastReceiver receiver2 = null;

	public SmsFunctions(Context ctx, Activity act) {
		Log.d(TAG, "SmsFunctions() Instantiated!");
		context = ctx;
		activity = act;
		phoneNumber = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_SMS_PHONE_NUMBER).getValue();
		message = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_SMS_MESSAGE_TO_SEND).getValue();
	}

	public void sendSMS() {
		Log.d(TAG, "sendSMS()");
		String SENT = "SMS_SENT";
		String DELIVERED = "SMS_DELIVERED";

		PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);

		PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(DELIVERED), 0);

		// ---when the SMS has been sent---
		Log.d(TAG, "sendSMS() - Registering Broadcast Receivers");

		receiver1 = new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				String smsStatus = "";
				switch (getResultCode()) {
					case Activity.RESULT_OK:
						smsStatus = "Enviado com sucesso.";
						break;
					case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
						smsStatus = "Não enviado. Falha genérica.";
						break;
					case SmsManager.RESULT_ERROR_NO_SERVICE:
						smsStatus = "Não enviado. Sem serviço de telefonia.";
						break;
					case SmsManager.RESULT_ERROR_NULL_PDU:
						smsStatus = "Não enviado. NULL PDU";
						break;
					case SmsManager.RESULT_ERROR_RADIO_OFF:
						smsStatus = "Não enviado. Conexão de telefonia desligada no aparelho.";
						break;
				}
				EventBuilder.createPanicEvent(EventTypeMap.PANIC_SMS_SENT_EVENT, smsStatus);
			}
		};

		activity.registerReceiver(receiver1, new IntentFilter(SENT));

		// ---when the SMS has been delivered---
		receiver2 = new BroadcastReceiver() {
			@Override
			public void onReceive(Context arg0, Intent arg1) {
				String smsStatus = "";
				switch (getResultCode()) {
					case Activity.RESULT_OK:
						smsStatus = "SMS entregue ao destinatário.";
						Toast.makeText(context, "SMS delivered", Toast.LENGTH_SHORT).show();
						break;
					case Activity.RESULT_CANCELED:
						smsStatus = "SMS não foi entregue ao destinatário.";
						break;
				}

				EventBuilder.createPanicEvent(EventTypeMap.PANIC_SMS_DELIVERED_EVENT, smsStatus);
			}
		};
		activity.registerReceiver(receiver2, new IntentFilter(DELIVERED));

		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
	}

	public void unregisterReceivers() {
		Log.d(TAG, "unregisterReceivers() - Unregistering Broadcast Receivers");
		activity.unregisterReceiver(receiver1);
		activity.unregisterReceiver(receiver2);
	}
}
