package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.Priority;
import com.br.svconsultoria.svnfcmanager.model.orm.PriorityDao;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;

import java.util.ArrayList;

public class SVPriorityDao {

	private static final String TAG = "SVPriorityDao";


	private static DaoSession session = ApplicationControl.daoSession;


	public static void insertOrUpdatePrioritys(final ArrayList<Priority> prioritys) {
		Log.d(TAG, "insertOrUpdateOccurrenceTypes() Starting...");
		session.runInTx(new Runnable() {

			@Override
			public void run() {

				PriorityDao prioritydao = session.getPriorityDao();
				prioritydao.insertOrReplaceInTx(prioritys);
			}
		});

		long xid = SVUpdateSequenceDao.getXid(UpdateSequenceMap.SRT_PRIORITYS).getxId();
		for (Priority object : prioritys) {
			if (xid < object.getXId()) {
				xid = object.getXId();
			}
		}
		SVUpdateSequenceDao.updateXId(new UpdateSequence(null, UpdateSequenceMap.SRT_PRIORITYS, xid));

		Log.d(TAG, "insertOrUpdateOccurrenceTypes() Finished!");
	}

	public static ArrayList<String> getPriorityLevelDescs() {
		ArrayList<String> descs = new ArrayList<String>();
		ArrayList<Priority> prioritys = getOccurrencePrioritys();
		for (Priority ot : prioritys) {
			descs.add(ot.getDescription());
		}
		return descs;
	}

	private static ArrayList<Priority> getOccurrencePrioritys() {
		ArrayList<Priority> list = (ArrayList<Priority>) session.getPriorityDao().queryBuilder().list();
		return list;
	}

	public static long getPriorityIdByDescription(String desc) {
		Log.d(TAG, "getPriorityIdByDescription()");
		Priority p = session.getPriorityDao().queryBuilder().where(PriorityDao.Properties.Description.eq(desc)).uniqueOrThrow();
		Log.d(TAG, "getPriorityIdByDescription()");
		return p.getPriorityId();
	}

}
