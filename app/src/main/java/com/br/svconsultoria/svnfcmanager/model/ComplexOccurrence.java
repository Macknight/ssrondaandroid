package com.br.svconsultoria.svnfcmanager.model;

/**
 * Created by rodrigo.poloni on 06/04/2018.
 */

public class ComplexOccurrence {




    private long id;



    String gerencia_value;
    String occurrence_place_value;
    String register_number_value;
    String code_value;
    String tipicidade_value;
    String graduation_value;
    //occurrence general info
    String emitente_value;
    String matricula_value;
    String summary_value;
    String description_value;
    String security_action_value;
    String public_organ_value;
    //involved #1
    String name1_value;
    String registration1_value;
    String cpf1_value;
    String identity1_value;
    String organ1_value;
    String cnh1_value;
    String category1_value;
    String validade1_value;
    String escolaridade1_value;
    String company1_value;
    String function1_value;
    String address1_value;
    String qualification1_value;
    String tel1_value;

    //involved #2
    String name2_value;
    String registration2_value;
    String identidade2_value;
    String orgao2_value;
    String cnh2_value;
    String categoria2_value;
    String validade2_value;
    String escolaridade2_value;
    String company2_value;
    String function2_value;
    String address2_value;
    String qualification2_value;
    String tel2_value;
    //car #1
    String plate1_value;
    String model1_value;
    String year1_value;
    String proprietary1_value;
    String car_company1_value;
    //car #2
    String plate2_value;
    String model2_value;
    String year2_value;
    String proprietary2_value;
    String car_company2_value;
    //misc
    String util_info_value;

    public ComplexOccurrence(String gerencia_value, String occurrence_place_value,
                             String register_number_value, String code_value, String tipicidade_value,
                             String graduation_value, String emitente_value, String matricula_value,
                             String summary_value, String description_value, String security_action_value,
                             String public_organ_value, String name1_value, String registration1_value,
                             String cpf1_value, String identity1_value, String organ1_value, String cnh1_value,
                             String category1_value, String validade1_value, String escolaridade1_value,
                             String company1_value, String function1_value, String address1_value,
                             String qualification1_value, String tel1_value, String name2_value, String registration2_value,
                             String identidade2_value, String orgao2_value, String cnh2_value, String categoria2_value,
                             String validade2_value, String escolaridade2_value, String company2_value,
                             String function2_value, String address2_value, String qualification2_value,
                             String tel2_value, String plate1_value, String model1_value, String year1_value,
                             String proprietary1_value, String car_company1_value, String plate2_value, String model2_value,
                             String year2_value, String proprietary2_value, String car_company2_value, String util_info_value) {
        this.gerencia_value = gerencia_value;
        this.occurrence_place_value = occurrence_place_value;
        this.register_number_value = register_number_value;
        this.code_value = code_value;
        this.tipicidade_value = tipicidade_value;
        this.graduation_value = graduation_value;
        this.emitente_value = emitente_value;
        this.matricula_value = matricula_value;
        this.summary_value = summary_value;
        this.description_value = description_value;
        this.security_action_value = security_action_value;
        this.public_organ_value = public_organ_value;
        this.name1_value = name1_value;
        this.registration1_value = registration1_value;
        this.cpf1_value = cpf1_value;
        this.identity1_value = identity1_value;
        this.organ1_value = organ1_value;
        this.cnh1_value = cnh1_value;
        this.category1_value = category1_value;
        this.validade1_value = validade1_value;
        this.escolaridade1_value = escolaridade1_value;
        this.company1_value = company1_value;
        this.function1_value = function1_value;
        this.address1_value = address1_value;
        this.qualification1_value = qualification1_value;
        this.tel1_value = tel1_value;
        this.name2_value = name2_value;
        this.registration2_value = registration2_value;
        this.identidade2_value = identidade2_value;
        this.orgao2_value = orgao2_value;
        this.cnh2_value = cnh2_value;
        this.categoria2_value = categoria2_value;
        this.validade2_value = validade2_value;
        this.escolaridade2_value = escolaridade2_value;
        this.company2_value = company2_value;
        this.function2_value = function2_value;
        this.address2_value = address2_value;
        this.qualification2_value = qualification2_value;
        this.tel2_value = tel2_value;
        this.plate1_value = plate1_value;
        this.model1_value = model1_value;
        this.year1_value = year1_value;
        this.proprietary1_value = proprietary1_value;
        this.car_company1_value = car_company1_value;
        this.plate2_value = plate2_value;
        this.model2_value = model2_value;
        this.year2_value = year2_value;
        this.proprietary2_value = proprietary2_value;
        this.car_company2_value = car_company2_value;
        this.util_info_value = util_info_value;
    }





    public ComplexOccurrence(){
    }

    public String getGerencia_value() {
        return gerencia_value;
    }

    public void setGerencia_value(String gerencia_value) {
        this.gerencia_value = gerencia_value;
    }

    public String getOccurrence_place_value() {
        return occurrence_place_value;
    }

    public void setOccurrence_place_value(String occurrence_place_value) {
        this.occurrence_place_value = occurrence_place_value;
    }

    public String getRegister_number_value() {
        return register_number_value;
    }

    public void setRegister_number_value(String register_number_value) {
        this.register_number_value = register_number_value;
    }

    public String getCode_value() {
        return code_value;
    }

    public void setCode_value(String code_value) {
        this.code_value = code_value;
    }

    public String getTipicidade_value() {
        return tipicidade_value;
    }

    public void setTipicidade_value(String tipicidade_value) {
        this.tipicidade_value = tipicidade_value;
    }

    public String getGraduation_value() {
        return graduation_value;
    }

    public void setGraduation_value(String graduation_value) {
        this.graduation_value = graduation_value;
    }

    public String getEmitente_value() {
        return emitente_value;
    }

    public void setEmitente_value(String emitente_value) {
        this.emitente_value = emitente_value;
    }

    public String getMatricula_value() {
        return matricula_value;
    }

    public void setMatricula_value(String matricula_value) {
        this.matricula_value = matricula_value;
    }

    public String getSummary_value() {
        return summary_value;
    }

    public void setSummary_value(String summary_value) {
        this.summary_value = summary_value;
    }

    public String getDescription_value() {
        return description_value;
    }

    public void setDescription_value(String description_value) {
        this.description_value = description_value;
    }

    public String getSecurity_action_value() {
        return security_action_value;
    }

    public void setSecurity_action_value(String security_action_value) {
        this.security_action_value = security_action_value;
    }

    public String getPublic_organ_value() {
        return public_organ_value;
    }

    public void setPublic_organ_value(String public_organ_value) {
        this.public_organ_value = public_organ_value;
    }

    public String getName1_value() {
        return name1_value;
    }

    public void setName1_value(String name1_value) {
        this.name1_value = name1_value;
    }

    public String getRegistration1_value() {
        return registration1_value;
    }

    public void setRegistration1_value(String registration1_value) {
        this.registration1_value = registration1_value;
    }

    public String getCpf1_value() {
        return cpf1_value;
    }

    public void setCpf1_value(String cpf1_value) {
        this.cpf1_value = cpf1_value;
    }

    public String getIdentity1_value() {
        return identity1_value;
    }

    public void setIdentity1_value(String identity1_value) {
        this.identity1_value = identity1_value;
    }

    public String getOrgan1_value() {
        return organ1_value;
    }

    public void setOrgan1_value(String organ1_value) {
        this.organ1_value = organ1_value;
    }

    public String getCnh1_value() {
        return cnh1_value;
    }

    public void setCnh1_value(String cnh1_value) {
        this.cnh1_value = cnh1_value;
    }

    public String getCategory1_value() {
        return category1_value;
    }

    public void setCategory1_value(String category1_value) {
        this.category1_value = category1_value;
    }

    public String getValidade1_value() {
        return validade1_value;
    }

    public void setValidade1_value(String validade1_value) {
        this.validade1_value = validade1_value;
    }

    public String getEscolaridade1_value() {
        return escolaridade1_value;
    }

    public void setEscolaridade1_value(String escolaridade1_value) {
        this.escolaridade1_value = escolaridade1_value;
    }

    public String getCompany1_value() {
        return company1_value;
    }

    public void setCompany1_value(String company1_value) {
        this.company1_value = company1_value;
    }

    public String getFunction1_value() {
        return function1_value;
    }

    public void setFunction1_value(String function1_value) {
        this.function1_value = function1_value;
    }

    public String getAddress1_value() {
        return address1_value;
    }

    public void setAddress1_value(String address1_value) {
        this.address1_value = address1_value;
    }

    public String getQualification1_value() {
        return qualification1_value;
    }

    public void setQualification1_value(String qualification1_value) {
        this.qualification1_value = qualification1_value;
    }

    public String getTel1_value() {
        return tel1_value;
    }

    public void setTel1_value(String tel1_value) {
        this.tel1_value = tel1_value;
    }

    public String getName2_value() {
        return name2_value;
    }

    public void setName2_value(String name2_value) {
        this.name2_value = name2_value;
    }

    public String getRegistration2_value() {
        return registration2_value;
    }

    public void setRegistration2_value(String registration2_value) {
        this.registration2_value = registration2_value;
    }

    public String getIdentidade2_value() {
        return identidade2_value;
    }

    public void setIdentidade2_value(String identidade2_value) {
        this.identidade2_value = identidade2_value;
    }

    public String getOrgao2_value() {
        return orgao2_value;
    }

    public void setOrgao2_value(String orgao2_value) {
        this.orgao2_value = orgao2_value;
    }

    public String getCnh2_value() {
        return cnh2_value;
    }

    public void setCnh2_value(String cnh2_value) {
        this.cnh2_value = cnh2_value;
    }

    public String getCategoria2_value() {
        return categoria2_value;
    }

    public void setCategoria2_value(String categoria2_value) {
        this.categoria2_value = categoria2_value;
    }

    public String getValidade2_value() {
        return validade2_value;
    }

    public void setValidade2_value(String validade2_value) {
        this.validade2_value = validade2_value;
    }

    public String getEscolaridade2_value() {
        return escolaridade2_value;
    }

    public void setEscolaridade2_value(String escolaridade2_value) {
        this.escolaridade2_value = escolaridade2_value;
    }

    public String getCompany2_value() {
        return company2_value;
    }

    public void setCompany2_value(String company2_value) {
        this.company2_value = company2_value;
    }

    public String getFunction2_value() {
        return function2_value;
    }

    public void setFunction2_value(String function2_value) {
        this.function2_value = function2_value;
    }

    public String getAddress2_value() {
        return address2_value;
    }

    public void setAddress2_value(String address2_value) {
        this.address2_value = address2_value;
    }

    public String getQualification2_value() {
        return qualification2_value;
    }

    public void setQualification2_value(String qualification2_value) {
        this.qualification2_value = qualification2_value;
    }

    public String getTel2_value() {
        return tel2_value;
    }

    public void setTel2_value(String tel2_value) {
        this.tel2_value = tel2_value;
    }

    public String getPlate1_value() {
        return plate1_value;
    }

    public void setPlate1_value(String plate1_value) {
        this.plate1_value = plate1_value;
    }

    public String getModel1_value() {
        return model1_value;
    }

    public void setModel1_value(String model1_value) {
        this.model1_value = model1_value;
    }

    public String getYear1_value() {
        return year1_value;
    }

    public void setYear1_value(String year1_value) {
        this.year1_value = year1_value;
    }

    public String getProprietary1_value() {
        return proprietary1_value;
    }

    public void setProprietary1_value(String proprietary1_value) {
        this.proprietary1_value = proprietary1_value;
    }

    public String getCar_company1_value() {
        return car_company1_value;
    }

    public void setCar_company1_value(String car_company1_value) {
        this.car_company1_value = car_company1_value;
    }

    public String getPlate2_value() {
        return plate2_value;
    }

    public void setPlate2_value(String plate2_value) {
        this.plate2_value = plate2_value;
    }

    public String getModel2_value() {
        return model2_value;
    }

    public void setModel2_value(String model2_value) {
        this.model2_value = model2_value;
    }

    public String getYear2_value() {
        return year2_value;
    }

    public void setYear2_value(String year2_value) {
        this.year2_value = year2_value;
    }

    public String getProprietary2_value() {
        return proprietary2_value;
    }

    public void setProprietary2_value(String proprietary2_value) {
        this.proprietary2_value = proprietary2_value;
    }

    public String getCar_company2_value() {
        return car_company2_value;
    }

    public void setCar_company2_value(String car_company2_value) {
        this.car_company2_value = car_company2_value;
    }

    public String getUtil_info_value() {
        return util_info_value;
    }

    public void setUtil_info_value(String util_info_value) {
        this.util_info_value = util_info_value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
