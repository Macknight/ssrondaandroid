package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteSchedule;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteScheduleDao;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;

import java.util.Date;
import java.util.List;

public class SVRouteScheduleDao {


	private static DaoSession session = ApplicationControl.daoSession;

	public static void insertOrUpdateRouteSchedules(final List<RouteSchedule> list) {
		Log.d("SVRouteScheduleDao", "insertOrUpdateRouteSchedules() Starting...");
		session.runInTx(new Runnable() {

			@Override
			public void run() {
				RouteScheduleDao routescheduledao = session.getRouteScheduleDao();
				routescheduledao.insertOrReplaceInTx(list);
			}
		});

		long xid = SVUpdateSequenceDao.getXid(UpdateSequenceMap.SRT_ROUTE_SCHEDULE).getxId();
		for (RouteSchedule c : list) {
			if (xid < c.getXId()) {
				xid = c.getXId();
			}
		}
		SVUpdateSequenceDao.updateXId(new UpdateSequence(null, UpdateSequenceMap.SRT_ROUTE_SCHEDULE, xid));

		Log.d("SVRouteScheduleDao", "insertOrUpdateRouteSchedules() Finished!");
	}

	public static List<RouteSchedule> getRouteScheduleByDate(Date actualDt) {
		Log.d("SVRouteScheduleDao", "getRouteScheduleByDate() - actualDt = " + actualDt);

		List<RouteSchedule> list = null;

		RouteScheduleDao routescheduledao = session.getRouteScheduleDao();
		// list = routescheduledao.queryBuilder().where(Properties.ScheduleDt.eq(actualDt)).where(Properties.StatusId.eq(1)).orderAsc(Properties.InitHr).list();
		list =
				routescheduledao
					.queryBuilder()
					.where(RouteScheduleDao.Properties.StatusId.eq(1))
					.orderAsc(RouteScheduleDao.Properties.InitHr)
					.list();
		Log.d("SVRouteScheduleDao", "getRouteScheduleByDate() Finished!");
		return list;
	}

	public static RouteSchedule getRouteScheduleById(long routeScheduleId) {
		Log.d("SVRouteScheduleDao", "getRouteScheduleById() - routeScheduleId = " + routeScheduleId);

		RouteSchedule routeSchedule = null;

		RouteScheduleDao routescheduledao = session.getRouteScheduleDao();
		routeSchedule =
				routescheduledao
					.queryBuilder()
					.where(RouteScheduleDao.Properties.RouteScheduleId.eq(routeScheduleId))
					.where(RouteScheduleDao.Properties.StatusId.eq(1))
					.unique();
		Log.d("SVRouteScheduleDao", "getRouteScheduleById() Finished!");
		return routeSchedule;
	}

	public static void insertOrReplaceRouteSchedule(RouteSchedule routeSchedule) {
		Log.d("SVRouteScheduleDao", "insertOrReplaceRouteSchedule() Starting...");
		RouteScheduleDao routescheduledao = session.getRouteScheduleDao();
		routescheduledao.insertOrReplace(routeSchedule);
		Log.d("SVRouteScheduleDao", "insertOrReplaceRouteSchedule() Finished!");
	}
}
