package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.SysParam;
import com.br.svconsultoria.svnfcmanager.model.orm.SysParamDao;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;

import org.greenrobot.greendao.DaoException;

import java.util.ArrayList;
import java.util.List;


public class SVSysParamsDao {

	private static final String TAG = "SVSysParamsDao";


	private static DaoSession session = ApplicationControl.daoSession;

	public static void insertOrUpdateSysParams(final List<SysParam> sysParams) {
		Log.d(TAG, "insertOrUpdateSysParams() Starting...");

		final List<SysParam> lstNewParams = new ArrayList<>();
		for (SysParam c : sysParams) {
			SysParam oldNamed = getSysParamByName(c.getParamDesc());
			if(oldNamed!=null) {
				c.setSysParamId(oldNamed.getSysParamId());
			}
		}

		session.runInTx(new Runnable() {

			@Override
			public void run() {
				SysParamDao sysparamdao = session.getSysParamDao();
				sysparamdao.insertOrReplaceInTx(sysParams);
			}
		});

		long xid = SVUpdateSequenceDao.getXid(UpdateSequenceMap.SRT_SYS_PARAMS).getxId();
		for (SysParam c : sysParams) {
			Log.d(TAG, "insertOrUpdateSysParams() " + c.getParamDesc() + " " + c.getValue());
			if (xid < c.getXId()) {
				xid = c.getXId();
			}
		}
		SVUpdateSequenceDao.updateXId(new UpdateSequence(null, UpdateSequenceMap.SRT_SYS_PARAMS, xid));

		Log.d(TAG, "insertOrUpdateSysParams() Finished!");
	}

	public static void insertIfNotExist(final List<SysParam> sysParams) {
		Log.d(TAG, "insertOrUpdateSysParams() Starting...");
		final List<SysParam> lstNewParams = new ArrayList<>();
		for (SysParam c : sysParams) {
			if(getSysParamByName(c.getParamDesc())==null) {
				lstNewParams.add(c);
			}
		}
		session.runInTx(new Runnable() {

			@Override
			public void run() {
				SysParamDao sysparamdao = session.getSysParamDao();
				sysparamdao.insertOrReplaceInTx(lstNewParams);
			}
		});

		long xid = SVUpdateSequenceDao.getXid(UpdateSequenceMap.SRT_SYS_PARAMS).getxId();
		for (SysParam c : lstNewParams){
				Log.d(TAG, "insertOrUpdateSysParams() " + c.getParamDesc() + " " + c.getValue());
				if (xid < c.getXId()) {
					xid = c.getXId();
				}
		}
		SVUpdateSequenceDao.updateXId(new UpdateSequence(null, UpdateSequenceMap.SRT_SYS_PARAMS, xid));

		Log.d(TAG, "insertOrUpdateSysParams() Finished!");
	}

	public static SysParam getSysParamByName(String name) {
		Log.d(TAG, "getSysParamByName() - ParamName: " + name + ".");

		SysParamDao sysparamdao = session.getSysParamDao();

		SysParam param = null;

		try {
			param = sysparamdao.queryBuilder().where(SysParamDao.Properties.Param.eq(name)).uniqueOrThrow();
		} catch (NullPointerException e) {
			Log.d(TAG, "getSysParamByName() - SystemParam not found. Error: " + e.getLocalizedMessage());
			return null;
		} catch (DaoException e) {
			Log.d(TAG, "getSysParamByName() - SystemParam " + name + " not found. Error: " + e.getLocalizedMessage());
			return null;
		}
		Log.d(TAG, "getSysParamByName() Finished! " + param.getValue());
		return param;
	}


	public static boolean isVehicleCheckEnabled() {
		SysParam vehicle =  getSysParamByName(SystemParamsMap.VEHICLE_CHECK);
		return vehicle != null && Boolean.parseBoolean(vehicle.getValue());
	}

	public static boolean isFreeRouteCheckRepetitionEnabled() {
		SysParam param =  getSysParamByName(SystemParamsMap.FREE_ROUT_CHEKPOINT_REPETITION);
		return param != null && Boolean.parseBoolean(param.getValue());
	}
}
