package com.br.svconsultoria.svnfcmanager.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.svconsultoria.svnfcmanager.R;
import com.br.svconsultoria.svnfcmanager.dao.SVSysParamsDao;
import com.br.svconsultoria.svnfcmanager.model.SystemCheckpoint;

import java.util.List;

/**
 * Created by bruno.hernandes on 05/09/2016.
 */
public class CheckpointAdapter extends ArrayAdapter<SystemCheckpoint> {

    private LayoutInflater inflater;

    static class ViewHolder {
        public ImageView row_check;
        public TextView row_name;
        public TextView row_repetition;
        public ImageButton row_skipButton;
    }

    private boolean repeat_check_enable = SVSysParamsDao.isFreeRouteCheckRepetitionEnabled();

    public CheckpointAdapter(Context context, List<SystemCheckpoint> checkpoints) {
        super(context, R.layout.row_free_checkpoint, checkpoints);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            // inflates a new cell
            convertView = inflater.inflate(R.layout.row_free_checkpoint, null);
            holder = new ViewHolder();

            holder.row_check = (ImageView) convertView.findViewById(R.id.row_checkpoint_check);
            holder.row_skipButton = (ImageButton) convertView.findViewById(R.id.row_checkpoint_skip);
            holder.row_name = (TextView) convertView.findViewById(R.id.row_checkpoint_name);
            holder.row_repetition = (TextView) convertView.findViewById(R.id.row_checkpoint_repetition);

            convertView.setTag(holder);

            convertView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Log.d("CHECKADAPTER", "holderLongClick");
                    return true;
                }
            });
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final SystemCheckpoint ck = getItem(position);
        holder.row_skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSkipClick(ck);
            }
        });
        holder.row_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNameClick(ck);
            }
        });

        assert ck != null;
        if(repeat_check_enable){
            holder.row_skipButton.setVisibility(View.GONE);
            holder.row_repetition.setText(String.valueOf(ck.getNumberOfChecks()));
            if (ck.getReadTimeMillis() > 0) {
                holder.row_check.setImageResource(android.R.drawable.checkbox_on_background);
            } else {
                holder.row_check.setImageResource(android.R.drawable.checkbox_off_background);
            }

        }else {
            holder.row_repetition.setVisibility(View.GONE);
            if (ck.getReadTimeMillis() > 0) {
                holder.row_check.setImageResource(android.R.drawable.checkbox_on_background);
                holder.row_skipButton.setEnabled(false);
                holder.row_skipButton.setImageResource(R.drawable.skip_checkpoint_icon_off);
                convertView.setAlpha(0.5f);
            } else {
                holder.row_check.setImageResource(android.R.drawable.checkbox_off_background);
                holder.row_skipButton.setEnabled(true);
                holder.row_skipButton.setImageResource(R.drawable.skip_checkpoint_icon);
                convertView.setAlpha(1f);
            }
        }
        holder.row_name.setText(ck.getDescription());
        Typeface typeface1 = Typeface.createFromAsset(getContext().getAssets(), "tt0246m_.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getContext().getAssets(), "tt0247m_.ttf");

        holder.row_name.setTypeface(ck.isMandatory() ? typeface2 : typeface1);


        return convertView;
    }

    public void onSkipClick(SystemCheckpoint ck) {

    }
    public void onNameClick(SystemCheckpoint ck) {

    }

}
