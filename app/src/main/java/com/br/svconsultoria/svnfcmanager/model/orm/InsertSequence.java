package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Entity mapped to table SRTM_INSERT_SEQUENCE.
 */
@Entity
public class InsertSequence {
    @Id(autoincrement = true)
    private Long id; //greendao entities must have a long or Long property as their primary key.
    @Index(unique = true)
    private String tableName;
    private long nextValue;
    @Generated(hash = 1728941205)
    public InsertSequence(Long id, String tableName, long nextValue) {
        this.id = id;
        this.tableName = tableName;
        this.nextValue = nextValue;
    }
    @Generated(hash = 362295606)
    public InsertSequence() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getTableName() {
        return this.tableName;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    public long getNextValue() {
        return this.nextValue;
    }
    public void setNextValue(long nextValue) {
        this.nextValue = nextValue;
    }
    public void setId(Long id) {
        this.id = id;
    }

}
