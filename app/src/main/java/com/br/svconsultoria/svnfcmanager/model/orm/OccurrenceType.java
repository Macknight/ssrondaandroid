package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Entity mapped to table SRTM_OCCURRENCE_TYPES.
 */
@Entity
public class OccurrenceType {

    @Id
    private Long occurrenceTypeId;
    private long statusId;
    private Long xId;
    private String description;
    private long customerId;
    @Generated(hash = 954947357)
    public OccurrenceType(Long occurrenceTypeId, long statusId, Long xId,
            String description, long customerId) {
        this.occurrenceTypeId = occurrenceTypeId;
        this.statusId = statusId;
        this.xId = xId;
        this.description = description;
        this.customerId = customerId;
    }
    @Generated(hash = 1214578731)
    public OccurrenceType() {
    }
    public Long getOccurrenceTypeId() {
        return this.occurrenceTypeId;
    }
    public void setOccurrenceTypeId(Long occurrenceTypeId) {
        this.occurrenceTypeId = occurrenceTypeId;
    }
    public long getStatusId() {
        return this.statusId;
    }
    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }
    public Long getXId() {
        return this.xId;
    }
    public void setXId(Long xId) {
        this.xId = xId;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public long getCustomerId() {
        return this.customerId;
    }
    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

}
