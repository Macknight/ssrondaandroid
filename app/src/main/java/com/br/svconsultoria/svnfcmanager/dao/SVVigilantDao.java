package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;
import com.br.svconsultoria.svnfcmanager.model.orm.Vigilant;
import com.br.svconsultoria.svnfcmanager.model.orm.VigilantDao;

import java.util.List;

public class SVVigilantDao {


	private static DaoSession session = ApplicationControl.daoSession;

	public static void insertOrUpdateVigilants(final List<Vigilant> list) {
		Log.d("SVVigilantDao", "insertOrUpdateVigilants() Starting...");
		session.runInTx(new Runnable() {

			@Override
			public void run() {
				VigilantDao vigilantdao = session.getVigilantDao();
				vigilantdao.insertOrReplaceInTx(list);
			}
		});

		long xid = SVUpdateSequenceDao.getXid(UpdateSequenceMap.SRT_VIGILANTS).getxId();
		for (Vigilant object : list) {
			if (xid < object.getXId()) {
				xid = object.getXId();
			}
		}
		SVUpdateSequenceDao.updateXId(new UpdateSequence(null, UpdateSequenceMap.SRT_VIGILANTS, xid));

		Log.d("SVVigilantDao", "insertOrUpdateVigilants() Finished!");
	}

	public static boolean isValidByTag(long tagId) {
		Log.d("SVVigilantDao", "isValidByTag()");
		VigilantDao vigilantDao = session.getVigilantDao();

		long count = 0;

		try {
			count = vigilantDao.queryBuilder().where(VigilantDao.Properties.LogonTagId.eq(tagId)).where(VigilantDao.Properties.StatusId.eq(1)).count();
		} catch (Exception e) {
			e.printStackTrace();
			Log.d("SVVigilantDao", "isValidByTag() = true (Exception)");
			return false;
		}

		if (count > 0) {
			Log.d("SVVigilantDao", "isValidByTag() = true");
			return true;
		} else {
			Log.d("SVVigilantDao", "isValidByTag() = false");
			return false;
		}
	}

	public static boolean exists(Vigilant vigilant) {
		Log.d("SVVigilantDao", "exists()");
		VigilantDao vigilantDao = session.getVigilantDao();
		long count = vigilantDao.queryBuilder().where(VigilantDao.Properties.VigilantId.eq(vigilant.getVigilantId())).where(VigilantDao.Properties.StatusId.eq(1)).count();

		if (count > 0) {
			Log.d("SVVigilantDao", "exists() = true");
			return true;
		} else {
			Log.d("SVVigilantDao", "exists() = false");
			return false;
		}
	}

	public static Vigilant getVigilantByTagId(long tagId) {
		Log.d("SVVigilantDao", "getVigilantByTagId()");
		VigilantDao vigilantDao = session.getVigilantDao();
		Vigilant vigilant = vigilantDao.queryBuilder().where(VigilantDao.Properties.LogonTagId.eq(tagId)).where(VigilantDao.Properties.StatusId.eq(1)).uniqueOrThrow();
		Log.d("SVVigilantDao", "getVigilantByTagId() Finished!");
		return vigilant;
	}

	public static void insertOrReplaceVigilant(Vigilant vigilant) {
		Log.d("SVVigilantDao", "insertOrReplaceVigilant()");
		final VigilantDao vigilantDao = session.getVigilantDao();
		vigilantDao.insertOrReplace(vigilant);
		Log.d("SVVigilantDao", "insertOrReplaceVigilant() Finished!");
	}
}
