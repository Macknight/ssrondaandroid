package com.br.svconsultoria.svnfcmanager.tools;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;

public class HandlerThread {
	
	private Context ctx = null;
	public static boolean TOASTER_ALIVE = true;
	
	public HandlerThread(Context context) {
		ctx = context;
	}
	
	private Handler handler = new Handler();
	
	private Runnable rToast = new Runnable() {
		@Override
		public void run() {
			if (ApplicationControl.message.length() > 0) {
				try {
					Toast.makeText(ctx, ApplicationControl.message, Toast.LENGTH_LONG).show();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				ApplicationControl.message = "";
			}
		};
	};
	
	private Thread thToaster = new Thread() {
		@Override
		public void run() {
			while (TOASTER_ALIVE) {
				if (ApplicationControl.message.length() > 0) {
					handler.post(rToast);
				} else {
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	};
	
	public void start() {
		Log.d("HandlerThread", "Message Handler Thread has been started successfully!");
		thToaster.start();
	}
}
