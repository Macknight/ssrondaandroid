package com.br.svconsultoria.svnfcmanager.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.PanicConfirmationActivity;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVSysParamsDao;
import com.br.svconsultoria.svnfcmanager.gps.GpsFunctions;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.model.orm.SysParam;
import com.br.svconsultoria.svnfcmanager.tools.EventBuilder;

/**
 * <i>Classe que implementa o alerta silencioso do dispositivo</i> <br />
 * <b>Atentar à sensibilidade do acelerómetro pois essa calibragem
 * deve ser feita em relação ao aparelho especificamente</b>
 */
public class SilentAlertService extends Service implements SensorEventListener {

    private static final String TAG = "SilentAlertService";

    PowerManager.WakeLock wl;
    PowerManager pm;
    boolean isFallSoundAllowed = false;


    private int shakes = 0;
    private long lastChange = 0;
    public static long shakeTime = 0;
    public static boolean bShakeActivated = false;
    private int panicShakeResetTime = 0;
    public static boolean bPanicPosition = false;

    private boolean top = true;
    private boolean falling = false;
    private long fallTime = 0;
    private long startFall = 0;
    private long lPanicPosition = 0;

    public static GpsFunctions gps = null;
    SensorManager mSensorEventManager = null;
    Sensor mSensor;

    // BroadcastReceiver for handling ACTION_SCREEN_OFF.
    public BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Check action just to be on the safe side.
            Log.d(TAG, "ACTION: " + intent.getAction());
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {

                Log.d(TAG, "trying re-registration");
                // Unregisters the listener and registers it again.
                mSensorEventManager.unregisterListener(SilentAlertService.this);
                mSensorEventManager.registerListener(SilentAlertService.this, mSensor, SensorManager.SENSOR_DELAY_FASTEST);
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate() - registering for shake");

        isFallSoundAllowed = SVMobileParamsDao.isFallSoundAllowed();

        // Obtain a reference to system-wide sensor event manager.
        mSensorEventManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);

        // Get the default sensor for accel
        mSensor = mSensorEventManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        // Register for events.
        mSensorEventManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);

        // Register our receiver for the ACTION_SCREEN_OFF action. This will
        // make our receiver
        // code be called whenever the phone enters standby mode.
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mReceiver, filter);

        pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, TAG);
        Log.d(TAG, "onCreate() - wake lock acquired");
        wl.acquire();

        // Starting Gps systems
        gps = new GpsFunctions(this);
        gps.initializeGpsServices(this);

        // if panic shake trigger time parameter absent OR equals to zero indicates that panic activation by device position is disabled
        SysParam sp = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_SHAKE_TRIGGER_TIME);
        panicShakeResetTime = sp == null ? 0 : Integer.parseInt(sp.getValue());
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        // Unregister our receiver.
        unregisterReceiver(mReceiver);

        // Unregister from SensorManager.
        mSensorEventManager.unregisterListener(this);
        wl.release();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand() startId: " + startId);
        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't need a IBinder interface.
        return null;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // not used right now
    }

    // Used to decide if it is a shake
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
            return;
        }
        // if panic shake trigger time parameter absent OR equals to zero indicates that panic activation by device position is disabled
        if (panicShakeResetTime == 0) {
            return;
        }


//		if (event.values[0] > 9 || event.values[0] < -9)
//			Log.d(TAG, "onSensorChanged() - celular deitado de lado (esquerda ou direita)");
//		else if (event.values[2] > 9 || event.values[2] < -9)
//			Log.d(TAG, "onSensorChanged() - celular deitado de frente ou costas");
//		else if (event.values[1] < -9)
//			Log.d(TAG, "onSensorChanged() - celular de cabeça pra baixo");


        if (event.values[1] < -9) {

            if (lastChange == 0) {
                lastChange = System.currentTimeMillis();
                //bPanicPosition = true;
            }

            // Panic shake trigger time
//			Log.d(TAG, "lastChange " + lastChange + " panicShakeTriggerTime " + SVSysParamsDao.getSysParamByName(SysParamsMap.PANIC_SHAKE_TRIGGER_TIME).getValue());


            //TODO parametrizar tempo do posicionamento de panico
            if ((System.currentTimeMillis() - lastChange) > /*5000*/
                    panicShakeResetTime
                    && bPanicPosition) {
                Vibrator vibrator;
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(1000);
                Log.d(TAG, "onSensorChanged() - PANIC ACTIONED!");
                bPanicPosition = false;

                Intent intent = new Intent(getApplicationContext(), PanicConfirmationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        } else if (event.values[1] >= -3) {
            lastChange = 0;
            bPanicPosition = false;
        }
        //
        // if (!bShakeActivated) {
        // if ((System.currentTimeMillis() - lastChange) > 700) {
        // if (shakes > 0) {
        // Log.d(TAG, "onSensorChanged() - RESET!");
        // shakes = 0;
        // }
        // }
        //
        // double valTest = event.values[2];
        // if (valTest > 13) {
        // if (top) {
        // Log.d(TAG, "onSensorChanged() - TOP shakes: " + shakes + " valTest: "
        // + valTest);
        // lastChange = System.currentTimeMillis();
        // shakes++;
        // top = false;
        // }
        // }
        // if (valTest < -13) {
        // if (!top) {
        // Log.d(TAG, "onSensorChanged() - BOTTOM shakes: " + shakes +
        // " valTest: " + valTest);
        // lastChange = System.currentTimeMillis();
        // shakes++;
        // top = true;
        // }
        // }
        //
        // if (shakes > 4) {
        //
        // if (SVSystemParamsDao.isPanicSoundAllowed()) {
        // Uri notification =
        // RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        // Ringtone r = RingtoneManager.getRingtone(getApplicationContext(),
        // notification);
        // r.play();
        // }

        // Vibrator vibrator;
        // vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // vibrator.vibrate(1000);
        // Log.d(TAG, "onSensorChanged() - SHAKE!");
        // bShakeActivated = true;
        // shakeTime = System.currentTimeMillis();
        // shakes = 0;
        // Intent intent = new Intent(getApplicationContext(),
        // PanicConfirmationActivity.class);
        // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // startActivity(intent);
        // }
        // } else {
        // if (System.currentTimeMillis() > (shakeTime +
        // panicShakeResetTime)) {
        // shakeTime = 0;
        // bShakeActivated = false;
        // }
        // }

        double val = Math.sqrt(Math.pow(event.values[0], 2) + Math.pow(event.values[1], 2) + Math.pow(event.values[2], 2));

//		if (val > 11) {
//			Log.d(TAG, "onSensorChanged() - VAL = " + val);
//			Log.d(TAG, "onSensorChanged() - X = " + event.values[0] + " Y = " + event.values[1] + " Z = " + event.values[2]);
//
//		}
        if (val < 4) {
            if (!falling) {
                Log.d(TAG, "onSensorChanged() - falling = true");
                falling = true;
                startFall = System.currentTimeMillis();
            }
        } else {
            if (falling) {
                falling = false;
                fallTime = System.currentTimeMillis() - startFall;
                if (fallTime > 400) {
                    Log.d(TAG, "onSensorChanged() - DROP!!!");
                    EventBuilder.createPhoneFallEvent();

                    prepareAndExecuteSyncTask();

                    if (isFallSoundAllowed) {
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                        r.play();
                    }
                }
            }
        }
    }

    private void prepareAndExecuteSyncTask() {
        Log.d(TAG, "prepareAndExecuteSyncTask()");
        Toast.makeText(this, "Enviando para o servidor...", Toast.LENGTH_LONG).show();
        ApplicationControl.startSend();
    }
}
