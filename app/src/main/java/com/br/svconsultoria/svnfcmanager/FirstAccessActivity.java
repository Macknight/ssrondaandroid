package com.br.svconsultoria.svnfcmanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.br.svconsultoria.svnfcmanager.cipher.Crypt;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVSysParamsDao;
import com.br.svconsultoria.svnfcmanager.http.HttpAuthenticate;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.model.orm.MobileParam;
import com.br.svconsultoria.svnfcmanager.model.orm.SysParam;
import com.br.svconsultoria.svnfcmanager.util.SVUtil;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FirstAccessActivity extends Activity {

    private static final String TAG = "FirstAccessActivity";

    private EditText host = null;
    private EditText username = null;
    private EditText password = null;
    private EditText repeatPassword = null;
    private TextView showImei = null;
    private CheckBox fallSound = null;
    private CheckBox panicSound = null;
    private CheckBox qrCode = null;
    private TextClock dc = null;
    final Context context = this;
    EditText panicShakeTriggerTimeEditText = null;

    private String sHost = "";
    private String sUserName = "";
    private String sPassword = "";
    private String sRepeatPassword = "";
    private boolean bFallSoundAllow = false;
    private String sFallSoundAllow = "";
    private boolean bPanicSoundAllow = false;
    private String sPanicSoundAllow = "";
    private boolean bQrCodeAllow = false;
    private String sQrCodeAllow = "";
    private boolean bAdminModeAllow = false;
    private String sAdminModeAllow = "";

    private boolean bContinue = false;

    private Button save = null;
    private Button checkpoint = null;

    private String sMessage = "";

    private CheckBox cbIsAdminMode = null;

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        SVUtil.touch(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_first_access);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "tt0246m_.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
        ((TextView) findViewById(R.id.title)).setTypeface(typeface2);
        ((TextView) findViewById(R.id.label2)).setTypeface(typeface);
        ((EditText) findViewById(R.id.field2)).setTypeface(typeface);
        ((TextView) findViewById(R.id.label4)).setTypeface(typeface);
        ((EditText) findViewById(R.id.field4)).setTypeface(typeface);
        ((TextView) findViewById(R.id.label5)).setTypeface(typeface);
        ((EditText) findViewById(R.id.field5)).setTypeface(typeface);
        ((TextView) findViewById(R.id.label6)).setTypeface(typeface);
        ((Button) findViewById(R.id.button1)).setTypeface(typeface2);
        ((Button) findViewById(R.id.button2)).setTypeface(typeface2);
        ((TextClock) findViewById(R.id.digitalClock1)).setTypeface(typeface);

        showImei = (TextView) findViewById(R.id.showImei);
        ((TextView) findViewById(R.id.showImei)).setTypeface(typeface);
        showImei.setText("IMEI: " + ApplicationControl.TELEPHONE_IMEI);

        host = (EditText) findViewById(R.id.field2);
        ((EditText) findViewById(R.id.field2)).setTypeface(typeface);

        Log.d(TAG, "Looking for old Host Address...");
        String thisHostAddress = SVMobileParamsDao.getHostParam();
        if (thisHostAddress.equals("") || (thisHostAddress == null)) {
            Log.d(TAG, "Old Host Address not found...");
        } else {
            host.setText(thisHostAddress);
        }

        host.setText(SVMobileParamsDao.getHostParam());
        username = (EditText) findViewById(R.id.field4);
        ((EditText) findViewById(R.id.field4)).setTypeface(typeface);

        Log.d(TAG, "Looking for old UserName...");
        String thisUserName = SVMobileParamsDao.getUserNameLogin();
        if (thisUserName.equals("") || (thisUserName == null)) {
            Log.d(TAG, "Old UserName not found...");
        } else {
            username.setText(thisUserName);
        }

        password = (EditText) findViewById(R.id.field5);
        ((EditText) findViewById(R.id.field5)).setTypeface(typeface);
        repeatPassword = (EditText) findViewById(R.id.field6);
        ((EditText) findViewById(R.id.field6)).setTypeface(typeface);

        Log.d(TAG, "Looking for old Password...");
        String thisPassword = SVMobileParamsDao.getUserPassword();
        if (thisPassword.equals("") || (thisPassword == null)) {
            Log.d(TAG, "Old Password not found...");
        } else {
            password.setText(thisUserName);
        }

        // CUSTOM jorge.lucas 20072015 panic shake trigger time
        panicShakeTriggerTimeEditText = (EditText) findViewById(R.id.panicShakeTriggerTimeEditText);
        Log.d(TAG, "Looking for old Password...");
        SysParam sp = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_SHAKE_TRIGGER_TIME);
        String panicShakeTriggerTime = sp==null?"": sp.getValue();
        if ((panicShakeTriggerTime == null) || panicShakeTriggerTime.equals("")) {
            Log.d(TAG, "Panic shake trigger time not found...");
            panicShakeTriggerTimeEditText.setText("5");
        } else {
            panicShakeTriggerTimeEditText.setText(String.valueOf(Integer.valueOf(panicShakeTriggerTime) / 1000));
        }
        // END CUSTOM


        save = (Button) findViewById(R.id.button1);
        save.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (setFields() && verifyFields()) {
                    if (saveData()) {

                        // CUSTOM jorge.lucas 22052015 first sync activity
                        if (ApplicationControl.isFirstRun()) {
                            Toast.makeText(getApplicationContext(), "Aguardando resposta inicial do servidor...", Toast.LENGTH_LONG).show();
                            AsyncTask<Void, Void, String> task = new HttpAuthenticate();
                            ((HttpAuthenticate) task).setIsFirstSyncCheck(true, getApplicationContext());
                            task.execute();

                        }
                        // END CUSTOM
                        else {
                            Intent ok = new Intent(getBaseContext(), MainActivity.class);
                            startActivity(ok);
                        }
                    }
                } else {
                    buildAlert();
                }
            }
        });

        checkpoint = (Button) findViewById(R.id.button2);
        checkpoint.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent ok = new Intent(getBaseContext(), CheckpointUpdateActivity.class);
                startActivity(ok);

            }
        });

        fallSound = (CheckBox) findViewById(R.id.fallSound);
        if (SVMobileParamsDao.isFallSoundAllowed()) {
            fallSound.setChecked(true);
        }
        panicSound = (CheckBox) findViewById(R.id.panicSound);
        if (SVMobileParamsDao.isPanicSoundAllowed()) {
            panicSound.setChecked(true);
        }
        qrCode = (CheckBox) findViewById(R.id.qrCode);
        if (SVMobileParamsDao.isQrCodeAllowed()) {
            qrCode.setChecked(true);
        }
        cbIsAdminMode = (CheckBox) findViewById(R.id.cbIsAdminMode);
        if (SVMobileParamsDao.isAdminModeAllowed()) {
            cbIsAdminMode.setChecked(true);
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
    }

    private boolean verifyFields() {
        sMessage = "Todos os campos devem ser preenchidos";
        if (sHost.equals(""))
            return false;
        if (sUserName.equals(""))
            return false;
        if (sPassword.equals(""))
            return false;
        if (sRepeatPassword.equals(""))
            return false;
        if (!sRepeatPassword.equals(sPassword)) {
            sMessage = "As senhas digitadas não são iguais";
            return false;
        }
        if (!bQrCodeAllow) {
            Log.d(TAG, "Qr Code Disabled.");
            try {
                NfcAdapter mAdapter = NfcAdapter.getDefaultAdapter(this);
                if (!mAdapter.isEnabled()) {
                    sMessage = "O NFC não está ligado. Ative-o e tente novamente.";
                    return false;
                }
            } catch (Exception e) {
                sMessage = "Este aparelho não suporta NFC. Ative a função QR Code.";
                return false;
            }
        }

        return true;
    }

    private boolean setFields() {
        sHost = host.getText().toString();
        sUserName = username.getText().toString();
        sPassword = password.getText().toString();
        sRepeatPassword = repeatPassword.getText().toString();
        bFallSoundAllow = fallSound.isChecked();
        if (bFallSoundAllow) {
            sFallSoundAllow = "true";
        } else {
            sFallSoundAllow = "false";
        }
        bPanicSoundAllow = panicSound.isChecked();
        if (bPanicSoundAllow) {
            sPanicSoundAllow = "true";
        } else {
            sPanicSoundAllow = "false";
        }
        bQrCodeAllow = qrCode.isChecked();
        if (bQrCodeAllow) {
            sQrCodeAllow = "true";
        } else {
            sQrCodeAllow = "false";
        }
        bAdminModeAllow = cbIsAdminMode.isChecked();
        if (bAdminModeAllow) {
            sAdminModeAllow = "true";
        } else {
            sAdminModeAllow = "false";
        }

        return true;
    }

    private boolean saveData() {
        stopService(ApplicationControl.alertServiceIntent);
        ProgressDialog dialog = ProgressDialog.show(this, "Enviando", "Salvando dados");
        List<MobileParam> params = new LinkedList<MobileParam>();
        params.add(new MobileParam(null, SystemParamsMap.HOST_ADDRESS, sHost));
        params.add(new MobileParam(null, SystemParamsMap.ADMIN_LOGIN_NAME, sUserName));
        params.add(new MobileParam(null, SystemParamsMap.ADMIN_PASSWORD, Crypt.passwordCrypt(sPassword)));
        params.add(new MobileParam(null, SystemParamsMap.IS_CONFIGURED, "true"));
        params.add(new MobileParam(null, SystemParamsMap.FALL_SOUND_ALLOWED, sFallSoundAllow));
        params.add(new MobileParam(null, SystemParamsMap.PANIC_SOUND_ALLOWED, sPanicSoundAllow));
        params.add(new MobileParam(null, SystemParamsMap.QR_CODE_ALLOWED, sQrCodeAllow));
        params.add(new MobileParam(null, SystemParamsMap.ADMIN_MODE_ALLOWED, sAdminModeAllow));
        bContinue = SVMobileParamsDao.insertMobileParams(params);

        // sys param
        List<SysParam> sysParams = new ArrayList<>();

        Integer newPanicShakeTime = Integer.valueOf(panicShakeTriggerTimeEditText.getText().toString()) * 1000;
        SysParam panicShakeTriggerTimeSysParam = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_SHAKE_TRIGGER_TIME);
        if(panicShakeTriggerTimeSysParam==null){
            panicShakeTriggerTimeSysParam = new SysParam(null,0,1,SystemParamsMap.PANIC_SHAKE_TRIGGER_TIME,"","0");
        }
        panicShakeTriggerTimeSysParam.setValue(newPanicShakeTime.toString());
        sysParams.add(panicShakeTriggerTimeSysParam);
        SVSysParamsDao.insertOrUpdateSysParams(sysParams);
        Log.d(TAG, "Saving new panic shake trigger time: " + panicShakeTriggerTimeSysParam.getValue());
        // end sys param


        while (!bContinue) {
            try {
                // Simulate network access.
                Thread.sleep(200);
                Log.d(TAG, "saveData() - Sleeping...");
            } catch (InterruptedException e) {
                return false;
            }
        }
        startService(ApplicationControl.alertServiceIntent);
        dialog.dismiss();
        return true;
    }

    private void buildAlert() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(sMessage).setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int id) {
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.first_access, menu);
        return true;
    }
}