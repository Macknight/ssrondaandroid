package com.br.svconsultoria.svnfcmanager.http.sync;

import android.os.AsyncTask;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.http.HttpAuthenticate;
import com.br.svconsultoria.svnfcmanager.model.Response;
import com.google.gson.Gson;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.params.HttpConnectionParams;

import java.io.IOException;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class DatabaseUpdaterThread extends AsyncTask<Void, Void, Void> {
	private String TAG = "DatabaseUpdaterThread";
	private static int AUTHENTICATION_CONNECTION_TIMEOUT = 30000;
	private static int AUTHENTICATION_SOCKET_TIMEOUT = 30000;
	private HttpPost httppost = null;
	private Response resp = null;
	private String responseType = "";
	private String tableName = "";

	@Override
	protected Void doInBackground(Void... params) {
		Log.d(TAG, "doInBackground() - Starting connection with server");

		setVariablesForParse();

		if (HttpAuthenticate.authenticate()) {
			Gson gson = new Gson();

			HttpClient httpclient = ApplicationControl.httpclient;
			HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), AUTHENTICATION_CONNECTION_TIMEOUT);
			HttpConnectionParams.setSoTimeout(httpclient.getParams(), AUTHENTICATION_SOCKET_TIMEOUT);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = "";
			try {
				responseBody = httpclient.execute(httppost, responseHandler);
				Log.d(TAG, "doInBackground() - Response: " + responseBody);

				// JsonReader reader = new JsonReader(new StringReader(responseBody));
				// reader.setLenient(true);
				String str = "";
				resp = gson.fromJson(responseBody, Response.class);
				resp.setTableName(tableName);

				if(responseBody.indexOf("\"list\":[") != -1){
					str = responseBody.substring(responseBody.indexOf("\"list\":[") + 7, responseBody.length() - 3);
					resp.setResponseContent(str);
				}

				if ((resp == null) || resp.equals("")) {
					Log.d(TAG, "doInBackground() - Response from server is empty. Exiting...");
					throw new NullPointerException();
				}

				getVariablesForParse();

				DatabaseUpdateResponseHandler updaterHandler = new DatabaseUpdateResponseHandler();
				updaterHandler.process(resp);

			} catch (ClientProtocolException e) {
				Log.d(TAG, "doInBackground() - ClientProtocolException");
				ApplicationControl.handleMessage("HTTP: Protocolo de comunicação não suportado");
				e.printStackTrace();
				return null;
			} catch (NullPointerException e) {
				Log.d(TAG, "doInBackground() - NullPointerException");
				ApplicationControl.handleMessage("Resposta do servidor vazia");
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				Log.d(TAG, "doInBackground() - IOException");
				ApplicationControl.handleMessage("HTTP: Entrada/saída de dados interrompida");
				e.printStackTrace();
				return null;
			} catch (RuntimeException e) {
				Log.d(TAG, "doInBackground() - RuntimeException");
				ApplicationControl.handleMessage("Problema crítico de execução no sistema");
				e.printStackTrace();
				return null;
			}

		} else {
			// ApplicationControl.handleMessage("Não foi possível autenticar. Operação cancelada.");
//			ApplicationControl.finishSynch();
		}
		Log.d(TAG, "DataBaseUpdaterThread.doInBackGround() - finishing Sync...");
		ApplicationControl.finishSynch();
		return null;
	}

	public void setResponse(Response resp) {
		this.resp = resp;
	}

	public void setHttpPost(HttpPost post) {
		httppost = post;
	}

	private void setVariablesForParse() {
		if ((resp.getType() != null) && (resp.getType() != "")) {
			responseType = resp.getType();
		}
		if ((resp.getTableName() != null) && (resp.getTableName() != "")) {
			tableName = resp.getTableName();
		}
	}

	private void getVariablesForParse() {
		if ((responseType != null) && (responseType != "")) {
			resp.setType(responseType);
		}
		if ((tableName != null) && (tableName != "")) {
			resp.setTableName(tableName);
		}
	}

}
