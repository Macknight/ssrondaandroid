package com.br.svconsultoria.svnfcmanager.tools;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.dao.SVEventDao;
import com.br.svconsultoria.svnfcmanager.dao.SVInsertSequenceDao;
import com.br.svconsultoria.svnfcmanager.dao.SVMobilePhoneDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRfidTagDao;
import com.br.svconsultoria.svnfcmanager.dao.SVVigilantDao;
import com.br.svconsultoria.svnfcmanager.map.EventTypeMap;
import com.br.svconsultoria.svnfcmanager.map.InsertSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.SystemCheckpoint;
import com.br.svconsultoria.svnfcmanager.model.orm.EventLog;
import com.br.svconsultoria.svnfcmanager.services.SilentAlertService;
import com.br.svconsultoria.svnfcmanager.util.DateTimeUtils;

import java.util.Date;

public class EventBuilder {

	private static final String TAG = "EventBuilder";

	public static void createUserTagEvent(String tagSerial) {
		Log.d(TAG, "createUserTagEvent()");
		long tagId = 0;
		long vigilantId = 0;

		EventLog event = new EventLog();
		/////////////// CUSTOM jorge.lucas 17112014 14h23
		// separacao em 2 try-catchs das buscas de Tag e Vigilant Id
		StringBuilder comment = new StringBuilder();
		event.setComments("");

		// try-catch da busca por tag Id
		try {
			tagId = SVRfidTagDao.getTagBySerialNumber(tagSerial).getTagId();
		} catch (Exception e) {
			Log.e(TAG, "Exception", e);
			comment.append("Erro ao buscar tagId (VigilantId ignorado); ");
		}
		// try catch da busca por Vigilante Id
		if (tagId != 0) { // somente tenta buscar pelo VigilantId se o tag ID tiver sido recuperado com sucesso
			try {
				vigilantId = SVVigilantDao.getVigilantByTagId(tagId).getVigilantId();
				if(vigilantId==0){
					comment.append("Erro ao buscar vigilantId; ");
					ApplicationControl.handleMessage("Erro ao inserir evento. ID do vigilante não encontrado");
					return;
				}else{
					ApplicationControl.lastLoggedViggilantId = vigilantId;
				}
			} catch (Exception e) {
				Log.e(TAG, "Exception", e);
				comment.append("Erro ao buscar vigilantId; ");
			}
		}
		comment.append("(tag id #" + tagSerial + ")");

		event.setComments("" + comment);
		/////////////// FIM CUSTOM
		Date actualDate = new Date();

		event.setEventDt(DateTimeUtils.formatFullDate(actualDate));
		event.setEventLogId(SVInsertSequenceDao.getNextId(InsertSequenceMap.SRTM_EVENT_LOG));
		event.setEventTypeId(EventTypeMap.USER_READ_TAG_EVENT);

		if (SilentAlertService.gps.hasAccuracy()) {
			// Log.d(TAG, "createUserTagEvent() - GPS has Accuracy. Latitude: " + SilentAlertService.gps.getLatitude() + " Longitude: " + SilentAlertService.gps.getLongitude());
			event.setLatitude(SilentAlertService.gps.getLatitude());
			event.setLongitude(SilentAlertService.gps.getLongitude());
		} else {
			// Log.d(TAG, "createUserTagEvent() - GPS has no Accuracy.");
			event.setLatitude((float) 0);
			event.setLongitude((float) 0);
		}

		event.setMobileId(SVMobilePhoneDao.getMobileId());
		event.setTagId(tagId);
		event.setRouteId((long) 0);
		event.setCheckpointId((long) 0);
		event.setVigilantId(vigilantId);

		if(!SVEventDao.insertNewEvent(event))
			ApplicationControl.handleMessage("Erro ao inserir evento");



	}

	public static void createCheckpointTagEvent(Long tagId, long routeId, long checkpointId, long eventType, String comments, String tasks) {
		Log.d(TAG, "createCheckpointTagEvent()");
		Date actualDate = new Date();

		EventLog event = new EventLog();
		event.setEventDt(DateTimeUtils.formatFullDate(actualDate));
		event.setEventLogId(SVInsertSequenceDao.getNextId(InsertSequenceMap.SRTM_EVENT_LOG));
		event.setEventTypeId(eventType);

		if (SilentAlertService.gps.hasAccuracy()) {
			// Log.d(TAG, "createCheckpointTagEvent() - GPS has Accuracy. Latitude: " + SilentAlertService.gps.getLatitude() + " Longitude: " + SilentAlertService.gps.getLongitude());
			event.setLatitude(SilentAlertService.gps.getLatitude());
			event.setLongitude(SilentAlertService.gps.getLongitude());
		} else {
			// Log.d(TAG, "createCheckpointTagEvent() - GPS has no Accuracy.");
			event.setLatitude((float) 0);
			event.setLongitude((float) 0);
		}

		event.setMobileId(SVMobilePhoneDao.getMobileId());
		event.setTagId(tagId);
		event.setRouteId(routeId);
		event.setCheckpointId(checkpointId);


		event.setVigilantId(ApplicationControl.lastLoggedViggilantId); //
		event.setCheckpointChecks(tasks);

		if (ApplicationControl.userSession != null)
			if (ApplicationControl.userSession.getLoggedVigilant() != null)
				if (ApplicationControl.userSession.getLoggedVigilant().getVigilantId() != null &&
						ApplicationControl.userSession.getLoggedVigilant().getVigilantId().longValue() != 0){
					event.setVigilantId(ApplicationControl.userSession.getLoggedVigilant().getVigilantId());
				}
		event.setComments(comments);

		if(!SVEventDao.insertNewEvent(event))
			ApplicationControl.handleMessage("Erro ao inserir evento");
	}

	public static void createSkipCheckpointEvent(long routeId, SystemCheckpoint checkpoint, String comments) {
		Log.d(TAG, "createSkipCheckpointEvent()");
		Date actualDate = new Date();

		EventLog event = new EventLog();
		event.setEventDt(DateTimeUtils.formatFullDate(actualDate));
		event.setEventLogId(SVInsertSequenceDao.getNextId(InsertSequenceMap.SRTM_EVENT_LOG));
		event.setEventTypeId(EventTypeMap.SKIP_CHECKPOINT_EVENT);

		if (SilentAlertService.gps.hasAccuracy()) {
			// Log.d(TAG, "createSkipCheckpointEvent() - GPS has Accuracy. Latitude: " + SilentAlertService.gps.getLatitude() + " Longitude: " + SilentAlertService.gps.getLongitude());
			event.setLatitude(SilentAlertService.gps.getLatitude());
			event.setLongitude(SilentAlertService.gps.getLongitude());
		} else {
			// Log.d(TAG, "createSkipCheckpointEvent() - GPS has no Accuracy.");
			event.setLatitude((float) 0);
			event.setLongitude((float) 0);
		}

		event.setMobileId(SVMobilePhoneDao.getMobileId());
		event.setTagId((long) 0);
		event.setRouteId(routeId);
		event.setCheckpointId(checkpoint.getCheckpointId());
		event.setVigilantId(ApplicationControl.userSession.getLoggedVigilant().getVigilantId());
		event.setComments(comments);

		if(!SVEventDao.insertNewEvent(event))
			ApplicationControl.handleMessage("Erro ao inserir evento");
	}

	public static void createSkipRouteEvent(long routeId, String comments) {
		Log.d(TAG, "createSkipRouteEvent()");
		Date actualDate = new Date();

		EventLog event = new EventLog();
		event.setEventDt(DateTimeUtils.formatFullDate(actualDate));
		event.setEventLogId(SVInsertSequenceDao.getNextId(InsertSequenceMap.SRTM_EVENT_LOG));
		event.setEventTypeId(EventTypeMap.SKIP_ROUTE_EVENT);

		if (SilentAlertService.gps.hasAccuracy()) {
			// Log.d(TAG, "createSkipRouteEvent() - GPS has Accuracy. Latitude: " + SilentAlertService.gps.getLatitude() + " Longitude: " + SilentAlertService.gps.getLongitude());
			event.setLatitude(SilentAlertService.gps.getLatitude());
			event.setLongitude(SilentAlertService.gps.getLongitude());
		} else {
			// Log.d(TAG, "createSkipRouteEvent() - GPS has no Accuracy.");
			event.setLatitude((float) 0);
			event.setLongitude((float) 0);
		}

		event.setMobileId(SVMobilePhoneDao.getMobileId());
		event.setTagId((long) 0);
		event.setRouteId(routeId);
		event.setCheckpointId((long) 0);
		event.setVigilantId(ApplicationControl.userSession.getLoggedVigilant().getVigilantId());
		event.setComments(comments);

		if(!SVEventDao.insertNewEvent(event))
			ApplicationControl.handleMessage("Erro ao inserir evento");
	}

	public static void createOutOfOrderEvent(long routeId, String comments, long tagId) {
		Log.d(TAG, "createSkipRouteEvent()");
		Date actualDate = new Date();

		EventLog event = new EventLog();
		event.setEventDt(DateTimeUtils.formatFullDate(actualDate));
		event.setEventLogId(SVInsertSequenceDao.getNextId(InsertSequenceMap.SRTM_EVENT_LOG));
		event.setEventTypeId(EventTypeMap.CHECKPOINT_OUTOF_ORDER);

		if (SilentAlertService.gps.hasAccuracy()) {
			// Log.d(TAG, "createSkipRouteEvent() - GPS has Accuracy. Latitude: " + SilentAlertService.gps.getLatitude() + " Longitude: " + SilentAlertService.gps.getLongitude());
			event.setLatitude(SilentAlertService.gps.getLatitude());
			event.setLongitude(SilentAlertService.gps.getLongitude());
		} else {
			// Log.d(TAG, "createSkipRouteEvent() - GPS has no Accuracy.");
			event.setLatitude((float) 0);
			event.setLongitude((float) 0);
		}

		event.setMobileId(SVMobilePhoneDao.getMobileId());
		event.setTagId(tagId);
		event.setRouteId(routeId);
		event.setCheckpointId((long) 0);
		event.setVigilantId(ApplicationControl.userSession.getLoggedVigilant().getVigilantId());
		event.setComments(comments);

		if(!SVEventDao.insertNewEvent(event))
			ApplicationControl.handleMessage("Erro ao inserir evento");
	}

	public static void createPhoneFallEvent() {
		Log.d(TAG, "createPhoneFallEvent()");
		Date actualDate = new Date();

		EventLog event = new EventLog();
		event.setEventDt(DateTimeUtils.formatFullDate(actualDate));
		event.setEventLogId(SVInsertSequenceDao.getNextId(InsertSequenceMap.SRTM_EVENT_LOG));
		event.setEventTypeId(EventTypeMap.PHONE_FALL_EVENT);

		if (SilentAlertService.gps.hasAccuracy()) {
			// Log.d(TAG, "createPhoneFallEvent() - GPS has Accuracy. Latitude: " + SilentAlertService.gps.getLatitude() + " Longitude: " + SilentAlertService.gps.getLongitude());
			event.setLatitude(SilentAlertService.gps.getLatitude());
			event.setLongitude(SilentAlertService.gps.getLongitude());
		} else {
			// Log.d(TAG, "createPhoneFallEvent() - GPS has no Accuracy.");
			event.setLatitude((float) 0);
			event.setLongitude((float) 0);
		}

		event.setMobileId(SVMobilePhoneDao.getMobileId());
		event.setTagId((long) 0);
		event.setRouteId((long) 0);
		event.setCheckpointId((long) 0);
		event.setVigilantId(ApplicationControl.lastLoggedViggilantId);
		try {
			event.setVigilantId(ApplicationControl.userSession.getLoggedVigilant().getVigilantId());
		} catch (Exception e) {
			Log.d(TAG, "createPanicShakeEvent() - Vigilant not found in session...");
			event.setVigilantId((long) 0);
		}

		event.setComments("");

		if(!SVEventDao.insertNewEvent(event))
			ApplicationControl.handleMessage("Erro ao inserir evento");
	}

	public static void createPanicEvent(long type, String comments) {

		Log.d(TAG, "createPanicShakeEvent()");
		Date actualDate = new Date();

		EventLog event = new EventLog();
		event.setEventDt(DateTimeUtils.formatFullDate(actualDate));
		event.setEventLogId(SVInsertSequenceDao.getNextId(InsertSequenceMap.SRTM_EVENT_LOG));
		event.setEventTypeId(type);

		if (SilentAlertService.gps.hasAccuracy()) {
			// Log.d(TAG, "createSkipRouteEvent() - GPS has Accuracy. Latitude: " + SilentAlertService.gps.getLatitude() + " Longitude: " + SilentAlertService.gps.getLongitude());
			event.setLatitude(SilentAlertService.gps.getLatitude());
			event.setLongitude(SilentAlertService.gps.getLongitude());
		} else {
			// Log.d(TAG, "createPanicShakeEvent() - GPS has no Accuracy.");
			event.setLatitude((float) 0);
			event.setLongitude((float) 0);
		}

		event.setMobileId(SVMobilePhoneDao.getMobileId());
		event.setTagId((long) 0);
		event.setRouteId((long) 0);
		event.setCheckpointId((long) 0);
		event.setComments(comments);
		event.setVigilantId(ApplicationControl.lastLoggedViggilantId);
		try {
			event.setVigilantId(ApplicationControl.userSession.getLoggedVigilant().getVigilantId());
		} catch (Exception e) {
			Log.d(TAG, "createPanicShakeEvent() - Vigilant not found in session...");
			event.setVigilantId((long) 0);
		}

		if(!SVEventDao.insertNewEvent(event))
			ApplicationControl.handleMessage("Erro ao inserir evento");

	}

	public static void createIdlenessEvent(long routeId, String comments) {
		Log.d(TAG, "createIdlenessEvent()");

		EventLog event = new EventLog();
		event.setEventDt(new Date());
		event.setEventLogId(SVInsertSequenceDao.getNextId(InsertSequenceMap.SRTM_EVENT_LOG));
		event.setEventTypeId(EventTypeMap.IDLENESS);

		if (SilentAlertService.gps.hasAccuracy()) {
			// Log.d(TAG, "createCheckpointTagEvent() - GPS has Accuracy. Latitude: " + SilentAlertService.gps.getLatitude() + " Longitude: " + SilentAlertService.gps.getLongitude());
			event.setLatitude(SilentAlertService.gps.getLatitude());
			event.setLongitude(SilentAlertService.gps.getLongitude());
		} else {
			// Log.d(TAG, "createCheckpointTagEvent() - GPS has no Accuracy.");
			event.setLatitude((float) 0);
			event.setLongitude((float) 0);
		}

		event.setMobileId(SVMobilePhoneDao.getMobileId());
		event.setRouteId(routeId);
		event.setVigilantId(ApplicationControl.lastLoggedViggilantId);
		event.setComments(comments);


		if (ApplicationControl.userSession != null)
			if (ApplicationControl.userSession.getLoggedVigilant() != null)
				if (ApplicationControl.userSession.getLoggedVigilant().getVigilantId() != null)
					event.setVigilantId(ApplicationControl.userSession.getLoggedVigilant().getVigilantId());

		if(!SVEventDao.insertNewEvent(event))
			ApplicationControl.handleMessage("Erro ao inserir evento");
	}
}
