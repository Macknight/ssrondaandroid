package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.InsertSequence;
import com.br.svconsultoria.svnfcmanager.model.orm.InsertSequenceDao;

public class SVInsertSequenceDao {

	private static DaoSession session = ApplicationControl.daoSession;
	public static long getNextId(String tableName) {
		Log.d("SVInsertSequenceDao", "getNextId()");
		long nextId = 0;
		InsertSequenceDao dao = session.getInsertSequenceDao();
		try {
			InsertSequence is = dao.queryBuilder().where(InsertSequenceDao.Properties.TableName.eq(tableName)).uniqueOrThrow();
			nextId = is.getNextValue();
			is.setNextValue(nextId + 1);
			dao.update(is);
		} catch (Exception e) {
			e.printStackTrace();
			ApplicationControl.handleMessage("Banco de dados não foi inicializado");
		}
		Log.d("SVInsertSequenceDao", "getNextId() nextId: " + nextId);
		return nextId;
	}
}
