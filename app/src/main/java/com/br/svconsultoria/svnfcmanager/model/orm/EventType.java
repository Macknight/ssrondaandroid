package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Entity mapped to table SRTM_EVENT_TYPES.
 */
@Entity
public class EventType {

    @Id
    private Long eventTypeId;
    private Long xId;
    private String description;
    @Generated(hash = 1024615395)
    public EventType(Long eventTypeId, Long xId, String description) {
        this.eventTypeId = eventTypeId;
        this.xId = xId;
        this.description = description;
    }
    @Generated(hash = 1218244869)
    public EventType() {
    }
    public Long getEventTypeId() {
        return this.eventTypeId;
    }
    public void setEventTypeId(Long eventTypeId) {
        this.eventTypeId = eventTypeId;
    }
    public Long getXId() {
        return this.xId;
    }
    public void setXId(Long xId) {
        this.xId = xId;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

}
