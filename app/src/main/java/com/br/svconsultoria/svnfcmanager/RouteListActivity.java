package com.br.svconsultoria.svnfcmanager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextClock;
import android.widget.TextView;

import com.br.svconsultoria.svnfcmanager.adapters.RouteListAdapter;
import com.br.svconsultoria.svnfcmanager.dao.SVRfidTagDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteScheduleDao;
import com.br.svconsultoria.svnfcmanager.dao.SVSysParamsDao;
import com.br.svconsultoria.svnfcmanager.map.StatusMap;
import com.br.svconsultoria.svnfcmanager.model.orm.Route;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteSchedule;
import com.br.svconsultoria.svnfcmanager.session.UserSession;
import com.br.svconsultoria.svnfcmanager.util.DateTimeUtils;
import com.br.svconsultoria.svnfcmanager.util.SVUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.svconsultoria.newmobile.remotedebug.Log;

/**
 * Tela que exibe a lista de rotas <br />
 */
public class RouteListActivity extends Activity {

    private static final String TAG = "RouteListActivity";

    private int ROUTE_DELAY = 10;

    private RouteListAdapter adapter = null;
    private ListView routeListView = null;
    private ArrayList<RouteListItem> routesListItems;
    private TextView notFoundView = null;
    private TextClock dc1;

    private List<RouteSchedule> routeSchedules = null;
    private Date actualDt = DateTimeUtils.formatDate(new Date());

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        SVUtil.touch(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_route_list);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "tt0246m_.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
        ((TextView) findViewById(R.id.title)).setTypeface(typeface2);
        ((TextClock) findViewById(R.id.digitalClockRouteList)).setTypeface(typeface);

        routeListView = (ListView) findViewById(R.id.routeList);
        notFoundView = (TextView) findViewById(R.id.routesNotFound);

        routesListItems = new ArrayList<RouteListItem>();
        getTodayRoutes();
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, UserValidationActivity.class);
        String sSerial = SVRfidTagDao.getTagById(ApplicationControl.userSession.getLoggedVigilant().getLogonTagId()).getSerialNumber();
        i.putExtra("serial", sSerial);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
    }

    private void buildRoutesScreen() {

        adapter = new RouteListAdapter(routesListItems, this);

        routeListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                // String s = (String) ((TextView) v.findViewById(R.id.routeDescription)).getText();
                // Toast.makeText(RouteListActivity.this, s, Toast.LENGTH_LONG).show();


                long scheduleId = routesListItems.get(position).getRouteSchedule().getRouteScheduleId();
                RouteSchedule sc = SVRouteScheduleDao.getRouteScheduleById(scheduleId);
                Route r = SVRouteDao.getRouteById(sc.getRouteId());

                ApplicationControl.userSession.setRouteScheduleId(scheduleId);
                boolean freeRoute = Boolean.parseBoolean(String.valueOf(r.getFreeRoute()));

                if(SVSysParamsDao.isVehicleCheckEnabled()){
                    Intent vehicle = new Intent(getApplicationContext(), VehicleCheckActivity.class);
                    vehicle.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(vehicle);
                }else{
                    if(freeRoute){
                        Intent i = new Intent(getApplicationContext(), FreeRouteActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                    }else{
                        Intent i = new Intent(getApplicationContext(), RouteMainActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                    }
                }



            }
        });

        routeListView.setAdapter(adapter);
    }


    @SuppressWarnings("deprecation")
    private void getTodayRoutes() {

        Route route = null;
        RouteListItem routeListItem = null;
        RouteSchedule routeSchedule = SVRouteScheduleDao.getRouteScheduleById(ApplicationControl.userSession.getRouteScheduleId());
        if(routeSchedule!=null){
            route = SVRouteDao.getRouteById(routeSchedule.getRouteId());
        }

        if (ApplicationControl.userSession.getRouteScheduleId() == 0 || routeSchedule == null || route == null
                || (SVSysParamsDao.isVehicleCheckEnabled() && !ApplicationControl.userSession.isVehicleChecked())) {

            Log.d(TAG, "getTodayRoutes() - actualDt: " + actualDt);
            routeSchedules = SVRouteScheduleDao.getRouteScheduleByDate(DateTimeUtils.formatDate(actualDt));

            for (RouteSchedule rs : routeSchedules) {
                if(rs.getStatusId()==StatusMap.STATUS_ACTIVE) {
                    if (SVRouteDao.getRouteById(rs.getRouteId()) != null) {
                        route = SVRouteDao.getRouteById(rs.getRouteId());
                        if(route.getStatusId()== StatusMap.STATUS_ACTIVE) {
                            Date now = new Date();
                            long nowMinutes = (now.getHours() * 60) + now.getMinutes();
                            Log.d(TAG, "RouteSchedule InitHr + Delay: " + (rs.getInitHr() + ROUTE_DELAY) + " Actual Hour: " + nowMinutes);
                            // if ((rs.getInitHr() + ROUTE_DELAY) >= (nowMinutes)) {

                            routeListItem = new RouteListItem();
                            routeListItem.setRoute(route);
                            routeListItem.setRouteSchedule(rs);
                            routesListItems.add(routeListItem);

                            Log.d("RoutesActivity", "getTodayRoutes() - Route: " + route.getDescription());
                        }
                    }
                }
            }

        } else {

            routeListItem = new RouteListItem();
            routeListItem.setRoute(route);
            routeListItem.setRouteSchedule(routeSchedule);
            routeListItem.setDetails("Esta rota deve ser terminada.");
            // routesListItems.add(routeListItem);

            long scheduleId = routeListItem.getRouteSchedule().getRouteScheduleId();
            RouteSchedule sc = SVRouteScheduleDao.getRouteScheduleById(scheduleId);
            Route r = SVRouteDao.getRouteById(sc.getRouteId());

            ApplicationControl.userSession.setRouteScheduleId(scheduleId);
            boolean freeRoute = Boolean.parseBoolean(String.valueOf(r.getFreeRoute()));


            if (freeRoute) {
                Intent i = new Intent(getApplicationContext(), FreeRouteActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
            } else {
                Intent i = new Intent(getApplicationContext(), RouteMainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
            }


        }

        // generateTestData();

        if (routesListItems.size() < 1) {
            notFoundView.setVisibility(View.VISIBLE);
            notFoundView.setText("Nenhuma rota encontrada para o dia de hoje.");
        } else {
            routeListView.setVisibility(View.VISIBLE);
            buildRoutesScreen();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.route_list, menu);
        return true;
    }

    public class RouteListItem {
        Route route;
        RouteSchedule routeSchedule;
        String details;

        public RouteListItem() {
            route = null;
            routeSchedule = null;
            details = "";
        }

        public Route getRoute() {
            return route;
        }

        public void setRoute(Route route) {
            this.route = route;
        }

        public RouteSchedule getRouteSchedule() {
            return routeSchedule;
        }

        public void setRouteSchedule(RouteSchedule routeSchedule) {
            this.routeSchedule = routeSchedule;
        }

        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }
    }

    public String getNextRoute() {

        Log.d(TAG, "getTodayRoutes() - actualDt: " + actualDt);
        routeSchedules = SVRouteScheduleDao.getRouteScheduleByDate(DateTimeUtils.formatDate(actualDt));

        for (RouteSchedule rs : routeSchedules) {
            if (SVRouteDao.getRouteById(rs.getRouteId()) != null) {
                Date now = new Date();
                long nowMinutes = (now.getHours() * 60) + now.getMinutes();
                if ((rs.getInitHr()) >= (nowMinutes)) {
                    return String.format(Locale.getDefault(), "%02d:%02d", rs.getInitHr() / 60, rs.getInitHr() % 60);
                }
            }
        }

        return "--:--";
    }
}