package com.br.svconsultoria.svnfcmanager.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.svconsultoria.svnfcmanager.R;
import com.br.svconsultoria.svnfcmanager.RouteListActivity.RouteListItem;
import com.br.svconsultoria.svnfcmanager.util.DateTimeUtils;

import java.util.ArrayList;

public class RouteListAdapter extends BaseAdapter {
	private ArrayList<RouteListItem> _data;
	Context _c;

	public RouteListAdapter(ArrayList<RouteListItem> data, Context c) {
		_data = data;
		_c = c;
	}

	@Override
	public int getCount() {
		return _data.size();
	}

	@Override
	public Object getItem(int position) {
		return _data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return _data.get(position).getRoute().getRouteId();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;

		if (v == null) {
			LayoutInflater vi = (LayoutInflater) _c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.route_list_item, null);
		}

		Typeface typeface2 = Typeface.createFromAsset(_c.getAssets(), "tt0247m_.ttf");
		Typeface typeface = Typeface.createFromAsset(_c.getAssets(), "tt0246m_.ttf");
		ImageView image = (ImageView) v.findViewById(R.id.icon);
		TextView routeDescription = (TextView) v.findViewById(R.id.routeDescription);
		routeDescription.setAllCaps(true);
		((TextView) v.findViewById(R.id.routeDescription)).setTypeface(typeface2);
		TextView timeView = (TextView) v.findViewById(R.id.time);
		timeView.setAllCaps(true);
		((TextView) v.findViewById(R.id.time)).setTypeface(typeface);
		TextView details = (TextView) v.findViewById(R.id.details);
		((TextView) v.findViewById(R.id.details)).setTypeface(typeface);

		RouteListItem route = _data.get(position);
		image.setImageResource(R.drawable.route_list_icon);
		routeDescription.setText(route.getRoute().getDescription());
		// timeView.setText(DateTimeUtils.parseDate(route.getRouteSchedule().getScheduleDt()) + ". Início às " + DateTimeUtils.getTimeTable(route.getRouteSchedule().getInitHr()));
		timeView.setText("Início às " + DateTimeUtils.getTimeTable(route.getRouteSchedule().getInitHr()));
		timeView.setGravity(Gravity.START);

		if (route.getDetails().equals("")) {
			details.setVisibility(View.INVISIBLE);
		} else {
			details.setVisibility(View.VISIBLE);
			details.setText(route.getDetails());
		}

		return v;
	}
}
