package com.br.svconsultoria.svnfcmanager.http.vigilantPhoto;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVVigilantPhotoDao;
import com.br.svconsultoria.svnfcmanager.map.HttpConnAction;
import com.br.svconsultoria.svnfcmanager.model.Response;
import com.br.svconsultoria.svnfcmanager.model.orm.VigilantPhotos;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class VigilantPhotoSender {

	private String TAG = "VigilantPhotoSender";

	protected VigilantPhotoSender() {}

	private static VigilantPhotoSender instance = new VigilantPhotoSender();

	private ArrayList<VigilantPhotos> photos = null;
	private Response resp = new Response();
	private HttpPost post = new HttpPost();

	public static VigilantPhotoSender getIntance() {
		instance = new VigilantPhotoSender();
		return instance;
	}

	private void initiate() {
		resp = new Response();
		post = new HttpPost();
	}

	public void sendPhotos() throws URISyntaxException {

		// initiate();
		if (SVVigilantPhotoDao.hasPhotos()) {
			ApplicationControl.IS_SYNC_PHOTOS = true;
			Log.d(TAG, "sendPhotos() - Starting VigilantPhotos sender - There are photos to send!");

			// String uri = HttpConnAction.DATABASE_URL + HttpConnAction.VIGILANT_PHOTO_UPDATE_URI;
			String uri = HttpConnAction.HTTP + SVMobileParamsDao.getHostParam() + HttpConnAction.VIGILANT_PHOTO_UPDATE_URI;
			Log.d(TAG, "sendPhotos() - URI: " + uri);

			if (!(uri.equals("") || (uri == null))) {

				Log.d(TAG, "sendPhotos() - Parameters has been set");

				try {
					VigilantPhotoThread call = new VigilantPhotoThread();
					Log.d(TAG, "sendPhotos() - Execute!");
					call.execute();
				} catch (NullPointerException e) {
					Log.d(TAG, "sendPhotos() - The necessary variables were not initialized. Closing HttpServerCall");
				} catch (Exception e) {
					e.printStackTrace();
					Log.d(TAG, "sendPhotos()- Exception: " + e.getMessage());
				}
			} else {
				ApplicationControl.handleMessage("Configuração inicial não encontrada.");
				Log.d(TAG, "sendPhotos() - The necessary variables were not initialized. Closing " + TAG);
				ApplicationControl.finishSendPhotos();
			}
		} else {
			Log.d(TAG, "sendPhotos() - There are no photos to send...");
			ApplicationControl.finishSendPhotos();
		}

		ApplicationControl.IS_SYNC_PHOTOS = false;
	}

	private void setEntities(List<NameValuePair> pairs) {
		try {
			post.setEntity(new UrlEncodedFormEntity(pairs));
		} catch (UnsupportedEncodingException e) {
			ApplicationControl.handleMessage("HTTP: Formato de dados não suportado");
			e.printStackTrace();
		}
	}
}
