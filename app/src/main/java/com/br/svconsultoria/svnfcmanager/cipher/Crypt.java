package com.br.svconsultoria.svnfcmanager.cipher;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Crypt {

	private static final String key = "ss_ronda_password_key";

	public static String md5(String s) {
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuilder hexString = new StringBuilder();
			for (byte element : messageDigest) {
				hexString.append(Integer.toHexString(0xFF & element));
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static String passwordCrypt(String s) {
		try {
			s = s + key;
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuilder hexString = new StringBuilder();
			for (byte element : messageDigest) {
				hexString.append(Integer.toHexString(0xFF & element));
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

}
