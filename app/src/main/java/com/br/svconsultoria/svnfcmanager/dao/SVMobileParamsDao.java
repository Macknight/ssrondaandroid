package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.MobileParam;
import com.br.svconsultoria.svnfcmanager.model.orm.MobileParam;
import com.br.svconsultoria.svnfcmanager.model.orm.MobileParamDao;

import org.greenrobot.greendao.DaoException;

import java.util.List;

/*
 * This class represents only the control parameters related to the mobile system.
 * For the parameters configured on the web interface, look for SysParams objects.
 */

public class SVMobileParamsDao {

	private static DaoSession session = ApplicationControl.daoSession;


	public static boolean insertMobileParams(final List<MobileParam> params) {
		Log.d("SVMobileParamsDao", "insertMobileParams() Starting...");
		ApplicationControl.daoSession.runInTx(new Runnable() {

			@Override
			public void run() {
				MobileParamDao mobileParamdao = ApplicationControl.daoSession.getMobileParamDao();
				mobileParamdao.insertOrReplaceInTx(params);
			}
		});
		Log.d("SVMobileParamsDao", "insertMobileParams() Finished!");
		return true;
	}

	public static void insertOrUpdateMobileParam(MobileParam param) {
		 Log.d("SVMobileParamsDao", "insertOrUpdateMobileParam()");
		try {
			MobileParamDao mobileParamdao = ApplicationControl.daoSession.getMobileParamDao();

			MobileParam oldParam = mobileParamdao.queryBuilder().where(MobileParamDao.Properties.ParamName.eq(param.getParamName())).unique();
			if(oldParam==null){
				mobileParamdao.insert(param);
			}else{
				param.setId(oldParam.getId());
				mobileParamdao.update(param);
			}

		} catch (NullPointerException e) {
			Log.d("SVMobileParamsDao", "insertOrUpdateMobileParam() - MobileParam (" + param.getParamName() + ") not found. Error: " + e.getMessage());
		}
		 Log.d("SVMobileParamsDao", "insertOrUpdateMobileParam() Finished!");
	}

	public static MobileParam getMobileParamByName(String name) {
		Log.d("SVMobileParamsDao", "getMobileParamByName() - ParamName: " + name + ".");

		MobileParamDao mobileParamdao = ApplicationControl.daoSession.getMobileParamDao();

		MobileParam param = null;

		try {
			param = mobileParamdao.queryBuilder().where(MobileParamDao.Properties.ParamName.eq(name)).uniqueOrThrow();
		} catch (NullPointerException e) {
			Log.d("SVMobileParamsDao", "getMobileParamByName() - MobileParam not found. Error: " + e.getLocalizedMessage());
			param = new MobileParam(null, name, "");
		} catch (DaoException e) {
			Log.d("SVMobileParamsDao", "getMobileParamByName() - MobileParam " + name + " not found. Error: " + e.getLocalizedMessage());
			param = new MobileParam(null, name, "");
		}
		Log.d("SVMobileParamsDao", "getMobileParamByName() Finished!");
		return param;
	}

	public static String getHostParam() {
		MobileParam hostParam = null;
		hostParam = getMobileParamByName(SystemParamsMap.HOST_ADDRESS);
		String host =hostParam.getParamValue();
		if(host!=null && !host.isEmpty() && !host.endsWith("/")){
			host+="/";
		}
		return host;
	}

	public static boolean isConfigured() {
		MobileParam isConfiguredParam = null;
		boolean isConfigured = false;

		isConfiguredParam = getMobileParamByName(SystemParamsMap.IS_CONFIGURED);

		if (isConfiguredParam.getParamValue().equals("true")) {
			isConfigured = true;
		}
		return isConfigured;
	}

	public static String getUserNameLogin() {
		MobileParam loginName = null;
		loginName = getMobileParamByName(SystemParamsMap.ADMIN_LOGIN_NAME);
		return loginName.getParamValue();
	}

	public static String getUserPassword() {
		MobileParam loginPassword = null;
		loginPassword = getMobileParamByName(SystemParamsMap.ADMIN_PASSWORD);
		return loginPassword.getParamValue();
	}

	public static long getLastRouteScheduleId() {
		MobileParam lastRouteSchedule = null;
		lastRouteSchedule = getMobileParamByName(SystemParamsMap.LAST_ROUTE_SCHEDULE_ID);
		if (lastRouteSchedule.getParamValue().equals("")) {
			lastRouteSchedule.setParamValue("0");
		}
		return Long.parseLong(lastRouteSchedule.getParamValue().trim());
	}

	public static long getLastRouteOrderId() {
		MobileParam lastOrder = null;
		lastOrder = getMobileParamByName(SystemParamsMap.LAST_ORDER_ID);
		if (lastOrder.getParamValue().equals("")) {
			lastOrder.setParamValue("0");
		}
		return Long.parseLong(lastOrder.getParamValue().trim());
	}

	public static boolean isVehicleChecked(){
		MobileParam vehicleChecked = null;
		boolean isVehicleChecked = false;

		vehicleChecked = getMobileParamByName(SystemParamsMap.VEHICLE_CHECKED);

		if (vehicleChecked.getParamValue().equals("true")) {
			isVehicleChecked = true;
		}
		return isVehicleChecked;
	}

	public static boolean isFallSoundAllowed() {
		MobileParam fallSoundAllowed = null;
		boolean isFallSoundAllowed = false;

		fallSoundAllowed = getMobileParamByName(SystemParamsMap.FALL_SOUND_ALLOWED);

		if (fallSoundAllowed.getParamValue().equals("true")) {
			isFallSoundAllowed = true;
		}
		return isFallSoundAllowed;
	}

	public static boolean isPanicSoundAllowed() {
		MobileParam panicSoundAllowed = null;
		boolean isPanicSoundAllowed = false;

		panicSoundAllowed = getMobileParamByName(SystemParamsMap.PANIC_SOUND_ALLOWED);

		if (panicSoundAllowed.getParamValue().equals("true")) {
			isPanicSoundAllowed = true;
		}
		return isPanicSoundAllowed;
	}

	public static boolean isQrCodeAllowed() {
		MobileParam qrCodeAllowed = null;
		boolean isQrCodeAllowed = false;

		qrCodeAllowed = getMobileParamByName(SystemParamsMap.QR_CODE_ALLOWED);

		if (qrCodeAllowed.getParamValue().equals("true")) {
			isQrCodeAllowed = true;
		}
		return isQrCodeAllowed;
	}


	public static boolean isAdminModeAllowed() {
		MobileParam adminModeAllowed = null;
		boolean isAdminModeAllowed = false;

		adminModeAllowed = getMobileParamByName(SystemParamsMap.ADMIN_MODE_ALLOWED);

		if (adminModeAllowed.getParamValue().equals("true")) {
			isAdminModeAllowed = true;
		}
		return isAdminModeAllowed;
	}

}
