package com.br.svconsultoria.svnfcmanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

/**
 * <b>Activity inicial do Ronda</b> <br />
 * Apresenta o logotipo e uma mensagem de <i>loading</i> <br />
 *  
 */
public class SplashScreenActivity extends Activity {
	
	// private static final String TAG = "SplashScreenActivity";
	
	// private View loadingView;
	
	// private SynchronizeDataTask mSyncTask = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_splash_screen);
		
		// loadingView = findViewById(R.id.progressBar_splash);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		// if (ApplicationControl.DEVELOPMENT) {
		// ApplicationControl.startSend();
		// ApplicationControl.startSynch(this);
		// }
		//
		// AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
		//
		// @Override
		// protected Void doInBackground(Void... params) {
		// try {
		// if (ApplicationControl.DEVELOPMENT) {
		// Thread.sleep(3000);
		// } else {
		// Thread.sleep(1000);
		// }
		// } catch (InterruptedException e) {}
		// return null;
		// }
		//
		// @Override
		// protected void onPostExecute(Void result) {
		// super.onPostExecute(result);
		// redirect();
		// }
		// };
		//
		// task.execute();
		
		redirect();
	}
	
	private void redirect() {
		Intent i = new Intent(getApplicationContext(), MainActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		getApplicationContext().startActivity(i);
	}
	
	@Override
	public void onBackPressed() {
		return;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.splash_screen, menu);
		return true;
	}
	
}
