package com.br.svconsultoria.svnfcmanager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.br.svconsultoria.svnfcmanager.dao.SVCheckpointDao;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVMobilePhoneDao;
import com.br.svconsultoria.svnfcmanager.dao.SVOccurrenceTypesDao;
import com.br.svconsultoria.svnfcmanager.dao.SVPriorityDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRfidTagDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteCompositionDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteScheduleDao;
import com.br.svconsultoria.svnfcmanager.dao.SVSysParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVUserDao;
import com.br.svconsultoria.svnfcmanager.dao.SVVigilantDao;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.menu.MenuController;
import com.br.svconsultoria.svnfcmanager.model.orm.Checkpoint;
import com.br.svconsultoria.svnfcmanager.model.orm.CheckpointDao;
import com.br.svconsultoria.svnfcmanager.model.orm.Customer;
import com.br.svconsultoria.svnfcmanager.model.orm.CustomerDao;
import com.br.svconsultoria.svnfcmanager.model.orm.EventLog;
import com.br.svconsultoria.svnfcmanager.model.orm.MobileParam;
import com.br.svconsultoria.svnfcmanager.model.orm.MobilePhone;
import com.br.svconsultoria.svnfcmanager.model.orm.Occurrence;
import com.br.svconsultoria.svnfcmanager.model.orm.OccurrenceType;
import com.br.svconsultoria.svnfcmanager.model.orm.Priority;
import com.br.svconsultoria.svnfcmanager.model.orm.RfidTag;
import com.br.svconsultoria.svnfcmanager.model.orm.Route;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteComposition;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteCompositionDao;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteDao;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteSchedule;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteScheduleDao;
import com.br.svconsultoria.svnfcmanager.model.orm.SysParam;
import com.br.svconsultoria.svnfcmanager.model.orm.User;
import com.br.svconsultoria.svnfcmanager.model.orm.UserDao;
import com.br.svconsultoria.svnfcmanager.model.orm.Vigilant;
import com.br.svconsultoria.svnfcmanager.model.orm.VigilantDao;
import com.br.svconsultoria.svnfcmanager.services.SilentAlertService;
import com.br.svconsultoria.svnfcmanager.util.DateTimeUtils;
import com.br.svconsultoria.svnfcmanager.util.SVUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Apresenta o logotipo e aguarda um toque na tela para iniciar <br />
 * <i>Cadastra registros <b>hard-coded</b> (MainActivity.generateTestData()) mas as chamadas a este método está comentada</i>
 */
public class MainActivity extends Activity {

	private static final String TAG = "MainActivity";

	private NfcAdapter mAdapter;
	private LocationManager locationManager;
	private TextView textView1 = null;
	private TextView versionText = null;
	final Context context = this;
	public static Context applicationContext;

	private static boolean SENSORS_ENABLED = false;

	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
		SVUtil.touch(this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);

		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		if (ApplicationControl.DEVELOPMENT) {
			// generateTestData();
			// generateTestRoutes();
			prepareAndExecuteSyncTaskDevelopment();
		} else {
			prepareAndExecuteSyncTask();
		}

		versionText = (TextView) findViewById(R.id.editText1);
		versionText.setText(versionText.getText() + " " + BuildConfig.VERSION_NAME);

		applicationContext = getApplicationContext();

		addSystemParams();

		// new PrintTable().printAll();

		// Toast.makeText(this, "IMEI: " + ApplicationControl.TELEPHONE_IMEI, Toast.LENGTH_LONG).show();


		// CUSTOM jorge.lucas 07052015 long click to see application internals
		ImageView configImageView = (ImageView) findViewById(R.id.menu_item_6);

		configImageView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				systemInformationDialog();
				return true; // prevents trigger regular click
			}
		});
		// FIM CUSTOM

		// CUSTOM jorge.lucas 19052015 long click to configure remote debug
		View configurationEditView = (View) findViewById(R.id.view1);

		configurationEditView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				configRemoteDebug();
				return true; // prevents trigger regular click
			}
		});
		// FIM CUSTOM
	}

	/**
	 * popups a dialog with internal system information
	 */
	private void systemInformationDialog() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);


		builder
			.setMessage(formattedSystemInformation())
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(final DialogInterface dialog, final int id) {
					dialog.cancel();
				}
		});
		builder.show();
	}

	/**
	 * returns formatted \n system information
	 * @return
	 */
	private String formattedSystemInformation() {
		StringBuilder message = new StringBuilder();
		Long aux = 0L;

		message.append("Informações do sistema\n\n");

		// Vigilant xid
		List<Vigilant> vigilantList = ApplicationControl.daoSession.getVigilantDao().queryBuilder().orderDesc(VigilantDao.Properties.XId).list();
		if (vigilantList != null && vigilantList.size() > 0)
			aux = vigilantList.get(0).getXId();
		message.append("("+vigilantList.size()+") Vigilant xId: " + aux + "\n");
		aux = 0L;

		// Users xid
		List<User> userList = ApplicationControl.daoSession.getUserDao().queryBuilder().orderDesc(UserDao.Properties.XId).list();
		if (userList != null && userList.size() > 0)
			aux = userList.get(0).getXId();
		message.append("("+userList.size()+") User xId: " + aux + "\n");
		aux = 0L;

		// Customers xid
		List<Customer> customerList = ApplicationControl.daoSession.getCustomerDao().queryBuilder().orderDesc(CustomerDao.Properties.XId).list();
		if (customerList != null && customerList.size() > 0)
			aux = customerList.get(0).getXId();
		message.append("("+customerList.size()+") Customer xId: " + aux + "\n");
		aux = 0L;

		// occurrence count
		List<Occurrence> occurrenceList = ApplicationControl.daoSession.getOccurrenceDao().queryBuilder().list();
		if (occurrenceList != null && occurrenceList.size() > 0)
			aux = (long) occurrenceList.size();
		message.append("Total de ocorrências: " + aux + "\n");
		aux = 0L;

		// eventlog count
		List<EventLog> eventLogList = ApplicationControl.daoSession.getEventLogDao().queryBuilder().list();
		if (eventLogList != null)
			aux = (long) eventLogList.size();
		message.append("Total de eventos: " + aux + "\n");
		aux = 0L;

		// checkpoint xid
		List<Checkpoint> checkPointList =  ApplicationControl.daoSession.getCheckpointDao().queryBuilder().orderDesc(CheckpointDao.Properties.XId).list();
		if (checkPointList != null && checkPointList.size() > 0)
			aux = checkPointList.get(0).getXId();
		message.append("("+checkPointList.size()+") Checkpoint xId: " + aux + "\n");
		aux = 0L;

		// route xid
		List<Route> routeList = ApplicationControl.daoSession.getRouteDao().queryBuilder().orderDesc(RouteDao.Properties.XId).list();
		if (routeList != null && routeList.size() > 0)
			aux = routeList.get(0).getXId();
		message.append("("+routeList.size()+") Route xId: " + aux + "\n");
		aux = 0L;

		// route composition xid
		List<RouteComposition> routeComposition = ApplicationControl.daoSession.getRouteCompositionDao().queryBuilder().orderDesc(RouteCompositionDao.Properties.XId).list();
		if (routeComposition != null && routeComposition.size() > 0)
			aux = routeComposition.get(0).getXId();
		message.append("("+routeComposition.size()+") Route composition xId: " + aux + "\n");
		aux = 0L;

		// route schedule composition xid
		List<RouteSchedule> routeScheduleList = ApplicationControl.daoSession.getRouteScheduleDao().queryBuilder().orderDesc(RouteScheduleDao.Properties.XId).list();
		if (routeScheduleList != null && routeScheduleList.size() > 0)
			aux = routeScheduleList.get(0).getXId();
		message.append("("+routeScheduleList.size()+") Route schedule xId: " + aux + "\n");
		aux = 0L;

		// remote debug info
		MobileParam remoteDebugAddress, remoteDebugPort, remoteDebugEnabled;
		remoteDebugAddress = remoteDebugPort = remoteDebugEnabled = new MobileParam();

		if (SVMobileParamsDao.getMobileParamByName("remote_debug_address") != null) remoteDebugAddress = SVMobileParamsDao.getMobileParamByName("remote_debug_address");
		if (SVMobileParamsDao.getMobileParamByName("remote_debug_port") != null) remoteDebugPort = SVMobileParamsDao.getMobileParamByName("remote_debug_port");
		if (SVMobileParamsDao.getMobileParamByName("remote_debug_enabled") != null) remoteDebugEnabled = SVMobileParamsDao.getMobileParamByName("remote_debug_enabled");

		message.append("\nRD ad: " +  remoteDebugAddress.getParamValue() + "\n port: "+ remoteDebugPort.getParamValue() + " enabled: " + remoteDebugEnabled.getParamValue() + "\n");

		// GPS info
		double lat = 0f, lng = 0f, acc = 0f;
		if (SilentAlertService.gps.hasAccuracy()) {
			lat = SilentAlertService.gps.getLatitude();
			lng = SilentAlertService.gps.getLongitude();
			acc = SilentAlertService.gps.getAccuracy();
		}
		message.append("\n GPS " + lat + " " + lng + " (" + acc + ")\n");

		// last sync info
		message.append("\n Last sync " + ApplicationControl.stAppLastSyncHour +" \n");

		// checkpoint distance check
		if (SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE_ENABLED) != null)
			message.append("\n CheckPoint distance enabled " + SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE_ENABLED).getValue() + " (" + SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE_ENABLED).getXId() + ")");
		if (SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE) != null)
			message.append("\n CheckPoint distance tolerance " + SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE).getValue() + " (" + SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE).getXId() + ")");

//		if (SVSysParamsDao.getSysParamById(1) != null)
//			message.append("\n CheckPoint distance enabled " + SVSysParamsDao.getSysParamById(1).getValue() + " (" + SVSysParamsDao.getSysParamById(1).getXId() + ")");
//		if (SVSysParamsDao.getSysParamById(2) != null)
//			message.append("\n CheckPoint distance tolerance " + SVSysParamsDao.getSysParamById(2).getValue() + " (" + SVSysParamsDao.getSysParamById(2).getXId() + ")");


		return message.toString();
	}

	private void prepareAndExecuteSyncTask() {
		Log.d(TAG, "prepareAndExecuteSyncTask()");
		ApplicationControl.syncWithServer();
		// if (ApplicationControl.SEND_FINISHED) {
		// ApplicationControl.startSend();
		// }
		// if (ApplicationControl.SYNC_FINISHED) {
		// ApplicationControl.startSynch(this);
		// }
	}

	private void prepareAndExecuteSyncTaskDevelopment() {
		Log.d(TAG, "prepareAndExecuteSyncTaskDevelopment()");
		// if (ApplicationControl.SEND_FINISHED) {
		// // ApplicationControl.startSend();
		// }
	}

	@Override
	public void onBackPressed() {
		return;
	}

	@Override
	public void onResume() {
		super.onResume();
		// Log.d(TAG, "onResume()");
		// manageMessage();
		testSensors();
	}

	// private void manageMessage() {
	// if (SVSystemParamsDao.isConfigured()) {
	// textView1.setText("TOQUE NA TELA PARA CONTINUAR");
	// } else {
	// textView1.setText("TOQUE NA TELA PARA FAZER A CONFIGURAÇÃO INICIAL");
	// // generateTestData();
	// }
	// }

	public void testSensors() {

		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			SENSORS_ENABLED = false;
			final Intent poke = new Intent();
			poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
			poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			poke.setData(Uri.parse("3"));
			sendBroadcast(poke);
			// showDialog(DIALOG_ALERT_GPS, null);
			// buildAlertMessageNoGps();
		} else {
			try {
				if (!SVMobileParamsDao.isQrCodeAllowed()) {
					mAdapter = NfcAdapter.getDefaultAdapter(this);
					if (!mAdapter.isEnabled()) {

						Toast.makeText(getApplicationContext(), "Toque em cima da opção NFC para habilitar o NFC e aperte VOLTAR para iniciar o SV Ronda", Toast.LENGTH_LONG).show();
						SENSORS_ENABLED = false;
						startActivity(new Intent(android.provider.Settings.ACTION_NFC_SETTINGS));
						// buildAlertMessageNoNfc();
					} else {
						SENSORS_ENABLED = true;
					}
				} else {
					SENSORS_ENABLED = true;
				}
			} catch (Exception e) {
				Log.d(TAG, "QR Code Enabled - SENSORS_ENABLED = true - Bypass Activated.");
				SENSORS_ENABLED = true;
			}
		}

	}

	private void buildAlertMessageNoNfc() {

		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("O NFC está desativado. Você deseja ativá-lo agora?").setCancelable(false).setPositiveButton("Sim", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int id) {
				startActivity(new Intent(android.provider.Settings.ACTION_NFC_SETTINGS));
			}
		}).setNegativeButton("Não", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int id) {
				dialog.cancel();
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	private void buildAlertMessageNoGps() {

		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setInverseBackgroundForced(true);
		builder.setMessage("O GPS está desativado. Você deseja ativá-lo agora?").setCancelable(false).setPositiveButton("Sim", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int id) {
				startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
			}
		}).setNegativeButton("Não", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int id) {
				dialog.cancel();
			}
		});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	public void clique(View v) {
		if (SENSORS_ENABLED) {

			//added && !ApplicationControl.isFirstRun() so the config screen
			//shows up until the device is correctly configured
			if (SVMobileParamsDao.isConfigured() && !ApplicationControl.isFirstRun()) {
				Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
				startActivity(intent);
			} else {
				Intent intent = new Intent(getApplicationContext(), FirstAccessActivity.class);
				startActivity(intent);
			}
		} else {
			Toast.makeText(getApplicationContext(), "GPS ou NFC desligados.", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(getApplicationContext(), MainActivity.class);
			startActivity(intent);
		}
	}

	/**
	 * Gera as configurações iniciais da aplicação <b>hard-coded</b>
	 */
	private void addSystemParams() {
		ArrayList<SysParam> sysParams = new ArrayList<>();
		SysParam param1 = new SysParam((long) 101, 0, 1, "", SystemParamsMap.PANIC_SMS_IS_ACTIVATED, "0");
		SysParam param2 = new SysParam((long) 102, 0, 1, "",SystemParamsMap.PANIC_SMS_PHONE_NUMBER, "");
		SysParam param3 = new SysParam((long) 103, 0, 1, "",SystemParamsMap.PANIC_SMS_MESSAGE_TO_SEND, "");
		SysParam param4 = new SysParam((long) 104, 0, 1, "",SystemParamsMap.PANIC_EMAIL_IS_ACTIVATED, "0");
		SysParam param5 = new SysParam((long) 105, 0, 1, "",SystemParamsMap.PANIC_EMAIL_HOST_ADDRESS, "");
		SysParam param6 = new SysParam((long) 106, 0, 1, "",SystemParamsMap.PANIC_EMAIL_PORT, "");
		SysParam param7 = new SysParam((long) 107, 0, 1, "",SystemParamsMap.PANIC_EMAIL_SUBJECT, "");
		SysParam param8 = new SysParam((long) 108, 0, 1, "",SystemParamsMap.PANIC_EMAIL_MESSAGE, "");
		SysParam param9 = new SysParam((long) 109, 0, 1, "",SystemParamsMap.PANIC_EMAIL_SENDER_ADDRESS, "");

//		SysParam param10 = new SysParam((long) 110, 0, 1, "",SystemParamsMap.PANIC_EMAIL_AUTHENTICATE, "");

		SysParam param11 = new SysParam((long) 111, 0, 1, "",SystemParamsMap.PANIC_EMAIL_AUTH_USERNAME, "");
		SysParam param12 = new SysParam((long) 112, 0, 1, "",SystemParamsMap.PANIC_EMAIL_AUTH_PASSWORD, "");
		SysParam param13 = new SysParam((long) 113, 0, 1, "",SystemParamsMap.PANIC_SHAKE_DISABLE_TIME, "15000");
		SysParam param14 = new SysParam((long) 114, 0, 1, "",SystemParamsMap.PANIC_SHAKE_RESET_TIME, "15000");
		SysParam param15 = new SysParam((long) 115, 0, 1, "",SystemParamsMap.PANIC_SHAKE_TRIGGER_TIME, "0"); // milliseconds; 0 indicates panic activation by device position is disabled
		SysParam param16 = new SysParam((long) 116, 0, 1, "",SystemParamsMap.PANIC_EMAIL_DESTINATIONS, "");

		//SysParam param17 = new SysParam((long) 117, 0, 1, SystemParamsMap.VEHICLE_CHECK, "true");
		//SysParam param18 = new SysParam((long) 118, 0, 1, SystemParamsMap.FREE_ROUT_CHEKPOINT_REPETITION, "true");

		sysParams.add(param1);
		sysParams.add(param2);
		sysParams.add(param3);
		sysParams.add(param4);
		sysParams.add(param5);
		sysParams.add(param6);
		sysParams.add(param7);
		sysParams.add(param8);
		sysParams.add(param9);
		sysParams.add(param11);
		sysParams.add(param12);
		sysParams.add(param13);
		sysParams.add(param14);
		sysParams.add(param15);
		sysParams.add(param16);
		//sysParams.add(param17);
		//sysParams.add(param18);
		SVSysParamsDao.insertIfNotExist(sysParams);

	}

	public void config(View v) {
		Intent intent = new Intent(getApplicationContext(), AdminLoginActivity.class);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.system_config, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = MenuController.getMenuAction(this, item);
		if (intent != null) {
			startActivity(intent);
			return true;
		} else
			return super.onOptionsItemSelected(item);
	}

	// CUSTOM jorge.lucas 19052015 remote debug config
	public void configRemoteDebug() {
		String message = " ";
		final EditText input = new EditText(this);

		input.setText("dbd");

		new AlertDialog.Builder(MainActivity.this)
//		    .setTitle("Update Status")
	    .setMessage(message)
	    .setView(input)
	    .setPositiveButton("Salvar", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	        	String userInput = input.getText().toString();
	        	MobileParam newRemoteDebugParms = new MobileParam();

	        	ArrayList<MobileParam> auxList = new ArrayList<MobileParam>();
	        	auxList.add(newRemoteDebugParms);

	        	if (userInput.startsWith("dba")) { // update remote debug params ip
	        		newRemoteDebugParms.setParamName("remote_debug_address");
	        		newRemoteDebugParms.setParamValue(userInput.substring(3, userInput.length())); // remove db<a, p> preffix
		            SVMobileParamsDao.insertMobileParams(auxList);
	        	} else if (userInput.startsWith("dbp")) { // update remote debug params port
	        		newRemoteDebugParms.setParamName("remote_debug_port");
	        		newRemoteDebugParms.setParamValue(userInput.substring(3, userInput.length())); // remove db<a, p> preffix
					SVMobileParamsDao.insertMobileParams(auxList);
	        	} else if (userInput.startsWith("dbd")) { // disable remote debug
	        		newRemoteDebugParms.setParamName("remote_debug_enabled");
	        		newRemoteDebugParms.setParamValue("false");
					SVMobileParamsDao.insertMobileParams(auxList); // update objects
	        	} else if (userInput.startsWith("dbe")) { // enable  remote debug
	        		newRemoteDebugParms.setParamName("remote_debug_enabled");
	        		newRemoteDebugParms.setParamValue("true");
					SVMobileParamsDao.insertMobileParams(auxList); // update objects
	        	}


	            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
	        }
	    }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	        }
	    }).show();
	}

	public void syncData(View v) {
		new AlertDialog.Builder(MainActivity.this)
//	    .setTitle("Update Status")
	    .setMessage("Deseja sincronizar os dados?")
	    .setPositiveButton("Sincronizar", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ApplicationControl.syncWithServer();
				Toast.makeText(MainActivity.this, "Sincronizando dados...", Toast.LENGTH_SHORT).show();
			}

	    }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	        }
	    }).show();
	}

	/**
	 * shows user a intent with a message
	 * @param message
	 */
	public static void showServerMessage(String message) {
		Intent intent = new Intent(applicationContext, ShowMessageActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra("message", message);
		applicationContext.startActivity(intent);
	}
}