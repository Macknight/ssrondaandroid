package com.br.svconsultoria.svnfcmanager.tools;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.text.TextUtils;

import com.br.svconsultoria.svnfcmanager.util.SVUtil;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class ReadCardTools {

	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
	protected ReadCardTools() {
	}

	private static ReadCardTools instance = new ReadCardTools();

	public static ReadCardTools getInstance() {
		return instance;
	}

	// This method receive the intent and parse the information in the Ndef
	// records into Strings.
	public String readTag(Intent intent) throws Exception {
		Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		String result = "";
		byte[] bTagId = tag.getId();
		result = SVUtil.bytesToHex(bTagId);
		Log.d("[ReadCardTools]", "Serial Number: " + result);
		return result;
	}

	public void resolveIntent(Intent intent) {
		// 1) Parse the intent and get the action that triggered this intent
		String action = intent.getAction();
		// 2) Check if it was triggered by a tag discovered interruption.
		if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)) {
			// 3) Get an instance of the TAG from the NfcAdapter
			Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
			// 4) Get an instance of the Mifare classic card from this TAG
			// intent
			MifareClassic mfc = MifareClassic.get(tagFromIntent);
			byte[] data;

			try { // 5.1) Connect to card
				mfc.connect();
				boolean auth = false;
				String cardData = null;
				// 5.2) and get the number of sectors this card has..and loop
				// thru these sectors
				int secCount = mfc.getSectorCount();
				int bCount = 0;
				int bIndex = 0;
				CharSequence ascii = "";

				auth = mfc.authenticateSectorWithKeyA(1, hexStringToByteArray("A0A1A2A3A4A5"));
				if (auth) {
					bIndex = mfc.sectorToBlock(1);
					data = mfc.readBlock(6);
					String hex = bytesToHex(data);

					if(String.valueOf(data[4]).equals("7")){
						Log.e("authenticateSectorWithKeyA", "Cartâo válido");
					}
				}


				for (int j = 0; j < secCount; j++) {
					// 6.1) authenticate the sector
					auth = mfc.authenticateSectorWithKeyA(j, hexStringToByteArray("A0A1A2A3A4A5"));
					if (auth) {
						// 6.2) In each sector - get the block count
						bCount = mfc.getBlockCountInSector(j);
						bIndex = 0;
						bIndex = mfc.sectorToBlock(j);
						for (int i = 0; i < bCount; i++) {
							// 6.3) Read the block
							data = mfc.readBlock(bIndex);

							String hex = bytesToHex(data);
							String s = System.getProperty("line.separator");
							 ascii = TextUtils.concat(ascii, " ", convertHexToString(hex), s);
							 Log.e("resolveIntent{}", ascii.toString());
							 bIndex++;
						}
					} else { // Authentication failed - Handle it

					}
				}
			} catch (Exception e) {
				 Log.e("resolveIntent", e.getLocalizedMessage());
				// showAlert(3);
				e.printStackTrace();
			}
		}
	}// End of method


	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		try {
			for (int i = 0; i < len; i += 2) {
				data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
			}
		} catch (Exception e) {
			Log.e("hexStringToByteArray", "Argument(s) for hexStringToByteArray(String s)" + "was not a hex string");
		}
		return data;
	}

	public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}

	public String convertStringToHex(String str){

		  char[] chars = str.toCharArray();

		  StringBuffer hex = new StringBuffer();
		  for(int i = 0; i < chars.length; i++){
		    hex.append(Integer.toHexString((int)chars[i]));
		  }

		  return hex.toString();
	  }

	  public String convertHexToString(String hex){

		  StringBuilder sb = new StringBuilder();
		  StringBuilder temp = new StringBuilder();

		  //49204c6f7665204a617661 split into two characters 49, 20, 4c...
		  for( int i=0; i<hex.length()-1; i+=2 ){

		      //grab the hex in pairs
		      String output = hex.substring(i, (i + 2));
		      //convert hex to decimal
		      int decimal = Integer.parseInt(output, 16);
		      //convert the decimal to character
		      sb.append((char)decimal);

		      temp.append(decimal);
		  }
		  System.out.println("Decimal : " + temp.toString());

		  return sb.toString();
	  }
}
