package com.br.svconsultoria.svnfcmanager.model;

public class OccurencePriority {
	
	private long priorityId;
	private String description;
	
	public OccurencePriority(long id, String desc) {
		setPriorityId(id);
		setDescription(desc);
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public long getPriorityId() {
		return priorityId;
	}
	
	public void setPriorityId(long priorityId) {
		this.priorityId = priorityId;
	}
	
}
