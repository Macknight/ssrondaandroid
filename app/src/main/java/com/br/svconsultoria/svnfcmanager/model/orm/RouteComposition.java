package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Entity mapped to table SRTM_ROUTE_COMPOSITION.
 */
@Entity
public class RouteComposition {

    @Id
    private Long routeCompositionId;
    private long routeId;
    private long orderId;
    private Long xId;
    private long statusId;
    private long checkPointId;
    private long stayTime;
    private long travelTime;
    private long timeAllowed;
    private Boolean mandatory;
    @Generated(hash = 632292427)
    public RouteComposition(Long routeCompositionId, long routeId, long orderId,
            Long xId, long statusId, long checkPointId, long stayTime,
            long travelTime, long timeAllowed, Boolean mandatory) {
        this.routeCompositionId = routeCompositionId;
        this.routeId = routeId;
        this.orderId = orderId;
        this.xId = xId;
        this.statusId = statusId;
        this.checkPointId = checkPointId;
        this.stayTime = stayTime;
        this.travelTime = travelTime;
        this.timeAllowed = timeAllowed;
        this.mandatory = mandatory;
    }
    @Generated(hash = 680776110)
    public RouteComposition() {
    }
    public Long getRouteCompositionId() {
        return this.routeCompositionId;
    }
    public void setRouteCompositionId(Long routeCompositionId) {
        this.routeCompositionId = routeCompositionId;
    }
    public long getRouteId() {
        return this.routeId;
    }
    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }
    public long getOrderId() {
        return this.orderId;
    }
    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }
    public Long getXId() {
        return this.xId;
    }
    public void setXId(Long xId) {
        this.xId = xId;
    }
    public long getStatusId() {
        return this.statusId;
    }
    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }
    public long getCheckPointId() {
        return this.checkPointId;
    }
    public void setCheckPointId(long checkPointId) {
        this.checkPointId = checkPointId;
    }
    public long getStayTime() {
        return this.stayTime;
    }
    public void setStayTime(long stayTime) {
        this.stayTime = stayTime;
    }
    public long getTravelTime() {
        return this.travelTime;
    }
    public void setTravelTime(long travelTime) {
        this.travelTime = travelTime;
    }
    public long getTimeAllowed() {
        return this.timeAllowed;
    }
    public void setTimeAllowed(long timeAllowed) {
        this.timeAllowed = timeAllowed;
    }
    public Boolean getMandatory() {
        return this.mandatory;
    }
    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

}
