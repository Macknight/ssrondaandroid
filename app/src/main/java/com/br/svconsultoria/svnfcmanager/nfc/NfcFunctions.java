package com.br.svconsultoria.svnfcmanager.nfc;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.tech.NfcF;

public class NfcFunctions {
	
	public static void enableReadMode(Activity activity, NfcAdapter mAdapter, Context context) {
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, context.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter tagDetected1 = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		IntentFilter[] filters = new IntentFilter[] { tagDetected1 };
		String[][] mTechLists = new String[][] { new String[] { NfcF.class.getName() } };
		mAdapter.enableForegroundDispatch(activity, pendingIntent, filters, mTechLists);
	}
	
	public static void disableReadMode(Activity activity, NfcAdapter mAdapter) {
		mAdapter.disableForegroundDispatch(activity);
	}
}
