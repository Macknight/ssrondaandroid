package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Entity mapped to table SRTM_VIGILANT_PHOTOS.
 */
@Entity
public class VigilantPhotos {

    @Id
    private Long vigilantPhotoId;
    private Long vigilantId;
    private byte[] photo;
    @Generated(hash = 1863606324)
    public VigilantPhotos(Long vigilantPhotoId, Long vigilantId, byte[] photo) {
        this.vigilantPhotoId = vigilantPhotoId;
        this.vigilantId = vigilantId;
        this.photo = photo;
    }
    @Generated(hash = 1775222047)
    public VigilantPhotos() {
    }
    public Long getVigilantPhotoId() {
        return this.vigilantPhotoId;
    }
    public void setVigilantPhotoId(Long vigilantPhotoId) {
        this.vigilantPhotoId = vigilantPhotoId;
    }
    public Long getVigilantId() {
        return this.vigilantId;
    }
    public void setVigilantId(Long vigilantId) {
        this.vigilantId = vigilantId;
    }
    public byte[] getPhoto() {
        return this.photo;
    }
    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

}
