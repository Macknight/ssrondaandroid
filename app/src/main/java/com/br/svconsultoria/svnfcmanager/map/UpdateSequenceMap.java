package com.br.svconsultoria.svnfcmanager.map;

public class UpdateSequenceMap {

	public static final String SRT_CUSTOMERS = "Customer";
	public static final String SRT_MOBILE_PHONES = "MobilePhones";
	public static final String SRT_USERS = "Users";
	public static final String SRT_VIGILANTS = "Vigilant";
	public static final String SRT_RFID_TAGS = "RfidTags";
	public static final String SRT_CHECKPOINTS = "CheckPoint";
	public static final String SRT_ROUTE_COMPOSITION = "RouteComposition";
	public static final String SRT_ROUTES = "Routes";
	public static final String SRT_ROUTE_SCHEDULE = "RouteSchedule";
	public static final String SRT_PRIORITYS = "Priority";
	public static final String SRT_OCCURRENCE_TYPES = "OccurrenceType";
	public static final String SRT_SYS_PARAMS = "SysParam";

	public static final String[] SRT_TABLES = { SRT_CUSTOMERS, SRT_MOBILE_PHONES, SRT_USERS, SRT_VIGILANTS, SRT_RFID_TAGS, SRT_CHECKPOINTS, SRT_ROUTE_COMPOSITION, SRT_ROUTES, SRT_ROUTE_SCHEDULE, SRT_PRIORITYS, SRT_OCCURRENCE_TYPES, SRT_SYS_PARAMS };

}
