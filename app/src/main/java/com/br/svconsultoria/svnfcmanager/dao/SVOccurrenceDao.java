package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.InsertSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.Occurrence;
import com.br.svconsultoria.svnfcmanager.model.orm.OccurrenceDao;

import java.util.ArrayList;

public class SVOccurrenceDao {

	private static final String TAG = "SVOccurrenceDao";

	private static DaoSession session = ApplicationControl.daoSession;

	public static ArrayList<Occurrence> getOccurrences() {
		ArrayList<Occurrence> list = (ArrayList<Occurrence>) session.getOccurrenceDao().queryBuilder().list();
		return list;
	}

	public static ArrayList<Occurrence> getOccurrencesLimit() {
		ArrayList<Occurrence> list = (ArrayList<Occurrence>) session.getOccurrenceDao().queryBuilder().orderAsc(OccurrenceDao.Properties.OccurrenceDt).limit(ApplicationControl.N_OCCURRENCES).list();
		return list;
	}

	public static void deleteCommittedOccurrences(ArrayList<Occurrence> occurrences) {
		session.getOccurrenceDao().deleteInTx(occurrences);
	}

	public static boolean hasOccurrences() {
		long count = session.getOccurrenceDao().queryBuilder().count();
		if (count > 0)
			return true;
		else
			return false;
	}

	public static boolean insertNewOccurrence(Occurrence occurr) {
		Log.d(TAG, "insertNewOccurrence() Insert new Occurrence!");
		long occurrId = SVInsertSequenceDao.getNextId(InsertSequenceMap.SRTM_OCCURRENCE);
		occurr.setOccurrenceId(occurrId);
		if(occurr != null && occurr.getMobileId() != 0 && occurr.getOccurrenceDt() != null && occurr.getVigilantId() != 0
				&& occurr.getPriorityId() != 0 && occurr.getOccurrenceTypeId() != 0)
		session.getOccurrenceDao().insert(occurr);
		else return false;
		Log.d(TAG, "insertNewOccurrence() Finished!");
		return true;
	}
}
