package com.br.svconsultoria.svnfcmanager.dao;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.InsertSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.EventLog;
import com.br.svconsultoria.svnfcmanager.model.orm.EventLogDao;

import java.util.ArrayList;

public class SVEventDao {


    private static DaoSession session = ApplicationControl.daoSession;

    public static ArrayList<EventLog> getEvents() {
        return (ArrayList<EventLog>) session.getEventLogDao().queryBuilder().list();
    }

    public static ArrayList<EventLog> getEventsLimit() {
        return (ArrayList<EventLog>) session.getEventLogDao().queryBuilder().orderAsc(EventLogDao.Properties.EventDt).limit(ApplicationControl.N_EVENTS).list();
    }

    public static void deleteCommittedEvents(ArrayList<EventLog> events) {
        session.getEventLogDao().deleteInTx(events);
    }

    public static boolean hasEvents() {
        if( session.getEventLogDao().queryBuilder().count() > 0){
            return true;
        }
        if(session.getOccurrenceDao().queryBuilder().count()>0){
            return true;
        }
       return false;
    }

    public static boolean insertNewEvent(EventLog event) {
        long eventId = SVInsertSequenceDao.getNextId(InsertSequenceMap.SRTM_EVENT_LOG);
        event.setEventLogId(eventId);
        if (event.getVigilantId() == 0) {
            event.setVigilantId(ApplicationControl.lastLoggedViggilantId);
        }
        if (event.getMobileId() != 0 && event.getEventDt() != null) {

            session.getEventLogDao().insert(event);
            return true;
        }
        return false;
    }
}
