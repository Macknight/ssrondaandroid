package com.br.svconsultoria.svnfcmanager.dao;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.EventLog;
import com.br.svconsultoria.svnfcmanager.model.orm.EventLogDao;
import com.br.svconsultoria.svnfcmanager.model.orm.VehicleCheck;
import com.br.svconsultoria.svnfcmanager.model.orm.VehicleCheckDao;

import java.util.ArrayList;

/**
 * Created by bruno.hernandes on 20/01/2017.
 */
public class SVVehicleCheckDao {

    private static DaoSession session = ApplicationControl.daoSession;

    public static void insertVehicleCheck(VehicleCheck check) {
            session.getVehicleCheckDao().insert(check);
    }

    public static VehicleCheck getOldestVehicleChecks() {
        return  session.getVehicleCheckDao().queryBuilder()
                .orderAsc(VehicleCheckDao.Properties.Date)
                .limit(1)
                .unique();
    }

    public static ArrayList<VehicleCheck> getVehicleChecks() {
        return (ArrayList<VehicleCheck>) session.getVehicleCheckDao().queryBuilder().list();
    }

    public static long getVehicleChecksCount() {
        return session.getVehicleCheckDao().queryBuilder().count();
    }

    public static void deleteCommittedVehicleChecks(ArrayList<VehicleCheck> events) {
        session.getVehicleCheckDao().deleteInTx(events);
    }

    public static boolean hasVehicleChecks() {
        return session.getVehicleCheckDao().queryBuilder().count() > 0;
    }
}
