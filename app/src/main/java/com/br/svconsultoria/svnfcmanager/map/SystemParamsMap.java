package com.br.svconsultoria.svnfcmanager.map;

public class SystemParamsMap {

	//---------------------- MOBILE PARAMETERS -----------------------------------------------------
	public static final String IS_INITIALIZED = "isInitialized";
	public static final String IS_CONFIGURED = "isConfigured";
	public static final String HOST_ADDRESS = "database_url";
	public static final String ADMIN_LOGIN_NAME = "adminLoginName";
	public static final String ADMIN_PASSWORD = "adminPassword";

	public static final String LAST_ROUTE_SCHEDULE_ID = "lastRouteScheduleId";
	public static final String LAST_ORDER_ID = "lastOrderId";
	public static final String VEHICLE_CHECKED = "vehicleChecked";


	public static final String FALL_SOUND_ALLOWED = "fallSoundAllowed";
	public static final String PANIC_SOUND_ALLOWED = "panicSoundAllowed";
	public static final String QR_CODE_ALLOWED = "qrCodeAllowed";
	public static final String ADMIN_MODE_ALLOWED = "adminModeAllowed";

	//---------------------- PARAMETERS RETRIVED FROM SERVER ---------------------------------------

	//PANIC SMS BEHAVIOUR
	public static final String PANIC_SMS_IS_ACTIVATED = "PANIC_SMS_IS_ACTIVATED";
	public static final String PANIC_SMS_PHONE_NUMBER = "PANIC_SMS_PHONE_NUMBER";
	public static final String PANIC_SMS_MESSAGE_TO_SEND = "PANIC_SMS_MESSAGE_TO_SEND";
	//PANIC EMAIL BEHAVIOUR
	public static final String PANIC_EMAIL_IS_ACTIVATED = "PANIC_EMAIL_IS_ACTIVATED";
	public static final String PANIC_EMAIL_HOST_ADDRESS = "PANIC_EMAIL_HOST_ADDRESS";
	public static final String PANIC_EMAIL_PORT = "PANIC_EMAIL_PORT";
	public static final String PANIC_EMAIL_SUBJECT = "PANIC_EMAIL_SUBJECT";
	public static final String PANIC_EMAIL_MESSAGE = "PANIC_EMAIL_MESSAGE";
	public static final String PANIC_EMAIL_SENDER_ADDRESS = "PANIC_EMAIL_SENDER_ADDRESS";
	public static final String PANIC_EMAIL_DESTINATIONS = "PANIC_EMAIL_DESTINATIONS";
	public static final String PANIC_EMAIL_AUTH_USERNAME = "PANIC_EMAIL_AUTH_USERNAME";
	public static final String PANIC_EMAIL_AUTH_PASSWORD = "PANIC_EMAIL_AUTH_PASSWORD";
	//PANIC TIMER
	public static final String PANIC_SHAKE_DISABLE_TIME = "PANIC_SHAKE_DISABLE_TIME";
	public static final String PANIC_SHAKE_RESET_TIME = "PANIC_SHAKE_RESET_TIME";
	public static final String PANIC_SHAKE_TRIGGER_TIME = "PANIC_SHAKE_TRIGGER_TIME";
	/**
	 * Checkpoint distance tolerance between checkpoint GPS coordinates read and Checkpoint coordinates from servlet
	 */
	public static final String CHECKPOINT_DISTANCE_TOLERANCE = "CHECKPOINT_DISTANCE_TOLERANCE";
	/**
	 * Enable/disable checkpoint distance verification
	 */
	public static final String CHECKPOINT_DISTANCE_TOLERANCE_ENABLED = "CHECKPOINT_DISTANCE_TOLERANCE_ENABLED";

	public static final String VEHICLE_CHECK = "VEHICLE_CHECK";
	public static final String FREE_ROUT_CHEKPOINT_REPETITION = "FREE_ROUT_CHEKPOINT_REPETITION";

	public static final String IDLENESS_EVENT_ENABLED = "IDLENESS_EVENT_ENABLED";
	public static final String IDLENESS_EVENT_TIME_MINUTES = "IDLENESS_EVENT_TIME_MINUTES";




}
