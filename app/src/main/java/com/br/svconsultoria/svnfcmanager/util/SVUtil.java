package com.br.svconsultoria.svnfcmanager.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.MainActivity;

import java.math.BigInteger;

@SuppressLint("SimpleDateFormat")
public class SVUtil {

	private static final String TAG = "SVUtil";
	final protected static char[] hexArray = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	public static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		int v;
		for (int j = 0; j < bytes.length; j++) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[(j * 2) + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	public static String toHex(String arg) {
		return String.format("%040x", new BigInteger(arg.getBytes(/* YOUR_CHARSET? */)));
	}

	public static String rot13(String s) {
		String result = "";
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if ((c >= 'a') && (c <= 'm')) {
				c += 13;
			} else if ((c >= 'A') && (c <= 'M')) {
				c += 13;
			} else if ((c >= 'n') && (c <= 'z')) {
				c -= 13;
			} else if ((c >= 'N') && (c <= 'Z')) {
				c -= 13;
			}
			result += c;
		}
		return result;
	}

	public static Intent restartApplication(Context context) {
		ApplicationControl.userSession.invalidateUser();
		Intent i = new Intent(context, MainActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		return i;
	}

	public static ApplicationControl getApp(Activity activity) {
		return (ApplicationControl) activity.getApplication();
	}

	public static void touch(Activity activity) {
		getApp(activity).touch();
		// Log.d(TAG, "User interaction to " + activity.toString());
		// Log.d(TAG, "My Activity Touched");
	}

}
