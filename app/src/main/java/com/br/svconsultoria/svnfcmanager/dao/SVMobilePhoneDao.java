package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.MobilePhone;
import com.br.svconsultoria.svnfcmanager.model.orm.MobilePhoneDao;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;

import java.util.List;

public class SVMobilePhoneDao {


	private static DaoSession session = ApplicationControl.daoSession;

	public static void insertOrUpdateMobilePhones(final List<MobilePhone> list) {
		Log.d("SVMobilePhoneDao", "insertOrUpdateMobilePhones() Starting...");
		session.runInTx(new Runnable() {

			@Override
			public void run() {
				MobilePhoneDao mobilephonedao = session.getMobilePhoneDao();
				mobilephonedao.insertOrReplaceInTx(list);
			}
		});

		long xid = SVUpdateSequenceDao.getXid(UpdateSequenceMap.SRT_MOBILE_PHONES).getxId();;
		for (MobilePhone object : list) {
			Log.d("SVMobilePhoneDao", "MobilePhoneID: " + object.getDescription() + " XID: " + object.getXId());
			if (xid < object.getXId()) {
				xid = object.getXId();
			}
		}
		SVUpdateSequenceDao.updateXId(new UpdateSequence(null, UpdateSequenceMap.SRT_MOBILE_PHONES, xid));

		Log.d("SVMobilePhoneDao", "insertOrUpdateMobilePhones() Finished!");
	}

	public static long getMobileId() {
		Log.d("SVMobilePhoneDao", "getMobileId()");
		long mobileId = 0;
		try {
			mobileId = session.getMobilePhoneDao().queryBuilder().where(MobilePhoneDao.Properties.Imei.eq(ApplicationControl.TELEPHONE_IMEI))
					.where(MobilePhoneDao.Properties.StatusId.eq(1)).uniqueOrThrow().getMobileId();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.d("SVMobilePhoneDao", "getMobileId() mobileId: " + mobileId);
		return mobileId;
	}

	public static void insertNewMobilePhone(MobilePhone phone) {
		Log.d("SVMobilePhoneDao", "insertNewMobilePhone() Insert new MobilePhone!");
		session.getMobilePhoneDao().insertOrReplace(phone);
		Log.d("SVMobilePhoneDao", "insertNewMobilePhone() Finished!");
	}

	public static String getSosNumber() {
		Log.d("SVMobilePhoneDao", "getSosNumber()");
		String sosNumber = null;
		try {
			sosNumber = session.getMobilePhoneDao().queryBuilder().where(MobilePhoneDao.Properties.Imei.eq(ApplicationControl.TELEPHONE_IMEI))
					.where(MobilePhoneDao.Properties.StatusId.eq(1)).unique().getSosNumber();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.d("SVMobilePhoneDao", "getMobileId() mobileId: " + sosNumber);
		return sosNumber;
	}
}
