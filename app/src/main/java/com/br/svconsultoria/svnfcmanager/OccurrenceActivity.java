package com.br.svconsultoria.svnfcmanager;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;


import com.br.svconsultoria.svnfcmanager.dao.SVMobilePhoneDao;
import com.br.svconsultoria.svnfcmanager.dao.SVOccurrenceDao;
import com.br.svconsultoria.svnfcmanager.dao.SVOccurrenceTypesDao;
import com.br.svconsultoria.svnfcmanager.dao.SVPriorityDao;
import com.br.svconsultoria.svnfcmanager.model.orm.Occurrence;
import com.br.svconsultoria.svnfcmanager.services.SilentAlertService;
import com.br.svconsultoria.svnfcmanager.util.SVUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class OccurrenceActivity extends Activity {

    private static final String TAG = "OccurrenceActivity";

    private Button occurrInsertButton = null;
    private EditText description = null;
    private TextView title = null;
    private ImageView photoView = null;
    private TextClock dc1;
    private android.widget.Spinner spinner = null; // Occurrence Type
    private android.widget.Spinner priorityLevel = null; // Priority Level


    private static final int CAMERA_REQUEST = 1888;

    private Bitmap bitmap;

    private byte[] photo = null;
    private long occurrenceTypeId = 0;
    private long occurrencePriorityId = 0;
    private float latitude = 0;
    private float longitude = 0;
    private String comments;

    private long routeId = 0; // if occurrence triggered from a route

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        SVUtil.touch(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_occurrence);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "tt0246m_.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");

        occurrInsertButton = (Button) findViewById(R.id.occurrInsertButton);
        ((Button) findViewById(R.id.occurrInsertButton)).setTypeface(typeface2);
        ((TextView) findViewById(R.id.title)).setTypeface(typeface2);
        // ((TextView) findViewById(R.id.occurenceType)).setTypeface(typeface);
        // ((TextView) findViewById(R.id.priorityLabel)).setTypeface(typeface);
        ((TextView) findViewById(R.id.descriptionLabel)).setTypeface(typeface);
        ((TextClock) findViewById(R.id.digitalClockOccurence)).setTypeface(typeface);

        // RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(300, LinearLayout.LayoutParams.WRAP_CONTENT);
        // layoutParams.setMargins(0, 40, 0, 0);
        // layoutParams.bottomMargin = Gravity.CENTER_HORIZONTAL;
        //
        // occurrInsertButton.setLayoutParams(layoutParams);

        description = (EditText) findViewById(R.id.description);
        ((EditText) findViewById(R.id.description)).setTypeface(typeface);
        spinner = (android.widget.Spinner) findViewById(R.id.occurenceType);
        priorityLevel = (android.widget.Spinner) findViewById(R.id.priorityLevel);


        addSpinnerItems();
        photoView = (ImageView) findViewById(R.id.photo);

        photoView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                startCameraActivity();
            }

        });

        occurrInsertButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                insertOccurrence();
                RouteMainActivity.DOES_OCCURRENCE_EXIST = true;
                FreeRouteActivity.DOES_OCCURRENCE_EXIST = true;
            }

        });


//		startCameraActivity();

    }

    private void addSpinnerItems() {
        spinner.setPrompt("Tipo de Ocorrência");
        List<String> list = SVOccurrenceTypesDao.getOccurrenceTypesDesc();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.custom_spinner, list) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                Typeface externalFont = Typeface.createFromAsset(getAssets(), "tt0246m_.ttf");
                ((TextView) v).setTypeface(externalFont);

                return v;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);

                Typeface externalFont = Typeface.createFromAsset(getAssets(), "tt0246m_.ttf");
                ((TextView) v).setTypeface(externalFont);

                return v;
            }
        };
        dataAdapter.setDropDownViewResource(R.layout.custom_spinner);
        spinner.setAdapter(dataAdapter);

        priorityLevel.setPrompt("Prioridade");
        List<String> list2 = SVPriorityDao.getPriorityLevelDescs();
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, R.layout.custom_spinner, list2) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);

                Typeface externalFont = Typeface.createFromAsset(getAssets(), "tt0246m_.ttf");
                ((TextView) v).setTypeface(externalFont);

                return v;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);

                Typeface externalFont = Typeface.createFromAsset(getAssets(), "tt0246m_.ttf");
                ((TextView) v).setTypeface(externalFont);

                return v;
            }
        };
        dataAdapter2.setDropDownViewResource(R.layout.custom_spinner);
        priorityLevel.setAdapter(dataAdapter2);

        if (getIntent().getExtras() != null)
            this.routeId = getIntent().getExtras().getLong("routeId");

    }

    private void startCameraActivity() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    private void setPhoto() {
        // Bitmap bmp = BitmapFactory.decodeByteArray(photo, 0, photo.length);
        // photoView.setLayoutParams(new RelativeLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight()));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        photoView.setLayoutParams(layoutParams);
        photoView.setImageBitmap(bitmap);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == CAMERA_REQUEST) && (resultCode == RESULT_OK)) {

            try {
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 30, stream);
                saveToInternalStorage(bitmap);
                saveToExternalStorage(bitmap);
                photo = stream.toByteArray();
                setPhoto();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    private void scanStorage() {
        MediaScannerConnection.scanFile(getBaseContext(),
                new String[]{Environment.getExternalStorageDirectory() + File.separator + "Pictures" + File.separator
                + "SSRonda"}, null, null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            File SSRonda = new File(Environment.getExternalStorageDirectory() + File.separator + "Pictures" + File.separator
                    + "SSRonda");
            final Uri contentUri = Uri.fromFile(SSRonda);
            scanIntent.setData(contentUri);
            sendBroadcast(scanIntent);

        } else {
            final Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory()));
            sendBroadcast(intent);
        }




    }


    private void saveToExternalStorage(Bitmap bmi){
        if(isExternalStorageWritable()){
            File photoDirectory = getAlbumStorageDir("SSRonda");
            if(!photoDirectory.exists()){
                photoDirectory.mkdirs();
            }
            int fileNumber = 0;
            File photoFile = new File(photoDirectory,"occurrence" + fileNumber + ".jpg");
            while(photoFile.exists()){
                fileNumber++;
                photoFile = new File(photoDirectory,"occurrence" + fileNumber + ".jpg") ;
            }
            FileOutputStream fos = null;

            try{
                fos = new FileOutputStream(photoFile);
                bmi.compress(Bitmap.CompressFormat.PNG,30,fos);
                if(!photoFile.exists()){
                    System.out.println("arquivo não criado");
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }finally{
                try {
                    fos.close();
                    scanStorage();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

    }
        public File getAlbumStorageDir(String albumName) {
            // Get the directory for the user's public pictures directory.
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), albumName);
            if (!file.mkdirs()) {
                Log.e("Num deu:", "Directory not created");
            }
            return file;
        }

        private String saveToInternalStorage(Bitmap bmi){
            ContextWrapper cw =  new ContextWrapper(getApplicationContext());

            File directory = cw.getDir("SSRonda", Context.MODE_WORLD_READABLE);
            File dir2 = new File(Environment.getDataDirectory() + File.separator + "SSRonda");
            File dir3  =  getFilesDir();
            if(!dir3.exists()){
                directory.mkdirs();
            }
            File myPath = new File(dir3,"occurrence.jpg");
            FileOutputStream fos = null;

            try{
                fos = new FileOutputStream(myPath);
            bmi.compress(Bitmap.CompressFormat.PNG,30,fos);
            if(!myPath.exists()){
                System.out.println("arquivo criado");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally{
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private void insertOccurrence() {


        Log.d(TAG, "insertOccurrence()");

        occurrenceTypeId = SVOccurrenceTypesDao.getOccurenceTypeIdByDescription(String.valueOf(spinner.getSelectedItem()));
        occurrencePriorityId = SVPriorityDao.getPriorityIdByDescription(String.valueOf(priorityLevel.getSelectedItem()));

        Log.d(TAG, "OccurrenceTypeId: " + String.valueOf(spinner.getSelectedItem()) + " PriorityId: " + priorityLevel.getSelectedItem());

        comments = description.getText().toString();

        if (comments.isEmpty()) {
            Toast.makeText(this, "O comentário é obrigatório para gerar a ocorrência.", Toast.LENGTH_LONG).show();
            return;
        }

        if (SilentAlertService.gps.hasAccuracy()) {
            latitude = SilentAlertService.gps.getLatitude();
            longitude = SilentAlertService.gps.getLongitude();
        }


        Occurrence occurr = new Occurrence(null, SVMobilePhoneDao.getMobileId(), ApplicationControl.userSession.getLoggedVigilant().getVigilantId(),
        occurrenceTypeId,occurrencePriorityId, photo, new Date(), latitude, longitude, comments, null);
        Log.d(TAG, "occurrId: " + occurrenceTypeId);

        if(!SVOccurrenceDao.insertNewOccurrence(occurr)) {
            ApplicationControl.handleMessage("Erro ao inserir ocorrência");
        }

        if (UserValidationActivity.numberOccurrence != null) {
            UserValidationActivity.numberOccurrence.setText(String.valueOf(SVOccurrenceDao.getOccurrences().size()));
        }

        Toast.makeText(this, "Ocorrência inserida com sucesso!", Toast.LENGTH_LONG).show();

        prepareAndExecuteSyncTask();

        onBackPressed();
    }

    private void prepareAndExecuteSyncTask() {
        Log.d(TAG, "prepareAndExecuteSyncTask()");
        ApplicationControl.startSend();
        // if (ApplicationControl.SEND_FINISHED) {
        // Toast.makeText(this, "Enviando para o servidor...", Toast.LENGTH_LONG).show();
        // ApplicationControl.startSend();
        // }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.occurrence, menu);
        return true;
    }


}
