package com.br.svconsultoria.svnfcmanager.http.occurrrence;

import android.os.AsyncTask;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.UserValidationActivity;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVOccurrenceDao;
import com.br.svconsultoria.svnfcmanager.http.HttpAuthenticate;
import com.br.svconsultoria.svnfcmanager.map.HttpConnAction;
import com.br.svconsultoria.svnfcmanager.model.Response;
import com.br.svconsultoria.svnfcmanager.model.orm.Occurrence;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;

import java.io.UnsupportedEncodingException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class OccurrenceSenderThread extends AsyncTask<Void, Void, Void> {

	private String TAG = "OccurrenceSenderThread";
	private static int AUTHENTICATION_CONNECTION_TIMEOUT = 30000;
	private static int AUTHENTICATION_SOCKET_TIMEOUT = 30000;

	private ArrayList<Occurrence> list = null;

	@Override
	protected Void doInBackground(Void... params) {

		final HttpPost httppost = new HttpPost();

		Log.d(TAG, "doInBackground() - Starting connection with server");
		String response;
		Gson gson = new Gson();

		if (HttpAuthenticate.authenticate() || true) {

			String uri = HttpConnAction.HTTP + SVMobileParamsDao.getHostParam() + HttpConnAction.OCCURRENCES_URI;
			Log.d(TAG, "sendOccurrences() - URI: " + uri);

			List<NameValuePair> args = new ArrayList<NameValuePair>();
			args.add(new BasicNameValuePair(HttpConnAction.ACTION_FIELD, HttpConnAction.INSERT_VALUE));
			list = SVOccurrenceDao.getOccurrencesLimit();

			gson = new GsonBuilder().setDateFormat("dd-MM-yyyyHH-mm-ss").create();
			String json = gson.toJson(list);

			Log.d(TAG, "sendOccurrences() - JSON Occurrences ListSize: " + json.length() + " json: " + json);
			final byte[] utf8Bytes;
			try {
				utf8Bytes = json.getBytes("UTF-8");
				System.out.println(utf8Bytes.length);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			Log.d(TAG, "sendOccurrences() - JSON Occurrences ListSize: " + json.length() + " json: " + json);
			args.add(new BasicNameValuePair(HttpConnAction.LIST_FIELD, json.toString()));

			try {
				httppost.setURI(new URI(uri));
			} catch (URISyntaxException e1) {
				e1.printStackTrace();
			}

			try {
				httppost.setEntity(new UrlEncodedFormEntity(args));
			} catch (UnsupportedEncodingException e) {
				ApplicationControl.handleMessage("HTTP: Formato de dados não suportado");
				e.printStackTrace();
			}

			HttpClient httpclient = ApplicationControl.httpclient;
			HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), AUTHENTICATION_CONNECTION_TIMEOUT);
			HttpConnectionParams.setSoTimeout(httpclient.getParams(), AUTHENTICATION_SOCKET_TIMEOUT);

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = "";
			try {
				httpclient.getConnectionManager().closeExpiredConnections();
				responseBody = httpclient.execute(httppost, responseHandler);
				Log.d(TAG, "doInBackground() - Response: " + responseBody);

				Response resp = gson.fromJson(responseBody, Response.class);

				if ((resp == null) || resp.equals("")) {
					Log.d(TAG, "doInBackground() - Response from server is empty. Exiting...");
					throw new NullPointerException();
				}
				resp.setType(HttpConnAction.OCCURRENCES_URI);
				resp.setList(list);

				process(resp);

				response = "true";

			} catch (ConnectTimeoutException cte) {
				Log.d(TAG, "authenticate() - Server Response Timeout. (ConnectTimeoutException)");
				response = "Verifique o funcionamento do servidor: tempo esgotado de resposta - " + SVMobileParamsDao.getHostParam();
				// cte.printStackTrace();
			} catch (JsonSyntaxException jse) {
				Log.d(TAG, "authenticate() - Malformed Json Response. (JsonSyntaxException)");
				response = " Malformed Json Response";
				// jse.printStackTrace();
			} catch (UnknownHostException uhe) {
				Log.d(TAG, "authenticate() - Invalid host address. (UnknownHostException)");
				response = "Verifique o endereço do servidor: endereço inválido do servidor - " + SVMobileParamsDao.getHostParam();
				uhe.printStackTrace();
			} catch (IllegalStateException ise) {
				Log.d(TAG, "authenticate() - Invalid host address. (IllegalStateException)");
				response = "Verifique o endereço do servidor: endereço inválido do servidor - " + SVMobileParamsDao.getHostParam();
				// ise.printStackTrace();
			} catch (HttpResponseException hre) {
				Log.d(TAG, "authenticate() - Invalid server response. (HttpResponseException)");
				response = "Verifique o funcionamento do servidor: resposta inválida do servidor - " + SVMobileParamsDao.getHostParam();
				// hre.printStackTrace();
			} catch (SocketException se) {
				Log.d(TAG, "authenticate() - Socket error. (SocketException)");
				response = "Verifique o endereço do servidor: erro de socket  - " + SVMobileParamsDao.getHostParam();
				se.printStackTrace();
			} catch (NullPointerException npe) {
				Log.d(TAG, "authenticate() - Empty server response. (NullPointerException)");
				response = "Verifique o endereço do servidor: falha de comunicação com o servidor - " + SVMobileParamsDao.getHostParam();
				// npe.printStackTrace();
			} catch (SocketTimeoutException ste) {
				Log.d(TAG, "authenticate() - Empty server response. (SocketTimeoutException)");
				response = "Verifique o endereço do servidor: falha de comunicação com o servidor - " + SVMobileParamsDao.getHostParam();
				// ste.printStackTrace();
			} catch (Exception e) {
				if ((e != null) && (e.getMessage() != null)) {
					Log.d(TAG, "authenticate() - " + e.toString());
					response = "Verifique se o servidor está ativo e ou o endereço IP está correto: " + e.getMessage();
					e.printStackTrace();
				} else {
					response = "Verifique se o servidor está ativo e ou o endereço IP está correto.";
				}

			}
			Log.d(TAG, "authenticate() - Finishing...");
			if (response.equals("true")) {
				ApplicationControl.IS_SERVER_ON = true;
			} else {
				ApplicationControl.IS_SERVER_ON = false;
			}
			UserValidationActivity.message = response;
			ApplicationControl.finishSendOccurrences();

		} else {
			// ApplicationControl.handleMessage("Não foi possível autenticar. Operação cancelada.");
			ApplicationControl.finishSendOccurrences();
		}

		ApplicationControl.IS_SYNC_OCCURRENCES = false;
		return null;
	}

	public void process(Response response) {
		Log.d(TAG, "process()");
		if (response.getSuccess()) {
			Log.d(TAG, "occurrenceResponse() -  Occurrences uploaded successfully");
			Log.d(TAG, "occurrenceResponse() -  Occurrences List size: " + response.getList().size());
			SVOccurrenceDao.deleteCommittedOccurrences((ArrayList<Occurrence>) response.getList());
			ApplicationControl.finishSendOccurrences();
			Log.d(TAG, "occurrenceResponse() -  Occurrences deleted successfully");
			ApplicationControl.stAppLastDataSendHour = ApplicationControl.getStrCurrentDateHour("HH:mm:ss");
		} else {
			Log.d(TAG, "process() - Transaction failed. This will try again later...");
		}
	}


	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if (UserValidationActivity.numberOccurrence != null) {
			UserValidationActivity.numberOccurrence.setText(String.valueOf(SVOccurrenceDao.getOccurrences().size()));
		}
		if (UserValidationActivity.tvServer != null) {
			if (ApplicationControl.IS_SERVER_ON) {
				UserValidationActivity.tvServer.setText("ON");

			} else {
				UserValidationActivity.tvServer.setText("OFF");
			}
		}
	}

}
