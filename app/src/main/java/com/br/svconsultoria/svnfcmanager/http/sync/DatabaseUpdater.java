package com.br.svconsultoria.svnfcmanager.http.sync;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVUpdateSequenceDao;
import com.br.svconsultoria.svnfcmanager.map.HttpConnAction;
import com.br.svconsultoria.svnfcmanager.model.Response;
import com.br.svconsultoria.svnfcmanager.model.XId;
import com.google.gson.Gson;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class DatabaseUpdater {
	protected DatabaseUpdater() {}

	private static DatabaseUpdater instance = new DatabaseUpdater();

	private Response resp = new Response();
	private HttpPost post = new HttpPost();

	public static DatabaseUpdater getIntance() {
		instance = new DatabaseUpdater();
		return instance;
	}

	private static final String TAG = "DatabaseUpdater";

	public void updateAllTables() throws URISyntaxException {

		initiate();

		Log.d(TAG, "updateAllTables() - Starting server verification for tables update");

		Gson gson = new Gson();

		String uri = HttpConnAction.HTTP + SVMobileParamsDao.getHostParam() + HttpConnAction.UPDATE_CHECK_URI;
		Log.d(TAG, "updateAllTables() - URI: " + uri);

		if (!(uri.equals("") || (uri == null))) {
			List<XId> list = SVUpdateSequenceDao.getUpdateObjects();
			String json = gson.toJson(list);
			Log.d(TAG, "updateAllTables() - JSON: " + json);

			List<NameValuePair> args = new ArrayList<NameValuePair>();
			args.add(new BasicNameValuePair(HttpConnAction.ACTION_FIELD, HttpConnAction.UPDATE_VALUE));
			args.add(new BasicNameValuePair(HttpConnAction.LIST_FIELD, json));

			resp.setType(HttpConnAction.UPDATE_CHECK_URI);

			post.setURI(new URI(uri));
			setEntities(args);

			Log.d(TAG, "updateAllTables() - Parameters had been set.");

			try {

				DatabaseUpdaterThread call = new DatabaseUpdaterThread();
				call.setResponse(resp);
				call.setHttpPost(post);
				Log.d(TAG, "updateAllTables() - Execute!");
				call.execute();
			} catch (NullPointerException e) {
				Log.d(TAG, "updateAllTables() - The necessary variables were not initialized. Closing HttpServerCall");
				return;
			} catch (Exception e) {
				e.printStackTrace();
				Log.d(TAG, "updateAllTables()- Exception: " + e.getMessage());
				return;
			}

		} else {
			ApplicationControl.handleMessage("Configuração inicial não encontrada.");
			Log.d(TAG, "updateAllTables() - The necessary variables were not initialized. Closing " + TAG);
			ApplicationControl.finishSynch();
		}

	}

	private void initiate() {
		resp = new Response();
		post = new HttpPost();
	}

	private void setEntities(List<NameValuePair> pairs) {
		try {
			post.setEntity(new UrlEncodedFormEntity(pairs));
		} catch (UnsupportedEncodingException e) {
			ApplicationControl.handleMessage("HTTP: Formato de dados não suportado");
			e.printStackTrace();
		}
	}

	public void updateTable(String table) throws URISyntaxException {
		initiate();
		Log.d(TAG, "updateTable() - Starting server verification for tables update. Table: " + table);
		// String uri = HttpConnAction.DATABASE_URL + HttpConnAction.UPDATE_TABLES_URI;
		String uri = HttpConnAction.HTTP + SVMobileParamsDao.getHostParam() + HttpConnAction.UPDATE_TABLES_URI;
		resp.setType(HttpConnAction.UPDATE_TABLES_URI);
		resp.setTableName(table);
		List<NameValuePair> args = new ArrayList<NameValuePair>();
		args.add(new BasicNameValuePair(HttpConnAction.ACTION_FIELD, table));
		args.add(new BasicNameValuePair(HttpConnAction.XID_FIELD, String.valueOf(SVUpdateSequenceDao.getXid(table).getxId())));
		post.setURI(new URI(uri));
		setEntities(args);
		Log.d(TAG, "updateTable() - Parameters has been set.");

		try {

			DatabaseUpdaterThread call = new DatabaseUpdaterThread();
			call.setResponse(resp);
			call.setHttpPost(post);
			Log.d(TAG, "updateAllTables() - Execute!");
			call.execute();
		} catch (NullPointerException e) {
			Log.d(TAG, "updateAllTables() - The necessary variables were not initialized. Closing HttpServerCall");
			return;
		} catch (Exception e) {
			e.printStackTrace();
			Log.d(TAG, "updateAllTables()- Exception: " + e.getMessage());
			return;
		}
	}

}
