package com.br.svconsultoria.svnfcmanager.model;

import com.br.svconsultoria.svnfcmanager.model.orm.Customer;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;

import java.sql.Date;

public class XId {

	private int xId;
	private String tableId;
	private Customer customerId;
	private Date lastDt;

	public XId(UpdateSequence updateSequence) {
		xId = (int) updateSequence.getXid();
		tableId = updateSequence.getTableName();
	}

	public int getxId() {
		return xId;
	}

	public void setxId(int xId) {
		this.xId = xId;
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public Customer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Customer customerId) {
		this.customerId = customerId;
	}

	public Date getLastDt() {
		return lastDt;
	}

	public void setLastDt(Date lastDt) {
		this.lastDt = lastDt;
	}

}
