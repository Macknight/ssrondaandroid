package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by bruno.hernandes on 20/01/2017.
 */
@Entity
public class MobileParam {

    @Id(autoincrement = true)
    private Long id; //greendao entities must have a long or Long property as their primary key.
    @Index(unique = true)
    private String paramName;
    private String paramValue;
    @Generated(hash = 660317960)
    public MobileParam(Long id, String paramName, String paramValue) {
        this.id = id;
        this.paramName = paramName;
        this.paramValue = paramValue;
    }
    @Generated(hash = 1768647448)
    public MobileParam() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getParamName() {
        return this.paramName;
    }
    public void setParamName(String paramName) {
        this.paramName = paramName;
    }
    public String getParamValue() {
        return this.paramValue;
    }
    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }
    public void setId(Long id) {
        this.id = id;
    }

}
