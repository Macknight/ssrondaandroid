package com.br.svconsultoria.svnfcmanager;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.br.svconsultoria.svnfcmanager.dao.SVSysParamsDao;
import com.br.svconsultoria.svnfcmanager.mail.Mail;
import com.br.svconsultoria.svnfcmanager.map.EventTypeMap;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.services.SilentAlertService;
import com.br.svconsultoria.svnfcmanager.sms.SmsFunctions;
import com.br.svconsultoria.svnfcmanager.tools.EventBuilder;

import java.util.Timer;
import java.util.TimerTask;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class PanicConfirmationActivity extends Activity {

	private Timer smsTimer;

	private Timer mailTimer;
	private int mailAttempt = 0;
	private Mail mail;
	private TimerTask mailTask = new TimerTask() {
		@Override
		public void run() {
			if(mailAttempt <5) {
				try {

					mail.send();
					Log.d(TAG, "EMAIL SENT!!!");
					cancel();
					mailTimer.cancel();
				} catch (Exception e) {
					// ApplicationControl.handleMessage("Email was not sent.");
					e.printStackTrace();
					Log.d(TAG, "EMAIL NOT SENT, ATTEMPT " + mailAttempt + "!!!");
					mailAttempt++;
				}
			}else{
				cancel();
				mailTimer.cancel();
			}

		}
	};

	private static final String TAG = "PanicConfirmationActivity";

	private TextView timerView = null;
	private CountDownTimer cTimer = null;
	private Button cancelPanicButton = null;
	private SmsFunctions sms = null;

	private boolean isSmsActivated = false;
	private boolean isEmailActivated = false;



	private String sPanicShakeDisableTime;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_panic_confirmation);
		setFinishOnTouchOutside(false);

		sPanicShakeDisableTime = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_SHAKE_DISABLE_TIME).getValue();

		String sSmsActivated = "0";
		try {
			sSmsActivated = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_SMS_IS_ACTIVATED).getValue();
		} catch (Exception e) {
			e.printStackTrace();
		}


		try{
			isSmsActivated = (Long.parseLong(sSmsActivated.trim()) == 1);
		}catch (NumberFormatException e){
			//noop
		}


		if (isSmsActivated) {
			sms = new SmsFunctions(this, this);
		}

		String sEmailActivated = "0";
		try {
			sEmailActivated = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_EMAIL_IS_ACTIVATED).getValue();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try{
			isEmailActivated = (Long.parseLong(sEmailActivated.trim()) == 1);
		}catch (NumberFormatException e){
			//noop
		}

		timerView = (TextView) findViewById(R.id.timer);
		cancelPanicButton = (Button) findViewById(R.id.cancelPanicButton);
		cancelPanicButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				cTimer.cancel();
				SilentAlertService.bShakeActivated = false;
				SilentAlertService.shakeTime = 0;
				Vibrator vibrator;
				vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				vibrator.vibrate(300);
				createPanicCancelEvent();
				finish();
			}
		});

		cTimer = new CountDownTimer(Long.parseLong(sPanicShakeDisableTime), 1000) {
			@Override
			public void onTick(long millisUntilFinished) {
				timerView.setText("Tempo restante: 00:" + ((millisUntilFinished / 1000) >= 10 ? "" : "0") + (millisUntilFinished / 1000));
			}

			@Override
			public void onFinish() {
				onPanicTrigger();
				Vibrator vibrator;
				vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				vibrator.vibrate(500);
				finish();
			}
		};
		//
		cTimer.start();

	}

	private void onPanicTrigger() {
		createPanicEvent();

		if (isSmsActivated) {
			sms.sendSMS();
			sms.unregisterReceivers();
		}
// Email is sent by the server
//		if (isEmailActivated) {
//
//
//			if(mailTimer!=null){
//				mailTimer.cancel();
//			}
//			mailTimer = new Timer();
//			mail = new Mail();
//			mailAttempt=0;
//			mailTimer.schedule(mailTask, 0,10000);
//		}
	}

	private void createPanicEvent() {
		EventBuilder.createPanicEvent(EventTypeMap.PANIC_SHAKE_EVENT, "");
		prepareAndExecuteSyncTask();
	}

	private void createPanicCancelEvent() {
		EventBuilder.createPanicEvent(EventTypeMap.PANIC_CANCEL_EVENT, "");
		prepareAndExecuteSyncTask();
	}


	private void prepareAndExecuteSyncTask() {
		Log.d(TAG, "prepareAndExecuteSyncTask()");
		ApplicationControl.startSend();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.panic_confirmation, menu);
		return true;
	}

}
