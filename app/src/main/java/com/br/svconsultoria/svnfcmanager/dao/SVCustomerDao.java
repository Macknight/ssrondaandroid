package com.br.svconsultoria.svnfcmanager.dao;

import android.database.Cursor;
import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.Customer;
import com.br.svconsultoria.svnfcmanager.model.orm.CustomerDao;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;

import java.util.ArrayList;

public class SVCustomerDao {

	private static DaoSession session = ApplicationControl.daoSession;

	public static void insertOrUpdateCustomers(final ArrayList<Customer> list) {
		Log.d("SVCustomerDao", "insertOrUpdateCustomers() Starting...");
		session.runInTx(new Runnable() {

			@Override
			public void run() {
				CustomerDao customerdao = session.getCustomerDao();
				customerdao.insertOrReplaceInTx(list);
				long xid = SVUpdateSequenceDao.getXid(UpdateSequenceMap.SRT_CUSTOMERS).getxId();
				for (Customer c : list) {
					if (xid < c.getXId()) {
						Log.e("SVCustomerDAO", "Received customer " + c.getCustomerName());
						xid = c.getXId();
					}
				}
				SVUpdateSequenceDao.updateXId(new UpdateSequence(null, UpdateSequenceMap.SRT_CUSTOMERS, xid));
			}
		});
		Log.d("SVCustomerDao", "insertOrUpdateCustomers() Finished!");
	}

	public static ArrayList<Customer> getCustomerList() {
		ArrayList<Customer> list = (ArrayList<Customer>) session.getCustomerDao().queryBuilder().list();
		return list;
	}

	public static ArrayList<String> getCustomerNameList() {
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<Customer> customers = getCustomerList();
		for (Customer c : customers) {
			names.add(c.getCustomerName());
		}
		return names;
	}


//	public static ArrayList<Customer> getCustomerList(long routeId) {
//		Log.d("CustomerFilter", "SVCustomerDao.getCustomerList routeId: " + routeId);
//		ArrayList<Customer> list = new ArrayList<>();
//
//			Cursor c = session.getDatabase().rawQuery(" select cs.customer_id as CustomerId, cs.x_id as XId, cs.status_id as StatusId, cs.customer_cnpj as CustomerCNPJ, cs.customer_name as CustomerName, cs.phone as Phone " +
//					" from srtm_routes r, srtm_route_composition rc, srtm_checkpoints ckp, srtm_customers cs " +
//					" where r.route_id=rc.route_id and rc.check_point_id = ckp.check_point_id and ckp.customer_id = cs.customer_id " +
//					" and r.route_id = " + routeId +
//					" group by cs.customer_id ", null);
//
//			if (c.moveToFirst()) {
//				do {
//					list.add(new Customer(c.getLong(0), c.getLong(1),  c.getLong(2), c.getString(3), c.getString(4), c.getString(5)));
//				} while (c.moveToNext());
//			}
//
//		return list;
//	}

//	public static ArrayList<String> getCustomerNameList(long routeId) {
//		ArrayList<String> names = new ArrayList<String>();
//		ArrayList<Customer> customers = getCustomerList(routeId);
//		for (Customer c : customers) {
//			names.add(c.getCustomerName());
//		}
//		return names;
//	}

	public static Long getCustomerId(String customerName) {

//		Customer p = session.getCustomerDao().queryBuilder().where(Properties.CustomerName.eq(customerName)).uniqueOrThrow();
//		return p.getCustomerId();
		return  null;
	}
}
