package com.br.svconsultoria.svnfcmanager;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.br.svconsultoria.svnfcmanager.model.ComplexOccurrence;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class ComplexOccurrenceActivity extends Activity {
    /*
    organ_switch;
    birthday1_value;
    birthday2_value;
    */
    boolean isOrganShowing = false;


    //might be set or not
    private int routeId;
    private static final int CAMERA_REQUEST = 1888;
    //variable to control which  photo will be set
    private static int whichPhoto = 0;

    public static Button btnHour;
    public static Button btnDate;

    private static  Calendar hourDate;


    private Bitmap  photo1;
    private Bitmap  photo2;
    private Bitmap  photo3;
    private Bitmap  photo4;

    private ImageButton btnPhoto1;
    private ImageButton btnPhoto2;
    private ImageButton btnPhoto3;
    private ImageButton btnPhoto4;


    private Bitmap bitmap;
    EditText gerencia_value;
    EditText occurrence_place_value;
    EditText register_number_value;
    EditText code_value;
    EditText tipicidade_value;
    EditText graduation_value;
    //occurrence general info
    EditText emitente_value;
    EditText   matricula_value;
    EditText summary_value;
    EditText   description_value;
    EditText  security_action_value;
    EditText  public_organ_value;
    //involved #1
    EditText name1_value;
    EditText registration1_value;
    EditText cpf1_value;
    EditText identity1_value;
    EditText organ1_value;
    EditText cnh1_value;
    EditText category1_value;
    EditText validade1_value;
    EditText escolaridade1_value;
    EditText company1_value;
    EditText function1_value;
    EditText address1_value;
    EditText qualification1_value;
    EditText tel1_value;

    //involved #2
    EditText name2_value;
    EditText registration2_value;
    EditText identidade2_value;
    EditText orgao2_value;
    EditText cnh2_value;
    EditText categoria2_value;
    EditText validade2_value;
    EditText escolaridade2_value;
    EditText company2_value;
    EditText function2_value;
    EditText address2_value;
    EditText qualification2_value;
    EditText tel2_value;
    //car #1
    EditText plate1_value;
    EditText model1_value;
    EditText year1_value;
    EditText proprietary1_value;
    EditText car_company1_value;
    //car #2
    EditText plate2_value;
    EditText model2_value;
    EditText year2_value;
    EditText proprietary2_value;
    EditText car_company2_value;
    //misc
    EditText util_info_value;

    TextView qual;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complex_occurrence);

        initializeFields();
        hourDate = Calendar.getInstance();

    }

    private void initializeFields() {


        qual = (TextView) findViewById(R.id.which_label);
        qual.setAlpha(0.0f);
        gerencia_value = (EditText)findViewById(R.id.gerencia_value);

        occurrence_place_value         = (EditText)findViewById(R.id.occurrence_place_value);
        register_number_value          = (EditText)findViewById(R.id.register_number_value);
        code_value                     = (EditText)findViewById(R.id.code_value);
        tipicidade_value               = (EditText)findViewById(R.id.tipicidade_value);
        graduation_value               = (EditText)findViewById(R.id.graduation_value);
        emitente_value                 = (EditText)findViewById(R.id.emitente_value);

        matricula_value                = (EditText)findViewById(R.id.matricula_value);
        summary_value                  = (EditText)findViewById(R.id.summary_value);
        description_value              = (EditText)findViewById(R.id.description_value);
        security_action_value          = (EditText)findViewById(R.id.security_action_value);
        public_organ_value             = (EditText)findViewById(R.id.public_organ_value);
        public_organ_value.setAlpha(0.0f);
        name1_value                    = (EditText)findViewById(R.id.name1_value);

        registration1_value            = (EditText)findViewById(R.id.registration1_value);
        cpf1_value                     = (EditText)findViewById(R.id.cpf1_value);
        identity1_value                = (EditText)findViewById(R.id.identity1_value);
        organ1_value                   = (EditText)findViewById(R.id.organ1_value);
        cnh1_value                     = (EditText)findViewById(R.id.cnh1_value);
        category1_value                = (EditText)findViewById(R.id.category1_value);
        validade1_value                = (EditText)findViewById(R.id.validade1_value);
        escolaridade1_value            = (EditText)findViewById(R.id.escolaridade1_value);
        company1_value                 = (EditText)findViewById(R.id.company1_value);
        function1_value                = (EditText)findViewById(R.id.function1_value);
        address1_value                 = (EditText)findViewById(R.id.address1_value);
        qualification1_value           = (EditText)findViewById(R.id.qualification1_value);
        tel1_value                     = (EditText)findViewById(R.id.tel1_value);
        name2_value                    = (EditText)findViewById(R.id.name2_value);

        registration2_value            = (EditText)findViewById(R.id.registration2_value);
        identidade2_value              = (EditText)findViewById(R.id.identidade2_value);
        orgao2_value                   = (EditText)findViewById(R.id.orgao2_value);
        cnh2_value                     = (EditText)findViewById(R.id.cnh2_value);
        categoria2_value               = (EditText)findViewById(R.id.categoria2_value);
        validade2_value                = (EditText)findViewById(R.id.validade2_value);
        escolaridade2_value            = (EditText)findViewById(R.id.escolaridade2_value);
        company2_value                 = (EditText)findViewById(R.id.company2_value);
        function2_value                = (EditText)findViewById(R.id.function2_value);
        address2_value                 = (EditText)findViewById(R.id.address2_value);
        qualification2_value           = (EditText)findViewById(R.id.qualification2_value);
        tel2_value                     = (EditText)findViewById(R.id.tel2_value);
        plate1_value                   = (EditText)findViewById(R.id.plate1_value);
        model1_value                   = (EditText)findViewById(R.id.model1_value);
        year1_value                    = (EditText)findViewById(R.id.year1_value);
        proprietary1_value             = (EditText)findViewById(R.id.proprietary1_value);
        car_company1_value             = (EditText)findViewById(R.id.car_company1_value);
        plate2_value                   = (EditText)findViewById(R.id.plate2_value);
        model2_value                   = (EditText)findViewById(R.id.model2_value);
        year2_value                    = (EditText)findViewById(R.id.year2_value);
        proprietary2_value             = (EditText)findViewById(R.id.proprietary2_value);
        car_company2_value             = (EditText)findViewById(R.id.car_company2_value);
        util_info_value                = (EditText)findViewById(R.id.util_info_value);

        btnPhoto1 = (ImageButton)findViewById(R.id.imgBtn1);

        btnPhoto2= (ImageButton)findViewById(R.id.imgBtn2);

        btnPhoto3= (ImageButton)findViewById(R.id.imgBtn3);

        btnPhoto4 = (ImageButton)findViewById(R.id.imgBtn4);


        btnHour = (Button)findViewById(R.id.hour_value);

        btnDate  = (Button)findViewById(R.id.date_value);

    }


    public void cancelClick(View view){
        finish();
    }

    public void cameraClick(View view){
        if(view.getId()==R.id.imgBtn1){
            whichPhoto = 1;
           startCameraActivity();
        }
        if(view.getId()==R.id.imgBtn2){
            whichPhoto = 2;
            startCameraActivity();
        }
        if(view.getId()==R.id.imgBtn3){
            whichPhoto = 3;
            startCameraActivity();
        }
        if(view.getId()==R.id.imgBtn4){
            whichPhoto = 4;
            startCameraActivity();
        }

    }

    public void handleOrganSwitch(View view){
        Switch s = (Switch) view;

        if(!isOrganShowing){
            public_organ_value.setAlpha(1f);
            qual.setAlpha(1f);
            isOrganShowing = true;
        }else{
            public_organ_value.setAlpha(0f);
            qual.setAlpha(0f);
            isOrganShowing = false;
        }
    }
    public void finishClick(View view){

        ComplexOccurrence co = new ComplexOccurrence();
        System.out.println("finishClick");
        finish();
}

    public void datePicker(View view){

    DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getFragmentManager(), "Escolha uma data");
        System.out.println("datePicker clicked");
}


    public void hourPicker(View view){
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "Escolha hora:");
    }

    private void startCameraActivity() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode == CAMERA_REQUEST) && (resultCode == RESULT_OK)) {

            try {
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 30, stream);



                setPhoto(bitmap);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

   private void setPhoto(Bitmap photo){
        if(whichPhoto==0){ // noPhoto

        }
        if(whichPhoto==1){//btn1
            photo1 = photo;
            btnPhoto1.setImageBitmap(photo);
        }
       if(whichPhoto==2){//btn1
           photo2= photo;
           btnPhoto2.setImageBitmap(photo);
       }
       if(whichPhoto==3){//btn1
           photo3= photo;
           btnPhoto3.setImageBitmap(photo);
       }
       if(whichPhoto==4){//btn1
           photo4= photo;
           btnPhoto4.setImageBitmap(photo);
       }
   }



    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
            hourDate.set(hourDate.get(Calendar.YEAR),hourDate.get(Calendar.MONTH),hourDate.get(Calendar.DAY_OF_MONTH), hourOfDay, minute);
            btnHour.setText(hourOfDay  + ":" + minute);
        }
    }





    public static class DatePickerFragment extends DialogFragment  implements DatePickerDialog.OnDateSetListener  {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(),

                          this, year, month, day);
        }


        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            hourDate.set(year, month, day);
            btnDate.setText(year + "/" + month + "/" + day);
        }
    }
}
