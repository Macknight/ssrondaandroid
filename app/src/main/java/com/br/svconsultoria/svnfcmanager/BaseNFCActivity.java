package com.br.svconsultoria.svnfcmanager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import com.br.svconsultoria.svnfcmanager.nfc.NfcFunctions;
import com.br.svconsultoria.svnfcmanager.tools.EventBuilder;
import com.br.svconsultoria.svnfcmanager.tools.ReadCardTools;
import com.br.svconsultoria.svnfcmanager.util.SVUtil;

import java.io.IOException;


public abstract class BaseNFCActivity extends Activity {

    private static final String TAG = "BaseNFCActivity";

    protected NfcAdapter mAdapter;
    protected String sTagSerial = "";
    protected ReadCardTools tools = ReadCardTools.getInstance();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_base_nfc);
        mAdapter = NfcAdapter.getDefaultAdapter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        NfcFunctions.enableReadMode(this, mAdapter, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAdapter.isEnabled()) {
            NfcFunctions.disableReadMode(this, mAdapter);
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        SVUtil.touch(this);

        String action = intent.getAction();
        Log.d(TAG, "Intent Action: " + action);

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)) {

            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

            boolean bNdefCheck = false;

            for (int i = 0; i < tag.getTechList().length; i++) {
                Log.d("LoginActivity", "onNewIntent() - Tech found. Description: " + tag.getTechList()[i]);
                if (tag.getTechList()[i].equals("android.nfc.tech.MifareUltralight")) {
                    bNdefCheck = true;
                    break;
                }
                if (tag.getTechList()[i].equals("android.nfc.tech.Ndef")) {
                    bNdefCheck = true;
                    break;
                }
                if (tag.getTechList()[i].equals("android.nfc.tech.NfcV")) {
                    bNdefCheck = true;
                    break;
                }
                if (tag.getTechList()[i].equals("android.nfc.tech.NfcA")) {
                    bNdefCheck = true;
                    break;
                }
                if (tag.getTechList()[i].equals("android.nfc.tech.NdefFormatable")) {
                    bNdefCheck = true;
                    break;
                }
                if (tag.getTechList()[i].equals("android.nfc.tech.IsoDep")) {
                    bNdefCheck = true;
                    break;
                }
            }

            Vibrator vibrator;
            vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(150);

            if (bNdefCheck) {
                try {
                    sTagSerial = tools.readTag(intent);
                    handleTag(sTagSerial);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Problema na leitura da tag. Tente novamente.", Toast.LENGTH_LONG).show();
                    Log.d("LoginActivity", "Problema na leitura da tag. (IOException)");
                }
            }else{
                Toast.makeText(this, "Essa tag não possui dados no formato NDEF compatível com o sistema.\nA ação disparada pela tag foi: " + action, Toast.LENGTH_LONG).show();
            }
        }
    }

    protected abstract void handleTag(String sTag);
}
