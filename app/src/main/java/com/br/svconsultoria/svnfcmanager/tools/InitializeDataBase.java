package com.br.svconsultoria.svnfcmanager.tools;

import android.content.Context;
import android.util.Log;

import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.model.orm.MobileParam;

import java.util.LinkedList;
import java.util.List;

public class InitializeDataBase {

	public static void init(Context context) {
		Log.d("InitializeDataBase", "Initializing the database now...");
		initSystemParams(context);
	}

	private static void initSystemParams(Context context) {
		Log.d("InitializeDataBase", "Initializing System Params");

		List<MobileParam> sysParams = new LinkedList<MobileParam>();
		sysParams.add(new MobileParam(null, SystemParamsMap.IS_INITIALIZED, "0"));
		sysParams.add(new MobileParam(null, SystemParamsMap.HOST_ADDRESS, ""));
		sysParams.add(new MobileParam(null, SystemParamsMap.ADMIN_LOGIN_NAME, ""));
		sysParams.add(new MobileParam(null, SystemParamsMap.ADMIN_PASSWORD, ""));
		SVMobileParamsDao.insertMobileParams(sysParams);
	}
}
