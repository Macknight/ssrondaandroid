package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;
import com.br.svconsultoria.svnfcmanager.model.orm.User;
import com.br.svconsultoria.svnfcmanager.model.orm.UserDao;

import java.util.List;

public class SVUserDao {


	private static DaoSession session = ApplicationControl.daoSession;

	public static void insertOrUpdateUsers(final List<User> list) {
		Log.d("SVUserDao", "insertOrUpdateUsers() Starting...");
		session.runInTx(new Runnable() {

			@Override
			public void run() {
				UserDao userdao = session.getUserDao();
				userdao.insertOrReplaceInTx(list);

				long xid = SVUpdateSequenceDao.getXid(UpdateSequenceMap.SRT_USERS).getxId();
				for (User object : list) {
					if (xid < object.getXId()) {
						xid = object.getXId();
					}
				}
				SVUpdateSequenceDao.updateXId(new UpdateSequence(null, UpdateSequenceMap.SRT_USERS, xid));
			}
		});
		Log.d("SVUserDao", "insertOrUpdateUsers() Finished!");
	}

	public static void insertOrReplaceUser(User user) {
		Log.d("SVUserDao", "insertOrReplaceUser()");
		UserDao userdao = session.getUserDao();
		userdao.insertOrReplace(user);
		Log.d("SVUserDao", "insertOrReplaceUser() Finished!");
	}

	public static void incrementUserXid(User user) {
		Log.d("SVUserDao", "incrementUserXid()");

		long actualXid = user.getXId();
		user.setXId(actualXid + 1);

		UserDao userdao = session.getUserDao();
		userdao.update(user);

		Log.d("SVUserDao", "incrementUserXid() Finished!");
	}

	public static User getUserById(long userId) {
		Log.d("SVUserDao", "getUserById() userId = " + userId);
		User user = session.getUserDao().queryBuilder().where(UserDao.Properties.UserId.eq(userId)).uniqueOrThrow();
		Log.d("SVUserDao", "getUserById() Finished!");
		return user;
	}

}
