package com.br.svconsultoria.svnfcmanager;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    public static class Marker implements Serializable {
        Float lon;
        Float lat;
        String title;
        Float color  = BitmapDescriptorFactory.HUE_RED;

        public Marker(Float lon, Float lat, String title) {
            this.lon = lon;
            this.lat = lat;
            this.title = title;
        }

        public Marker(Float lon, Float lat, String title, Float color) {
            this.lon = lon;
            this.lat = lat;
            this.title = title;
            this.color = color;
        }

    }

    private GoogleMap mMap;

    public static final String EXTRA_MARKERS = "markers";



    private List<Marker> markers;
    public static final int REQUEST_PERMISSIONS = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        markers = (List<Marker>) getIntent().getSerializableExtra(EXTRA_MARKERS);
        setContentView(R.layout.activity_maps);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }





    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;



        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSIONS);
            }else{
                Toast.makeText(MapsActivity.this, "Sem permissão para visualizar localozação atual!", Toast.LENGTH_LONG);
            }
        }else{
            mMap.setMyLocationEnabled(true);
        }

        boolean firstMarker =true;

        for(Marker m :markers){

            LatLng destination = new LatLng(m.lat, m.lon);
            mMap.addMarker(new MarkerOptions().position(destination).title(m.title)
                    .icon(BitmapDescriptorFactory.defaultMarker(m.color)));

            if(firstMarker) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(destination, 18));
                firstMarker=false;
            }
        }
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        mMap.getUiSettings().setMapToolbarEnabled(false);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(REQUEST_PERMISSIONS==resultCode){
            if(mMap!=null) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                }
            }
        }
    }

}
