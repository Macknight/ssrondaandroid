package com.br.svconsultoria.svnfcmanager.map;

public class HttpConnAction {

	// HTTP SERVEL REQUEST URL's
	// public static final String DATABASE_URL = SVSystemParamsDao.getHostParam();

	// public static final String URI = "http://aqua.intranet.svconsultoria.com.br/SSRonda/";
	// public static final String URI = "http://192.168.1.11/SSRonda/";
	public static final String HTTP = "http://";

	// public static final String HOST_URI = "/controller?command=operator.Login";
	public static final String AUTHENTICATE_URI = "controller?command=mobile.Authenticate";
	public static final String UPDATE_TABLES_URI = "controller?command=mobile.UpdateFromWeb";
	public static final String EVENTS_URI = "controller?command=mobile.Event";
	public static final String OCCURRENCES_URI = "controller?command=mobile.Occurrence";
	public static final String VEHICLE_CHECK_URI = "controller?command=mobile.VehicleCheck";
	public static final String VIGILANT_PHOTO_UPDATE_URI = "controller?command=mobile.UpdateFromMobile";
	public static final String UPDATE_CHECK_URI = "controller?command=mobile.XId";

	// HTTP FIELDS
	public static final String ACTION_FIELD = "action";
	public static final String ANSWER_FIELD = "answer";
	public static final String AUTHENTICATE_FIELD = "authenticate";
	public static final String MESSAGE_FIELD = "message";
	public static final String CUSTOMER_ID_FIELD = "customerId";
	public static final String IMEI_FIELD = "imei";
	public static final String LIST_FIELD = "list";
	public static final String CHALLENGE_FIELD = "challenge";
	public static final String XID_FIELD = "xid";

	// HTTP FIELD VALUES
	public static final String OPERATOR_TYPES_VALUE = "OperatorTypes";
	public static final String CHECKPOINT_VALUE = "CheckPoint";
	public static final String CUSTOMER_VALUE = "Customer";
	public static final String MOBILE_PHONES_VALUE = "MobilePhones";
	public static final String RFID_TAGS_VALUE = "RfidTags";
	public static final String ROUTE_COMPOSITION_VALUE = "RouteComposition";
	public static final String ROUTES_VALUE = "Routes";
	public static final String VIGILANT_VALUE = "Vigilant";
	public static final String SUCCESS_VALUE = "success";
	public static final String UPDATE_VALUE = "update";
	public static final String CHALLENGE_VALUE = "challenge";
	public static final String ANSWER_VALUE = "answer";
	public static final String INSERT_VALUE = "insert";
	public static final String VIGILANT_PHOTO_VALUE = "vigilantPhoto";
	public static final String IS_AUTHENTICATE_VALUE = "isAuthenticate";

}
