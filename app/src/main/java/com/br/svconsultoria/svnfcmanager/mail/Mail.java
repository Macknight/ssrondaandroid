package com.br.svconsultoria.svnfcmanager.mail;

import android.text.TextUtils;
import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.dao.SVSysParamsDao;
import com.br.svconsultoria.svnfcmanager.map.EventTypeMap;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.model.orm.SysParam;
import com.br.svconsultoria.svnfcmanager.services.SilentAlertService;
import com.br.svconsultoria.svnfcmanager.tools.EventBuilder;
import com.itextpdf.text.log.SysoCounter;

import java.util.Date;
import java.util.Properties;

import javax.activation.CommandMap;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.MailcapCommandMap;
import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Mail extends javax.mail.Authenticator {

    private String _user;
    private String _pass;

    private String[] _to;
    private String _from;

    private String _port;
    private String _sport;

    private String _host;

    private String _subject;
    private String _body;

    private boolean _auth;

    private boolean _debuggable;

    private Multipart _multipart;

    public Mail() {

        _host = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_EMAIL_HOST_ADDRESS).getValue(); // "smtp.gmail.com"; // default smtp server
        _port = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_EMAIL_PORT).getValue(); // _port = "465"; // default smtp port

        _from = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_EMAIL_SENDER_ADDRESS).getValue(); // email sent from
        String to = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_EMAIL_DESTINATIONS).getValue();
        _to = to.split(";");

        Log.d(getClass().getSimpleName(), "from: " + _from);
        Log.d(getClass().getSimpleName(), "to: " + to);

        _subject = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_EMAIL_SUBJECT).getValue(); // email subject
        _body = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_EMAIL_MESSAGE).getValue(); // email body

        try {
                    _body+="\n"
                            +"\n----- Dados coletados do dispositivo -----"
                            +"\nVigilante: " + ApplicationControl.userSession.getLoggedUser().getName()
                            +"\nPosição GPS: Lat/Lon: "  + SilentAlertService.gps.getLatitude() + "/" + SilentAlertService.gps.getLongitude()
                            +"\nLink para o mapa: www.google.com.br/maps/@"+SilentAlertService.gps.getLatitude() +","+SilentAlertService.gps.getLongitude()+",15z";

        }catch (Exception e){
            e.printStackTrace();
        }


        _debuggable = false; // debug mode on or off - default off
        _auth = false; // true; // smtp authentication - default on

        boolean auth =false;

        SysParam spPass =  SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_EMAIL_AUTH_PASSWORD);
        auth = spPass!=null && !TextUtils.isEmpty(spPass.getValue());

        if (auth) {
            _auth = true;
            _user = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_EMAIL_AUTH_USERNAME).getValue(); // username
            _pass = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_EMAIL_AUTH_PASSWORD).getValue(); // password
            _sport = SVSysParamsDao.getSysParamByName(SystemParamsMap.PANIC_EMAIL_PORT).getValue(); // default socketfactory port
        }

        _multipart = new MimeMultipart();

        // There is something wrong with MailCap, javamail can not find a handler for the multipart/mixed part, so this bit needs to be added.
        MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
        mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
        mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
        mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
        mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
        mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
        CommandMap.setDefaultCommandMap(mc);
    }

    public void send() throws Exception {
        Properties props = _setProperties();

        Session session = null;
        if(!_auth) {
            Session.getInstance(props, this);
        }else {
            session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        //Authenticating the password
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(_user, _pass);
                        }
                    });
        }

        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(_from));

        InternetAddress[] addressTo = new InternetAddress[_to.length];
        for (int i = 0; i < _to.length; i++) {
            addressTo[i] = new InternetAddress(_to[i]);
        }
        msg.setRecipients(MimeMessage.RecipientType.TO, addressTo);

        msg.setSubject(_subject);
        msg.setSentDate(new Date());

        // setup message body
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText(_body);
        _multipart.addBodyPart(messageBodyPart);

        // Put parts in message
        msg.setContent(_multipart);

        // send email
        Transport.send(msg);

        EventBuilder.createPanicEvent(EventTypeMap.PANIC_EMAIL_SENT_EVENT, "");

    }

    public void addAttachment(String filename) throws Exception {
        BodyPart messageBodyPart = new MimeBodyPart();
        DataSource source = new FileDataSource(filename);
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(filename);

        _multipart.addBodyPart(messageBodyPart);
    }

    @Override
    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(_user, _pass);
    }

    private Properties _setProperties() {
        Properties props = new Properties();


        props.put("mail.smtp.host", _host);

        if (_debuggable) {
            props.put("mail.debug", "true");
        }

        if (_auth) {
            props.put("mail.smtp.auth", "true");
        }

        props.put("mail.smtp.port", _port);

        /*
		 * props.put("mail.smtp.socketFactory.port", _sport);
		 * props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		 * props.put("mail.smtp.socketFactory.fallback", "false");
		 */
        return props;
    }

    public String getUser() {
        return _user;
    }

    public void setUser(String _user) {
        this._user = _user;
    }

    public String getPass() {
        return _pass;
    }

    public void setPass(String _pass) {
        this._pass = _pass;
    }

    public String[] getTo() {
        return _to;
    }

    public void setTo(String[] _to) {
        this._to = _to;
    }

    public String getFrom() {
        return _from;
    }

    public void setFrom(String _from) {
        this._from = _from;
    }

    public String getPort() {
        return _port;
    }

    public void setPort(String _port) {
        this._port = _port;
    }

    public String getSport() {
        return _sport;
    }

    public void setSport(String _sport) {
        this._sport = _sport;
    }

    public String getHost() {
        return _host;
    }

    public void setHost(String _host) {
        this._host = _host;
    }

    public String getSubject() {
        return _subject;
    }

    public void setSubject(String _subject) {
        this._subject = _subject;
    }

    public String getBody() {
        return _body;
    }

    public void setBody(String _body) {
        this._body = _body;
    }

    public boolean isAuth() {
        return _auth;
    }

    public void setAuth(boolean _auth) {
        this._auth = _auth;
    }

    public boolean isDebuggable() {
        return _debuggable;
    }

    public void setDebuggable(boolean _debuggable) {
        this._debuggable = _debuggable;
    }

    public Multipart getMultipart() {
        return _multipart;
    }

    public void setMultipart(Multipart _multipart) {
        this._multipart = _multipart;
    }
}
