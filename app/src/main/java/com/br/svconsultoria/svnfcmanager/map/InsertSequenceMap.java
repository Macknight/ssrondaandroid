package com.br.svconsultoria.svnfcmanager.map;

public class InsertSequenceMap {
	public static final String SRTM_EVENT_LOG = "EventsLog";
	public static final String SRTM_OCCURRENCE = "Occurrence";
	public static final String SRTM_VIGILANT_PHOTOS = "VigilantPhotos";
	public static final String SRTM_COMPLEX_OCCURRENCE= "ComplexOccurrence";
}
