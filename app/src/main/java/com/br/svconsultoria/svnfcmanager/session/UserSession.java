package com.br.svconsultoria.svnfcmanager.session;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.model.SystemCheckpoint;

import com.br.svconsultoria.svnfcmanager.model.orm.MobileParam;
import com.br.svconsultoria.svnfcmanager.model.orm.User;
import com.br.svconsultoria.svnfcmanager.model.orm.Vigilant;

import java.util.List;

public class UserSession {

	// Actual operator information
	private User loggedUser = null;
	private Vigilant loggedVigilant = null;

	// Route Progression Variables
	private long routeScheduleId = 0;
	private long orderCount = 0;
	private boolean vehicleChecked = false;
	private List<SystemCheckpoint> checkpoints = null;


	public UserSession() {
		this(null, null, SVMobileParamsDao.getLastRouteScheduleId(), SVMobileParamsDao.getLastRouteOrderId(), SVMobileParamsDao.isVehicleChecked());
	}

	public UserSession(User loggedUser, Vigilant loggedVigilant, long routeScheduleId, long orderCount, boolean vehicleChecked) {
		this.loggedUser = loggedUser;
		this.loggedVigilant = loggedVigilant;
		this.routeScheduleId = routeScheduleId;
		this.orderCount = orderCount;
		this.vehicleChecked = vehicleChecked;
	}

	public List<SystemCheckpoint> getCheckpoints() {
		return checkpoints;
	}

	public void setCheckpoints(List<SystemCheckpoint> checkpoints) {
		this.checkpoints = checkpoints;
	}

	public void invalidateUser() {
		loggedUser = null;
		loggedVigilant = null;
	}

	public User getLoggedUser() {
		return loggedUser;
	}

	public boolean isVehicleChecked(){
		return  vehicleChecked;
	}

	public void setLoggedUser(User loggedUser) {
		this.loggedUser = loggedUser;
	}

	public Vigilant getLoggedVigilant() {
		return loggedVigilant;
	}

	public void setLoggedVigilant(Vigilant loggedVigilant) {
		this.loggedVigilant = loggedVigilant;
	}

	public long getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(long orderCount) {
		SVMobileParamsDao.insertOrUpdateMobileParam(new MobileParam(null, SystemParamsMap.LAST_ORDER_ID, String.valueOf(orderCount)));
		this.orderCount = orderCount;
	}

	public long getRouteScheduleId() {
		return routeScheduleId;
	}

	public void setRouteScheduleId(long routeScheduleId) {
		try {
			SVMobileParamsDao.insertOrUpdateMobileParam(new MobileParam(null, SystemParamsMap.LAST_ROUTE_SCHEDULE_ID, String.valueOf(routeScheduleId)));
		} catch (NullPointerException e) {

		}
		this.routeScheduleId = routeScheduleId;
	}

	public void setVehicleCheck(boolean vehicleChecked){
		try {
			SVMobileParamsDao.insertOrUpdateMobileParam(new MobileParam(null, SystemParamsMap.VEHICLE_CHECKED, String.valueOf(vehicleChecked)));
		} catch (NullPointerException e) {

		}
		this.vehicleChecked = vehicleChecked;
	}
}
