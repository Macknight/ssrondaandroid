package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Entity mapped to table SRTM_USERS.
 */
@Entity
public class User {

    @Id
    private Long userId;

    private Long xId;
    private Long flagVigilant;
    private Long flagOperator;
    private String name;
    private String identDoc;
    private String contactInfo;
    private byte[] photo;
    @Generated(hash = 405425818)
    public User(Long userId, Long xId, Long flagVigilant, Long flagOperator, String name, String identDoc, String contactInfo, byte[] photo) {
        this.userId = userId;
        this.xId = xId;
        this.flagVigilant = flagVigilant;
        this.flagOperator = flagOperator;
        this.name = name;
        this.identDoc = identDoc;
        this.contactInfo = contactInfo;
        this.photo = photo;
    }
    @Generated(hash = 586692638)
    public User() {
    }
    public Long getUserId() {
        return this.userId;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public Long getXId() {
        return this.xId;
    }
    public void setXId(Long xId) {
        this.xId = xId;
    }
    public Long getFlagVigilant() {
        return this.flagVigilant;
    }
    public void setFlagVigilant(Long flagVigilant) {
        this.flagVigilant = flagVigilant;
    }
    public Long getFlagOperator() {
        return this.flagOperator;
    }
    public void setFlagOperator(Long flagOperator) {
        this.flagOperator = flagOperator;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getIdentDoc() {
        return this.identDoc;
    }
    public void setIdentDoc(String identDoc) {
        this.identDoc = identDoc;
    }
    public String getContactInfo() {
        return this.contactInfo;
    }
    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }
    public byte[] getPhoto() {
        return this.photo;
    }
    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

}
