package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.Route;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteDao;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;

import java.util.List;

public class SVRouteDao {


	private static DaoSession session = ApplicationControl.daoSession;

	public static void insertOrUpdateRoutes(final List<Route> list) {
		Log.d("SVRouteDao", "insertOrUpdateRoutes() Starting...");
		session.runInTx(new Runnable() {

			@Override
			public void run() {
				RouteDao routedao = session.getRouteDao();
				routedao.insertOrReplaceInTx(list);
			}
		});

		long xid = SVUpdateSequenceDao.getXid(UpdateSequenceMap.SRT_ROUTES).getxId();
		for (Route object : list) {
			if (xid < object.getXId()) {
				xid = object.getXId();
			}
		}
		SVUpdateSequenceDao.updateXId(new UpdateSequence(null, UpdateSequenceMap.SRT_ROUTES, xid));

		Log.d("SVRouteDao", "insertOrUpdateRoutes() Finished!");
	}

	public static Route getRouteById(long routeId) {
		Log.d("SVRouteDao", "getRouteById() routeId: " + routeId);
		Route route = null;
		RouteDao routedao = session.getRouteDao();

		try {
			route = routedao.queryBuilder().where(RouteDao.Properties.RouteId.eq(routeId)).where(RouteDao.Properties.StatusId.eq(1)).uniqueOrThrow();
		} catch (Exception e) {
			Log.d("SVRouteDao", "getRouteById() - Route with ID = " + routeId + " not found.");
		}
		Log.d("SVRouteDao", "getRouteById() Finished!");
		return route;
	}

	public static void insertOrReplaceRoute(Route route) {
		Log.d("SVRouteDao", "insertOrReplaceRoute()");
		RouteDao routedao = session.getRouteDao();
		routedao.insertOrReplace(route);
		Log.d("SVRouteDao", "insertOrReplaceRoute() Finished!");
	}
}
