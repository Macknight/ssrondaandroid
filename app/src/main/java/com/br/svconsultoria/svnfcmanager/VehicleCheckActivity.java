package com.br.svconsultoria.svnfcmanager;


import android.app.Activity;
import android.app.ActionBar;
import android.app.FragmentTransaction;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Environment;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.br.svconsultoria.svnfcmanager.dao.SVEventDao;
import com.br.svconsultoria.svnfcmanager.dao.SVInsertSequenceDao;
import com.br.svconsultoria.svnfcmanager.dao.SVMobilePhoneDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteCompositionDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteScheduleDao;
import com.br.svconsultoria.svnfcmanager.dao.SVSysParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVUserDao;
import com.br.svconsultoria.svnfcmanager.dao.SVVehicleCheckDao;
import com.br.svconsultoria.svnfcmanager.map.EventTypeMap;
import com.br.svconsultoria.svnfcmanager.map.InsertSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.EventLog;
import com.br.svconsultoria.svnfcmanager.model.orm.Route;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteComposition;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteSchedule;
import com.br.svconsultoria.svnfcmanager.model.orm.User;
import com.br.svconsultoria.svnfcmanager.model.orm.VehicleCheck;
import com.br.svconsultoria.svnfcmanager.services.SilentAlertService;
import com.br.svconsultoria.svnfcmanager.session.UserSession;
import com.br.svconsultoria.svnfcmanager.tools.EventBuilder;
import com.br.svconsultoria.svnfcmanager.util.DateTimeUtils;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.html.table.Table;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class VehicleCheckActivity extends Activity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    public VehiclePage1Fragment p1 = VehiclePage1Fragment.newInstance();
    public VehiclePage2Fragment p2 = VehiclePage2Fragment.newInstance();
    public VehiclePage3Fragment p3 = VehiclePage3Fragment.newInstance();

    private boolean finished = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_check);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_vehicle_check, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void cancelVehicleCheck(View view){
        Intent i = new Intent(this, RouteListActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
    }

    public void finishVehicleCheck(View view){
        if(!finished) {
            finished = true;

            VehicleCheck vc = new VehicleCheck();
            p1.fillVehicleCheckData(vc);
            p2.fillVehicleCheckData(vc);

            vc.setVehicleCheckId(vc.getPlate()+"_"+ApplicationControl.userSession.getLoggedVigilant().getVigilantId()+"_"+System.currentTimeMillis());
            vc.setMobileId(SVMobilePhoneDao.getMobileId());
            vc.setVigilantId(ApplicationControl.userSession.getLoggedVigilant().getVigilantId());
            if (SilentAlertService.gps.hasAccuracy()) {
                vc.setLatitude(SilentAlertService.gps.getLatitude());
                vc.setLongitude(SilentAlertService.gps.getLongitude());
            }
            vc.setDate(new Date());

            if(p1.validatePlate() && p1.validateModel() && p1.validateKm()) {

                SVVehicleCheckDao.insertVehicleCheck(vc);

                EventLog event = new EventLog();
                event.setEventDt(vc.getDate());
                event.setEventLogId(SVInsertSequenceDao.getNextId(InsertSequenceMap.SRTM_EVENT_LOG));
                event.setEventTypeId(EventTypeMap.VEHICLE_CHECK);

                if (SilentAlertService.gps.hasAccuracy()) {
                    // Log.d(TAG, "createSkipRouteEvent() - GPS has Accuracy. Latitude: " + SilentAlertService.gps.getLatitude() + " Longitude: " + SilentAlertService.gps.getLongitude());
                    event.setLatitude(SilentAlertService.gps.getLatitude());
                    event.setLongitude(SilentAlertService.gps.getLongitude());
                } else {
                    // Log.d(TAG, "createSkipRouteEvent() - GPS has no Accuracy.");
                    event.setLatitude((float) 0);
                    event.setLongitude((float) 0);
                }

                event.setMobileId(SVMobilePhoneDao.getMobileId());
                event.setTagId((long) 0);

                long routeScheduleId = ApplicationControl.userSession.getRouteScheduleId();
                RouteSchedule routeSchedule = SVRouteScheduleDao.getRouteScheduleById(routeScheduleId);
                event.setRouteId(routeSchedule.getRouteId());

                event.setVigilantId(ApplicationControl.userSession.getLoggedVigilant().getVigilantId());
                event.setVehicleCheckId(vc.getVehicleCheckId());

                if(!SVEventDao.insertNewEvent(event)) {
                    ApplicationControl.handleMessage("Erro ao inserir evento de Checklist Veicular");
                }

//                generatePDF(vc);

                ApplicationControl.userSession.setVehicleCheck(true);
                Intent i = new Intent(this, RouteListActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(i);
            }else{
                mViewPager.setCurrentItem(0);
            }

            finished=false;
        }
    }

    private void generatePDF( VehicleCheck vc){
        Document document = new Document();

        try {

            File file = new File(Environment.getExternalStorageDirectory() + "/VehicleCheck/" +"VehicleCheck_"+System.currentTimeMillis()+".pdf");
            file.getParentFile().mkdirs();
            file.createNewFile();

            PdfWriter.getInstance(document, new FileOutputStream(file));

            document.open();
            document.add(new Paragraph("Inspeção veicular"));

            User user = SVUserDao.getUserById(ApplicationControl.userSession.getLoggedVigilant().getUserId());
            document.add(new Paragraph("Nome:" + user.getName()));
            document.add(new Paragraph("Placa:" +vc.getPlate()));
            document.add(new Paragraph("Modelo:" +vc.getModel()));
            document.add(new Paragraph("Combustível:" +vc.getFuel()));
            document.add(new Paragraph("KM:" +vc.getKm()));

            document.add(new Paragraph(vc.getComments()));


            Image img = Image.getInstance(vc.getDamages());
            img.scaleToFit(400,200);
            document.add(img);

            PdfPTable photos = new PdfPTable(2);
            if(vc.getPhoto1()!=null){
                Image photo = Image.getInstance(vc.getPhoto1());
                photo.scaleToFit(200,400);
                photos.addCell(photo);
            }
            if(vc.getPhoto2()!=null){
                Image photo = Image.getInstance(vc.getPhoto2());
                photo.scaleToFit(200,400);
                photos.addCell(photo);
            }
            if(vc.getPhoto3()!=null){
                Image photo = Image.getInstance(vc.getPhoto3());
                photo.scaleToFit(200,400);
                photos.addCell(photo);
            }
            if(vc.getPhoto4()!=null){
                Image photo = Image.getInstance(vc.getPhoto4());
                photo.scaleToFit(200,400);
                photos.addCell(photo);
            }
            document.add(photos);
            document.add(new Paragraph(vc.getDamageComments()));
            document.close(); // no need to close PDFwriter?

        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        }
    }

    public void clearImagePoints(View view) {
        p2.clearImagePoints();
    }

    public void removeLastImagePoint(View view) {
        p2.removeLastPoint();
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            if(position==0){
                return p1;
            }else if(position==1){
                return p2;
            } else{
                return p3;
            }
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "1/3";
                case 1:
                    return "2/3";
                case 2:
                    return "3/3";
            }
            return null;
        }
    }
}
