package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import java.util.Date;

/**
 * Entity mapped to table SRTM_OCCURRENCES.
 */
@Entity
public class Occurrence {

    @Id
    private Long occurrenceId;
    private long mobileId;
    private long vigilantId;
    private long occurrenceTypeId;
    private long priorityId;
    private byte[] photo;
    private java.util.Date occurrenceDt;
    private Float latitude;
    private Float longitude;
    private String comments;
    private Long customerId;
    @Generated(hash = 1608200986)
    public Occurrence(Long occurrenceId, long mobileId, long vigilantId,
            long occurrenceTypeId, long priorityId, byte[] photo,
            java.util.Date occurrenceDt, Float latitude, Float longitude,
            String comments, Long customerId) {
        this.occurrenceId = occurrenceId;
        this.mobileId = mobileId;
        this.vigilantId = vigilantId;
        this.occurrenceTypeId = occurrenceTypeId;
        this.priorityId = priorityId;
        this.photo = photo;
        this.occurrenceDt = occurrenceDt;
        this.latitude = latitude;
        this.longitude = longitude;
        this.comments = comments;
        this.customerId = customerId;
    }
    @Generated(hash = 2032986122)
    public Occurrence() {
    }
    public Long getOccurrenceId() {
        return this.occurrenceId;
    }
    public void setOccurrenceId(Long occurrenceId) {
        this.occurrenceId = occurrenceId;
    }
    public long getMobileId() {
        return this.mobileId;
    }
    public void setMobileId(long mobileId) {
        this.mobileId = mobileId;
    }
    public long getVigilantId() {
        return this.vigilantId;
    }
    public void setVigilantId(long vigilantId) {
        this.vigilantId = vigilantId;
    }
    public long getOccurrenceTypeId() {
        return this.occurrenceTypeId;
    }
    public void setOccurrenceTypeId(long occurrenceTypeId) {
        this.occurrenceTypeId = occurrenceTypeId;
    }
    public long getPriorityId() {
        return this.priorityId;
    }
    public void setPriorityId(long priorityId) {
        this.priorityId = priorityId;
    }
    public byte[] getPhoto() {
        return this.photo;
    }
    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }
    public java.util.Date getOccurrenceDt() {
        return this.occurrenceDt;
    }
    public void setOccurrenceDt(java.util.Date occurrenceDt) {
        this.occurrenceDt = occurrenceDt;
    }
    public Float getLatitude() {
        return this.latitude;
    }
    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }
    public Float getLongitude() {
        return this.longitude;
    }
    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }
    public String getComments() {
        return this.comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }
    public Long getCustomerId() {
        return this.customerId;
    }
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }


}
