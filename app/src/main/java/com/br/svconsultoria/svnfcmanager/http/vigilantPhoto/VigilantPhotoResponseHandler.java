package com.br.svconsultoria.svnfcmanager.http.vigilantPhoto;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.dao.SVVigilantPhotoDao;
import com.br.svconsultoria.svnfcmanager.model.Response;
import com.br.svconsultoria.svnfcmanager.model.orm.VigilantPhotos;

import java.util.ArrayList;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class VigilantPhotoResponseHandler {

	private String TAG = "VigilantPhotoResponseHandler";

	public void process(Response response) {
		Log.d(TAG, "process()");
		if (response.getSuccess()) {
			vigilantPhotosResponse(response);
		} else {
			Log.d(TAG, "process() - Transaction failed. This will try again later...");
		}
	}

	@SuppressWarnings("unchecked")
	private void vigilantPhotosResponse(Response response) {
		Log.d(TAG, "vigilantPhotosResponse() -  VigilantPhotos uploaded successfully");
		Log.d(TAG, "vigilantPhotosResponse() -  VigilantPhotos List size: " + response.getList().size());
		SVVigilantPhotoDao.deleteCommittedPhotos((ArrayList<VigilantPhotos>) response.getList());
		ApplicationControl.finishSendPhotos();
		Log.d(TAG, "vigilantPhotosResponse() -  VigilantPhotos deleted successfully");
	}
}
