package com.br.svconsultoria.svnfcmanager.model;

import java.util.ArrayList;

public class ResponseObject {
	
	private ArrayList<?> list;
	
	public ArrayList<?> getList() {
		return list;
	}
	
	public void setList(ArrayList<?> list) {
		this.list = list;
	}
}
