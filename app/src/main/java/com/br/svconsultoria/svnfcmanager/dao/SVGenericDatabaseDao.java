package com.br.svconsultoria.svnfcmanager.dao;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.InsertSequenceMap;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.InsertSequence;
import com.br.svconsultoria.svnfcmanager.model.orm.InsertSequenceDao;
import com.br.svconsultoria.svnfcmanager.model.orm.MobileParam;
import com.br.svconsultoria.svnfcmanager.model.orm.MobileParamDao;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequenceDao;
import com.br.svconsultoria.svnfcmanager.util.PrintTable;

public class SVGenericDatabaseDao {

	private static DaoSession session = ApplicationControl.daoSession;

	public static void reCreateDatabase(SQLiteOpenHelper helper) {
		Log.d("SVGenericDatabaseDao", "reCreateDatabase()");
		SQLiteDatabase db = helper.getWritableDatabase();
		helper.onUpgrade(db, db.getVersion(), db.getVersion() + 1);
		Log.d("SVGenericDatabaseDao", "reCreateDatabase() Finished!");
	}

	public static boolean isInitialized() {
		Log.d("isInitialized()", "Verifying if database is initialized...");
		MobileParamDao systemParamsDao = session.getMobileParamDao();
		MobileParam param = null;
		try {
			param = systemParamsDao.queryBuilder().where(MobileParamDao.Properties.ParamName.eq(SystemParamsMap.IS_INITIALIZED)).uniqueOrThrow();
			Log.d("isInitialized()", "Initialized: " + param.getParamValue());
		} catch (Exception e) {
			Log.d("isInitialized()", "Exception: " + e.getMessage());
			return false;
		}
		if (param.getParamValue().trim().equals("true"))
			return true;
		else
			return false;
	}

	public static void initializeDatabase() {
		Log.d("SVGenericDatabaseDao", "initializeDatabase()");

		session.runInTx(new Runnable() {

			@Override
			public void run() {
				MobileParamDao systemParamsDao = session.getMobileParamDao();
				UpdateSequenceDao updateSequenceDao = session.getUpdateSequenceDao();
				InsertSequenceDao insertSequenceDao = session.getInsertSequenceDao();

				MobileParam initializedSystemParam = new MobileParam(null, SystemParamsMap.IS_INITIALIZED, "true");
				MobileParam isConfiguredParam = new MobileParam(null,SystemParamsMap.IS_CONFIGURED, "false");
				MobileParam databaseUrlParam = new MobileParam(null,SystemParamsMap.HOST_ADDRESS, SVMobileParamsDao.getHostParam());
				MobileParam adminLoginName = new MobileParam(null,SystemParamsMap.ADMIN_LOGIN_NAME, "");
				MobileParam adminLoginPassword = new MobileParam(null,SystemParamsMap.ADMIN_PASSWORD, "");
				MobileParam lastRouteScheduleId = new MobileParam(null,SystemParamsMap.LAST_ROUTE_SCHEDULE_ID, "0");
				MobileParam lastOrderId = new MobileParam(null,SystemParamsMap.LAST_ORDER_ID, "0");
				MobileParam fallSoundAllowed = new MobileParam(null,SystemParamsMap.FALL_SOUND_ALLOWED, "true");
				MobileParam panicSoundAllowed = new MobileParam(null,SystemParamsMap.PANIC_SOUND_ALLOWED, "true");
				MobileParam qrCodeAllowed = new MobileParam(null,SystemParamsMap.QR_CODE_ALLOWED, "false");

				InsertSequence eventSequence = new InsertSequence(null, InsertSequenceMap.SRTM_EVENT_LOG, 1);
				InsertSequence occurrenceSequence = new InsertSequence(null, InsertSequenceMap.SRTM_OCCURRENCE, 1);
				InsertSequence vigilantPhotos = new InsertSequence(null, InsertSequenceMap.SRTM_VIGILANT_PHOTOS, 1);

				Log.d("SVGenericDatabaseDao", "initializeDatabase() - initializedSystemParam");
				systemParamsDao.insert(initializedSystemParam);
				Log.d("SVGenericDatabaseDao", "initializeDatabase() - isConfiguredParam");
				systemParamsDao.insert(isConfiguredParam);
				Log.d("SVGenericDatabaseDao", "initializeDatabase() - databaseUrlParam");
				systemParamsDao.insert(databaseUrlParam);
				Log.d("SVGenericDatabaseDao", "initializeDatabase() - adminLoginName");
				systemParamsDao.insert(adminLoginName);
				Log.d("SVGenericDatabaseDao", "initializeDatabase() - adminLoginPassword");
				systemParamsDao.insert(adminLoginPassword);
				Log.d("SVGenericDatabaseDao", "initializeDatabase() - lastRouteId");
				systemParamsDao.insert(lastRouteScheduleId);
				Log.d("SVGenericDatabaseDao", "initializeDatabase() - lastOrderId");
				systemParamsDao.insert(lastOrderId);
				Log.d("SVGenericDatabaseDao", "initializeDatabase() - fallSoundAllowed");
				systemParamsDao.insert(fallSoundAllowed);
				Log.d("SVGenericDatabaseDao", "initializeDatabase() - panicSoundAllowed");
				systemParamsDao.insert(panicSoundAllowed);
				Log.d("SVGenericDatabaseDao", "initializeDatabase() - qrCodeAllowed");
				systemParamsDao.insert(qrCodeAllowed);

				Log.d("SVGenericDatabaseDao", "initializeDatabase() - eventSequence");
				insertSequenceDao.insert(eventSequence);
				Log.d("SVGenericDatabaseDao", "initializeDatabase() - occurrenceSequence");
				insertSequenceDao.insert(occurrenceSequence);
				Log.d("SVGenericDatabaseDao", "initializeDatabase() - vigilantPhotos");
				insertSequenceDao.insert(vigilantPhotos);

				for (String table : UpdateSequenceMap.SRT_TABLES) {
					Log.d("SVGenericDatabaseDao", "initializeDatabase() - TABLE: " + table);
					updateSequenceDao.insert(new UpdateSequence(null, table, 0));
				}

				PrintTable.printSystemParams();
			}
		});
		Log.d("SVGenericDatabaseDao", "initializeDatabase() - Initialization completed successfully");
	}
}
