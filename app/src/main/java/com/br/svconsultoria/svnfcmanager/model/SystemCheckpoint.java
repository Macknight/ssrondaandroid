package com.br.svconsultoria.svnfcmanager.model;

import java.util.ArrayList;
import java.util.List;

public class SystemCheckpoint {

    private long systemCheckpointId;
    private long checkpointId;
    private long orderId;
    private String description;
    private long tagId;
    private float latitude = 0;
    private float longitude = 0;
    private long time;//Minute Of Day
    private long delayMinutes;//Minutes
    private Long customerId;
    private String checkpointChecks = "";
    private String checkedTasks;
    private long travelMinutes = 0;

    private boolean mandatory = true;

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public long getTravelMinutes() {
        return travelMinutes;
    }

    public void setTravelMinutes(long travelMinutes) {
        this.travelMinutes = travelMinutes;
    }

    /**
     * Time vigilant must wait in checkpoint
     */
    private Long stayTimeMinutes = 0L;

    /**
     * Flag to indicate that current checkpoint were added to checkpoint list
     * because stay time
     */
    private Boolean wait = false;

    /**
     * Time millis when checkpoint were read
     */
    private long readTimeMillis = 0;
    private int numberOfChecks = 0;

    public int getNumberOfChecks() {
        return numberOfChecks;
    }

    public void setNumberOfChecks(int numberOfChecks) {
        this.numberOfChecks = numberOfChecks;
    }

    public void setCheckpointChecks(String checkpointChecks) {
        this.checkpointChecks = checkpointChecks;
    }

    public String getCheckedTasks() {
        return checkedTasks;
    }

    public void setCheckedTasks(String checkedTasks) {
        this.checkedTasks = checkedTasks;
    }

    public String getCheckpointChecks() {
        return checkpointChecks;
    }

    public String concatTasks(List<String> ls) {
        if (ls == null || ls.isEmpty()) {
            setCheckpointChecks("");
            return "[]";
        }

        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < ls.size(); i++) {
            String s = ls.get(i);
            if (i > 0) {
                sb.append(", ");
            }
            sb.append("\"");
            //escape
            for (int j = 0; j < s.length(); j++) {
                char c = s.charAt(j);
                if (c == '\\') {
                    sb.append("\\\\");
                } else if (c == '\"') {
                    sb.append("\\\"");
                } else {
                    sb.append(c);
                }
            }
            sb.append("\"");
        }
        sb.append("]");

        return sb.toString();
    }

    public List<String> getTaskList() {
        List<String> ls = new ArrayList<String>();
        String s = getCheckpointChecks();
        if (s == null || s.trim().isEmpty()) {
            return ls;
        }

        s = s.trim();

        if (!s.startsWith("[") || !s.endsWith("]")) {
            return ls;
        }

        int state = 0; //0 - starting, 1-waiting start of string, 2 - parsing string, 3 - next or finish;
        boolean bEscape = false;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            switch (state) {
                case 0: //starting
                    if (c == '[') {
                        state = 1; //waiting string
                        sb.setLength(0);
                    } else if (!Character.isWhitespace(c)) {
                        return ls;
                    }

                    break;

                case 1: //waiting string
                    if (c == '\"') {
                        state = 2; //parsing string
                        sb.setLength(0);
                    } else if (!Character.isWhitespace(c)) {
                        return ls; //finished or invalid char.
                    }

                    break;
                case 2: //parsing string
                    if (c == '\\') {
                        if (bEscape) {
                            sb.append('\\');
                            bEscape = false;
                        } else {
                            bEscape = true;
                        }
                    } else if (c == '\"') {
                        if (bEscape) {
                            sb.append("\"");
                            bEscape = false;
                        } else {
                            ls.add(sb.toString());
                            state = 3; //next of finish
                            sb.setLength(0);
                        }
                    } else {
                        sb.append(c);
                        bEscape = false;
                    }
                    break;

                case 3: //finishing or next
                    if (c == ',') {
                        state = 1;
                    } else if (c == ']') {
                        return ls;
                    }
                    break;
            }
        }

        return ls;

    }

    public long getReadTimeMillis() {
        return readTimeMillis;
    }

    public void setReadTimeMillis(long readTimeMillis) {
        this.readTimeMillis = readTimeMillis;
    }

    public Long getStayTimeMinutes() {
        return stayTimeMinutes;
    }

    public void setStayTimeMinutes(Long stayTime) {
        this.stayTimeMinutes = stayTime;
    }

    public boolean isWait() {
        return wait;
    }

    public void setWait(Boolean wait) {
        this.wait = wait;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public long getSystemCheckpointId() {
        return systemCheckpointId;
    }

    public void setSystemCheckpointId(long systemCheckpointId) {
        this.systemCheckpointId = systemCheckpointId;
    }

    public long getCheckpointId() {
        return checkpointId;
    }

    public void setCheckpointId(long checkpointId) {
        this.checkpointId = checkpointId;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getTagId() {
        return tagId;
    }

    public void setTagId(long tagId) {
        this.tagId = tagId;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public long getTime() {
        return time;
    }

    /**
     * @param time minutes of day
     */
    public void setTime(long time) {
        this.time = time;
    }

    public long getDelayMinutes() {
        return delayMinutes;
    }

    public void setDelayMinutes(long delayMinutes) {
        this.delayMinutes = delayMinutes;
    }

}
