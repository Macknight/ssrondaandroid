package com.br.svconsultoria.svnfcmanager.util;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.model.orm.Checkpoint;
import com.br.svconsultoria.svnfcmanager.model.orm.Customer;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.EventLog;
import com.br.svconsultoria.svnfcmanager.model.orm.MobileParam;
import com.br.svconsultoria.svnfcmanager.model.orm.MobilePhone;
import com.br.svconsultoria.svnfcmanager.model.orm.RfidTag;
import com.br.svconsultoria.svnfcmanager.model.orm.Route;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteComposition;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteSchedule;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;
import com.br.svconsultoria.svnfcmanager.model.orm.User;
import com.br.svconsultoria.svnfcmanager.model.orm.Vigilant;

import java.util.List;

public class PrintTable {

	private static DaoSession session = ApplicationControl.daoSession;

	public static void printEvents() {
		List<EventLog> events = session.getEventLogDao().queryBuilder().list();
		if (events.size() == 0) {
			Log.d("DBDATA", "Events table is empty...");
		}
		for (EventLog event : events) {
			Log.d("DBDATA", "EventLog EventId: " + event.getEventLogId());
			Log.d("DBDATA", "EventLog EventTypeId: " + event.getEventTypeId());
			Log.d("DBDATA", "EventLog EventDt: " + event.getEventDt());
			Log.d("DBDATA", "EventLog Latitude: " + event.getLatitude());
			Log.d("DBDATA", "EventLog Longitude: " + event.getLongitude());
			Log.d("DBDATA", "EventLog VigilantID: " + event.getVigilantId());
		}
	}

	public static void printUpdateEvents() {
		List<UpdateSequence> updateSequences = session.getUpdateSequenceDao().queryBuilder().list();
		if (updateSequences.size() == 0) {
			Log.d("DBDATA", "UpdateSequence table is empty...");
		}
		for (UpdateSequence sequence : updateSequences) {
			Log.d("DBDATA", "UpdateSequence TableName: " + sequence.getTableName() + " XID: " + sequence.getId());
		}
	}

	public static void printMobilePhones() {
		List<MobilePhone> phones = session.getMobilePhoneDao().queryBuilder().list();
		if (phones.size() == 0) {
			Log.d("DBDATA", "MobilePhones table is empty...");
		}
		for (MobilePhone phone : phones) {
			Log.d("DBDATA", "MobilePhone MobilePhoneId: " + phone.getMobileId());
			Log.d("DBDATA", "MobilePhone Description: " + phone.getDescription());
			Log.d("DBDATA", "MobilePhone IMEI: " + phone.getImei());
			Log.d("DBDATA", "MobilePhone StatusID: " + phone.getStatusId());
		}
	}

	public static void printRouteComposition() {
		List<RouteComposition> routeCompositions = session.getRouteCompositionDao().queryBuilder().list();
		if (routeCompositions.size() == 0) {
			Log.d("DBDATA", "MobilePhones table is empty...");
		}
		for (RouteComposition routeComposition : routeCompositions) {
			Log.d("DBDATA", "routeComposition RouteCompositionId: " + routeComposition.getRouteCompositionId());
			Log.d("DBDATA", "routeComposition OrderId: " + routeComposition.getOrderId());
			Log.d("DBDATA", "routeComposition CheckPointId: " + routeComposition.getCheckPointId());
			Log.d("DBDATA", "routeComposition StatusId: " + routeComposition.getStatusId());
			Log.d("DBDATA", "routeComposition RouteId: " + routeComposition.getRouteId());
		}
	}

	public static void printCheckPoints() {
		List<Checkpoint> checkpoints = session.getCheckpointDao().queryBuilder().list();
		if (checkpoints.size() == 0) {
			Log.d("DBDATA", "MobilePhones table is empty...");
		}
		for (Checkpoint checkpoint : checkpoints) {
			Log.d("DBDATA", "checkpoint getDescription: " + checkpoint.getDescription());
			Log.d("DBDATA", "checkpoint CheckPointId: " + checkpoint.getCheckPointId());
			Log.d("DBDATA", "checkpoint StatusId: " + checkpoint.getStatusId());
		}
	}

	public static void printCustomers() {
		List<Customer> customers = session.getCustomerDao().queryBuilder().list();
		if (customers.size() == 0) {
			Log.d("DBDATA", "Customers table is empty...");
		}
		for (Customer c : customers) {
			Log.d("DBDATA", "Customer CustomerID: " + c.getCustomerId());
			Log.d("DBDATA", "Customer CustomerName: " + c.getCustomerName());
			Log.d("DBDATA", "Customer CustomerCNPJ: " + c.getCustomerCNPJ());
			Log.d("DBDATA", "Customer CustomerPhone: " + c.getPhone());
			Log.d("DBDATA", "Customer StatusID: " + c.getStatusId());
		}
	}

	public static void printVigilants() {
		List<Vigilant> vigilants = session.getVigilantDao().queryBuilder().list();
		if (vigilants.size() == 0) {
			Log.d("DBDATA", "Vigilants table is empty...");
		}
		for (Vigilant v : vigilants) {
			Log.d("DBDATA", "Vigilant VigilantID: " + v.getVigilantId());
			Log.d("DBDATA", "Vigilant UserID: " + v.getUserId());
			Log.d("DBDATA", "Vigilant TagID: " + v.getLogonTagId());
			Log.d("DBDATA", "Vigilant StatusID: " + v.getStatusId());
		}
	}

	public static void printTags() {
		List<RfidTag> tags = session.getRfidTagDao().queryBuilder().list();
		if (tags.size() == 0) {
			Log.d("DBDATA", "RfidTags table is empty...");
		}
		for (RfidTag tag : tags) {
			Log.d("DBDATA", "RfidTag RfidTagID: " + tag.getTagId());
			Log.d("DBDATA", "RfidTag TagTypeID: " + tag.getTagTypeId());
			Log.d("DBDATA", "RfidTag SerialNumber: " + tag.getSerialNumber());
			Log.d("DBDATA", "RfidTag TechTypeID: " + tag.getTechTypeId());
			Log.d("DBDATA", "RfidTag StatusID: " + tag.getStatusId());
		}
	}

	public static void printUsers() {
		List<User> users = session.getUserDao().queryBuilder().list();
		if (users.size() == 0) {
			Log.d("DBDATA", "Users table is empty...");
		}
		for (User user : users) {
			Log.d("DBDATA", "User UserID: " + user.getUserId());
			Log.d("DBDATA", "User UserName: " + user.getName());
			Log.d("DBDATA", "User IdentDoc: " + user.getIdentDoc());
			Log.d("DBDATA", "User ContactInfo: " + user.getContactInfo());
		}
	}

	public static void printSystemParams() {
		List<MobileParam> params = session.getMobileParamDao().queryBuilder().list();
		if (params.size() == 0) {
			Log.d("DBDATA", "SystemParams table is empty...");
		}
		for (MobileParam param : params) {
			Log.d("DBDATA", "SystemParam ParamName: " + param.getParamName());
			Log.d("DBDATA", "SystemParam ParamValue: " + param.getParamValue());
		}
	}

	public static void printRoutes() {
		List<Route> routes = session.getRouteDao().queryBuilder().list();
		if (routes.size() == 0) {
			Log.d("DBDATA", "Routes table is empty...");
		}
		for (Route r : routes) {
			Log.d("DBDATA", "Route RouteID: " + r.getRouteId());
			Log.d("DBDATA", "Route Description: " + r.getDescription());
		}
	}

	public static void printRouteSchedules() {
		List<RouteSchedule> rSchedules = session.getRouteScheduleDao().queryBuilder().list();
		if (rSchedules.size() == 0) {
			Log.d("DBDATA", "RouteSchedules table is empty...");
		}
		for (RouteSchedule r : rSchedules) {
			Log.d("DBDATA", "RouteSchedule RouteScheduleID: " + r.getRouteScheduleId());
			Log.d("DBDATA", "RouteSchedule RouteID: " + r.getRouteId());
			Log.d("DBDATA", "RouteSchedule ScheduleDt: " + r.getScheduleDt());
		}
	}

	public static void printAll() {
		Log.d("DBDATA", "##############################################################");
		printEvents();
		Log.d("DBDATA", "##############################################################");
		printUpdateEvents();
		Log.d("DBDATA", "##############################################################");
		printMobilePhones();
		Log.d("DBDATA", "##############################################################");
		printCustomers();
		Log.d("DBDATA", "##############################################################");
		printVigilants();
		Log.d("DBDATA", "##############################################################");
		printTags();
		Log.d("DBDATA", "##############################################################");
		printUsers();
		Log.d("DBDATA", "##############################################################");
		printSystemParams();
		Log.d("DBDATA", "##############################################################");
		printRoutes();
		Log.d("DBDATA", "##############################################################");
		printRouteSchedules();
		Log.d("DBDATA", "##############################################################");
		printCheckPoints();
		Log.d("DBDATA", "##############################################################");
		printRouteComposition();
		Log.d("DBDATA", "##############################################################");
	}

	public static void main(String[] args) {
		new PrintTable().printAll();
	}

}
