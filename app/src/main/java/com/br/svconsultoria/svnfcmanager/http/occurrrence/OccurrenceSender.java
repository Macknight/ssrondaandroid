package com.br.svconsultoria.svnfcmanager.http.occurrrence;

import android.text.TextUtils;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVOccurrenceDao;
import com.br.svconsultoria.svnfcmanager.map.HttpConnAction;
import com.br.svconsultoria.svnfcmanager.model.Response;
import com.br.svconsultoria.svnfcmanager.model.orm.Occurrence;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class  OccurrenceSender {

	private String TAG = "OccurrenceSender";

	private OccurrenceSender() {}

	private static OccurrenceSender instance = new OccurrenceSender();

	public static OccurrenceSender getIntance() {
		return instance;
	}

	public void sendOccurrences() throws URISyntaxException {

		if (SVOccurrenceDao.hasOccurrences()) {

			Log.d(TAG, "sendOccurrences() - Starting occurrences sender - There are Occurrences to send!");
			ApplicationControl.IS_SYNC_OCCURRENCES = true;

			if(TextUtils.isEmpty(SVMobileParamsDao.getHostParam())){
				ApplicationControl.handleMessage("Configuração inicial não encontrada.");
				Log.d(TAG, "sendOccurrences() - The necessary variables were not initialized. Closing " + TAG);
				ApplicationControl.finishSendOccurrences();
			}else{
				String uri = HttpConnAction.HTTP + SVMobileParamsDao.getHostParam() + HttpConnAction.OCCURRENCES_URI;
				Log.d(TAG, "sendOccurrences() - URI: " + uri);
				Log.d(TAG, "sendOccurrences() - Parameters has been set.");
				try {
					OccurrenceSenderThread call = new OccurrenceSenderThread();
					Log.d(TAG, "sendOccurrences() - Execute!");
					call.execute();
				} catch (NullPointerException e) {
					Log.d(TAG, "sendOccurrences() - The necessary variables were not initialized. Closing HttpServerCall");
				} catch (Exception e) {
					e.printStackTrace();
					Log.d(TAG, "sendOccurrences()- Exception: " + e.getMessage());
				}
			}
		} else {
			Log.d(TAG, "sendOccurrences() - There are no occurrences to send...");
			ApplicationControl.finishSendOccurrences();
		}
	}

}
