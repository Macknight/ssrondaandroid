package com.br.svconsultoria.svnfcmanager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.br.svconsultoria.svnfcmanager.dao.SVRfidTagDao;
import com.br.svconsultoria.svnfcmanager.model.orm.RfidTag;

public class CheckpointUpdateActivity extends BaseNFCActivity {

    private Button save = null;
    private EditText id = null;
    private EditText tag = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_checkpoint_update);

        id = (EditText) findViewById(R.id.editText1);
        tag = (EditText) findViewById(R.id.editText2);

        save = (Button) findViewById(R.id.button1);

        save.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if ((id != null) && (id.getText().toString() != "") && (tag != null) && (tag.getText().toString() != "")) {
                    Toast.makeText(getApplicationContext(), "" + id.getText().toString(), Toast.LENGTH_LONG).show();

                    RfidTag rfidTag = SVRfidTagDao.getTagById(Long.valueOf(id.getText().toString()));

                    if (rfidTag != null) {
                        rfidTag.setSerialNumber(tag.getText().toString());
                        SVRfidTagDao.insertOrReplaceRfidTag(rfidTag);
                        Toast.makeText(getApplicationContext(), "Tag inserido com sucesso", Toast.LENGTH_LONG).show();
                        tag.setText("");
                        id.setText("");
                    } else {
                        Toast.makeText(getApplicationContext(), "Tag não existe", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Insira dados em todos os campos!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.checkpoint_update, menu);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String content = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                Vibrator vibrator;
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(150);

                tag.setText(content);

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Leitura falhou!", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void handleTag(String sTag) {
        tag.setText(sTag);
    }

}
