package com.br.svconsultoria.svnfcmanager;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.support.multidex.MultiDexApplication;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.br.svconsultoria.svnfcmanager.dao.SVGenericDatabaseDao;
import com.br.svconsultoria.svnfcmanager.dao.SVMobilePhoneDao;

import com.br.svconsultoria.svnfcmanager.http.events.EventSender;
import com.br.svconsultoria.svnfcmanager.http.events.VehicleCheckSender;
import com.br.svconsultoria.svnfcmanager.http.occurrrence.OccurrenceSender;
import com.br.svconsultoria.svnfcmanager.http.sync.DatabaseUpdater;
import com.br.svconsultoria.svnfcmanager.http.vigilantPhoto.VigilantPhotoSender;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoMaster;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.services.RouteMonitorService;
import com.br.svconsultoria.svnfcmanager.services.SilentAlertService;
import com.br.svconsultoria.svnfcmanager.session.UserSession;
import com.br.svconsultoria.svnfcmanager.tools.HandlerThread;
import com.br.svconsultoria.svnfcmanager.util.SVUtil;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 	Possui <b>diretivas globais</b> para a aplicação e onde são <b>inicializados os serviços.</b> <br />
 * 	Para informações mais detalhadas, consultar README nos documentos do sistema na intranet <br /><br />
 * 	<b>RESTART_DATABASE</b>: recria o banco de dados todo quando seu valor é <b>true</b> (necessário no primeiro deploy) <br />
 * 	<b>DEVELOPMENT</b>: <b>true</b> indica que a aplicação está no modo <b>DESENVOLVIMENTO</b>
 *
 */
public class ApplicationControl extends MultiDexApplication {

	private MyCounter timer;
	private static final String TAG = "ApplicationControl";

	// TODO parametrizar timeout
	private static final long TIMEOUT_MILLIS = 5*60*60*1000;//5 hours
	private static final long COUNTDOWN_INTERVAL = 10000;//10 ses

	public static String TELEPHONE_IMEI = "";

	// For development, delete the entire database and re-create it
	private boolean RESTART_DATABASE = false;
	public static boolean DEVELOPMENT = false;

	private static SQLiteOpenHelper helper = null;
	private static SQLiteDatabase db = null;

	private static DaoMaster daoMaster = null;
	public static DaoSession daoSession = null;

	// All messages set in here will be "toasted" and then the variable will be
	// restarted
	public static String message = "";

	// Http Server Control - All server calls use this client
	public static HttpClient httpclient = null;
	public static boolean IS_AUTHENTICATED = false;
//	public static boolean SEND_FINISHED = true;
	public static boolean IS_SERVER_ON = true;
//	public static boolean SYNC_FINISHED = true;
	public static boolean IS_SYNCHRONIZING = false;
	public static boolean IS_ADMIN_MODE = true;
	public static int SYNC_COUNTER = 0;

	public static boolean IS_SYNC_OCCURRENCES = false;
	public static boolean IS_SYNC_PHOTOS = false;

	public static UserSession userSession = null;
	public static long lastLoggedViggilantId = 0L;
	public static Intent alertServiceIntent = null;
	public static Intent routeMonitorServiceIntent = null;

	// TODO parametrizar numero de pacotes a serem enviados
	public static int N_EVENTS = 5;
	public static int N_OCCURRENCES = 5;
	public static int N_PHOTOS = 5;

	// CUSTOM jorge.lucas 20052015
	// string que indica a data e hora que o aplicativo foi inicializado
	public static String stAppStartDate = getStrCurrentDateHour("dd/MM/yyyy HH:mm:ss");
	public static String stAppLastSyncHour = "-"; // hora da ultima sincronizacao
	public static String stAppLastDataSendHour = "-"; // hora do ultimo envio de dados (eventos ou ocorrencias)

	public static Context context;

	/**
	 * retorna a hora atual do dispositivo formatada dd-MM-yyyy HH:mm:ss
	 * @return String
	 */
	public static String getStrCurrentDateHour(String mask) {  return new SimpleDateFormat(mask).format(new Date()); }

	@Override
	public void onCreate() {
		super.onCreate();

		try {
			HandlerThread hThread = new HandlerThread(getApplicationContext());
			hThread.start();

			helper = new DaoMaster.DevOpenHelper(getApplicationContext(), "svronda", null);
			db = helper.getWritableDatabase();
			daoMaster = new DaoMaster(db);
			daoSession = daoMaster.newSession();


			TELEPHONE_IMEI = getDeviceIMEI();

			if (RESTART_DATABASE) {
				Log.d(TAG, "[DEVELOPMENT MODE] RECREATING DATABASE");
				SVGenericDatabaseDao.reCreateDatabase(helper);
				SVGenericDatabaseDao.initializeDatabase();
			} else {
				if (!SVGenericDatabaseDao.isInitialized()) {
					Log.d(TAG, "Database is not initialized.");
					SVGenericDatabaseDao.reCreateDatabase(helper);
					SVGenericDatabaseDao.initializeDatabase();
				} else {
					Log.d(TAG, "Database is already initialized.");
				}
			}

			timer = new MyCounter(TIMEOUT_MILLIS, COUNTDOWN_INTERVAL);
			timer.start();

			// HttpSender sender = HttpSender.getIntance();
			// sender.send("", null);
			userSession = new UserSession();
			startHttpPost();

			Log.d(TAG, "onCreate() Starting RouteMonitorService");
			routeMonitorServiceIntent = new Intent(getApplicationContext(), RouteMonitorService.class);
			getApplicationContext().startService(routeMonitorServiceIntent);

			Log.d(TAG, "onCreate() Starting SilentAlertService");
			alertServiceIntent = new Intent(getApplicationContext(), SilentAlertService.class);
			getApplicationContext().startService(alertServiceIntent);
		} catch (Exception e) {
			handleMessage(e.getMessage());
		}

		EventSender.startEventSenderTask();
		VehicleCheckSender.startVehicleCheckSenderSenderTask();

		context = getApplicationContext();
	}

	public static boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if ((netInfo != null) && netInfo.isConnectedOrConnecting())
			return true;
		return false;
	}

	private void startHttpPost() {
		Log.d(TAG, "Initializing HTTPCLIENT and HTTPPOST");
		httpclient = new DefaultHttpClient();
	}

	public void touch() {
		timer.cancel();
		timer.start();
	}

	public class MyCounter extends CountDownTimer {

		public MyCounter(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			Log.d(TAG, "Timer Completed.");
			Log.d(TAG, "finish is called");
			userSession.invalidateUser();
			Intent i = SVUtil.restartApplication(getBaseContext());
			startActivity(i);
		}

		@Override
		public void onTick(long millisUntilFinished) {
			// Log.d(TAG, "Timer: " + (millisUntilFinished / 1000));
		}
	}

	public static void handleMessage(String msg) {
		message = msg;
	}

	public static void isAuthenticated() {
		IS_AUTHENTICATED = true;
	}

	public static void isNotAuthenticated() {
		IS_AUTHENTICATED = false;
	}

	public static void startSend() {
//		SEND_FINISHED = false;
		Log.d(TAG, "Starting objects sending...");

		sendOccurrences();
		sendVigilantPhotos();

	}


	public static void sendVigilantPhotos() {
		VigilantPhotoSender vPhotoSender = VigilantPhotoSender.getIntance();
		try {
			vPhotoSender.sendPhotos();
//			if (!IS_SYNC_PHOTOS){
//				vPhotoSender.sendPhotos();
//				IS_SYNC_PHOTOS= true;
//			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	public static void sendOccurrences() {
		OccurrenceSender occurrenceSender = OccurrenceSender.getIntance();
		Log.d(TAG, "Starting occurrences sending...");
		try {
			occurrenceSender.sendOccurrences();
//			if (!IS_SYNC_OCCURRENCES){
//				IS_SYNC_OCCURRENCES = true;
//				occurrenceSender.sendOccurrences();
//			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}


//	public static void finishSend() {
//		SEND_FINISHED = true;
//		Log.d(TAG, "finishSend() - Sending Events has finished.");
//	}

	public static void finishSendOccurrences() {
		IS_SYNC_OCCURRENCES = false;
		Log.d(TAG, "finishSendOccurrences() - Sending Occurrences has finished.");
	}

	public static void finishSendPhotos() {
		IS_SYNC_PHOTOS = false;
		Log.d(TAG, "finishSendPhotos() - Sending Photos has finished.");
	}


	public static void finishSynch() {
		IS_SYNCHRONIZING = false;
		// Log.d(TAG, "finishSynch() - SYNC_COUNTER: " + SYNC_COUNTER);
		// if (SYNC_COUNTER == 0) {
		// SYNC_FINISHED = true;
		// Log.d(TAG,
		// "finishSynch() - Synchronizing with server has finished.");
		// handleMessage("Sincronização concluída!");
		// // PrintTable.printAll();
		// }
	}

	public static void checkFinishedSynch(boolean callSyncAgain) {
	    Log.d(TAG, "checkFinishedSynch() - ApplicationControl.SYNC_COUNTER = " + ApplicationControl.SYNC_COUNTER + ".");
	    Log.d(TAG, "checkFinishedSynch() - ApplicationControl.IS_SYNCHRONIZING = " + IS_SYNCHRONIZING + ".");

		if(callSyncAgain){
			Log.d(TAG, "checkFinishedSynch() - Resynching...");
			IS_SYNCHRONIZING = true;
			ApplicationControl.syncWithServer();
		}else {
			if (/*ApplicationControl.SYNC_COUNTER <= 0 &&*/ !IS_SYNCHRONIZING) {
//			ApplicationControl.syncWithServer();
				IS_SYNCHRONIZING = false;
				Log.d(TAG, "checkFinishedSynch() - Synchronizing with server has finished.");
			} else {
				Log.d(TAG, "checkFinishedSynch() - Resynching...");
				IS_SYNCHRONIZING = true;
				ApplicationControl.syncWithServer();
			}
		}
	}


	public static void syncWithServer() {
		Log.d("break","point");
		if (ApplicationControl.isOnline()) {
		DatabaseUpdater updater = DatabaseUpdater.getIntance();
		try {
			Log.d(TAG, "Trying to synchronize data with server...");
			IS_SYNCHRONIZING = true;
			updater.updateAllTables();
		} catch (URISyntaxException e) {
			Log.d(TAG, "Synchronization failed...");
			handleMessage("Falha na sincronização dos dados. Tente novamente mais tarde.");
			e.printStackTrace();
		}
		} else {
			ApplicationControl.handleMessage("Sem conexão com o servidor...");
		}
	}

	public static long getDeviceId()
	{
		long mobileId = 0L;

		try {
			mobileId = SVMobilePhoneDao.getMobileId();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mobileId;
	}

	// CUSTOM jorge.lucas 22052015 first run check - taken from NewMobile
	public static boolean isFirstRun() {
		Log.d(TAG, "isFirstRun getDeviceId()" + getDeviceId());

		return (getDeviceId() == 0);// && (isUpdateSequenceTableEmpty()) /*&& (database_url.length() == 0)*/;
	}

//	public static boolean isUpdateSequenceTableEmpty()
//	{
//		// retrieve SSTM_UPDATE_SEQUENCE rows
//		// if a single row is != than 0, update sequence is not empty
//		List<InsertSequence> insertSequenceList = ApplicationControl.daoSession.getInsertSequenceDao().queryBuilder().list();
//		boolean isInsertSequenceEmpty = true;
//		for (InsertSequence is : insertSequenceList)
//		{
//			if (!"".equals(is.getTableName()))
//				isInsertSequenceEmpty = false;
//
//			break;
//		}
//		return isInsertSequenceEmpty;
//	}

	public String getDeviceIMEI() {
		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		String imei = telephonyManager.getDeviceId();
		if (imei.equals("00000000")) {
			try {
				Class<?>[] parameter = new Class[1];
				parameter[0] = int.class;
				Method getFirstMethod = telephonyManager.getClass().getMethod(
						"getDeviceId", parameter);
				Object[] obParameter = new Object[1];
				obParameter[0] = 1;
				imei = (String) getFirstMethod.invoke(telephonyManager,
						obParameter);
			} catch (Exception ex) {
				imei = "???";
			}
		}
		return imei;
	}


}
