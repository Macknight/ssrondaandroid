package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Transient;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Entity mapped to table SRTM_CUSTOMERS.
 */
@Entity
public class Customer {

    @Id
    private Long customerId;
    private Long xId;
    private long statusId;
    private String customerCNPJ;
    private String customerName;
    private String phone;


    @Transient
    public static final String NO_CUSTOMER_FLAG = "";


    @Generated(hash = 1746389175)
    public Customer(Long customerId, Long xId, long statusId, String customerCNPJ,
            String customerName, String phone) {
        this.customerId = customerId;
        this.xId = xId;
        this.statusId = statusId;
        this.customerCNPJ = customerCNPJ;
        this.customerName = customerName;
        this.phone = phone;
    }


    @Generated(hash = 60841032)
    public Customer() {
    }


    public Long getCustomerId() {
        return this.customerId;
    }


    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }


    public Long getXId() {
        return this.xId;
    }


    public void setXId(Long xId) {
        this.xId = xId;
    }


    public long getStatusId() {
        return this.statusId;
    }


    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }


    public String getCustomerCNPJ() {
        return this.customerCNPJ;
    }


    public void setCustomerCNPJ(String customerCNPJ) {
        this.customerCNPJ = customerCNPJ;
    }


    public String getCustomerName() {
        return this.customerName;
    }


    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }


    public String getPhone() {
        return this.phone;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }

}
