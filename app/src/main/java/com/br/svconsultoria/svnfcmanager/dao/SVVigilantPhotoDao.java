package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.InsertSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.VigilantPhotos;
import com.br.svconsultoria.svnfcmanager.model.orm.VigilantPhotosDao;

import java.util.ArrayList;

public class SVVigilantPhotoDao {

	private static final String TAG = "SVVigilantPhotoDao";

	private static DaoSession session = ApplicationControl.daoSession;

	public static ArrayList<VigilantPhotos> getVigilantPhotos() {
		return (ArrayList<VigilantPhotos>) session.getVigilantPhotosDao().queryBuilder().list();
	}

	public static long getVigilantPhotosCountById(long vigilantId) {
		Log.d(TAG, "getVigilantPhotosCountById()");

		VigilantPhotosDao dao = session.getVigilantPhotosDao();

		return dao.queryBuilder().where(VigilantPhotosDao.Properties.VigilantId.eq(vigilantId)).count();

	}

	public static void deleteCommittedPhotos(ArrayList<VigilantPhotos> vigilantPhotos) {
		session.getVigilantPhotosDao().deleteInTx(vigilantPhotos);
	}

	public static boolean hasPhotos() {
		VigilantPhotosDao dao = session.getVigilantPhotosDao();

		long count = dao.queryBuilder().count();
		return count > 0;
	}

	public static boolean vigilantHasPhotos(long vigilantId) {
		VigilantPhotosDao dao = session.getVigilantPhotosDao();
		long count = dao.queryBuilder().where(VigilantPhotosDao.Properties.VigilantId.eq(vigilantId)).count();
		return count > 0;
	}

	public static byte[] getVigilantPhoto(long vigilantId) {
		VigilantPhotosDao dao = session.getVigilantPhotosDao();
		return dao.queryBuilder().where(VigilantPhotosDao.Properties.VigilantId.eq(vigilantId)).uniqueOrThrow().getPhoto();
	}

	public static void insertNewVigilantPhoto(VigilantPhotos photo) {
		Log.d(TAG, "insertNewVigilantPhoto()");
		long vigilantPhotoId = SVInsertSequenceDao.getNextId(InsertSequenceMap.SRTM_VIGILANT_PHOTOS);
		photo.setVigilantPhotoId(vigilantPhotoId);
		session.getVigilantPhotosDao().insert(photo);
		Log.d(TAG, "insertNewVigilantPhoto() Finished!");
	}
}
