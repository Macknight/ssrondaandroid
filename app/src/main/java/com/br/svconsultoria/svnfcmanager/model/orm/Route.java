package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Entity mapped to table SRTM_ROUTES.
 */
@Entity
public class Route {

    @Id
    private Long routeId;
    private long statusId;
    private Long xId;
    private String description;
    private Boolean freeRoute;
    @Generated(hash = 557838031)
    public Route(Long routeId, long statusId, Long xId, String description,
            Boolean freeRoute) {
        this.routeId = routeId;
        this.statusId = statusId;
        this.xId = xId;
        this.description = description;
        this.freeRoute = freeRoute;
    }
    @Generated(hash = 467763370)
    public Route() {
    }
    public Long getRouteId() {
        return this.routeId;
    }
    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }
    public long getStatusId() {
        return this.statusId;
    }
    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }
    public Long getXId() {
        return this.xId;
    }
    public void setXId(Long xId) {
        this.xId = xId;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Boolean getFreeRoute() {
        return this.freeRoute;
    }
    public void setFreeRoute(Boolean freeRoute) {
        this.freeRoute = freeRoute;
    }

}
