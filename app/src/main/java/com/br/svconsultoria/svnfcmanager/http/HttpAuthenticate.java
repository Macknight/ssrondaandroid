package com.br.svconsultoria.svnfcmanager.http;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.FirstSyncActivity;
import com.br.svconsultoria.svnfcmanager.UserValidationActivity;
import com.br.svconsultoria.svnfcmanager.cipher.Crypt;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.map.HttpConnAction;
import com.br.svconsultoria.svnfcmanager.model.Response;
import com.br.svconsultoria.svnfcmanager.util.SVUtil;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;

import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class HttpAuthenticate extends AsyncTask<Void, Void, String> {

	private static final String TAG = "HttpAuthenticate";

	private static int AUTHENTICATION_CONNECTION_TIMEOUT = 30000;
	private static int AUTHENTICATION_SOCKET_TIMEOUT = 30000;
	private static String PHONE_IMEI = ApplicationControl.TELEPHONE_IMEI;

	private boolean isFirstSyncCheck = false;
	private Context activityContext = null;
	private String authResponse = null;

	public void setIsFirstSyncCheck(boolean src) {
		this.setIsFirstSyncCheck(src, null);
	}
	public void setIsFirstSyncCheck(boolean src, Context context) {
		this.isFirstSyncCheck = src;
		this.activityContext = context;
	}

	public static boolean authenticate() {
		try {
			Gson gson = new Gson();

			HttpClient httpclient = ApplicationControl.httpclient;
			HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), AUTHENTICATION_CONNECTION_TIMEOUT);
			HttpConnectionParams.setSoTimeout(httpclient.getParams(), AUTHENTICATION_SOCKET_TIMEOUT);

			URI uri = new URI(HttpConnAction.HTTP + SVMobileParamsDao.getHostParam() + HttpConnAction.AUTHENTICATE_URI);
			Log.d(TAG, "authenticate() - URI: " + uri);
			Log.d(TAG, "authenticate() - IS_AUTHENTICATE??");

			HttpPost httpPost = new HttpPost();
			httpPost.setURI(uri);

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair(HttpConnAction.ACTION_FIELD, HttpConnAction.IS_AUTHENTICATE_VALUE));
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = httpclient.execute(httpPost, responseHandler);
			Log.d(TAG, "authenticate() - Response: " + responseBody);
			Response resp = new Response();
			resp = gson.fromJson(responseBody, Response.class);

			if (resp.getSuccess()) {
				Log.d(TAG, "authenticate() - Authenticate: " + resp.getSuccess());
				Log.d(TAG, "authenticate() - New authentication not needed....");
				ApplicationControl.isAuthenticated();
				return true;
			} else {
				Log.d(TAG, "authenticate() - Not authenticated. Starting handshaking...");
				httpPost = new HttpPost(uri);
				nameValuePairs = new ArrayList<NameValuePair>(2);
				nameValuePairs.add(new BasicNameValuePair(HttpConnAction.ACTION_FIELD, HttpConnAction.CHALLENGE_VALUE));
				nameValuePairs.add(new BasicNameValuePair(HttpConnAction.IMEI_FIELD, PHONE_IMEI));
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				responseHandler = new BasicResponseHandler();
				responseBody = httpclient.execute(httpPost, responseHandler);
				Log.d(TAG, "authenticate() - ResponseBody: " + responseBody);

				resp = new Response();
				resp = gson.fromJson(responseBody, Response.class);

				nameValuePairs = new ArrayList<NameValuePair>(2);

				String answer = resp.getMessage() + PHONE_IMEI;

				answer = Crypt.md5(answer);
				answer = SVUtil.rot13(answer);
				answer = SVUtil.toHex(answer);

				nameValuePairs.add(new BasicNameValuePair(HttpConnAction.ACTION_FIELD, HttpConnAction.ANSWER_VALUE));
				nameValuePairs.add(new BasicNameValuePair(HttpConnAction.ANSWER_FIELD, answer));

				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				ResponseHandler<String> secondResponseHandler = new BasicResponseHandler();
				String secondResponseBody = httpclient.execute(httpPost, secondResponseHandler);

				resp = new Response();
				resp = gson.fromJson(secondResponseBody, Response.class);

				Log.d(TAG, "authenticate() - Authenticate: " + resp.getSuccess());

				if (resp.getSuccess()) {
					ApplicationControl.isAuthenticated();
					return true;
				} else {
					ApplicationControl.isNotAuthenticated();
					return false;
				}
			}
		} catch (Exception e) {
			Log.e(TAG, "authenticate() - " + e.toString());
			e.printStackTrace();
		}
		Log.d(TAG, "authenticate() - Finishing...");
		return false;
	}


	@Override
	protected String doInBackground(Void... params) {
		String response = "";
		try {
			Gson gson = new Gson();
			String domain = SVMobileParamsDao.getHostParam();
			ApplicationControl.IS_SERVER_ON = false;

			HttpClient httpclient = ApplicationControl.httpclient;
			HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), 5000);
			HttpConnectionParams.setSoTimeout(httpclient.getParams(), 5000);

			// URI uri = new URI(SVSystemParamsDao.getHostParam() + HttpConnAction.AUTHENTICATE_URI);
			URI uri = new URI(HttpConnAction.HTTP + domain + HttpConnAction.AUTHENTICATE_URI);

			HttpPost httpPost = new HttpPost();
			httpPost.setURI(uri);

			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair(HttpConnAction.ACTION_FIELD, HttpConnAction.IS_AUTHENTICATE_VALUE));
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = httpclient.execute(httpPost, responseHandler);
			Log.d(TAG, "isServerAlive() - Response: " + responseBody);

			////////////////////////////////////////////////////////////// ORIGINAL
			// Response resp = new Response();
			// resp = gson.fromJson(responseBody, Response.class);

			// /if (resp.getSuccess()) {
			// Log.d(TAG, "isServerAlive() : " + resp.getSuccess());

//			response = "true";
//			ApplicationControl.IS_SERVER_ON = true;
			// }
			//////////////////////////////////////////////////////////////ORIGINAL FIM



			//////////////////////////////////////////////////////////////////////////////////// TENTATIVA
			Response resp = gson.fromJson(responseBody, Response.class);
			if (resp.getSuccess()) {
				Log.d(TAG, "authenticate() - Authenticate: " + resp.getSuccess());
				Log.d(TAG, "authenticate() - New authentication not needed....");
				ApplicationControl.isAuthenticated();

				response = "true";
				ApplicationControl.IS_SERVER_ON = true;
			} else {
				Log.d(TAG, "authenticate() - Not authenticated. Starting handshaking...");
				httpPost = new HttpPost(uri);
				nameValuePairs = new ArrayList<NameValuePair>(2);
				nameValuePairs.add(new BasicNameValuePair(HttpConnAction.ACTION_FIELD, HttpConnAction.CHALLENGE_VALUE));
				nameValuePairs.add(new BasicNameValuePair(HttpConnAction.IMEI_FIELD, PHONE_IMEI));
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				responseHandler = new BasicResponseHandler();
				responseBody = httpclient.execute(httpPost, responseHandler);
				Log.d(TAG, "authenticate() - ResponseBody: " + responseBody);

				resp = new Response();
				resp = gson.fromJson(responseBody, Response.class);

				nameValuePairs = new ArrayList<NameValuePair>(2);

				String answer = resp.getMessage() + PHONE_IMEI;

				answer = Crypt.md5(answer);
				answer = SVUtil.rot13(answer);
				answer = SVUtil.toHex(answer);

				nameValuePairs.add(new BasicNameValuePair(HttpConnAction.ACTION_FIELD, HttpConnAction.ANSWER_VALUE));
				nameValuePairs.add(new BasicNameValuePair(HttpConnAction.ANSWER_FIELD, answer));

				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				ResponseHandler<String> secondResponseHandler = new BasicResponseHandler();
				String secondResponseBody = httpclient.execute(httpPost, secondResponseHandler);

				resp = new Response();
				resp = gson.fromJson(secondResponseBody, Response.class);

				Log.d(TAG, "authenticate() - Authenticate: " + resp.getSuccess());

				if (resp.getSuccess()) {
					ApplicationControl.isAuthenticated();

					response = "true";
					ApplicationControl.IS_SERVER_ON = true;
				} else {
					ApplicationControl.isNotAuthenticated();
					response = "false";
					ApplicationControl.IS_SERVER_ON = false;
					authResponse = resp.getMessage();
				}
			}
			//////////////////////////////////////////////////////////////////////////////////// FIM TENTATIVA
		} catch (ConnectTimeoutException cte) {
			Log.d(TAG, "authenticate() - Server Response Timeout. (ConnectTimeoutException)");
			response = "Verifique o funcionamento do servidor: tempo esgotado de resposta - " + SVMobileParamsDao.getHostParam();
			cte.printStackTrace();
		} catch (JsonSyntaxException jse) {
			Log.d(TAG, "authenticate() - Malformed Json Response. (JsonSyntaxException)");
			response = " Malformed Json Response";
			jse.printStackTrace();
		} catch (UnknownHostException uhe) {
			Log.d(TAG, "authenticate() - Invalid host address. (UnknownHostException)");
			response = "Verifique o endereço do servidor: endereço inválido do servidor - " + SVMobileParamsDao.getHostParam();
			uhe.printStackTrace();
		} catch (IllegalStateException ise) {
			Log.d(TAG, "authenticate() - Invalid host address. (IllegalStateException)");
			response = "Verifique o endereço do servidor: endereço inválido do servidor - " + SVMobileParamsDao.getHostParam();
			ise.printStackTrace();
		} catch (HttpResponseException hre) {
			Log.d(TAG, "authenticate() - Invalid server response. (HttpResponseException)");
			response = "Verifique o funcionamento do servidor: resposta inválida do servidor - " + SVMobileParamsDao.getHostParam();
			hre.printStackTrace();
		} catch (SocketException se) {
			Log.d(TAG, "authenticate() - Socket error. (SocketException)");
			response = "Verifique o endereço do servidor: erro de socket  - " + SVMobileParamsDao.getHostParam();
			se.printStackTrace();
		} catch (NullPointerException npe) {
			Log.d(TAG, "authenticate() - Empty server response. (NullPointerException)");
			response = "Verifique o endereço do servidor: falha de comunicação com o servidor - " + SVMobileParamsDao.getHostParam();
			npe.printStackTrace();
		} catch (SocketTimeoutException ste) {
			Log.d(TAG, "authenticate() - Empty server response. (SocketTimeoutException)");
			response = "Verifique o endereço do servidor: falha de comunicação com o servidor - " + SVMobileParamsDao.getHostParam();
			ste.printStackTrace();
		} catch (Exception e) {
				Log.d(TAG, "authenticate() - " + e.toString());
				response = "Verifique se o servidor está ativo e ou o endereço IP está correto: " + e.toString();
				e.printStackTrace();
		}
		Log.d(TAG, "authenticate() - Finishing...");
		ApplicationControl.IS_SERVER_ON = response.equals("true");
		UserValidationActivity.message = response;
		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (UserValidationActivity.tvServer != null) {
			if (ApplicationControl.IS_SERVER_ON) {
				UserValidationActivity.tvServer.setText("ON");

			} else {
				UserValidationActivity.tvServer.setText("OFF");
			}
		}

		if (isFirstSyncCheck) {
			Log.d(TAG, "First run!!!");

			// test if server is alive
			if (!ApplicationControl.IS_SERVER_ON) {
				Toast.makeText(activityContext, "Ocorreu um erro na tentativa de comunicação com o servidor"
						+ (this.authResponse != null ? "\n - " + this.authResponse : "\n - Verifique se ele está ativo!"), Toast.LENGTH_LONG).show();
				Log.e(TAG, "ERROR: Server offline!");
				return;
			}

			Intent ok = new Intent(activityContext, FirstSyncActivity.class);
			ok.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			activityContext.startActivity(ok);
		}
	}

}
