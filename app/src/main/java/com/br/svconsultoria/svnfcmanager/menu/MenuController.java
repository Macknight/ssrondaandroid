package com.br.svconsultoria.svnfcmanager.menu;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.MenuItem;

import com.br.svconsultoria.svnfcmanager.AdminLoginActivity;
import com.br.svconsultoria.svnfcmanager.FirstAccessActivity;
import com.br.svconsultoria.svnfcmanager.R;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;

public class MenuController {

	public static Intent getMenuAction(Context ctx, MenuItem item) {

		Intent intent = null;

		if (item.getItemId() == R.id.action_admin) {
				if (SVMobileParamsDao.isConfigured()) {
					Log.d("MenuController", "getMenuAction() - System is already configured. Redirecting to Logon screen.");
					intent = new Intent(ctx, AdminLoginActivity.class);
				} else {
					Log.d("MenuController", "getMenuAction() - System is not configured yet. Redirecting to First Access Screen.");
					intent = new Intent(ctx, FirstAccessActivity.class);
				}
		}
		else
				intent = null;

		return intent;
	}
}
