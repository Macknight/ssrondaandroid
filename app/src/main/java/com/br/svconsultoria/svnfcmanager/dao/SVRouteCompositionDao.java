package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteComposition;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteCompositionDao;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;

import java.util.List;

public class SVRouteCompositionDao {


	private static DaoSession session = ApplicationControl.daoSession;

	public static void insertOrUpdateRouteCompositions(final List<RouteComposition> list) {
		Log.d("SVRouteCompositionDao", "insertOrUpdateRouteCompositions() Starting...");
		session.runInTx(new Runnable() {

			@Override
			public void run() {
				RouteCompositionDao routecompositiondao = session.getRouteCompositionDao();
				routecompositiondao.insertOrReplaceInTx(list);
			}
		});

		long xid = SVUpdateSequenceDao.getXid(UpdateSequenceMap.SRT_ROUTE_COMPOSITION).getxId();
		for (RouteComposition object : list) {
			if (xid < object.getXId()) {
				xid = object.getXId();
			}
		}
		SVUpdateSequenceDao.updateXId(new UpdateSequence(null, UpdateSequenceMap.SRT_ROUTE_COMPOSITION, xid));

		Log.d("SVRouteCompositionDao", "insertOrUpdateRouteCompositions() Finished!");
	}

	public static List<RouteComposition> getRouteComposition(long routeId) {
		Log.d("SVRouteCompositionDao", "getRouteComposition() Starting...");
		List<RouteComposition> composition = null;
		RouteCompositionDao routecompositiondao = session.getRouteCompositionDao();
		composition = routecompositiondao.queryBuilder().where(RouteCompositionDao.Properties.RouteId.eq(routeId))
				.where(RouteCompositionDao.Properties.StatusId.eq(1))
				.orderAsc(RouteCompositionDao.Properties.OrderId).list();
		Log.d("SVRouteCompositionDao", "getRouteComposition() Finished!");
		return composition;
	}
}
