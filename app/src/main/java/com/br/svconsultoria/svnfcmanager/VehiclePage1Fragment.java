package com.br.svconsultoria.svnfcmanager;

import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.br.svconsultoria.svnfcmanager.model.orm.VehicleCheck;

import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VehiclePage1Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VehiclePage1Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehiclePage1Fragment extends Fragment {

    EditText vehiclePlate;
    AutoCompleteTextView vehicleModel;
    EditText vehiclekm;
    SeekBar fuelSeekBar;
    TextView fuelSeekBarText;
    DatePicker lastOilChange;

    RadioGroup cleaning;
    RadioButton cleaningClean;
    RadioButton cleaningDirty;
    RadioButton cleaningVeryDirty;

    RadioGroup triangle;
    RadioButton trianglePresent;
    RadioButton triangleAbsent;

    RadioGroup wheelWrench;
    RadioButton wheelWrenchPresent;
    RadioButton wheelWrenchAbsent;

    RadioGroup jack;
    RadioButton jackPresent;
    RadioButton jackAbsent;

    RadioGroup steppe;
    RadioButton steppePressurized;
    RadioButton steppeDepressurized;
    RadioButton steppeAbsent;

    RadioGroup horn;
    RadioButton hornWorking;
    RadioButton hornDamaged;

    RadioGroup water;
    RadioButton waterOnLevel;
    RadioButton waterBelowLevel;

    RadioGroup oil;
    RadioButton oilOnLevel;
    RadioButton oilBelowLevel;

    RadioGroup lowBeam;
    RadioButton lowBeamWorking;
    RadioButton lowBeamBurned;

    RadioGroup highBeam;
    RadioButton highBeamWorking;
    RadioButton highBeamBurned;

    RadioGroup indicator;
    RadioButton indicatorWorking;
    RadioButton indicatorBurned;

    RadioGroup breakLight;
    RadioButton breakLightWorking;
    RadioButton breakLightBurned;

    RadioGroup reverseLight;
    RadioButton reverseLightWorking;
    RadioButton reverseLightBurned;

    EditText vehicleComments;


    private OnFragmentInteractionListener mListener;

    public VehiclePage1Fragment() {
        // Required empty public constructor
    }

    private String getSelectedRadioText(RadioGroup radioButtonGroup){
        int radioButtonID = radioButtonGroup.getCheckedRadioButtonId();
        View radioButton = radioButtonGroup.findViewById(radioButtonID);
        int idx = radioButtonGroup.indexOfChild(radioButton);
        RadioButton r = (RadioButton)  radioButtonGroup.getChildAt(idx);
        return r.getText().toString();
    }
    public void fillVehicleCheckData(VehicleCheck vehicleCheck){

        vehicleCheck.setPlate(vehiclePlate.getText().toString());
        vehicleCheck.setModel(vehicleModel.getText().toString());
        vehicleCheck.setKm(vehiclekm.getText().toString());
        vehicleCheck.setFuel(fuelSeekBarText.getText().toString());
        vehicleCheck.setComments(vehicleComments.getText().toString());

        int day = lastOilChange.getDayOfMonth();
        int month = lastOilChange.getMonth();
        int year =  lastOilChange.getYear();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        vehicleCheck.setLastOilChange(calendar.getTime());

        vehicleCheck.setCleaning(getSelectedRadioText(cleaning));
        vehicleCheck.setTriangle(getSelectedRadioText(triangle));
        vehicleCheck.setWheelWrench(getSelectedRadioText(wheelWrench));
        vehicleCheck.setJack(getSelectedRadioText(jack));
        vehicleCheck.setSteppe(getSelectedRadioText(steppe));
        vehicleCheck.setHorn(getSelectedRadioText(horn));
        vehicleCheck.setWaterLevel(getSelectedRadioText(water));
        vehicleCheck.setOilLevel(getSelectedRadioText(oil));
        vehicleCheck.setLowBeam(getSelectedRadioText(lowBeam));
        vehicleCheck.setHighBeam(getSelectedRadioText(highBeam));
        vehicleCheck.setIndicator(getSelectedRadioText(indicator));
        vehicleCheck.setBreakLight(getSelectedRadioText(breakLight));
        vehicleCheck.setReverseLight(getSelectedRadioText(reverseLight));


    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment VehiclePage1Fragment.
     */
    public static VehiclePage1Fragment newInstance() {
        VehiclePage1Fragment fragment = new VehiclePage1Fragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vehicle_page1, container, false);

        vehiclePlate = (EditText) view.findViewById(R.id.vehiclePlate);
        vehicleModel = (AutoCompleteTextView) view.findViewById(R.id.vehicleModel);
        vehiclekm = (EditText) view.findViewById(R.id.vehiclekm);
        fuelSeekBar = (SeekBar) view.findViewById(R.id.fuelSeekBar);
        fuelSeekBarText = (TextView) view.findViewById(R.id.fuelSeekBarText);

        cleaning = (RadioGroup) view.findViewById(R.id.cleaning);
        cleaningClean = (RadioButton) view.findViewById(R.id.cleaningClean);
        cleaningDirty = (RadioButton) view.findViewById(R.id.cleaningDirty);
        cleaningVeryDirty = (RadioButton) view.findViewById(R.id.cleaningVeryDirty);

        triangle = (RadioGroup) view.findViewById(R.id.triangle);
        trianglePresent = (RadioButton) view.findViewById(R.id.trianglePresent);
        triangleAbsent = (RadioButton) view.findViewById(R.id.triangleAbsent);


        wheelWrench = (RadioGroup) view.findViewById(R.id.wheelWrench);
        wheelWrenchPresent = (RadioButton) view.findViewById(R.id.wheelWrenchPresent);
        wheelWrenchAbsent = (RadioButton) view.findViewById(R.id.wheelWrenchAbsent);

        jack = (RadioGroup) view.findViewById(R.id.jack);
        jackPresent = (RadioButton) view.findViewById(R.id.jackPresent);
        jackAbsent = (RadioButton) view.findViewById(R.id.jackAbsent);

        steppe = (RadioGroup) view.findViewById(R.id.steppe);
        steppePressurized = (RadioButton) view.findViewById(R.id.steppePressurized);
        steppeDepressurized= (RadioButton) view.findViewById(R.id.steppeDepressurized);
        steppeAbsent = (RadioButton) view.findViewById(R.id.steppeAbsent);

        horn = (RadioGroup) view.findViewById(R.id.horn);
        hornWorking = (RadioButton) view.findViewById(R.id.hornWorking);
        hornDamaged= (RadioButton) view.findViewById(R.id.hornDamaged);

        water = (RadioGroup) view.findViewById(R.id.water);
        waterOnLevel = (RadioButton) view.findViewById(R.id.waterOnLevel);
        waterBelowLevel = (RadioButton) view.findViewById(R.id.waterBelowLevel);

        oil = (RadioGroup) view.findViewById(R.id.oil);
        oilOnLevel = (RadioButton) view.findViewById(R.id.oilOnLevel);
        oilBelowLevel = (RadioButton) view.findViewById(R.id.oilBelowLevel);

        lowBeam = (RadioGroup) view.findViewById(R.id.lowBeam);
        lowBeamWorking = (RadioButton) view.findViewById(R.id.lowBeamWorking);
        lowBeamBurned= (RadioButton) view.findViewById(R.id.lowBeamBurned);

        highBeam = (RadioGroup) view.findViewById(R.id.highBeam);
        highBeamWorking = (RadioButton) view.findViewById(R.id.highBeamWorking);
        highBeamBurned = (RadioButton) view.findViewById(R.id.highBeamBurned);

        indicator = (RadioGroup) view.findViewById(R.id.indicator);
        indicatorWorking = (RadioButton) view.findViewById(R.id.indicatorWorking);
        indicatorBurned= (RadioButton) view.findViewById(R.id.indicatorBurned);

        breakLight = (RadioGroup) view.findViewById(R.id.breakLight);
        breakLightWorking = (RadioButton) view.findViewById(R.id.breakLightWorking);
        breakLightBurned = (RadioButton) view.findViewById(R.id.breakLightBurned);

        reverseLight = (RadioGroup) view.findViewById(R.id.reverseLight);
        reverseLightWorking = (RadioButton) view.findViewById(R.id.reverseLightWorking);
        reverseLightBurned = (RadioButton) view.findViewById(R.id.reverseLightBurned);

        lastOilChange = (DatePicker) view.findViewById(R.id.lastOilChange);
        vehicleComments = (EditText) view.findViewById(R.id.vehicleComments);


        fuelSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                fuelSeekBarText.setText(progress+"%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        vehiclePlate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validatePlate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        vehicleModel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validateModel();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        vehiclekm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validateKm();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        String[] countries = getResources().getStringArray(R.array.cars);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, countries);
        vehicleModel.setAdapter(adapter);

        return view;
    }

    public boolean validatePlate(){
        String plate = vehiclePlate.getText().toString();
        if (TextUtils.isEmpty(plate)) {
            vehiclePlate.setError(getString(R.string.error_field_required));
            return false;
        }
        if(!plate.matches("[a-zA-Z]{3}[0-9]{4}")){
            vehiclePlate.setError("Placa inválida");
            return false;
        }else{
            vehiclePlate.setError(null);
            return true;
        }
    }

    public boolean validateModel(){
        String model = vehicleModel.getText().toString();
        if (TextUtils.isEmpty(model)) {
            vehicleModel.setError(getString(R.string.error_field_required));
            return false;
        }
        vehicleModel.setError(null);
        return true;
    }

    public boolean validateKm(){
        String km = vehiclekm.getText().toString();
        if (TextUtils.isEmpty(km)) {
            vehiclekm.setError(getString(R.string.error_field_required));
            return false;
        }
        vehiclekm.setError(null);
        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
