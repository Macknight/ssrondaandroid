package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.model.XId;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequenceDao;

import java.util.ArrayList;
import java.util.List;

public class SVUpdateSequenceDao {


	private static DaoSession session = ApplicationControl.daoSession;

	public static List<XId> getUpdateObjects() {
		Log.d("SVUpdateSequenceDao", "getUpdateObjects() Starting...");
		List<XId> objects = new ArrayList<XId>();
		List<UpdateSequence> updateSequences = new ArrayList<UpdateSequence>();
		XId xid = null;

		updateSequences = session.getUpdateSequenceDao().queryBuilder().list();

		for (UpdateSequence us : updateSequences) {
			xid = new XId(us);
			objects.add(xid);
			// Log.d("SVUpdateSequenceDao", "getUpdateObjects() TableName: " + xid.getTableId());
			// Log.d("SVUpdateSequenceDao", "getUpdateObjects() XID: " + xid.getxId());
		}
		Log.d("SVUpdateSequenceDao", "getUpdateObjects() Finished!");
		return objects;
	}

	public static XId getXid(String table) {
		XId xid = null;
		UpdateSequence us = session.getUpdateSequenceDao().queryBuilder().where(UpdateSequenceDao.Properties.TableName.eq(table)).uniqueOrThrow();
		xid = new XId(us);
		return xid;
	}

	public static void updateXId(UpdateSequence updateSequence) {
		Log.d("SVUpdateSequenceDao", "getUpdateObjects() - " + updateSequence.getTableName() + " XID: " + updateSequence.getId());

		UpdateSequence us = session.getUpdateSequenceDao().queryBuilder().where(UpdateSequenceDao.Properties.TableName.eq(updateSequence.getTableName())).uniqueOrThrow();
		if (us != null) {
			updateSequence.setId(us.getId());
		}
		UpdateSequenceDao updatesequencedao = session.getUpdateSequenceDao();
		updatesequencedao.update(updateSequence);
		Log.d("SVUpdateSequenceDao", "getUpdateObjects() Finished!");
	}
}
