package com.br.svconsultoria.svnfcmanager;

import android.animation.LayoutTransition;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.br.svconsultoria.svnfcmanager.adapters.CheckpointAdapter;
import com.br.svconsultoria.svnfcmanager.dao.SVCheckpointDao;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRfidTagDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteCompositionDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteScheduleDao;
import com.br.svconsultoria.svnfcmanager.dao.SVSysParamsDao;
import com.br.svconsultoria.svnfcmanager.map.EventTypeMap;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.model.SystemCheckpoint;
import com.br.svconsultoria.svnfcmanager.model.orm.Checkpoint;
import com.br.svconsultoria.svnfcmanager.model.orm.RfidTag;
import com.br.svconsultoria.svnfcmanager.model.orm.Route;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteComposition;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteSchedule;
import com.br.svconsultoria.svnfcmanager.model.orm.SysParam;
import com.br.svconsultoria.svnfcmanager.model.orm.SysParamDao;
import com.br.svconsultoria.svnfcmanager.services.RouteMonitorService;
import com.br.svconsultoria.svnfcmanager.services.SilentAlertService;
import com.br.svconsultoria.svnfcmanager.tools.CheckpointTools;
import com.br.svconsultoria.svnfcmanager.tools.EventBuilder;
import com.br.svconsultoria.svnfcmanager.util.GPSDiff;
import com.br.svconsultoria.svnfcmanager.util.SVUtil;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.svconsultoria.newmobile.remotedebug.Log;

import static java.lang.Integer.*;

public class FreeRouteActivity extends BaseNFCActivity implements View.OnClickListener {

    private static final String TAG = "FreeRouteActivity";

    public static final boolean csnReport = true;
    private static class ClockHandler extends Handler {


        private WeakReference<FreeRouteActivity> mActivity;

        public ClockHandler(FreeRouteActivity activity) {
            mActivity = new WeakReference<FreeRouteActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {

            synchronized (mActivity.get()) {
                if (!IS_THIS_ACTIVITY_ALIVE) {
                    removeMessages(1);
                    return;
                }

                mActivity.get().setClock();
                sendMessageDelayed(obtainMessage(1), 1000);
            }
        }
    }

    private ClockHandler clockHandler;

    public static boolean IS_THIS_ACTIVITY_ALIVE = false;
    public static boolean IS_READ_ALLOWED = false;

    public static boolean DOES_OCCURRENCE_EXIST = false;

    private ArrayList<Integer> selectedItens;
    private static List<String> checked;

    private long routeId = 0;

    private AlertDialog taskDialog;

    public static TextView clock = null;
    private static TextView tvRoute = null;

    private List<SystemCheckpoint> checkpoints = null;

    private ImageView qrCodeRead = null;

    private static final int QRCODE_REQUEST = 0;

    private boolean resetTime = false;
    private long resetTimeSec = 0;

    private CheckpointAdapter checkpointAdapter;
    private ListView checkpointList;

    private boolean repeat_check_enable = false;

    private boolean idlenessEventEnable = false;
    private long idlenessEventTimeSeconds = 60*60;
    private boolean idlenessEventCreated = false;

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        SVUtil.touch(this); //can touch this
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_free_route);
        selectedItens = new ArrayList<Integer>();
        // Custom font
        Typeface typeface = Typeface.createFromAsset(getAssets(), "tt0246m_.ttf");
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
        ((TextView) findViewById(R.id.title)).setTypeface(typeface2);
        ((TextView) findViewById(R.id.tvRoute)).setTypeface(typeface);
        ((TextClock) findViewById(R.id.digitalClockRouteList)).setTypeface(typeface);


        //////// panic button
        ImageButton panicActivationButton = (ImageButton) findViewById(R.id.free_route_menu_panic);
        panicActivationButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent intent = new Intent(getApplicationContext(), PanicConfirmationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return false;
            }
        });
        /////// end panic button

        clockHandler = new ClockHandler(this);

        LayoutTransition l = new LayoutTransition();
        l.enableTransitionType(LayoutTransition.CHANGING);

        ImageView itemMap = (ImageView) findViewById(R.id.free_route_menu_item_map);
        itemMap.setOnClickListener(this);

        ImageView itemOccurrence = (ImageView) findViewById(R.id.free_route_menu_item_occurence);
        itemOccurrence.setOnClickListener(this);

        ImageView itemExit = (ImageView) findViewById(R.id.free_route_menu_item_exit);
        itemExit.setOnClickListener(this);


        qrCodeRead = (ImageView) findViewById(R.id.free_route_qrcode);
        qrCodeRead.setVisibility(View.GONE);

        if (SVMobileParamsDao.isQrCodeAllowed()) {
            qrCodeRead.setVisibility(View.VISIBLE);
            qrCodeRead.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    callQrCodeReader();
                }
            });
        }

        clock = (TextView) findViewById(R.id.clock);

        tvRoute = (TextView) findViewById(R.id.tvRoute);

        long routeScheduleId = ApplicationControl.userSession.getRouteScheduleId();
        RouteSchedule routeSchedule = SVRouteScheduleDao.getRouteScheduleById(routeScheduleId);
        List<RouteComposition> composition = SVRouteCompositionDao.getRouteComposition(routeSchedule.getRouteId());
        Route route = SVRouteDao.getRouteById(routeSchedule.getRouteId());
        routeId = route.getRouteId();
        tvRoute.setText(route.getDescription());

        if (savedInstanceState == null) {
            resetTime = true;
        }
        // Load route progression data from user session
        if (ApplicationControl.userSession.getCheckpoints() == null) {
            checkpoints = CheckpointTools.getSystemCheckpoints(composition, routeSchedule);
            ApplicationControl.userSession.setCheckpoints(checkpoints);
        } else {
            checkpoints = ApplicationControl.userSession.getCheckpoints();
        }

        checkpointList = (ListView) findViewById(R.id.free_route_checkpoint_list);


        checkpointAdapter = new CheckpointAdapter(this, checkpoints) {
            @Override
            public void onSkipClick(SystemCheckpoint ck) {
                skipCheckpointConfirm(ck);
            }

            @Override
            public void onNameClick(SystemCheckpoint ck) {
                Checkpoint cp = SVCheckpointDao.getCheckpointById(ck.getCheckpointId());
                if ((cp.getPhoto() != null)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(FreeRouteActivity.this);

                    ImageView photo = new ImageView(FreeRouteActivity.this);
                    Bitmap bmp = BitmapFactory.decodeByteArray(cp.getPhoto(), 0, cp.getPhoto().length);
                    photo.setImageBitmap(scaleBitmap(bmp));
                    builder.setView(photo).setTitle(ck.getDescription());
                    AlertDialog dialog = builder.create();
                    dialog.getWindow().setBackgroundDrawableResource(R.drawable.black_container);
                    dialog.show();
                }
            }
        };
        checkpointList.setAdapter(checkpointAdapter);

        SysParam param = SVSysParamsDao.getSysParamByName(SystemParamsMap.IDLENESS_EVENT_ENABLED);
        idlenessEventEnable = param != null && Boolean.parseBoolean(param.getValue());
        param = SVSysParamsDao.getSysParamByName(SystemParamsMap.IDLENESS_EVENT_TIME_MINUTES);
        idlenessEventTimeSeconds = param==null?60*60:parseInt(param.getValue())*60;
        repeat_check_enable = SVSysParamsDao.isFreeRouteCheckRepetitionEnabled();
        nextCheckpoint();
    }

    private Bitmap scaleBitmap(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        int maxHeight = (int) (getWindowManager().getDefaultDisplay().getHeight() * 0.8);
        int maxWidth = (int) (getWindowManager().getDefaultDisplay().getWidth() * 0.8);

        if (width > height) {
            // landscape
            float ratio = (float) width / maxWidth;
            width = maxWidth;
            height = (int) (height / ratio);
        } else if (height > width) {
            // portrait
            float ratio = (float) height / maxHeight;
            height = maxHeight;
            width = (int) (width / ratio);
        } else {
            // square
            height = maxHeight;
            width = maxWidth;
        }

        bm = Bitmap.createScaledBitmap(bm, width, height, true);
        return bm;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putLong("resetTimeSec", resetTimeSec);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("resetTimeSec")) {
            resetTimeSec = savedInstanceState.getLong("resetTimeSec");
        }
    }

    private void setClock() {
        Date now = new Date();

        long nowSecondsOfDay = (((now.getHours() * (60 * 60)) + ((now.getMinutes()) * 60))) + now.getSeconds();

        if (resetTime) {
            resetTimeSec = nowSecondsOfDay;
            resetTime = false;
            idlenessEventCreated = false;
        }

        long diffSec = Math.abs(nowSecondsOfDay - resetTimeSec);
        clock.setText(String.format(Locale.getDefault(), "%02d:%02d", diffSec / 60, diffSec % 60));
        IS_READ_ALLOWED = true;

        if(idlenessEventEnable){
            if(!idlenessEventCreated && (nowSecondsOfDay - resetTimeSec)>idlenessEventTimeSeconds){
//                EventBuilder.createIdlenessEvent(routeId);
                idlenessEventCreated=true;
                idlenessEventCreated=true;
                Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(2000);
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                r.play();

                //Show alert
                showIdlenessDialog();
            }
        }

    }

    private void showIdlenessDialog(){
        //Dialog creation
        final Dialog dialog = new Dialog(FreeRouteActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_idleness);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        final TextView text = (TextView) dialog.findViewById(R.id.dialog_idleness_message);
        Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
        text.setTypeface(typeface2);

        Button dialogButton = (Button) dialog.findViewById(R.id.dialog_idleness_ok);
        dialogButton.setTypeface(typeface2);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!text.getText().toString().isEmpty()){
                    EventBuilder.createIdlenessEvent(routeId, text.getText().toString());
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private void prepareAndExecuteSyncTask() {
        Log.d(TAG, "prepareAndExecuteSyncTask()");
        ApplicationControl.startSend();
    }


    @Override
    public void onBackPressed() {
        skipRouteConfirm();
    }

    @Override
    protected void onResume() {
        super.onResume();

        RouteMonitorService.setServiceInactive();

        Log.d(TAG, "onResume()");
        if (!IS_THIS_ACTIVITY_ALIVE) {
            IS_THIS_ACTIVITY_ALIVE = true;
            clockHandler.sendMessage(clockHandler.obtainMessage(1));
        }
        if (ApplicationControl.userSession == null
                || ApplicationControl.userSession.getLoggedVigilant() == null
                || ApplicationControl.userSession.getLoggedVigilant().getVigilantId() == null){
            Toast.makeText(this, "Sessão expirada!", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        RouteMonitorService.setServiceActive();
        Log.d(TAG, "onPause()");
        IS_THIS_ACTIVITY_ALIVE = false;
    }

    @Override
    protected void handleTag(String sTag) {
        validateCheckpoint();
    }

    @Override
    protected void onStop() {
        super.onStop();
        IS_THIS_ACTIVITY_ALIVE = false;
    }

    private void createAndShowTaskDialog(final SystemCheckpoint checkpoint) {

        List<String> activities = new ArrayList<>();
        CharSequence[] cs;
        if (checkpoint.getCheckpointChecks() != null && !checkpoint.getCheckpointChecks().isEmpty()) {
            //TODO: set the tasks to the activity.
            activities = checkpoint.getTaskList();
            cs = activities.toArray(new CharSequence[activities.size()]);
        } else {
            cs = null;
        }
        final List<String> fTasks = new ArrayList<String>();
        fTasks.addAll(activities);
        selectedItens.clear();

        //Dialog creation
        AlertDialog.Builder builder = new AlertDialog.Builder(FreeRouteActivity.this);
        builder.setTitle(checkpoint.getDescription() + "\nATIVIDADES A SEREM EXECUTADAS")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (selectedItens.size() != 0) {
                            checked = new ArrayList<String>();
                            for (int i = 0; i < selectedItens.size(); i++) {
                                checked.add(fTasks.get(selectedItens.get(i)));
                            }
                        }
                        checkpoint.setCheckedTasks(checkpoint.concatTasks(checked));
                        generateEventAndProceed2NextCheckpoint(checkpoint, true);

                    }
                })
                .setMultiChoiceItems(cs, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface taskDialog, int which, boolean isChecked) {
                                if (isChecked) {
                                    selectedItens.add(which);
                                } else if (selectedItens.contains(which)) {
                                    selectedItens.remove(valueOf(which));
                                }

                            }
                        })
                .setCancelable(false);

        taskDialog = builder.create();
        taskDialog.getWindow().setBackgroundDrawableResource(android.R.color.black);
        taskDialog.show();

    }


    private void callQrCodeReader() {
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
        startActivityForResult(intent, QRCODE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == QRCODE_REQUEST) {
            if (resultCode == RESULT_OK) {
                String content = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                Log.d(TAG, "QR CODE - Content: " + content + " Format: " + format);

                sTagSerial = content;

                Vibrator vibrator;
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(150);

                validateCheckpoint();

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Leitura falhou!", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void validateCheckpoint() {

        float minutesDiff = 0f;
        int remainingTime = 0;
        float measured = 0f; // distance measured between ckpoint read and ckpoint coordinates


        RfidTag tag = SVRfidTagDao.getTagBySerialNumber(sTagSerial);
        if (tag == null) {
            Toast.makeText(this, "TAG não cadastrada.", Toast.LENGTH_LONG).show();
            prepareAndExecuteSyncTask();
            return;
        }

        SystemCheckpoint checkpoint = null;
        for (SystemCheckpoint ck : checkpoints) {
            if (tag.getTagId().equals(ck.getTagId())) {
                checkpoint = ck;
                break;
            }
        }

        if (checkpoint == null) {
            Toast.makeText(this, "Ponto inválido.", Toast.LENGTH_LONG).show();
            prepareAndExecuteSyncTask();
            return;
        }

        Log.d(TAG, "check point id: " + checkpoint.getCheckpointId());
        if (!repeat_check_enable && checkpoint.getReadTimeMillis() != 0) {
            Toast.makeText(this, "Ponto já foi lido na rota atual.", Toast.LENGTH_LONG).show();
            prepareAndExecuteSyncTask();
            return;
        }

        // hold time millis when this expectedCheckpoint is read
        checkpoint.setReadTimeMillis(System.currentTimeMillis());
        checkpoint.setNumberOfChecks(checkpoint.getNumberOfChecks()+1);

        /*   checks gps distance */

        // if gps present & distance check enabled
        if (SilentAlertService.gps.getLatitude() != 0 && SilentAlertService.gps.getLongitude() != 0 // if gps available
                && SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE_ENABLED) != null // if distance check parameter present
                && "true".equals(SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE_ENABLED).getValue()) // if distance check enabled
                && (checkpoint.getLatitude() != 0.0f && checkpoint.getLongitude() != 0.0f) // if point have coordinates currently
                ) {
            measured = 0.0f; // km

            // calculate distance between current GPS position and expectedCheckpoint position
            measured = (float) GPSDiff.GPSDistance(SilentAlertService.gps.getLatitude(),
                    SilentAlertService.gps.getLongitude(),
                    checkpoint.getLatitude(),
                    checkpoint.getLongitude());

            // retrieve current distance tolerance (if any)
            float tolerance = (SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE) != null ?  // if distance tolerance present in database, retrieve
                    Float.valueOf(SVSysParamsDao.getSysParamByName(SystemParamsMap.CHECKPOINT_DISTANCE_TOLERANCE).getValue()) : .8f); // else, use .8 (km) as default
            // km // tolerance (local and sync)

            if (measured > tolerance) { // if too far from original expectedCheckpoint position, shows a dialog informing user about it

                // to proceed, vigilant must create an occurrence
                if (DOES_OCCURRENCE_EXIST) {
                    DOES_OCCURRENCE_EXIST = false; // value reset
                    FreeRouteActivity.this.generateEventAndProceed2NextCheckpoint(checkpoint, false); // generate event and proceed to next expectedCheckpoint
                } else
                    new AlertDialog.Builder(this)
                            .setMessage("Você está longe da posição original do expectedCheckpoint! \n\nCrie uma ocorrência para justificar a distância e leia o expectedCheckpoint novamente.")
                            .setCancelable(false)
                            .setPositiveButton("Criar ocorrência", new DialogInterface.OnClickListener() { // proceed with normal method execution
                                public void onClick(DialogInterface dialog, int id) {
                                    // occurrence button click
                                    View v = new View(FreeRouteActivity.this);
                                    v.setId(R.id.free_route_menu_item_occurence);
                                    FreeRouteActivity.this.onClick(v);
                                }
                            })
                            .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() { // read expectedCheckpoint again
                                public void onClick(DialogInterface dialog, int id) {
                                    return; // no action - waits again for current expectedCheckpoint
                                }
                            })
                            .show();
            } else {// calculated distance between current GPS coords and expectedCheckpoint coords are within current tolerance. Proceed...
                this.generateEventAndProceed2NextCheckpoint(checkpoint, false); // generate event and proceed 2 next expectedCheckpoint
            }
        } ///////////////// end gps checking distance
        else { // no GPS position read or GPS verification disabled; proceed
            this.generateEventAndProceed2NextCheckpoint(checkpoint, false); // generate event and proceed 2 next expectedCheckpoint
        }


    }

    private void generateEventAndProceed2NextCheckpoint(SystemCheckpoint checkpoint, boolean tasksChecked) {

        //Show Task check dialog if necessary
        if (!tasksChecked
                && (checkpoint.getCheckpointChecks() != null)
                && (!checkpoint.getCheckpointChecks().isEmpty())
                && (!checkpoint.getCheckpointChecks().equals("[]"))) {
            createAndShowTaskDialog(checkpoint);
            return;
        }

        // builds an event for expectedCheckpoint read
        EventBuilder.createCheckpointTagEvent(SVRfidTagDao.getTagBySerialNumber(sTagSerial.trim()).getTagId(), routeId, checkpoint.getCheckpointId(), EventTypeMap.CHECKPOINT_POSITIVE_TAG_READ_EVENT, "", checkpoint.getCheckedTasks());


        // updates expectedCheckpoint GPS position if latitude not present
        if (checkpoint.getLatitude() == 0) {
            if (SilentAlertService.gps.hasAccuracy()) {
                Log.d(TAG, "validateCheckpoint() - Updating expectedCheckpoint (id = " + checkpoint.getCheckpointId() + ")");
                checkpoint.setLatitude(SilentAlertService.gps.getLatitude());
                checkpoint.setLongitude(SilentAlertService.gps.getLongitude());
                CheckpointTools.updateCheckpointCoordinates(checkpoint);
                Log.d(TAG, "validateCheckpoint() - Update finished!");
            }
        }

        Toast.makeText(this, "Ponto registrado com sucesso!", Toast.LENGTH_LONG).show();
        prepareAndExecuteSyncTask();
        resetTime = true;

        nextCheckpoint();

    }

    private void nextCheckpoint() {

        checkpointAdapter.notifyDataSetChanged();

        boolean finished = true;
        for (SystemCheckpoint ck : checkpoints) {
            if (ck.getReadTimeMillis() == 0) {
                finished = false;
                break;
            }
        }

        if (finished && !repeat_check_enable) {
            routeFinished();
            return;
        }
    }


    private void routeFinished() {

        ApplicationControl.userSession.setRouteScheduleId(0);
        ApplicationControl.userSession.setVehicleCheck(false);
        ApplicationControl.userSession.setOrderCount(0);
        ApplicationControl.userSession.setCheckpoints(null);


        final Intent i = new Intent(this, UserValidationActivity.class);
        String sSerial = "";

        if (ApplicationControl.userSession != null)
            if (ApplicationControl.userSession.getLoggedVigilant() != null)
                if (ApplicationControl.userSession.getLoggedVigilant().getLogonTagId() != null)
                    sSerial = SVRfidTagDao.getTagById(ApplicationControl.userSession.getLoggedVigilant().getLogonTagId()).getSerialNumber();

        i.putExtra("serial", sSerial);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        // alert dialog informing user that route have finished
        new AlertDialog.Builder(this)
                .setMessage("Rota encerrada!")
                .setCancelable(false)
                .setNegativeButton("Prosseguir", new DialogInterface.OnClickListener() { // read expectedCheckpoint again
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(i);
                    }
                })
                .show();
    }

    private void skipCheckpointConfirm(final SystemCheckpoint checkpoint) {

        // IF gerado para não permitir que o vigilante pule um ponto sem criar ocorrência
        if ((!checkpoint.isMandatory()) || DOES_OCCURRENCE_EXIST) {

            // custom dialog
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.alertdialog_close_session);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            // set the custom dialog components - text, image and button
            TextView text = (TextView) dialog.findViewById(R.id.message);
            text.setText("Você confirma que deseja abandonar o ponto \"" + checkpoint.getDescription() + "\"?");
            Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
            text.setTypeface(typeface2);

            Button dialogButton2 = (Button) dialog.findViewById(R.id.noButton);
            Button dialogButton = (Button) dialog.findViewById(R.id.yesButton);
            dialogButton.setTypeface(typeface2);
            dialogButton2.setTypeface(typeface2);

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBuilder.createSkipCheckpointEvent(routeId, checkpoint, "");
                    checkpoint.setReadTimeMillis(System.currentTimeMillis());
                    nextCheckpoint();
                    dialog.dismiss();
                }

            });

            dialog.show();

            dialogButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        } else {
            // custom dialog
            final Dialog dialog = new Dialog(this);
            // requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.alertdialog_small);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            // set the custom dialog components - text, image and button
            TextView text = (TextView) dialog.findViewById(R.id.message);

            text.setText("Por favor, crie uma ocorrência justificando o motivo de pular o ponto!");
            Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
            text.setTypeface(typeface2);

            Button dialogButton = (Button) dialog.findViewById(R.id.okButton);
            dialogButton.setTypeface(typeface2);

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }

            });
            dialog.show();
        }
        DOES_OCCURRENCE_EXIST = false;
    }

    private void skipRouteConfirm() {

        boolean hasPendingMandatoryCheckpoints = false;

        for (SystemCheckpoint ck : checkpoints) {
            if (ck.isMandatory() && (ck.getReadTimeMillis() == 0)) {
                hasPendingMandatoryCheckpoints = true;
                break;
            }
        }

        // Não permitir que o vigilante saia da rota com ponto obrigatório pendente sem criar ocorrência
        if ((!hasPendingMandatoryCheckpoints) || DOES_OCCURRENCE_EXIST) {

            // custom dialog
            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.alertdialog_close_session);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            // set the custom dialog components - text, image and button
            TextView text = (TextView) dialog.findViewById(R.id.message);
            text.setText("Você confirma que deseja abandonar a rota atual?");
            Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
            text.setTypeface(typeface2);

            Button dialogButton2 = (Button) dialog.findViewById(R.id.noButton);
            Button dialogButton = (Button) dialog.findViewById(R.id.yesButton);
            dialogButton.setTypeface(typeface2);
            dialogButton2.setTypeface(typeface2);

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBuilder.createSkipRouteEvent(routeId, "");
                    prepareAndExecuteSyncTask();
                    routeFinished();
                }

            });
            dialogButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } else {
            // custom dialog
            final Dialog dialog = new Dialog(this);
            // requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.alertdialog_small);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            // set the custom dialog components - text, image and button
            TextView text = (TextView) dialog.findViewById(R.id.message);

            text.setText("Ainda existem pontos obrigatórios na rota. Por favor, crie uma ocorrência justificando o motivo de abandonar a rota atual!");
            Typeface typeface2 = Typeface.createFromAsset(getAssets(), "tt0247m_.ttf");
            text.setTypeface(typeface2);

            Button dialogButton = (Button) dialog.findViewById(R.id.okButton);
            dialogButton.setTypeface(typeface2);

            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }

            });
            dialog.show();
            //			Toast.makeText(this, "Por favor, crie uma ocorrência justificando o motivo do abandono da rota", Toast.LENGTH_LONG).show();
        }
        DOES_OCCURRENCE_EXIST = false;
    }

    @Override
    public void onClick(View v) {
        int which = v.getId();
        Log.i(TAG, "onClick which " + which);
        if (which == R.id.free_route_menu_item_map) {
            Intent i = new Intent(this, MapsActivity.class);
            List<MapsActivity.Marker> markers = new ArrayList<>();
            for (SystemCheckpoint ck : checkpoints) {
                Float color = BitmapDescriptorFactory.HUE_RED;
                if (ck.getReadTimeMillis() > 0) {
                    color = BitmapDescriptorFactory.HUE_GREEN;
                }
                markers.add(new MapsActivity.Marker(ck.getLongitude(), ck.getLatitude(), ck.getDescription(), color));
            }
            i.putExtra(MapsActivity.EXTRA_MARKERS, (Serializable) markers);
            startActivity(i);

        } else if (which == R.id.free_route_menu_item_occurence) {
            if(!csnReport) {
                Intent i = new Intent(this, OccurrenceActivity.class);
                Log.d("CustomerFilter", "Opening OccurrenceActivity w/ extra: " + this.routeId);
                i.putExtra("routeId", this.routeId);
                startActivity(i);
            }else{
                Intent i = new Intent(this, ComplexOccurrenceActivity.class);
                Log.d("CustomerFilter", "Opening OccurrenceActivity w/ extra: " + this.routeId);
                i.putExtra("routeId", this.routeId);
                startActivity(i);
            }
        } else if (which == R.id.free_route_menu_item_exit) {
            skipRouteConfirm();
        }

    }

}
