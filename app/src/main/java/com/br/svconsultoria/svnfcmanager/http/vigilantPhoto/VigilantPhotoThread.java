package com.br.svconsultoria.svnfcmanager.http.vigilantPhoto;

import android.os.AsyncTask;
import android.os.StrictMode;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVVigilantPhotoDao;
import com.br.svconsultoria.svnfcmanager.http.HttpAuthenticate;
import com.br.svconsultoria.svnfcmanager.map.HttpConnAction;
import com.br.svconsultoria.svnfcmanager.model.Response;
import com.br.svconsultoria.svnfcmanager.model.orm.VigilantPhotos;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class VigilantPhotoThread extends AsyncTask<Void, Void, Void> {

	private String TAG = "VigilantPhotoThread";
	private static int AUTHENTICATION_CONNECTION_TIMEOUT = 30000;
	private static int AUTHENTICATION_SOCKET_TIMEOUT = 30000;
	private HttpPost httppost = null;
	private Response resp = null;
	private String responseType = "";
	private ArrayList<VigilantPhotos> photos = null;

	private void initiate() {
		resp = new Response();
		httppost = new HttpPost();
	}

	@Override
	protected Void doInBackground(Void... params) {

		initiate();
		Log.d(TAG, "doInBackground() - Starting connection with server");

		setVariablesForParse();

		if (HttpAuthenticate.authenticate()) {
			Gson gson = new Gson();

			HttpClient httpclient = ApplicationControl.httpclient;
			HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), AUTHENTICATION_CONNECTION_TIMEOUT);
			HttpConnectionParams.setSoTimeout(httpclient.getParams(), AUTHENTICATION_SOCKET_TIMEOUT);

			// String uri = HttpConnAction.DATABASE_URL + HttpConnAction.VIGILANT_PHOTO_UPDATE_URI;
			String uri = HttpConnAction.HTTP + SVMobileParamsDao.getHostParam() + HttpConnAction.VIGILANT_PHOTO_UPDATE_URI;
			Log.d(TAG, "sendPhotos() - URI: " + uri);

			// /
			if (!(uri.equals("") || (uri == null))) {
				resp.setType(HttpConnAction.VIGILANT_PHOTO_UPDATE_URI);

				List<NameValuePair> args = new ArrayList<NameValuePair>();
				args.add(new BasicNameValuePair(HttpConnAction.ACTION_FIELD, HttpConnAction.VIGILANT_PHOTO_VALUE));
				photos = SVVigilantPhotoDao.getVigilantPhotos();

				for (VigilantPhotos photo : photos) {
					Log.d(TAG, "sendPhotos() - VigilantPhotoId: " + photo.getVigilantPhotoId() + " Size: " + photo.getPhoto().length);
				}

				gson = new GsonBuilder().setDateFormat("dd-MM-yyyyHH-mm-ss").create();

				String json = gson.toJson(photos);
				Log.d(TAG, "sendPhotos() - JSON VigilantPhotos ListSize: " + json.length());
				args.add(new BasicNameValuePair(HttpConnAction.LIST_FIELD, json));
				try {
					httppost.setURI(new URI(uri));
				} catch (URISyntaxException e1) {
					e1.printStackTrace();
				}

				try {
					httppost.setEntity(new UrlEncodedFormEntity(args));
				} catch (UnsupportedEncodingException e) {
					ApplicationControl.handleMessage("HTTP: Formato de dados não suportado");
					e.printStackTrace();
				}// /

			}

			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = "";
			try {
				responseBody = httpclient.execute(httppost, responseHandler);
				Log.d(TAG, "doInBackground() - Response: " + responseBody);

				resp = gson.fromJson(responseBody, Response.class);

				if ((resp == null) || resp.equals("")) {
					Log.d(TAG, "doInBackground() - Response from server is empty. Exiting...");
					throw new NullPointerException();
				}

				resp.setList(photos);
				getVariablesForParse();

				VigilantPhotoResponseHandler photoResponseHandler = new VigilantPhotoResponseHandler();
				photoResponseHandler.process(resp);

			} catch (ClientProtocolException e) {
				Log.d(TAG, "doInBackground() - ClientProtocolException");
				// ApplicationControl.handleMessage("HTTP: Protocolo de comunicação não suportado");
				e.printStackTrace();
				return null;
			} catch (NullPointerException e) {
				Log.d(TAG, "doInBackground() - NullPointerException");
				// ApplicationControl.handleMessage("Resposta do servidor vazia");
				e.printStackTrace();
				return null;
			} catch (IOException e) {
				Log.d(TAG, "doInBackground() - IOException");
				// ApplicationControl.handleMessage("HTTP: Entrada/saída de dados interrompida");
				 e.printStackTrace();
				return null;
			} catch (Exception e) {
				Log.d(TAG, "doInBackground() - Exception");
				// ApplicationControl.handleMessage("HTTP: Entrada/saída de dados interrompida");
				e.printStackTrace();
				return null;
			}

		} else {
			ApplicationControl.finishSendPhotos();
		}
		ApplicationControl.finishSendPhotos();
		return null;
	}

	public void setResponse(Response resp) {
		this.resp = resp;
	}

	public void setHttpPost(HttpPost post) {
		httppost = post;
	}


	private void setVariablesForParse() {
		if ((resp.getType() != null) && (resp.getType() != "")) {
			responseType = resp.getType();
		}
	}

	private void getVariablesForParse() {
		if ((responseType != null) && (responseType != "")) {
			resp.setType(responseType);
		}
	}
}
