package com.br.svconsultoria.svnfcmanager.http.sync;

import android.annotation.SuppressLint;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.dao.SVCheckpointDao;
import com.br.svconsultoria.svnfcmanager.dao.SVCustomerDao;
import com.br.svconsultoria.svnfcmanager.dao.SVMobilePhoneDao;
import com.br.svconsultoria.svnfcmanager.dao.SVOccurrenceTypesDao;
import com.br.svconsultoria.svnfcmanager.dao.SVPriorityDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRfidTagDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteCompositionDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteScheduleDao;
import com.br.svconsultoria.svnfcmanager.dao.SVSysParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVUserDao;
import com.br.svconsultoria.svnfcmanager.dao.SVVigilantDao;
import com.br.svconsultoria.svnfcmanager.map.HttpConnAction;
import com.br.svconsultoria.svnfcmanager.map.SystemParamsMap;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.Response;
import com.br.svconsultoria.svnfcmanager.model.orm.Checkpoint;
import com.br.svconsultoria.svnfcmanager.model.orm.Customer;
import com.br.svconsultoria.svnfcmanager.model.orm.MobilePhone;
import com.br.svconsultoria.svnfcmanager.model.orm.OccurrenceType;
import com.br.svconsultoria.svnfcmanager.model.orm.Priority;
import com.br.svconsultoria.svnfcmanager.model.orm.RfidTag;
import com.br.svconsultoria.svnfcmanager.model.orm.Route;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteComposition;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteSchedule;
import com.br.svconsultoria.svnfcmanager.model.orm.SysParam;
import com.br.svconsultoria.svnfcmanager.model.orm.User;
import com.br.svconsultoria.svnfcmanager.model.orm.Vigilant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import br.com.svconsultoria.newmobile.remotedebug.Log;

public class DatabaseUpdateResponseHandler {
	private String TAG = "DatabaseUpdateResponseHandler";


	/**
	 * Because this code sucks
	 */
	boolean callSyncAgain = false;

	public void process(Response response) {
		Log.d(TAG, "process()");
		Log.d(TAG, "process() - responseType: " + response.getType());
		try {
			if (response.getSuccess()) {
				if (response.getType().equals(HttpConnAction.UPDATE_CHECK_URI)) {
					Log.d(TAG, "process() - updateAllTables received (size " + response.getList().size() + ").");
					ApplicationControl.SYNC_COUNTER = response.getList().size();
					for (Object table : response.getList()) {
						Log.d(TAG, "process() - processing table " + (String)table);
						if (contain((String) table)) {
							String t = (String) table;
							Log.d(TAG, "process() - updating " + t);
							DatabaseUpdater.getIntance().updateTable(t);

							// CUSTOM jorge.lucas 20052015 last sync formatted date hour string
//							ApplicationControl.stAppLastSyncHour = ApplicationControl.getStrCurrentDateHour("HH:mm:ss");
							// FIM CUSTOM
						} else {
							Log.e(TAG, "process() - updateAllTables - Table " + table + " is not a system Table.");
						}
					}
					ApplicationControl.finishSynch();
				} else if (response.getType().equals(HttpConnAction.UPDATE_TABLES_URI)) {
					parseResponseFromUpdateTable(response);
				}
			} else {
				ApplicationControl.IS_SYNCHRONIZING = false;
				ApplicationControl.handleMessage(response.getMessage());
			}
		} catch (NullPointerException e) {
			Log.d(TAG, "process() - Transaction failed. This will try again later... (NullPointerException)");
			ApplicationControl.handleMessage("DatabaseUpdateResponseHandler()- Exception: " + e.getMessage());
			ApplicationControl.finishSynch();
			e.printStackTrace();
		} catch (URISyntaxException e) {
			Log.d(TAG, "process() - Transaction failed. This will try again later... (URISyntaxException)");
			ApplicationControl.handleMessage("DatabaseUpdateResponseHandler()- Exception: " + e.getMessage());
			ApplicationControl.finishSynch();

		}
	}


	@SuppressWarnings("unchecked")
	private void parseResponseFromUpdateTable(Response response) {

		Log.d(TAG, "parseResponseFromUpdateTable() - updateTable received");
		Log.d(TAG, "parseResponseFromUpdateTable() - " + response.getTableName());
		Log.d(TAG, "parseResponseFromUpdateTable() - getList() = " + response.getList());
		Log.d(TAG, "parseResponseFromUpdateTable() - getList().size = " + response.getList().size());

		if (response.getList() != null && response.getList().size() > 0) {
			if (response.getTableName().equals(UpdateSequenceMap.SRT_CUSTOMERS)) {
				ArrayList<?> customers = convertList(response, Customer.class);
				SVCustomerDao.insertOrUpdateCustomers((ArrayList<Customer>) customers);
				Log.d(TAG, "parseResponseFromUpdateTable() - Customers updated successfully!");
			} else if (response.getTableName().equals(UpdateSequenceMap.SRT_MOBILE_PHONES)) {
				ArrayList<?> mobilePhones = convertList(response, MobilePhone.class);
				SVMobilePhoneDao.insertOrUpdateMobilePhones((ArrayList<MobilePhone>) mobilePhones);
				Log.d(TAG, "parseResponseFromUpdateTable() - MobilePhones updated successfully!");
			} else if (response.getTableName().equals(UpdateSequenceMap.SRT_USERS)) {
				ArrayList<?> users = convertList(response, User.class);
				SVUserDao.insertOrUpdateUsers((ArrayList<User>) users);
				Log.d(TAG, "parseResponseFromUpdateTable() - Users updated successfully!");
			} else if (response.getTableName().equals(UpdateSequenceMap.SRT_VIGILANTS)) {
				ArrayList<?> vigilants = convertList(response, Vigilant.class);
				SVVigilantDao.insertOrUpdateVigilants((ArrayList<Vigilant>) vigilants);
				Log.d(TAG, "parseResponseFromUpdateTable() - Vigilants updated successfully!");
			} else if (response.getTableName().equals(UpdateSequenceMap.SRT_RFID_TAGS)) {
				ArrayList<?> rfidTags = convertList(response, RfidTag.class);
				SVRfidTagDao.insertOrUpdateRfidTags((ArrayList<RfidTag>) rfidTags);
				Log.d(TAG, "parseResponseFromUpdateTable() - RfidTags updated successfully!");
			} else if (response.getTableName().equals(UpdateSequenceMap.SRT_CHECKPOINTS)) {
				ArrayList<?> checkpoints = convertList(response, Checkpoint.class);
				SVCheckpointDao.insertOrUpdateCheckpoints((ArrayList<Checkpoint>) checkpoints);
				Log.d(TAG, "parseResponseFromUpdateTable() - Checkpoints updated successfully!");
				callSyncAgain = true;
			} else if (response.getTableName().equals(UpdateSequenceMap.SRT_ROUTE_COMPOSITION)) {
				ArrayList<?> routeCompositions = convertList(response, RouteComposition.class);
				SVRouteCompositionDao.insertOrUpdateRouteCompositions((ArrayList<RouteComposition>) routeCompositions);
				Log.d(TAG, "parseResponseFromUpdateTable() - RouteCompositions updated successfully!");
			} else if (response.getTableName().equals(UpdateSequenceMap.SRT_ROUTES)) {
				ArrayList<?> routes = convertList(response, Route.class);
				SVRouteDao.insertOrUpdateRoutes((ArrayList<Route>) routes);
				Log.d(TAG, "parseResponseFromUpdateTable() - Routes updated successfully!");
			} else if (response.getTableName().equals(UpdateSequenceMap.SRT_ROUTE_SCHEDULE)) {
				ArrayList<?> routeSchedules = convertList(response, RouteSchedule.class);
				SVRouteScheduleDao.insertOrUpdateRouteSchedules((ArrayList<RouteSchedule>) routeSchedules);
				Log.d(TAG, "parseResponseFromUpdateTable() - RouteSchedules updated successfully!");
			} else if (response.getTableName().equals(UpdateSequenceMap.SRT_PRIORITYS)) {
				ArrayList<?> prioritys = convertList(response, Priority.class);
				SVPriorityDao.insertOrUpdatePrioritys((ArrayList<Priority>) prioritys);
				Log.d(TAG, "parseResponseFromUpdateTable() - Prioritys updated successfully!");
			} else if (response.getTableName().equals(UpdateSequenceMap.SRT_OCCURRENCE_TYPES)) {
				ArrayList<?> types = convertList(response, OccurrenceType.class);
				SVOccurrenceTypesDao.insertOrUpdateOccurrenceTypes((ArrayList<OccurrenceType>) types);
				Log.d(TAG, "parseResponseFromUpdateTable() - OccurrenceTypes updated successfully!");
			}
			else if (response.getTableName().equals(UpdateSequenceMap.SRT_SYS_PARAMS)) {
				ArrayList<?> sysParams = convertList(response, SysParam.class);

				// below routine converts sysParam.paramDesc incoming from web to a new value known internally here
				// based in SysParamsMap (WebApp have ParamDesc & Param and Android have only ParamDesc).
				// Best here would be if current android database schema also have ParamDesc and Param (not only ParamDesc)
				// in order to avoid this conversion routine and make both sides more alike.
				for (int i = 0; i < sysParams.size(); i ++) {
					((SysParam) sysParams.get(i)).setParamDesc(((SysParam) sysParams.get(i)).getParamDesc());
				}

				Log.d(TAG, "parseResponseFromUpdateTable() - SysParams updated successfully! " + sysParams.size());

				SVSysParamsDao.insertOrUpdateSysParams((ArrayList<SysParam>) sysParams);
			}


		} else {
			ApplicationControl.IS_SYNCHRONIZING = false;
		}

		ApplicationControl.SYNC_COUNTER--;
		ApplicationControl.checkFinishedSynch(callSyncAgain);

		// CUSTOM jorge.lucas 20052015 last sync formatted date hour string
		ApplicationControl.stAppLastSyncHour = ApplicationControl.getStrCurrentDateHour("HH:mm:ss");
		// FIM CUSTOM
	}

	private boolean contain(String table) {
		for (String t : UpdateSequenceMap.SRT_TABLES) {
			if (table.equals(t))
				return true;
		}
		return false;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ArrayList convertList(Response response, Class<?> type) {

		Log.d(TAG, "convertList()");

		JsonSerializer<Date> ser = new JsonSerializer<Date>() {
			@Override
			public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
				return src == null ? null : new JsonPrimitive(src.getTime());
			}
		};
		JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
			@SuppressLint("SimpleDateFormat")
			@Override
			public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
				try {
					return json == null ? null : new Date(new SimpleDateFormat("dd-MM-yyyyHH-mm-ss").parse(json.getAsString()).getTime());
				} catch (ParseException e) {
					e.printStackTrace();
					return null;
				}
			}
		};

		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, ser).registerTypeAdapter(Date.class, deser).create();
		ArrayList list = new ArrayList();

		try {

			JSONArray jsonArray = new JSONArray(response.getResponseContent());

			Object object;

			for (int i = 0; i < jsonArray.length(); i++) {
				object = gson.fromJson(jsonArray.get(i).toString(), type);
				list.add(object);
			}

		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		Log.d(TAG, "convertList() - finished!");

		return list;
	}
}
