package com.br.svconsultoria.svnfcmanager.http.events;

import android.os.Handler;
import android.util.Base64;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.UserValidationActivity;
import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.dao.SVVehicleCheckDao;
import com.br.svconsultoria.svnfcmanager.http.HttpAuthenticate;
import com.br.svconsultoria.svnfcmanager.map.HttpConnAction;
import com.br.svconsultoria.svnfcmanager.model.Response;
import com.br.svconsultoria.svnfcmanager.model.orm.VehicleCheck;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;

import java.net.URI;
import java.security.spec.PSSParameterSpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import br.com.svconsultoria.newmobile.remotedebug.Log;

/**
 * Created by bruno.hernandes on 20/01/2017.
 */
public class VehicleCheckSender {


    private String TAG = "VehicleCheckSender";

    private static final int AUTHENTICATION_CONNECTION_TIMEOUT = 30000;
    private static final int AUTHENTICATION_SOCKET_TIMEOUT = 30000;

    private Handler handler = new Handler();

    private Runnable changeUIRunnable = new Runnable() {
        @Override
        public void run() {
            if (UserValidationActivity.tvServer != null) {
                if (ApplicationControl.IS_SERVER_ON) {
                    UserValidationActivity.tvServer.setText("ON");

                } else {
                    UserValidationActivity.tvServer.setText("OFF");
                }
            }
        }
    };

    private Timer timer = new Timer();
    private TimerTask sendVehicleChecksTask = new TimerTask() {
        @Override
        public void run() {

            if (SVVehicleCheckDao.hasVehicleChecks() && HttpAuthenticate.authenticate()) {

                HttpPost post = new HttpPost();
                Response resp;

                Log.d(TAG, "Starting connection with server");

                String response;
                VehicleCheck vehicleCheck;
                String uri = HttpConnAction.HTTP + SVMobileParamsDao.getHostParam() + HttpConnAction.VEHICLE_CHECK_URI;
                android.util.Log.d(TAG, "sendVehicleChecks() - URI: " + uri);


                List<NameValuePair> args = new ArrayList<NameValuePair>();
                args.add(new BasicNameValuePair(HttpConnAction.ACTION_FIELD, HttpConnAction.INSERT_VALUE));
                vehicleCheck = SVVehicleCheckDao.getOldestVehicleChecks();
                if(vehicleCheck!=null){
                    Gson gson = new GsonBuilder().setDateFormat("dd-MM-yyyyHH-mm-ss").create();

                    String json = gson.toJson(vehicleCheck);

                    android.util.Log.d(TAG, "sendVehicles() - JSON  : " + json);

                    args.add(new BasicNameValuePair("vehicle", json));

                    if(vehicleCheck.getPhoto1()!=null) {
                        args.add(new BasicNameValuePair("photo1", Base64.encodeToString(vehicleCheck.getPhoto1(), Base64.DEFAULT)));
                    }
                    if(vehicleCheck.getPhoto2()!=null) {
                        args.add(new BasicNameValuePair("photo2", Base64.encodeToString(vehicleCheck.getPhoto2(), Base64.DEFAULT)));
                    }
                    if(vehicleCheck.getPhoto3()!=null) {
                        args.add(new BasicNameValuePair("photo3", Base64.encodeToString(vehicleCheck.getPhoto3(), Base64.DEFAULT)));
                    }
                    if(vehicleCheck.getPhoto4()!=null) {
                        args.add(new BasicNameValuePair("photo4", Base64.encodeToString(vehicleCheck.getPhoto4(), Base64.DEFAULT)));
                    }

                    try {

                        post.setURI(new URI(uri));
                        post.setEntity(new UrlEncodedFormEntity(args));

                        HttpClient httpclient = ApplicationControl.httpclient;
                        HttpConnectionParams.setConnectionTimeout(httpclient.getParams(), AUTHENTICATION_CONNECTION_TIMEOUT);
                        HttpConnectionParams.setSoTimeout(httpclient.getParams(), AUTHENTICATION_SOCKET_TIMEOUT);

                        android.util.Log.d(TAG, "VehicleSender - URI: " + post.getURI());

                        ResponseHandler<String> responseHandler = new BasicResponseHandler();
                        String responseBody = "";

                        httpclient.getConnectionManager().closeExpiredConnections();

                        responseBody = httpclient.execute(post, responseHandler);
                        android.util.Log.d(TAG, "doInBackground() - Response: " + responseBody);

                        resp = gson.fromJson(responseBody, Response.class);
                        resp.setType(HttpConnAction.VEHICLE_CHECK_URI);
                        ArrayList<VehicleCheck> lst = new ArrayList<>();
                        lst.add(vehicleCheck);

                        resp.setList(lst);
                        SVVehicleCheckDao.deleteCommittedVehicleChecks(lst);
                        ApplicationControl.stAppLastDataSendHour = ApplicationControl.getStrCurrentDateHour("HH:mm:ss");

                        response = "true";

                    } catch (Exception e) {
                        android.util.Log.e(TAG, "Error sync vehicles: ", e);
                        response = "Verifique se o servidor está ativo e ou o endereço IP está correto: " + e.getMessage();
                        e.printStackTrace();
                    }
                    android.util.Log.d(TAG, "authenticate() - Finishing...");

                    ApplicationControl.IS_SERVER_ON = response.equals("true");
                    UserValidationActivity.message = response;

                    handler.post(changeUIRunnable);
                }
            }

        }

    };

    private VehicleCheckSender() {
    }

    private static final VehicleCheckSender instance = new VehicleCheckSender();


    public static void startVehicleCheckSenderSenderTask(){
        instance.timer.schedule(instance.sendVehicleChecksTask, 1000, 1000);
    }
}
