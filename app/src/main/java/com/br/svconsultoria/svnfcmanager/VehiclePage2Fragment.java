package com.br.svconsultoria.svnfcmanager;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.br.svconsultoria.svnfcmanager.model.orm.VehicleCheck;
import com.br.svconsultoria.svnfcmanager.util.DrawableImageView;

import java.io.ByteArrayOutputStream;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VehiclePage2Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VehiclePage2Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VehiclePage2Fragment extends Fragment {


    private static final int CAMERA_REQUEST_1 = 1;
    private static final int CAMERA_REQUEST_2 = 2;
    private static final int CAMERA_REQUEST_3 = 3;
    private static final int CAMERA_REQUEST_4 = 4;


    private OnFragmentInteractionListener mListener;

    private DrawableImageView imageDamage;
    private EditText damageComments;
    private ImageView vehicleDamage1;
    private ImageView vehicleDamage2;
    private ImageView vehicleDamage3;
    private ImageView vehicleDamage4;
    byte[] photo1;
    byte[] photo2;
    byte[] photo3;
    byte[] photo4;


    public void fillVehicleCheckData(VehicleCheck vehicleCheck) {

        vehicleCheck.setDamageComments(damageComments.getText().toString());

        Bitmap bitmap = imageDamage.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        vehicleCheck.setDamages(stream.toByteArray());

        vehicleCheck.setPhoto1(photo1);
        vehicleCheck.setPhoto2(photo2);
        vehicleCheck.setPhoto3(photo3);
        vehicleCheck.setPhoto4(photo4);

    }


    public VehiclePage2Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment VehiclePage2Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static VehiclePage2Fragment newInstance() {
        VehiclePage2Fragment fragment = new VehiclePage2Fragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    public void clearImagePoints(){
        imageDamage.clearPoints();
    }

    public void removeLastPoint(){
        imageDamage.removeLastPoint();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vehicle_page2, container, false);

        imageDamage = (DrawableImageView) view.findViewById(R.id.imageDamage);
        damageComments = (EditText) view.findViewById(R.id.damageComments);
        vehicleDamage1 = (ImageView) view.findViewById(R.id.vehicleDamage1);
        vehicleDamage2 = (ImageView) view.findViewById(R.id.vehicleDamage2);
        vehicleDamage3 = (ImageView) view.findViewById(R.id.vehicleDamage3);
        vehicleDamage4 = (ImageView) view.findViewById(R.id.vehicleDamage4);

        vehicleDamage1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST_1);
            }
        });
        vehicleDamage2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST_2);
            }
        });
        vehicleDamage3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST_3);
            }
        });
        vehicleDamage4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST_4);
            }
        });
        return view;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private byte[] getBitmap(Intent data){
        Bitmap bitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((resultCode == Activity.RESULT_OK)) {

            switch (requestCode){
                case CAMERA_REQUEST_1:
                    photo1 = getBitmap(data);
                    Bitmap bmp = BitmapFactory.decodeByteArray(photo1, 0, photo1.length);
                    vehicleDamage1.setImageBitmap(bmp);
                    break;
                case CAMERA_REQUEST_2:
                    photo2 = getBitmap(data);
                    Bitmap bmp2 = BitmapFactory.decodeByteArray(photo2, 0, photo2.length);
                    vehicleDamage2.setImageBitmap(bmp2);
                    break;
                case CAMERA_REQUEST_3:
                    photo3 = getBitmap(data);
                    Bitmap bmp3 = BitmapFactory.decodeByteArray(photo3, 0, photo3.length);
                    vehicleDamage3.setImageBitmap(bmp3);
                    break;
                case CAMERA_REQUEST_4:
                    photo4 = getBitmap(data);
                    Bitmap bmp4 = BitmapFactory.decodeByteArray(photo4, 0, photo4.length);
                    vehicleDamage4.setImageBitmap(bmp4);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
