package com.br.svconsultoria.svnfcmanager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ShowMessageActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_message);

		// incoming intent
		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();

		// set message
		TextView textView = (TextView) findViewById(R.id.show_message_text_view);
		textView.setText(bundle.getString("message"));

		setTitle("Mensagem"); // activity title
		getWindow().getDecorView().setBackgroundColor(Color.BLACK); // activity background color
	}

	public void invokeMainActivity(View v)
	{
		// invoke MainActivity
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
//		clearLights(intent);
	}
}
