package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Entity mapped to table SRTM_PRIORITYS.
 */
@Entity
public class Priority {

    @Id
    private Long priorityId;
    private long statusId;
    private Long xId;
    private String description;
    private long customerId;
    @Generated(hash = 654253819)
    public Priority(Long priorityId, long statusId, Long xId, String description,
            long customerId) {
        this.priorityId = priorityId;
        this.statusId = statusId;
        this.xId = xId;
        this.description = description;
        this.customerId = customerId;
    }
    @Generated(hash = 320200205)
    public Priority() {
    }
    public Long getPriorityId() {
        return this.priorityId;
    }
    public void setPriorityId(Long priorityId) {
        this.priorityId = priorityId;
    }
    public long getStatusId() {
        return this.statusId;
    }
    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }
    public Long getXId() {
        return this.xId;
    }
    public void setXId(Long xId) {
        this.xId = xId;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public long getCustomerId() {
        return this.customerId;
    }
    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

}
