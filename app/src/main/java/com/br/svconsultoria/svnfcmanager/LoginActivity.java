package com.br.svconsultoria.svnfcmanager;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.br.svconsultoria.svnfcmanager.dao.SVMobileParamsDao;
import com.br.svconsultoria.svnfcmanager.http.HttpAuthenticate;
import com.br.svconsultoria.svnfcmanager.tools.EventBuilder;

public class LoginActivity extends BaseNFCActivity {

    private static final String TAG = "LoginActivity";

    private ImageView imageView1 = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        // loginMessage = (TextView) findViewById(R.id.loginMessage);
        imageView1 = (ImageView) findViewById(R.id.imageView1);

        Log.d(TAG, "onCreate");

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (SVMobileParamsDao.isQrCodeAllowed()) {
            imageView1.setImageResource(R.drawable.qrcode);
            imageView1.setBackgroundResource(R.drawable.white_container);
            imageView1.setPadding(15, 15, 15, 15);
            imageView1.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    callQrCodeReader();
                }
            });
        }

    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(i);
    }


    @Override
    protected void handleTag(String sTag) {
        tryLogin(sTag);
    }

    private void tryLogin(String sTag) {
        try {
            EventBuilder.createUserTagEvent(sTagSerial);
            prepareAndExecuteSyncTask();

            Intent i = new Intent(this, UserValidationActivity.class);
            i.putExtra("serial", sTagSerial);
            startActivity(i);

        } catch (Exception e) {
            Log.d("LoginActivity", "onNewIntent() - Impossible to create the Event.");
            ApplicationControl.handleMessage("Erro no Login. Evento de leitura não foi criado...");
        }
    }

    private void prepareAndExecuteSyncTask() {
        Log.d(TAG, "prepareAndExecuteSyncTask()");

        // if (ApplicationControl.SEND_FINISHED) {

        if (isConnected()) {

            Toast.makeText(this, "Conectando com o servidor ...", Toast.LENGTH_LONG).show();
            new HttpAuthenticate().execute();
            ApplicationControl.startSend();

        } else {
            ApplicationControl.IS_SERVER_ON = false;
            Toast.makeText(this, "Acesso a rede indisponível - Acumulando eventos OFF-LINE", Toast.LENGTH_LONG).show();
            UserValidationActivity.message = "Acesso a rede indisponível - Acumulando eventos OFF-LINE";

        }
        // }
    }

    private void callQrCodeReader() {
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
        startActivityForResult(intent, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        Log.d(TAG, "onActivityResult()");
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                String content = intent.getStringExtra("SCAN_RESULT");
                String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

                Log.d(TAG, "QR CODE - Content: " + content + " Format: " + format);

                sTagSerial = content;
                Vibrator vibrator;
                vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(150);

                tryLogin(sTagSerial);

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Leitura falhou!", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    // check network connection
    public boolean isConnected() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if ((networkInfo != null) && networkInfo.isConnected())
            return true;
        else
            return false;
    }

}
