package com.br.svconsultoria.svnfcmanager.tools;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.dao.SVCheckpointDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteDao;
import com.br.svconsultoria.svnfcmanager.model.SystemCheckpoint;
import com.br.svconsultoria.svnfcmanager.model.orm.Checkpoint;
import com.br.svconsultoria.svnfcmanager.model.orm.Route;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteComposition;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteSchedule;
import com.br.svconsultoria.svnfcmanager.util.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;

public class CheckpointTools {

	private static final String TAG = "CheckpointTools";

	public static List<SystemCheckpoint> getSystemCheckpoints(List<RouteComposition> composition, RouteSchedule schedule) {

		Log.d(TAG, "getSystemCheckpoints()");
		List<SystemCheckpoint> checkpoints = new ArrayList<SystemCheckpoint>();
		long initMinuteOfDay = schedule.getInitHr();
		long orderId = 0;
		Route route = SVRouteDao.getRouteById(schedule.getRouteId());
		boolean freeRoute = Boolean.parseBoolean(String.valueOf(route.getFreeRoute()));

		for (RouteComposition comp : composition) {
			try {
				Checkpoint checkpoint = SVCheckpointDao.getCheckpointById(comp.getCheckPointId());
				orderId++;
				SystemCheckpoint sCheckpoint = new SystemCheckpoint();
				sCheckpoint.setSystemCheckpointId(orderId);
				sCheckpoint.setCheckpointId(checkpoint.getCheckPointId());
				sCheckpoint.setOrderId(orderId);
				sCheckpoint.setDescription(checkpoint.getDescription());
				sCheckpoint.setTagId(checkpoint.getTagId());
				sCheckpoint.setLatitude(checkpoint.getLatitude());
				sCheckpoint.setLongitude(checkpoint.getLongitude());
				sCheckpoint.setDelayMinutes(comp.getTimeAllowed());
				sCheckpoint.setCustomerId(checkpoint.getCustomerId());
				sCheckpoint.setCheckpointChecks(checkpoint.getCheckpointChecks());
				sCheckpoint.setTravelMinutes(comp.getTravelTime());
				sCheckpoint.setStayTimeMinutes(comp.getStayTime());
				sCheckpoint.setMandatory(Boolean.parseBoolean(String.valueOf(comp.getMandatory())));

				if (checkpoints.isEmpty()) {
					sCheckpoint.setTime(initMinuteOfDay + comp.getTravelTime());
				} else {
					sCheckpoint.setTime(checkpoints.get(checkpoints.size() - 1).getTime() + comp.getTravelTime());
				}

				checkpoints.add(sCheckpoint);


				if (comp.getStayTime() > 0 && !freeRoute) {
					orderId++;
					sCheckpoint = new SystemCheckpoint();
					sCheckpoint.setSystemCheckpointId(orderId);
					sCheckpoint.setCheckpointId(checkpoint.getCheckPointId());
					sCheckpoint.setOrderId(orderId);
					sCheckpoint.setDescription(checkpoint.getDescription() + "\n AGUARDE " + comp.getStayTime() + " MINUTO(S)");
					sCheckpoint.setTagId(checkpoint.getTagId());
					sCheckpoint.setLatitude(checkpoint.getLatitude());
					sCheckpoint.setLongitude(checkpoint.getLongitude());
					sCheckpoint.setDelayMinutes(comp.getTimeAllowed());
					sCheckpoint.setCustomerId(checkpoint.getCustomerId());
					sCheckpoint.setTime(checkpoints.get(checkpoints.size() - 1).getTime() + comp.getStayTime());
					sCheckpoint.setTravelMinutes(comp.getTravelTime());
					sCheckpoint.setStayTimeMinutes(comp.getStayTime());
					sCheckpoint.setMandatory(Boolean.parseBoolean(String.valueOf(comp.getMandatory())));

					// flags this object as a result of stay time of previous object:
					// when stayTime > 0, checkpoint are added twice in checkpoint list
					// and makes the app asks for the current point twice
					sCheckpoint.setWait(true);

					checkpoints.add(sCheckpoint);
				}
			} catch (Exception e) {
				Log.d(TAG, "getSystemCheckpoints() - Checkpoint (" + comp.getCheckPointId() + ") not found!");
			}
		}
		printSystemCheckpoints(checkpoints);
		return checkpoints;
	}

	private static void printSystemCheckpoints(List<SystemCheckpoint> checkpoints) {
		for (SystemCheckpoint check : checkpoints) {
			Log.d(TAG, "printSystemCheckpoints() - TimeMinutes: " + check.getTime() + " TimeReal: " + DateTimeUtils.getTimeTable(check.getTime()));
		}
	}

	public static void updateCheckpointCoordinates(SystemCheckpoint sCheckpoint) {
		Log.d(TAG, "updateCheckpointCoordinates()");
		Checkpoint checkpoint = SVCheckpointDao.getCheckpointById(sCheckpoint.getCheckpointId());
		checkpoint.setLatitude(sCheckpoint.getLatitude());
		checkpoint.setLongitude(sCheckpoint.getLongitude());
		SVCheckpointDao.insertOrReplaceCheckpoint(checkpoint);
		Log.d(TAG, "updateCheckpointCoordinates() - Finished!");
	}
}
