package com.br.svconsultoria.svnfcmanager.dao;

import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.map.UpdateSequenceMap;
import com.br.svconsultoria.svnfcmanager.model.orm.DaoSession;
import com.br.svconsultoria.svnfcmanager.model.orm.OccurrenceType;
import com.br.svconsultoria.svnfcmanager.model.orm.OccurrenceTypeDao;
import com.br.svconsultoria.svnfcmanager.model.orm.UpdateSequence;

import java.util.ArrayList;
import java.util.List;

public class SVOccurrenceTypesDao {
	private static final String TAG = "SVOccurrenceTypesDao";


	private static DaoSession session = ApplicationControl.daoSession;

	public static ArrayList<OccurrenceType> getOccurrenceTypes() {
		ArrayList<OccurrenceType> list = (ArrayList<OccurrenceType>) session.getOccurrenceTypeDao().queryBuilder().where(OccurrenceTypeDao.Properties.StatusId.eq(1)).list();
		return list;
	}

	public static ArrayList<String> getOccurrenceTypesDesc() {
		ArrayList<String> descs = new ArrayList<String>();
		ArrayList<OccurrenceType> types = getOccurrenceTypes();
		for (OccurrenceType ot : types) {
			descs.add(ot.getDescription());
		}
		return descs;
	}

	public static void insertOrUpdateOccurrenceTypes(final List<OccurrenceType> list) {
		Log.d("SVCheckpointDao", "insertOrUpdateOccurrenceTypes() Starting...");
		session.runInTx(new Runnable() {

			@Override
			public void run() {

				OccurrenceTypeDao oTypedao = session.getOccurrenceTypeDao();
				oTypedao.insertOrReplaceInTx(list);
			}
		});

		long xid = SVUpdateSequenceDao.getXid(UpdateSequenceMap.SRT_OCCURRENCE_TYPES).getxId();
		for (OccurrenceType object : list) {
			if (xid < object.getXId()) {
				xid = object.getXId();
			}
		}
		SVUpdateSequenceDao.updateXId(new UpdateSequence(null, UpdateSequenceMap.SRT_OCCURRENCE_TYPES, xid));

		Log.d("SVCheckpointDao", "insertOrUpdateOccurrenceTypes() Finished!");
	}

	public static boolean hasOccurrenceTypes() {
		long count = session.getOccurrenceTypeDao().queryBuilder().count();
		if (count > 0)
			return true;
		else
			return false;
	}

	public static void insertNewOccurrenceType(OccurrenceType occurr) {
		Log.d(TAG, "insertNewOccurrenceType() Insert new OccurrenceType!");
		session.getOccurrenceTypeDao().insert(occurr);
		Log.d(TAG, "insertNewOccurrenceType() Finished!");
	}

	public static long getOccurenceTypeIdByDescription(String desc) {
		Log.d(TAG, "getOccurenceTypeIdByDescription()");
		OccurrenceType type = session.getOccurrenceTypeDao().queryBuilder().where(OccurrenceTypeDao.Properties.Description.eq(desc)).uniqueOrThrow();
		Log.d(TAG, "getOccurenceTypeIdByDescription()");
		return type.getOccurrenceTypeId();
	}
}
