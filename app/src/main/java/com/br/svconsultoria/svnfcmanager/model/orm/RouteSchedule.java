package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import java.util.Date;

/**
 * Entity mapped to table SRTM_ROUTE_SCHEDULE.
 */
@Entity
public class RouteSchedule {

    @Id
    private Long routeScheduleId;
    private long routeId;
    private Long xId;
    private long statusId;
    private long initHr;
    private Date scheduleDt;

    @Generated(hash = 449319506)
    public RouteSchedule(Long routeScheduleId, long routeId, Long xId,
            long statusId, long initHr, Date scheduleDt) {
        this.routeScheduleId = routeScheduleId;
        this.routeId = routeId;
        this.xId = xId;
        this.statusId = statusId;
        this.initHr = initHr;
        this.scheduleDt = scheduleDt;
    }
    @Generated(hash = 1219606329)
    public RouteSchedule() {
    }
    public Long getRouteScheduleId() {
        return this.routeScheduleId;
    }
    public void setRouteScheduleId(Long routeScheduleId) {
        this.routeScheduleId = routeScheduleId;
    }
    public long getRouteId() {
        return this.routeId;
    }
    public void setRouteId(long routeId) {
        this.routeId = routeId;
    }
    public Long getXId() {
        return this.xId;
    }
    public void setXId(Long xId) {
        this.xId = xId;
    }
    public long getStatusId() {
        return this.statusId;
    }
    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }
    public long getInitHr() {
        return this.initHr;
    }
    public void setInitHr(long initHr) {
        this.initHr = initHr;
    }
    public java.util.Date getScheduleDt() {
        return this.scheduleDt;
    }
    public void setScheduleDt(java.util.Date scheduleDt) {
        this.scheduleDt = scheduleDt;
    }

}
