package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Entity mapped to table SRTM_UPDATE_SEQUENCE.
 */
@Entity
public class UpdateSequence {

    @Id(autoincrement = true)
    private Long id; //greendao entities must have a long or Long property as their primary key.
    @Index(unique = true)
    private String tableName;
    private long xid;
    @Generated(hash = 1635200989)
    public UpdateSequence(Long id, String tableName, long xid) {
        this.id = id;
        this.tableName = tableName;
        this.xid = xid;
    }
    @Generated(hash = 721089883)
    public UpdateSequence() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getTableName() {
        return this.tableName;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    public long getXid() {
        return this.xid;
    }
    public void setXid(long xid) {
        this.xid = xid;
    }
    public void setId(Long id) {
        this.id = id;
    }

}
