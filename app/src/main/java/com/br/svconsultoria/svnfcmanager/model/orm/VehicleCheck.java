package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;

import java.util.Date;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Created by bruno.hernandes on 19/01/2017.
 */
@Entity
public class VehicleCheck {

    @Id(autoincrement = true)
    private Long id; //greendao entities must have a long or Long property as their primary key.
    @Index(unique = true)
    private String vehicleCheckId;

    private long mobileId;
    private long vigilantId;
    private Float latitude;


    private Float longitude;
    private Date date;
    private Long customerId;

    private String plate;
    private String model;
    private String km;
    private String fuel;
    private Date lastOilChange;
    private String comments;

    private String cleaning;
    private String triangle;
    private String wheelWrench;
    private String jack;
    private String steppe;
    private String horn;
    private String waterLevel;
    private String oilLevel;
    private String lowBeam;
    private String highBeam;
    private String indicator;
    private String breakLight;
    private String reverseLight;

    private String damageComments;
    private byte[] damages;
    private transient byte[] photo1;
    private transient byte[] photo2;
    private transient byte[] photo3;
    private transient byte[] photo4;
    @Generated(hash = 1581935409)
    public VehicleCheck(Long id, String vehicleCheckId, long mobileId, long vigilantId, Float latitude,
            Float longitude, Date date, Long customerId, String plate, String model, String km,
            String fuel, Date lastOilChange, String comments, String cleaning, String triangle,
            String wheelWrench, String jack, String steppe, String horn, String waterLevel,
            String oilLevel, String lowBeam, String highBeam, String indicator, String breakLight,
            String reverseLight, String damageComments, byte[] damages) {
        this.id = id;
        this.vehicleCheckId = vehicleCheckId;
        this.mobileId = mobileId;
        this.vigilantId = vigilantId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
        this.customerId = customerId;
        this.plate = plate;
        this.model = model;
        this.km = km;
        this.fuel = fuel;
        this.lastOilChange = lastOilChange;
        this.comments = comments;
        this.cleaning = cleaning;
        this.triangle = triangle;
        this.wheelWrench = wheelWrench;
        this.jack = jack;
        this.steppe = steppe;
        this.horn = horn;
        this.waterLevel = waterLevel;
        this.oilLevel = oilLevel;
        this.lowBeam = lowBeam;
        this.highBeam = highBeam;
        this.indicator = indicator;
        this.breakLight = breakLight;
        this.reverseLight = reverseLight;
        this.damageComments = damageComments;
        this.damages = damages;
    }
    @Generated(hash = 553487076)
    public VehicleCheck() {
    }
    public Long getId() {
        return this.id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getVehicleCheckId() {
        return this.vehicleCheckId;
    }
    public void setVehicleCheckId(String vehicleCheckId) {
        this.vehicleCheckId = vehicleCheckId;
    }
    public long getMobileId() {
        return this.mobileId;
    }
    public void setMobileId(long mobileId) {
        this.mobileId = mobileId;
    }
    public long getVigilantId() {
        return this.vigilantId;
    }
    public void setVigilantId(long vigilantId) {
        this.vigilantId = vigilantId;
    }
    public Float getLatitude() {
        return this.latitude;
    }
    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }
    public Float getLongitude() {
        return this.longitude;
    }
    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }
    public Date getDate() {
        return this.date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public Long getCustomerId() {
        return this.customerId;
    }
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }
    public String getPlate() {
        return this.plate;
    }
    public void setPlate(String plate) {
        this.plate = plate;
    }
    public String getModel() {
        return this.model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public String getKm() {
        return this.km;
    }
    public void setKm(String km) {
        this.km = km;
    }
    public String getFuel() {
        return this.fuel;
    }
    public void setFuel(String fuel) {
        this.fuel = fuel;
    }
    public Date getLastOilChange() {
        return this.lastOilChange;
    }
    public void setLastOilChange(Date lastOilChange) {
        this.lastOilChange = lastOilChange;
    }
    public String getComments() {
        return this.comments;
    }
    public void setComments(String comments) {
        this.comments = comments;
    }
    public String getCleaning() {
        return this.cleaning;
    }
    public void setCleaning(String cleaning) {
        this.cleaning = cleaning;
    }
    public String getTriangle() {
        return this.triangle;
    }
    public void setTriangle(String triangle) {
        this.triangle = triangle;
    }
    public String getWheelWrench() {
        return this.wheelWrench;
    }
    public void setWheelWrench(String wheelWrench) {
        this.wheelWrench = wheelWrench;
    }
    public String getJack() {
        return this.jack;
    }
    public void setJack(String jack) {
        this.jack = jack;
    }
    public String getSteppe() {
        return this.steppe;
    }
    public void setSteppe(String steppe) {
        this.steppe = steppe;
    }
    public String getHorn() {
        return this.horn;
    }
    public void setHorn(String horn) {
        this.horn = horn;
    }
    public String getWaterLevel() {
        return this.waterLevel;
    }
    public void setWaterLevel(String waterLevel) {
        this.waterLevel = waterLevel;
    }
    public String getOilLevel() {
        return this.oilLevel;
    }
    public void setOilLevel(String oilLevel) {
        this.oilLevel = oilLevel;
    }
    public String getLowBeam() {
        return this.lowBeam;
    }
    public void setLowBeam(String lowBeam) {
        this.lowBeam = lowBeam;
    }
    public String getHighBeam() {
        return this.highBeam;
    }
    public void setHighBeam(String highBeam) {
        this.highBeam = highBeam;
    }
    public String getIndicator() {
        return this.indicator;
    }
    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }
    public String getBreakLight() {
        return this.breakLight;
    }
    public void setBreakLight(String breakLight) {
        this.breakLight = breakLight;
    }
    public String getReverseLight() {
        return this.reverseLight;
    }
    public void setReverseLight(String reverseLight) {
        this.reverseLight = reverseLight;
    }
    public String getDamageComments() {
        return this.damageComments;
    }
    public void setDamageComments(String damageComments) {
        this.damageComments = damageComments;
    }
    public byte[] getDamages() {
        return this.damages;
    }
    public void setDamages(byte[] damages) {
        this.damages = damages;
    }
    public byte[] getPhoto1() {
        return this.photo1;
    }
    public void setPhoto1(byte[] photo1) {
        this.photo1 = photo1;
    }
    public byte[] getPhoto2() {
        return this.photo2;
    }
    public void setPhoto2(byte[] photo2) {
        this.photo2 = photo2;
    }
    public byte[] getPhoto3() {
        return this.photo3;
    }
    public void setPhoto3(byte[] photo3) {
        this.photo3 = photo3;
    }
    public byte[] getPhoto4() {
        return this.photo4;
    }
    public void setPhoto4(byte[] photo4) {
        this.photo4 = photo4;
    }
    public void setId(Long id) {
        this.id = id;
    }

}
