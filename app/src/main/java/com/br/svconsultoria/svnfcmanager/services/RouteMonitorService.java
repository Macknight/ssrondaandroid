package com.br.svconsultoria.svnfcmanager.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;

import com.br.svconsultoria.svnfcmanager.ApplicationControl;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteCompositionDao;
import com.br.svconsultoria.svnfcmanager.dao.SVRouteScheduleDao;
import com.br.svconsultoria.svnfcmanager.model.SystemCheckpoint;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteComposition;
import com.br.svconsultoria.svnfcmanager.model.orm.RouteSchedule;
import com.br.svconsultoria.svnfcmanager.tools.CheckpointTools;

import java.util.Date;
import java.util.List;

public class RouteMonitorService extends Service {

	private static final String TAG = "RouteMonitorService";

	private long routeScheduleId = 0;
	private RouteSchedule routeSchedule = null;
	private long routeId = 0;
	private List<RouteComposition> composition = null;
	private List<SystemCheckpoint> checkpoints = null;
	private long orderCount = 0;
	private SystemCheckpoint checkpoint = null;

	private boolean IS_SERVICE_ALIVE = true;
	private boolean IS_VIBRATE_ALLOWED = true;
	private static boolean IS_SERVICE_ACTIVE = true;

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(TAG, "onStartCommand() startId: " + startId);
		return Service.START_NOT_STICKY;
	}

	@Override
	public void onCreate() {
		Log.d(TAG, "onCreate()");
		super.onCreate();
		Thread routeMonitor = createSyncControlThread();
		routeMonitor.start();
	}

	private boolean checkRouteInformation() {
		long newRouteScheduleId = ApplicationControl.userSession.getRouteScheduleId();
		long newOrderCount = ApplicationControl.userSession.getOrderCount();

		if (newRouteScheduleId == 0) {
			routeId = 0;
			return false;
		} else {
			if (newRouteScheduleId != routeScheduleId) {
				Log.d(TAG, "routeScheduleId: " + routeScheduleId);
				routeScheduleId = newRouteScheduleId;
				if (routeSchedule==null){
					routeId = 0;
					return false;
				}
				routeSchedule = SVRouteScheduleDao.getRouteScheduleById(routeScheduleId);
				routeId = routeSchedule.getRouteId();
				composition = SVRouteCompositionDao.getRouteComposition(routeId);
				checkpoints = CheckpointTools.getSystemCheckpoints(composition, routeSchedule);
			}

			if (newOrderCount > 0) {
				if (newOrderCount != orderCount) {
					orderCount = newOrderCount;
					IS_VIBRATE_ALLOWED = true;
				}
				return true;
			} else
				return false;
		}

	}

	private Thread createSyncControlThread() {
		return new Thread() {
			@Override
			public void run() {
				Log.d(TAG, "createSyncControlThread() - SyncThread Activated!");
				while (IS_SERVICE_ALIVE) {
					// Log.d(TAG, "Thread() IS_SERVICE_ALIVE:" + IS_SERVICE_ALIVE);
					// Log.d(TAG, "Thread() IS_SERVICE_ACTIVE:" + IS_SERVICE_ACTIVE);
					// Log.d(TAG, "Thread() checkRouteInformation():" + checkRouteInformation());
					if (IS_SERVICE_ACTIVE) {
						if (checkRouteInformation()) {
							checkClock();
						}
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				Log.d(TAG, "SyncThread Deactivated!");
				return;
			}
		};
	}

	@SuppressWarnings("deprecation")
	private void checkClock() {
		if(checkpoints==null){
			return;
		}

		Date now = new Date();

		int nextIndex = (int) (orderCount - 1);

		long nowMinutes = (now.getHours() * 60) + now.getMinutes();
		// long delay = checkpoint.getDelayMinutes();

		if (nextIndex > checkpoints.size())
			return;

		checkpoint = checkpoints.get(nextIndex);
		long checkpointTime = checkpoint.getTime();

		// boolean timeCheck = (checkpointTime == nowMinutes);
		// Log.d(TAG, "Thread() IS_VIBRATE_ALLOWED: " + IS_VIBRATE_ALLOWED);
		// Log.d(TAG, "Thread() checkpointTime: " + checkpointTime + " nowMinutes: " + nowMinutes);
		// Log.d(TAG, "Thread() checkpointTime == nowMinutes: " + timeCheck);
		if ((checkpointTime == nowMinutes) && IS_VIBRATE_ALLOWED) {
			IS_VIBRATE_ALLOWED = false;
			Vibrator vibrator;
			vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
			vibrator.vibrate(2000);
			Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
			r.play();
			// r.stop();
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy()");
		super.onDestroy();
		IS_SERVICE_ALIVE = false;
	}

	public static void setServiceActive() {
		IS_SERVICE_ACTIVE = true;
	}

	public static void setServiceInactive() {
		IS_SERVICE_ACTIVE = false;
	}
}
