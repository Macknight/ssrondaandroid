package com.br.svconsultoria.svnfcmanager.model.orm;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * Entity mapped to table SRTM_RFID_TAGS.
 */
@Entity
public class RfidTag {

    @Id
    private Long tagId;
    private Long tagTypeId;
    private Long techTypeId;
    private Long xId;
    private String serialNumber;
    private long statusId;
    @Generated(hash = 2085136866)
    public RfidTag(Long tagId, Long tagTypeId, Long techTypeId, Long xId,
            String serialNumber, long statusId) {
        this.tagId = tagId;
        this.tagTypeId = tagTypeId;
        this.techTypeId = techTypeId;
        this.xId = xId;
        this.serialNumber = serialNumber;
        this.statusId = statusId;
    }
    @Generated(hash = 673521075)
    public RfidTag() {
    }
    public Long getTagId() {
        return this.tagId;
    }
    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }
    public Long getTagTypeId() {
        return this.tagTypeId;
    }
    public void setTagTypeId(Long tagTypeId) {
        this.tagTypeId = tagTypeId;
    }
    public Long getTechTypeId() {
        return this.techTypeId;
    }
    public void setTechTypeId(Long techTypeId) {
        this.techTypeId = techTypeId;
    }
    public Long getXId() {
        return this.xId;
    }
    public void setXId(Long xId) {
        this.xId = xId;
    }
    public String getSerialNumber() {
        return this.serialNumber;
    }
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
    public long getStatusId() {
        return this.statusId;
    }
    public void setStatusId(long statusId) {
        this.statusId = statusId;
    }

}
